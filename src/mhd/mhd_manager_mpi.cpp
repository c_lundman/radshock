//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "mhd_manager.h"
#include <string>
#include <vector>
#include <math.h>
#include <mpi.h>
#include <iostream>

// This file includes all MPI functions used by the MHDManager class; all functions are private

void MHDManager::initialize_mpi() {
  MPI::Init();
}

void MHDManager::finalize_mpi() {
  MPI::Finalize();
}

void MHDManager::set_rank_and_num_proc() {
  MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
  MPI_Comm_size(MPI_COMM_WORLD, &NUM_PROC);
  if(NUM_PROC > 0 && ((NUM_PROC & (NUM_PROC-1)) == 0)) {
    NUM_MPI_CYCLES_POWER_OF_TWO = log2(NUM_PROC);
  } else {
    if(is_master() && NUM_PROC > 1) {
      std::cout << std::endl << "            WARNING: NUM_PROC is not a power of 2; a slower MPI algorithm will be used!" << std::endl << std::endl;
    }
  }
}

void MHDManager::fill_mhd_grid_range(int RANK_p, int NUM_PROC_p, int *i_min_p, int *i_max_p) {

  // Finds the slice of the grid on which this thread should operate (defined by the boundaries i_min, i_max)
  // Information is then taken from the range i_min+3 -> i_max-3

  double n_c = n_bs-2*ghosts-1;
  int Dj = std::ceil(n_c/NUM_PROC_p);
  *i_min_p = RANK_p*Dj-1;
  *i_max_p = (RANK_p+1)*Dj+2*ghosts+1;
  if(*i_min_p < 0) *i_min_p = 0;
  if(*i_max_p > n_bs-1) *i_max_p = n_bs-1;
}

void MHDManager::initialize_vector_slices() {

  // Sets sizes of the vector slices, based on i_min and i_max

  // n_bs_slice = i_max-i_min+2;
  n_bs_slice = i_max-i_min+1;

  m_bs_slice.resize(n_bs_slice);
  r_bs_slice.resize(n_bs_slice);
  vs_slice.resize(n_bs_slice-1);
  rhos_slice.resize(n_bs_slice-1);
  ps_slice.resize(n_bs_slice-1);
  bs_slice.resize(n_bs_slice-1);
  V_ms_slice.resize(n_bs_slice-1);
  E_ms_slice.resize(n_bs_slice-1);
  S_ms_slice.resize(n_bs_slice-1);
  M_ms_slice.resize(n_bs_slice-1);
  G0s_slice.resize(n_bs_slice-1);
  G1s_slice.resize(n_bs_slice-1);
  dotPhis_slice.resize(n_bs_slice-1);
}

void MHDManager::set_slices_from_grid() {

  // Copy the slices from the grid
  m_bs_slice[0] = m_bs[i_min];
  r_bs_slice[0] = r_bs[i_min];

  for(int i=0; i<n_bs_slice-1; i++) {
    m_bs_slice[i+1] = m_bs[i_min+i+1];
    r_bs_slice[i+1] = r_bs[i_min+i+1];
    vs_slice[i] = vs[i_min+i];
    rhos_slice[i] = rhos[i_min+i];
    ps_slice[i] = ps[i_min+i];
    bs_slice[i] = bs[i_min+i];

    V_ms_slice[i] = V_ms[i_min+i];
    E_ms_slice[i] = E_ms[i_min+i];
    S_ms_slice[i] = S_ms[i_min+i];
    M_ms_slice[i] = M_ms[i_min+i];

    G0s_slice[i] = G0s[i_min+i];
    G1s_slice[i] = G1s[i_min+i];
    dotPhis_slice[i] = dotPhis[i_min+i];

  }
}

void MHDManager::put_slices_on_grid() {

  // Copy the slices back into the full grid
  m_bs[i_min] = m_bs_slice[0];
  r_bs[i_min] = r_bs_slice[0];

  for(int i=0; i<n_bs_slice-1; i++) {
    m_bs[i_min+i+1] = m_bs_slice[i+1];
    r_bs[i_min+i+1] = r_bs_slice[i+1];
    vs[i_min+i] = vs_slice[i];
    rhos[i_min+i] = rhos_slice[i];
    ps[i_min+i] = ps_slice[i];
    bs[i_min+i] = bs_slice[i];

    V_ms[i_min+i] = V_ms_slice[i];
    E_ms[i_min+i] = E_ms_slice[i];
    S_ms[i_min+i] = S_ms_slice[i];
    M_ms[i_min+i] = M_ms_slice[i];

    G0s[i_min+i] = G0s_slice[i];
    G1s[i_min+i] = G1s_slice[i];
    dotPhis[i_min+i] = dotPhis_slice[i];

  }
}

void MHDManager::collect_mhd_grid() {

  // A wrapper, which selects the algorithm to use

  // The below statement checks if NUM_PROC is an even power of two
  if(NUM_PROC > 0 && ((NUM_PROC & (NUM_PROC-1)) == 0)) {
    collect_mhd_grid_power_of_two();
  } else {
    collect_mhd_grid_old();
  }

}

void MHDManager::distribute_mhd_grid() {

  // A wrapper, which selects the algorithm to use

  // The below statement checks if NUM_PROC is an even power of two
  if(NUM_PROC > 0 && ((NUM_PROC & (NUM_PROC-1)) == 0)) {
    distribute_mhd_grid_power_of_two();
  } else {
    distribute_mhd_grid_old();
  }

}

void MHDManager::collect_mhd_grid_power_of_two() {

  // Collects the MHD grid from all workers
  // Uses a faster algorithm, which assumes that the number of threads (NUM_PROC) are a power of two
  // The wall time of this algorithm should scale as log(NUM_PROC), while the old algorithm scales as NUM_PROC

  MPI_Status stat;
  int a, b;

  // A cycle is a "communication step"
  for(int i=0; i<NUM_MPI_CYCLES_POWER_OF_TWO; i++) {
    a = pow(2, i);
    b = pow(2, i+1);

    // Should the thread communicate?
    if(RANK % a == 0) {

      int sender, receiver;

      // Is the thread a "receiver"?
      if(RANK % b == 0) {
	sender = RANK + a;
	receiver = RANK;
      } else {
	sender = RANK;
	receiver = RANK - a;
      }

      // int i_min_p, i_max_p;
      // fill_mhd_grid_range(sender/a, NUM_PROC/a, &i_min_p, &i_max_p);
      // int di_p = i_max_p-i_min_p-2*ghosts+1-2; // Only taking the real cells, omitting ghosts
      // if(sender/a == NUM_PROC/a-1) di_p += 1; // Correcting so that the end of the grid gets taken as well

      int i_min_p, i_max_p, tmp;
      fill_mhd_grid_range(sender, NUM_PROC, &i_min_p, &tmp);
      fill_mhd_grid_range(sender+a-1, NUM_PROC, &tmp, &i_max_p);
      int di_p = i_max_p-i_min_p-2*ghosts+1-2; // Only taking the real cells, omitting ghosts
      if(sender+a == NUM_PROC) di_p += 1; // Correcting so that the end of the grid gets taken as well

      // Is the thread a "receiver"?
      if(RANK % b == 0) {

	MPI_Recv(&m_bs[i_min_p+ghosts+1], di_p, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&r_bs[i_min_p+ghosts+1], di_p, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&vs[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&rhos[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&ps[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&bs[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&V_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&S_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&E_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&M_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);

      } else {

	MPI_Send(&m_bs[i_min_p+ghosts+1], di_p, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&r_bs[i_min_p+ghosts+1], di_p, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&vs[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&rhos[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&ps[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&bs[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&V_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&S_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&E_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&M_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);

      }
    }
  }

}

void MHDManager::distribute_mhd_grid_power_of_two() {

  // Distributes the MHD grid to all workers
  // Uses a faster algorithm, which assumes that the number of threads (NUM_PROC) are a power of two
  // The wall time of this algorithm should scale as log(NUM_PROC), while the old algorithm scales as NUM_PROC

  MPI_Status stat;
  int a, b;

  // A cycle is a "communication step"
  for(int i=NUM_MPI_CYCLES_POWER_OF_TWO-1; i>=0; i--) {
    a = pow(2, i);
    b = pow(2, i+1);

    // Should the thread communicate?
    if(RANK % a == 0) {

      int sender, receiver;

      // Is the thread a "sender"?
      if(RANK % b == 0) {
	receiver = RANK + a;
	sender = RANK;
      } else {
	receiver = RANK;
	sender = RANK - a;
      }

      // Send full grid... (This can be optimized, but likely only a small gain)

      // Is the thread a "sender"?
      if(RANK % b == 0) {

	MPI_Send(&m_bs[0], n_bs, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&r_bs[0], n_bs, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&vs[0], n_bs-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&rhos[0], n_bs-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&ps[0], n_bs-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&bs[0], n_bs-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&V_ms[0], n_bs-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&S_ms[0], n_bs-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&E_ms[0], n_bs-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);
	MPI_Send(&M_ms[0], n_bs-1, MPI_DOUBLE, receiver, 1, MPI_COMM_WORLD);

      } else {

	MPI_Recv(&m_bs[0], n_bs, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&r_bs[0], n_bs, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&vs[0], n_bs-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&rhos[0], n_bs-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&ps[0], n_bs-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&bs[0], n_bs-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&V_ms[0], n_bs-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&S_ms[0], n_bs-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&E_ms[0], n_bs-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);
	MPI_Recv(&M_ms[0], n_bs-1, MPI_DOUBLE, sender, 1, MPI_COMM_WORLD, &stat);

      }
    }
  }

}

void MHDManager::collect_mhd_grid_old() {

  // Collects grid parts from the workers

  MPI_Status stat;

  // TMP
  unsigned int grid_size;
  // /TMP

  if(RANK == 0) {
    // Collect the grid

    for(int worker=1; worker<NUM_PROC; worker++) {
      // TMP
      // Checking that each worker has the same number of bins
      MPI_Recv(&grid_size, 1, MPI_INT, worker, 1, MPI_COMM_WORLD, &stat);
      if(grid_size != m_bs.size()) {
	std::cout << "AMR GRID SIZE IS MESSED UP!" << std::endl;
      }
      // /TMP

      int i_min_p, i_max_p, RANK_p=worker;
      fill_mhd_grid_range(RANK_p, NUM_PROC, &i_min_p, &i_max_p);
      int di_p = i_max_p-i_min_p-2*ghosts+1-2; // Only taking the real cells, omitting ghosts

      if(worker == NUM_PROC-1) di_p += 1; // Correcting so that the end of the grid gets taken as well

      MPI_Recv(&m_bs[i_min_p+ghosts+1], di_p, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&r_bs[i_min_p+ghosts+1], di_p, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&vs[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&rhos[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&ps[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&bs[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&V_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&S_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&E_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      MPI_Recv(&M_ms[i_min_p+ghosts+1], di_p-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
    }

  } else {
    // Send the grid
    // TMP
    grid_size = m_bs.size();
    MPI_Send(&grid_size, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
    // /TMP

    int di = i_max-i_min-2*ghosts+1-2; // Only taking the real cells, omitting ghosts
    if(RANK == NUM_PROC-1) di += 1; // Correcting so that the end of the grid gets taken as well

    MPI_Send(&m_bs[i_min+ghosts+1], di, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&r_bs[i_min+ghosts+1], di, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&vs[i_min+ghosts+1], di-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&rhos[i_min+ghosts+1], di-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&ps[i_min+ghosts+1], di-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&bs[i_min+ghosts+1], di-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&V_ms[i_min+ghosts+1], di-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&S_ms[i_min+ghosts+1], di-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&E_ms[i_min+ghosts+1], di-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    MPI_Send(&M_ms[i_min+ghosts+1], di-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);

  }
}

void MHDManager::distribute_mhd_grid_old() {

  // Sends the full grid to all workers

  MPI_Status stat;

  if(RANK == 0) {
    // Send the grid

    for(int worker=1; worker<NUM_PROC; worker++) {
      MPI_Send(&m_bs[0], n_bs, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&r_bs[0], n_bs, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&vs[0], n_bs-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&rhos[0], n_bs-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&ps[0], n_bs-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&bs[0], n_bs-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&V_ms[0], n_bs-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&S_ms[0], n_bs-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&E_ms[0], n_bs-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
      MPI_Send(&M_ms[0], n_bs-1, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
    }

  } else {
    // Obtain the grid

    MPI_Recv(&m_bs[0], n_bs, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&r_bs[0], n_bs, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&vs[0], n_bs-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&rhos[0], n_bs-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&ps[0], n_bs-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&bs[0], n_bs-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&V_ms[0], n_bs-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&S_ms[0], n_bs-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&E_ms[0], n_bs-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
    MPI_Recv(&M_ms[0], n_bs-1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);

  }
}

int MHDManager::check_if_code_crashed(int OK) {

  // Checks if any thread crashed
  MPI_Status stat;
  int survived;

  // All threads sends their OK
  if(RANK == 0) {
    survived = OK;
    int OK_p = 1;
    for(int worker=1; worker<NUM_PROC; worker++) {
      MPI_Recv(&OK_p, 1, MPI_INT, worker, 1, MPI_COMM_WORLD, &stat);
      if(OK_p == 0) {
	survived = 0;
      }
    }

  } else {
    MPI_Send(&OK, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
  }

  // The master sends the survival info to the workers
  if(RANK == 0) {
    for(int worker=1; worker<NUM_PROC; worker++) {
      MPI_Send(&survived, 1, MPI_INT, worker, 1, MPI_COMM_WORLD);
    }
  } else {
    MPI_Recv(&survived, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &stat);
  }

  return survived;
}

// - ************************************ -

// void MHDManager::fill_mhd_grid_range(int RANK_p, int NUM_PROC_p, int *i_min_p, int *i_max_p) {

//   // Finds the slice of the grid on which this thread should operate (defined by the boundaries i_min, i_max)
//   // Information is then taken from the bins inside the boundary range i_min+2 -> i_max-2

//   double n_c = n_bs-2*ghosts-1;
//   int Dj = std::ceil(n_c/NUM_PROC_p);
//   *i_min_p = RANK_p*Dj;
//   *i_max_p = (RANK_p+1)*Dj + 4;
//   if(*i_max_p > n_bs-1) {*i_max_p = n_bs-1;}
// }


// void MHDManager::initialize_vector_slices() {

//   // Sets sizes of the vector slices, based on i_min and i_max

//   // n_bs_slice = i_max-i_min+2;
//   n_bs_slice = i_max-i_min+1;

//   m_bs_slice.resize(n_bs_slice);
//   r_bs_slice.resize(n_bs_slice);
//   vs_slice.resize(n_bs_slice-1);
//   rhos_slice.resize(n_bs_slice-1);
//   ps_slice.resize(n_bs_slice-1);
//   bs_slice.resize(n_bs_slice-1);
//   V_ms_slice.resize(n_bs_slice-1);
//   E_ms_slice.resize(n_bs_slice-1);
//   S_ms_slice.resize(n_bs_slice-1);
//   M_ms_slice.resize(n_bs_slice-1);
//   G0s_slice.resize(n_bs_slice-1);
//   G1s_slice.resize(n_bs_slice-1);
//   dotPhis_slice.resize(n_bs_slice-1);
// }

// void MHDManager::set_slices_from_grid() {

//   // Copy the slices from the grid
//   m_bs_slice[0] = m_bs[i_min];
//   r_bs_slice[0] = r_bs[i_min];

//   for(int i=0; i<n_bs_slice-1; i++) {
//     m_bs_slice[i+1] = m_bs[i_min+i+1];
//     r_bs_slice[i+1] = r_bs[i_min+i+1];
//     vs_slice[i] = vs[i_min+i];
//     rhos_slice[i] = rhos[i_min+i];
//     ps_slice[i] = ps[i_min+i];
//     bs_slice[i] = bs[i_min+i];

//     V_ms_slice[i] = V_ms[i_min+i];
//     E_ms_slice[i] = E_ms[i_min+i];
//     S_ms_slice[i] = S_ms[i_min+i];
//     M_ms_slice[i] = M_ms[i_min+i];

//     G0s_slice[i] = G0s[i_min+i];
//     G1s_slice[i] = G1s[i_min+i];
//     dotPhis_slice[i] = dotPhis[i_min+i];

//   }
// }

// void MHDManager::put_slices_on_grid() {

//   // Copy the slices back into the full grid
//   m_bs[i_min] = m_bs_slice[0];
//   r_bs[i_min] = r_bs_slice[0];

//   for(int i=0; i<n_bs_slice-1; i++) {
//     m_bs[i_min+i+1] = m_bs_slice[i+1];
//     r_bs[i_min+i+1] = r_bs_slice[i+1];
//     vs[i_min+i] = vs_slice[i];
//     rhos[i_min+i] = rhos_slice[i];
//     ps[i_min+i] = ps_slice[i];
//     bs[i_min+i] = bs_slice[i];

//     V_ms[i_min+i] = V_ms_slice[i];
//     E_ms[i_min+i] = E_ms_slice[i];
//     S_ms[i_min+i] = S_ms_slice[i];
//     M_ms[i_min+i] = M_ms_slice[i];

//     G0s[i_min+i] = G0s_slice[i];
//     G1s[i_min+i] = G1s_slice[i];
//     dotPhis[i_min+i] = dotPhis_slice[i];

//   }
// }

