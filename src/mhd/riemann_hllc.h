//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef RIEMANN_HLLC_H
#define RIEMANN_HLLC_H

// -----------------------------------------------
// An HLLC Riemann solver, based on
// 
// Physical-constraints-preserving Lagrangian
// finite volume schemes for one- and two-
// dimensional special relativistic hydrodynamics
// 
// Ling, Duan & Tang (2019)
// 
// https://arxiv.org/pdf/1901.10625.pdf
// -----------------------------------------------

double findSoundSpeed(const double rho, const double p, const double b, const double gamma_ad);
void findConstFromPrim(const double v, const double rho, const double p, const double b, double *D, double *E, double *S, const double gamma_ad);
void Riemann_problem(double p_L, double rho_L, double v_L, double b_L,
		     double p_R, double rho_R, double v_R, double b_R,
		     double *pstar, double *vstar,
		     const double gamma_ad, const int wavespeed, const int iter, const int i);

#endif
