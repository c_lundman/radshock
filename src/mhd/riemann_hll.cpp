//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "riemann_hll.h"
#include <math.h>
#include <iostream>

// --------------------------------------------------------
//  An approximate Riemann solver, based on the HLL method
// --------------------------------------------------------

double findSoundSpeed(const double rho, const double p, const double b, const double gamma_ad) {

  double h, h_star, c_s, v_A, c_s_star;
  h = 1. + gamma_ad*p/(gamma_ad-1.)/rho;
  h_star = 1. + gamma_ad*p/(gamma_ad-1.)/rho + b*b/rho;
  c_s = sqrt(gamma_ad*p/h/rho);
  v_A = sqrt(b*b/h_star/rho);
  c_s_star = sqrt((1.-v_A*v_A)*c_s*c_s + v_A*v_A);

  return(c_s_star);

}

void findWaveSpeedsUnity(const double v_L, const double rho_L, const double p_L, const double b_L,
			 const double v_R, const double rho_R, const double p_R, const double b_R,
			 double *B_L, double *B_R, const double gamma_ad) {

  // Amazingly amazing

  *B_L = -1.;
  *B_R = +1.;
}

void findWaveSpeedsSchneider(const double v_L, const double rho_L, const double p_L, const double b_L,
			     const double v_R, const double rho_R, const double p_R, const double b_R,
			     double *B_L, double *B_R, const double gamma_ad) {

  // From Schneider et al. (1993)

  double c_s_L, c_s_R; // Sound speeds
  c_s_L = findSoundSpeed(rho_L, p_L, b_L, gamma_ad);
  c_s_R = findSoundSpeed(rho_R, p_R, b_R, gamma_ad);

  double c_s, v; // Simple averages!
  c_s = .5*(c_s_L+c_s_R);
  v = .5*(v_L+v_R);

  // Lab frame speeds of "average waves"
  *B_L = (v-c_s)/(1.-v*c_s);
  *B_R = (v+c_s)/(1.+v*c_s);

  double d_L, d_R; // Lab frame speeds of left and right sound waves
  d_L = (v_L-c_s_L)/(1.-v_L*c_s_L);
  d_R = (v_R+c_s_R)/(1.+v_R*c_s_R);

  if(*B_R < d_R) {
    *B_R = d_R;
  }
  if(*B_L > d_L) {
    *B_L = d_L;
  }
}

void findWaveSpeedsMignone(const double v_L, const double rho_L, const double p_L, const double b_L,
			   const double v_R, const double rho_R, const double p_R, const double b_R,
			   double *B_L, double *B_R, const double gamma_ad) {

  // From Mignone & Bodo (2005)

  double c_s_L, c_s_R; // Sound speeds
  c_s_L = findSoundSpeed(rho_L, p_L, b_L, gamma_ad);
  c_s_R = findSoundSpeed(rho_R, p_R, b_R, gamma_ad);

  double gamma_L, gamma_R;
  gamma_L = 1./sqrt(1.-v_L*v_L);
  gamma_R = 1./sqrt(1.-v_R*v_R);

  double sigma_s_L, sigma_s_R;
  sigma_s_L = c_s_L*c_s_L/gamma_L/gamma_L/(1. - c_s_L*c_s_L);
  sigma_s_R = c_s_R*c_s_R/gamma_R/gamma_R/(1. - c_s_R*c_s_R);

  *B_L = (v_L - sqrt(sigma_s_L*(1.-v_L*v_L+sigma_s_L)))/(1.+sigma_s_L);
  *B_R = (v_R + sqrt(sigma_s_R*(1.-v_R*v_R+sigma_s_R)))/(1.+sigma_s_R);
}

void findMdots(const double v_L, const double rho_L, const double p_L, const double b_L,
	       const double v_R, const double rho_R, const double p_R, const double b_R,
	       double *Lmdot, double *Rmdot, const double gamma_ad, const int wavespeed) {

  double B_L=0., B_R=0.; // This is actually a speed.. (poor choice of variable name!)

  if(wavespeed == 0) {
    findWaveSpeedsUnity(v_L, rho_L, p_L, b_L, v_R, rho_R, p_R, b_R, &B_L, &B_R, gamma_ad);
  } else if(wavespeed == 1) {
    findWaveSpeedsSchneider(v_L, rho_L, p_L, b_L, v_R, rho_R, p_R, b_R, &B_L, &B_R, gamma_ad);
  } else if(wavespeed == 2) {
    findWaveSpeedsMignone(v_L, rho_L, p_L, b_L, v_R, rho_R, p_R, b_R, &B_L, &B_R, gamma_ad);
  }

  double dB_L, dB_R;
  dB_L = B_L-v_L;
  dB_R = B_R-v_R;

  double gamma_L, gamma_R;
  gamma_L = 1./sqrt(1.-v_L*v_L);
  gamma_R = 1./sqrt(1.-v_R*v_R);

  *Lmdot = gamma_L*rho_L*dB_L;
  *Rmdot = gamma_R*rho_R*dB_R;
}

void findConstFromPrim(const double v, const double rho, const double p, const double b,
		       double *D, double *E, double *S, const double gamma_ad) {

  double gamma;

  gamma = 1./sqrt(1.-v*v);
  *D = 1./gamma/rho;
  *E = (*D)*(gamma*gamma*(rho + gamma_ad/(gamma_ad-1.)*p + b*b) - (p + .5*b*b));
  *S = (*D)*gamma*gamma*(rho + gamma_ad/(gamma_ad-1.)*p + b*b)*v;
}

void findFstars(double v_L, double p_L, double b_L, double v_R, double p_R, double b_R,
		double D_L, double E_L, double S_L, double D_R, double E_R, double S_R,
		double Lmdot, double Rmdot, double Fstars[]) {

  double LF, RF, LU, RU;
  double p_L_tilde, p_R_tilde;

  p_L_tilde = p_L + .5*b_L*b_L;
  p_R_tilde = p_R + .5*b_R*b_R;

  // std::cout << "p_L_tilde = " << p_L_tilde << std::endl;
  // std::cout << "p_R_tilde = " << p_R_tilde << std::endl;

  LF = -v_L;
  RF = -v_R;
  LU = D_L;
  RU = D_R;
  if(Rmdot != Lmdot) {
    Fstars[0] = (Rmdot*LF - Lmdot*RF + Lmdot*Rmdot*(RU-LU))/(Rmdot-Lmdot);
  } else {
    Fstars[0] = 0.;
  }

  // std::cout << "Rmdot*LF = " << Rmdot*LF << ", Lmdot*RF = " << Lmdot*RF << std::endl;
  // std::cout << "Rmdot*LF - Lmdot*RF = " << Rmdot*LF - Lmdot*RF << std::endl;
  // std::cout << "RU-LU = " << RU-LU << std::endl;
  // std::cout << "Lmdot*Rmdot*(RU-LU) = " << Lmdot*Rmdot*(RU-LU) << std::endl;
  // std::cout << "Rmdot-Lmdot = " << Rmdot-Lmdot << std::endl;

  LF = v_L*p_L_tilde;
  RF = v_R*p_R_tilde;
  LU = E_L;
  RU = E_R;
  if(Rmdot != Lmdot) {
    Fstars[1] = (Rmdot*LF - Lmdot*RF + Lmdot*Rmdot*(RU-LU))/(Rmdot-Lmdot);
  } else {
    Fstars[1] = 0.;
  }

  LF = p_L_tilde;
  RF = p_R_tilde;
  LU = S_L;
  RU = S_R;
  if(Rmdot != Lmdot) {
    Fstars[2] = (Rmdot*LF - Lmdot*RF + Lmdot*Rmdot*(RU-LU))/(Rmdot-Lmdot);
  } else {
    Fstars[2] = 0.;
  }

}

void findVstarPstar(double Fstars[], double *vstar, double *pstar) {

  *vstar = -Fstars[0];
  *pstar = +Fstars[2];

  // std::cout << "Fstars[0] = " << Fstars[0] << ", -Fstars[1]/Fstars[2] = " << -Fstars[1]/Fstars[2] << std::endl;
  // std::cout << "Fstars[1] = " << Fstars[1] << ", -Fstars[0]*Fstars[2] = " << -Fstars[0]*Fstars[2] << std::endl;
  // std::cout << "Fstars[2] = " << Fstars[2] << ", -Fstars[1]/Fstars[0] = " << -Fstars[1]/Fstars[0] << std::endl;

  // *vstar = Fstars[1]/Fstars[2];
  // *pstar = Fstars[2];
  // if(*pstar < 0) {
  //   std::cout << "pstar = " << *pstar << std::endl;
  //   std::cout << "vstar = " << *vstar << std::endl;
  //   std::cout << "-Fstars[0] = " << -Fstars[0] << std::endl;
  // }
}

void limitPressures(double *p_L, double rho_L, double v_L,
		    double *p_R, double rho_R, double v_R) {

  // Seems like xi = 4 is the best compromize!
  double xi = 4.;
  // xi = 2.;
  // xi = 10.;

  if(*p_L > xi*(*p_R)) {
    *p_R = (*p_L)/xi;
  } else if(*p_R > xi*(*p_L)) {
    *p_L = (*p_R)/xi;
  }

}

void Riemann_problem(double p_L, double rho_L, double v_L, double b_L,
		     double p_R, double rho_R, double v_R, double b_R,
		     double *pstar, double *vstar,
		     const double gamma_ad, const int wavespeed,
		     const int iter, const int i) {

  // If pressure ratio is large, "increase" lower pressure to avoid oscillations and crashes;
  // shocks propagate at the correct speed anyway

  // limitPressures(&p_L, rho_L, v_L, &p_R, rho_R, v_R);

  // Determining left and right mdot

  double Lmdot, Rmdot;
  findMdots(v_L, rho_L, p_L, b_L, v_R, rho_R, p_R, b_R, &Lmdot, &Rmdot, gamma_ad, wavespeed);
  // findMdots(v_L, rho_L, p_L, b_L, v_R, rho_R, p_R, b_R, &Lmdot, &Rmdot, gamma_ad, 1);

  // if(iter == 0 && i == 2) {
  //   std::cout << "Lmdot = " << Lmdot << ", Rmdot = " << Rmdot << std::endl;
  //   std::cout << "Lmdot + Rmdot = " << Lmdot + Rmdot << std::endl;
  //   std::cout << "v_L + v_R = " << v_L + v_R << std::endl;
  // }

  // Finding initial const states

  double D_L, E_L, S_L;
  double D_R, E_R, S_R;
  findConstFromPrim(v_L, rho_L, p_L, b_L, &D_L, &E_L, &S_L, gamma_ad);
  findConstFromPrim(v_R, rho_R, p_R, b_R, &D_R, &E_R, &S_R, gamma_ad);

  // Finding fluxes, vstar & pstar

  double Fstars[3];
  findFstars(v_L, p_L, b_L, v_R, p_R, b_R, D_L, E_L, S_L, D_R, E_R, S_R, Lmdot, Rmdot, Fstars);
  findVstarPstar(Fstars, vstar, pstar);

  // if(*pstar < 0) {
  //   *pstar = .5*(p_L + p_R);
  // }

  // if(*vstar < -1) {
  //   // std::cout << "vstar < -1" << std::endl;
  //   *vstar = -.9999;
  // }

  // if(*vstar > +1) {
  //   // std::cout << "vstar > +1" << std::endl;
  //   *vstar = +.9999;
  // }

  // *pstar = .5*(p_L + p_R);
  // *vstar = .5*(v_L + v_R);
  // *pstar = 0.;

  // if(p_L > p_R) {
  //   *pstar = p_L;
  // } else {
  //   *pstar = p_R;
  // }

  // *pstar = p_L + p_R;

  // std::cout << "iter = " << iter << std::endl;
  // std::cout << "i = " << i << std::endl;
  // std::cout << "p_L = " << p_L << ", pstar = " << *pstar << ", p_R = " << p_R << ", pstar/p_L = " << *pstar/p_L << ", pstar/p_R = " << *pstar/p_R << std::endl;
  // std::cout << "v_L = " << v_L << ", vstar = " << *vstar << ", v_R = " << v_R << ", vstar/v_L = " << *vstar/v_L << ", vstar/v_R = " << *vstar/v_R << std::endl << std::endl;

  double pstar_TEST, vstar_TEST;
  Riemann_problem_TEST(p_L, rho_L, v_L, b_L, p_R, rho_R, v_R, b_R, &pstar_TEST, &vstar_TEST, gamma_ad, wavespeed, iter, i);

  if((pstar_TEST == pstar_TEST) && (vstar_TEST == vstar_TEST)) {
    *vstar = vstar_TEST;
    *pstar = pstar_TEST;
  } else {
    // std::cout << "vstar_TEST = " << vstar_TEST << ", pstar_TEST = " << pstar_TEST << std::endl;
    // std::cout << "warning at iter = " << iter << ", i = " << i << std::endl;
    // std::cout << "v_L = " << v_L << ", rho_L = " << rho_L << ", p_L = " << p_L << ", b_L = " << b_L << std::endl;
    // std::cout << "v_R = " << v_R << ", rho_R = " << rho_R << ", p_R = " << p_R << ", b_R = " << b_R << std::endl;
  }

}

void Riemann_problem_TEST(double p_L, double rho_L, double v_L, double b_L,
			  double p_R, double rho_R, double v_R, double b_R,
			  double *pstar, double *vstar,
			  const double gamma_ad, const int wavespeed,
			  const int iter, const int i) {

  // --------------------------------------
  // A HLLC Riemann solver, based on
  // Ling, Duan & Tang (2019)
  // https://arxiv.org/pdf/1901.10625.pdf
  // --------------------------------------

  // Finding the speeds s^\pm (s_m, s_p)
  double c_s_L, c_s_R; // Sound speeds
  c_s_L = findSoundSpeed(rho_L, p_L, b_L, gamma_ad);
  c_s_R = findSoundSpeed(rho_R, p_R, b_R, gamma_ad);

  double s_m_1 = (v_L - c_s_L)/(1. - v_L*c_s_L);
  double s_m_2 = (v_R - c_s_R)/(1. - v_R*c_s_R);
  double s_p_1 = (v_L + c_s_L)/(1. + v_L*c_s_L);
  double s_p_2 = (v_R + c_s_R)/(1. + v_R*c_s_R);

  double s_m, s_p;
  if(s_m_1 < s_m_2) {
    s_m = s_m_1;
  } else {
    s_m = s_m_2;
  }
  if(s_p_1 > s_p_2) {
    s_p = s_p_1;
  } else {
    s_p = s_p_2;
  }

  // Finding initial const states
  double D_L, E_L, S_L;
  double D_R, E_R, S_R;
  findConstFromPrim(v_L, rho_L, p_L, b_L, &D_L, &E_L, &S_L, gamma_ad);
  findConstFromPrim(v_R, rho_R, p_R, b_R, &D_R, &E_R, &S_R, gamma_ad);

  // Finding "his" E (e), m, which are energy and momentum per volume, not mass
  double m_L = S_L/D_L;
  double m_R = S_R/D_R;
  double e_L = E_L/D_L;
  double e_R = E_R/D_R;

  // Computing all constants, and finally vstar, pstar
  double A_m = s_m*e_L - m_L;
  double A_p = s_p*e_R - m_R;

  double B_m = m_L*(s_m - v_L) - p_L;
  double B_p = m_R*(s_p - v_R) - p_R;

  double C_0 = B_p-B_m;
  double C_1 = A_m + s_p*B_m - A_p - s_m*B_p;
  double C_2 = s_m*A_p - s_p*A_m;

  if(C_2 == 0) {
    *vstar = 0.;
  } else if(C_1*C_1 - 4.*C_0*C_2 < 0.) {
    *vstar = -.5*C_1/C_2;
  } else {
    *vstar = .5*(-C_1 - sqrt(C_1*C_1 - 4.*C_0*C_2))/C_2;
  }

  if(*vstar != *vstar) {
    std::cout << "warning at iter = " << iter << ", i = " << i << std::endl;
    std::cout << "v_L = " << v_L << ", rho_L = " << rho_L << ", p_L = " << p_L << ", b_L = " << b_L << std::endl;
    std::cout << "v_R = " << v_R << ", rho_R = " << rho_R << ", p_R = " << p_R << ", b_R = " << b_R << std::endl;
    std::cout << "C_1*C_1 - 4.*C_0*C_2 = " << C_1*C_1 - 4.*C_0*C_2 << std::endl;
    std::cout << "C_0 = " << C_0 << ", C_1 = " << C_1 << ", C_2 = " << C_2 << std::endl;
    std::cout << "B_m = " << B_m << ", B_p = " << B_p << std::endl;
    std::cout << "A_m = " << A_m << ", A_p = " << A_p << std::endl;
  }

  *pstar = (*vstar*A_m - B_m)/(1. - *vstar*s_m);

}
