//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef MHD_MANAGER_H
#define MHD_MANAGER_H

#include <string>
#include <vector>
#include <fstream>
#include "stopwatch.h"

// The function implementations are found in:
// (1) mhd_manager.cpp
// (2) mhd_manager_physics.cpp
// (3) mhd_manager_mpi.cpp
// (4) mhd_manager_save.cpp
// (5) mhd_manager_crash_metrics.cpp

class MHDManager {

  // A class that stores and manages the MHD grid

 private:

  std::string sim_name;
  int n_bs;
  int alpha;
  int RANK;
  int NUM_PROC;
  int NUM_MPI_CYCLES_POWER_OF_TWO;

  int ghosts=2;
  int boundary_L=1;
  int boundary_R=1;
  double gamma_ad=4./3.;
  /* double epsilon=.5; */
  double epsilon=.593;
  /* double epsilon=.25; */
  /* double epsilon=.05; */
  double M_grav=0;
  int wavespeed=2;
  int iter=0;
  int i_min, i_max, di; // Used for MPI grid splitting
  double t;
  double piston_vgamma_L=0;
  double piston_vgamma_R=0;
  std::string run_dir;

  Stopwatch mhd_stopwatch;
  Stopwatch mpi_stopwatch;

  // Full grid
  std::vector<double> m_bs, r_bs;
  std::vector<double> vs, rhos, ps, bs;
  std::vector<double> V_ms, E_ms, S_ms, M_ms;
  std::vector<double> G0s, G1s, dotPhis;

  // NEW: two-temp (xi = theta_e/theta_p, dotxi = dxi/dt (lab frame time derivative))
  std::vector<double> xis, dotxis;

  // Thread slices
  int n_bs_slice;
  std::vector<double> m_bs_slice, r_bs_slice;
  std::vector<double> vs_slice, rhos_slice, ps_slice, bs_slice;
  std::vector<double> V_ms_slice, E_ms_slice, S_ms_slice, M_ms_slice;
  std::vector<double> G0s_slice, G1s_slice, dotPhis_slice;

  // Save files
  std::ofstream mhd_file;
  std::ofstream g0s_file;
  std::ofstream g1s_file;
  std::ofstream tim_file;
  std::ofstream save_state_file;
  std::ifstream load_state_file;

  // NEW: two-temp
  std::ofstream xis_file;

  // Save state file name
  std::string mhd_output_state_file;

  // Save snapshot file name
  std::string mhd_output_snapshot_file;

  int take_mhd_step_wrapper(double dt, int debug_start_iter, int debug_end_iter);

  void initialize_mpi();
  void finalize_mpi();
  void set_rank_and_num_proc();
  void fill_mhd_grid_range(int RANK_p, int NUM_PROC_p, int *i_min_p, int *i_max_p);
  void collect_mhd_grid();
  void collect_mhd_grid_old();
  void collect_mhd_grid_power_of_two();
  void distribute_mhd_grid();
  void distribute_mhd_grid_old();
  void distribute_mhd_grid_power_of_two();
  int check_if_code_crashed(int OK);

  void initialize_vector_slices();
  void set_slices_from_grid();
  void put_slices_on_grid();
  void open_all_files();
  void close_all_files();

  void merge_neighbouring_bins();
  double get_max_tau_column();

 public:

  /* MHDManager(std::string sim_name_p, int alpha_p); */
  MHDManager(std::string sim_name_p);
  /* MHDManager(std::string input_state_file_p); */

  void set_alpha(int alpha_p);
  void set_boundary_L(int boundary_L_p) {boundary_L = boundary_L_p;}
  void set_boundary_R(int boundary_R_p) {boundary_R = boundary_R_p;}
  void set_gamma_ad(double gamma_ad_p) {gamma_ad = gamma_ad_p;}
  void set_epsilon(double epsilon_p) {epsilon = epsilon_p;}
  void set_grav_mass(double M_grav_p) {M_grav = M_grav_p;}
  void set_t(double t_p) {t = t_p;}
  void set_iter(int iter_p) {iter = iter_p;}

  /* void set_m_bs(std::vector<double> &m_bs_p); */
  /* void set_r_bs(std::vector<double> &r_bs_p); */
  /* void set_prims(std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p); */

  void resize_all_grids(int n_bs_p);
  void set_grid(std::vector<double> &m_bs_p, std::vector<double> &r_bs_p, std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p);
  void set_grid_mass(double R_i, std::vector<double> &m_bs_p, std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p);
  void set_grid_consts(std::vector<double> &m_bs_p, std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p);
  void set_grid_full(std::vector<double> &m_bs_p, std::vector<double> &r_bs_p, std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p,
		     std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p,
		     std::vector<double> &xis_p);

  void get_grid(std::vector<double> &m_bs_p, std::vector<double> &r_bs_p, std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p);
  void get_grid_consts(std::vector<double> &m_bs_p, std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p);

  void set_piston_L(double piston_vgamma_L_p) {piston_vgamma_L = piston_vgamma_L_p;}
  void set_piston_R(double piston_vgamma_R_p) {piston_vgamma_R = piston_vgamma_R_p;}
  double get_piston_L() {return piston_vgamma_L;}
  double get_piston_R() {return piston_vgamma_R;}

  void set_run_dir(std::string run_dir_p) {run_dir = run_dir_p;}

  int get_boundary_L() {return boundary_L;}
  int get_boundary_R() {return boundary_R;}

  double get_mhd_stopwatch_t() {return mhd_stopwatch.get_t();}
  double get_mpi_stopwatch_t() {return mpi_stopwatch.get_t();}

  std::vector<double> get_linear_time_vector(double t_i, double t_f, int M);
  std::vector<double> get_logarithmic_time_vector(double t_i, double t_f, int M);

  void set_mhd_output_state_file(std::string mhd_output_state_file_p) {mhd_output_state_file = mhd_output_state_file_p;}
  void set_mhd_output_snapshot_file(std::string mhd_output_snapshot_file_p) {mhd_output_snapshot_file = mhd_output_snapshot_file_p;}
  std::string get_mhd_output_state_file() {return mhd_output_state_file;}
  std::string get_mhd_output_snapshot_file() {return mhd_output_snapshot_file;}

  void save_state();
  void load_state(std::string sim_name_of_state);

  bool is_master() {return RANK == 0;}

  void print_progress(double t_f, int k, int n_ts);
  void print_grid(int RANK_p);
  void print_slices(int RANK_p);
  int get_iter() {return iter;}
  double get_t() {return t;}
  double get_time_step_wrapper();
  int get_rank() {return RANK;}
  int get_num_proc() {return NUM_PROC;}
  /* void compute_radius_vector_wrapper(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &V_ms, double r_L, int alpha); */
  void compute_radius_vector_wrapper(double R_i);
  void reconstruct_prims_wrapper();
  void construct_consts_wrapper();
  double get_cooling_time_step_wrapper(std::vector<double> &I_0s, std::vector<double> &I_1s, std::vector<double> &theta_Cs, std::vector<double> &Z_pms, double f_heat);
  void make_pressure_hydrostatic(double norm=1);
  void flush_sources();
  void add_G_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p);
  void add_all_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p, std::vector<double> &dotPhis_p);
  void add_gravity_sources();
  /* void add_radiation_moment_sources(std::vector<double> &I_0s, std::vector<double> &I_1s, std::vector<double> &theta_Cs, std::vector<double> &Z_pms, double f_heat, double t); */
  /* double get_theta(double rho, double p, double Z_pm, double gamma_ad, double f_heat); */
  /* double get_p(double rho, double theta, double Z_pm, double gamma_ad, double f_heat); */
  void floor_pressures(std::vector<double> &theta_Cs, std::vector<double> &Z_pms, double f_heat);
  void set_ghost_cells_wrapper();
  int update_single_step(double dt, int debug_start_iter, int debug_end_iter);
  int update_until_time(double t_f);
  int update_until_time_and_save(double t_f);
  int sync_grid(int OK);
  void save_time_file_with_rad(int k, double wt, double wt_mhd, double wt_rad, double t, int iter_mhd, int iter_rad, double wt_mhd_mpi, double wt_rad_mpi, double wt_sav,
			       double wt_rad_prop, double wt_rad_pair, double wt_rad_smooth, double wt_rad_mom);
  void save_initial_conditions();
  void save_data();
  void close();

  // NEW: two-temp
  double get_theta_e_two_temp(double rho, double p, double Z_pm, double xi, double f_heat);
  void add_radiation_moment_sources_two_temp(std::vector<double> &I_0s, std::vector<double> &I_1s, std::vector<double> &theta_Cs, std::vector<double> &Z_pms, double f_heat, double t,
					     int two_temp_plasma);
  void evolve_two_temp(double dt);
  void get_xis(std::vector<double> &xis_p);

  // Merge bins?
  void check_if_bins_should_merge(double tau_limit);

  // Crash metrics
  int is_code_about_to_crash(std::string &message);
  int is_density_grid_too_spikey(std::string &message);
};

#endif
