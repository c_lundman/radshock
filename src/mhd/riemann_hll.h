//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef RIEMANN_HLL_H
#define RIEMANN_HLL_H

// --------------------------------------------------------
//  An approximate Riemann solver, based on the HLL method
// --------------------------------------------------------

double findSoundSpeed(const double rho, const double p, const double b, const double gamma_ad);

void findWaveSpeedsSchneider(const double v_L, const double rho_L, const double p_L, const double b_L,
			     const double v_R, const double rho_R, const double p_R, const double b_R,
			     double *B_L, double *B_R, const double gamma_ad);

void findWaveSpeedsMignone(const double v_L, const double rho_L, const double p_L, const double b_L,
			   const double v_R, const double rho_R, const double p_R, const double b_R,
			   double *B_L, double *B_R, const double gamma_ad);

void findWaveSpeedsUnity(const double v_L, const double rho_L, const double p_L, const double b_L,
			 const double v_R, const double rho_R, const double p_R, const double b_R,
			 double *B_L, double *B_R, const double gamma_ad);

void findMdots(const double v_L, const double rho_L, const double p_L, const double b_L,
	       const double v_R, const double rho_R, const double p_R, const double b_R,
	       double *Lmdot, double *Rmdot, const double gamma_ad, const int wavespeed);

void findConstFromPrim(const double v, const double rho, const double p, const double b,
		       double *D, double *E, double *S, const double gamma_ad);

void findFstars(double v_L, double p_L, double b_L, double v_R, double p_R, double b_R,
		double D_L, double E_L, double S_L, double D_R, double E_R, double S_R,
		double Lmdot, double Rmdot, double Fstars[]);

void findVstarPstar(double Fstars[], double *vstar, double *pstar);

void limitPressures(double *p_L, double rho_L, double v_L,
		    double *p_R, double rho_R, double v_R);

void Riemann_problem(double p_L, double rho_L, double v_L, double b_L,
		     double p_R, double rho_R, double v_R, double b_R,
		     double *pstar, double *vstar,
		     const double gamma_ad, const int wavespeed,
		     const int iter, const int i);

//

void Riemann_problem_TEST(double p_L, double rho_L, double v_L, double b_L,
			  double p_R, double rho_R, double v_R, double b_R,
			  double *pstar, double *vstar,
			  const double gamma_ad, const int wavespeed,
			  const int iter, const int i);

#endif
