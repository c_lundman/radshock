//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef LAGRANGIAN_MHD_FUNCTIONS_H
#define LAGRANGIAN_MHD_FUNCTIONS_H

// This file contains my implementation of Lagrangian, special relativistic (magneto)hydrodynamics

#include <string>
#include <vector>

// --------------------------------------------------
//  Variable interpolation (PPM, Colella & Woodward)
// --------------------------------------------------

double get_dma_j(double a_jm1, double a_j, double a_jp1);
double find_intermediate_a(double a_jm1, double a_j, double a_jp1, double a_jp2, double Dm_jm1, double Dm_j, double Dm_jp1, double Dm_jp2);
void find_a_L_a_R(double a_jmhalf, double a_jphalf, double a_j, double *a_L, double *a_R);
double get_a_6j(double a_j, double a_L, double a_R);
double get_Delta_a_j(double a_L, double a_R);
double find_a_pjphalf(double a_j, double a_L, double a_R, double x_j);
double find_a_mjmhalf(double a_j, double a_L, double a_R, double x_j);
double get_avg_r(double r_1, double r_2, int alpha);
double get_next_r_jhalf(double r_jhalf, double bar_v_jhalf, double Delta_t);
double get_bar_A_jhalf(double r_jhalf, double next_r_jhalf, double bar_v_jhalf, double Delta_t, int alpha);
double get_next_V_m_j(double next_r_jhalf, double next_rjneghalf, double Delta_m, int alpha);
double get_next_S_m_j(double S_m_j, double bar_A_jhalf, double bar_A_jneghalf, double bar_p_jhalf, double bar_p_jneghalf, double Delta_t, double Delta_m, double H1);
double get_next_E_m_j(double E_m_j, double bar_A_jhalf, double bar_A_jneghalf, double bar_v_jhalf, double bar_v_jneghalf, double bar_p_jhalf, double bar_p_jneghalf,
		      double Delta_t, double Delta_m, double H0);
double get_next_M_m_j(double M_m_j, double Delta_t, double HPhi);

// --------------------------------------------------------------------------------
//  Functions for computing V_m, S_m, E_m and M_m from the v, rho, p and b vectors
// --------------------------------------------------------------------------------

double get_V_m(double v, double rho);
double get_S_m(double v, double rho, double p, double b, const double gamma_ad);
double get_E_m(double v, double rho, double p, double b, const double gamma_ad);
double get_M_m(double rho, double b, double r, const int alpha);

// -------------------------------------------------------------------------------------
//  Functions for reconstructing v, rho, p and b from the V_m, S_m, E_m and M_m vectors
// -------------------------------------------------------------------------------------

void theEEquation(double w, double *f, double *dfdw, double V, double E, double S, double M, double r, int alpha, double gamma_ad);
double rtnewt(double w_guess, double w_acc, double V, double E, double S, double M, double r, int alpha, double gamma_ad);
int reconstruct_v_rho_p_b(double V_m, double S_m, double E_m, double M_m, double *v, double *rho, double *p, double *b, double r, int alpha, double gamma_ad);

// -----------------------
//  Finding the time step
// -----------------------

double get_time_step(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, std::vector<double> &m_bs,
		     int n_bs, int ghosts, int alpha, double gamma_ad, double epsilon);

// --------------------
//  Setting boundaries
// --------------------

void set_ghost_cells(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs,
		     int boundary_L, int boundary_R, double gamma_ad, int alpha, int ghosts, double piston_vgamma_L, double piston_vgamma_R);

// --------------------------------------
//  Smoothing over nan's that may happen
// --------------------------------------

void smooth_over_nans(std::vector<double> &As, int n_bs, int ghosts);

// --------------------------------
//  Finding mass or radius vectors
// --------------------------------

/* void get_radius_vector(double R_i, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, int alpha); */
/* void get_mass_vector(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, int alpha); */
void get_radius_vector(double R_i, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &V_ms, int alpha);
void get_mass_vector(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &V_ms, int alpha);

// -------------------
//  Taking a MHD step
// -------------------

int take_mhd_step(double dt, std::vector<double> &m_bs, std::vector<double> &r_bs,
		  std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms,
		  std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
		  const std::vector<double> &G0s, const std::vector<double> &G1s, const std::vector<double> &Phidots,
		  const int n_bs, const int alpha, const double gamma_ad, const int boundary_L, const int boundary_R,
		  const int debug_start_iter, const int debug_end_iter,
		  const int wavespeed, const int iter, const int RANK);

void do_ppm(std::vector<double> &m_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, int ghosts,
	    std::vector<double> &v_bs, std::vector<double> &rho_bs, std::vector<double> &p_bs, std::vector<double> &b_bs);

void fix_ppm_edges(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, int ghosts,
		   std::vector<double> &v_bs, std::vector<double> &rho_bs, std::vector<double> &p_bs, std::vector<double> &b_bs);

void fix_periodic_boundaries(int ghosts, int boundary_L, int boundary_R,
			     std::vector<double> &v_bs, std::vector<double> &rho_bs, std::vector<double> &p_bs, std::vector<double> &b_bs);

void find_left_and_right_states_inside_each_bin(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
						std::vector<double> &v_bs, std::vector<double> &rho_bs, std::vector<double> &p_bs, std::vector<double> &b_bs, int ghosts,
						std::vector<double> &vLs, std::vector<double> &vRs, std::vector<double> &rhoLs, std::vector<double> &rhoRs,
						std::vector<double> &pLs, std::vector<double> &pRs, std::vector<double> &bLs, std::vector<double> &bRs);

void find_left_and_right_states_at_each_boundary(std::vector<double> &m_bs, std::vector<double> &r_bs,
						 std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
						 std::vector<double> &vLs, std::vector<double> &vRs, std::vector<double> &rhoLs, std::vector<double> &rhoRs,
						 std::vector<double> &pLs, std::vector<double> &pRs, std::vector<double> &bLs, std::vector<double> &bRs,
						 int ghosts, double gamma_ad, int alpha, double dt,
						 std::vector<double> &vL_bs, std::vector<double> &vR_bs, std::vector<double> &rhoL_bs, std::vector<double> &rhoR_bs,
						 std::vector<double> &pL_bs, std::vector<double> &pR_bs, std::vector<double> &bL_bs, std::vector<double> &bR_bs);

void solve_the_Riemann_problem(std::vector<double> &vL_bs, std::vector<double> &vR_bs, std::vector<double> &rhoL_bs, std::vector<double> &rhoR_bs,
			       std::vector<double> &pL_bs, std::vector<double> &pR_bs, std::vector<double> &bL_bs, std::vector<double> &bR_bs,
			       int ghosts, double gamma_ad, int wavespeed, int iter,
			       std::vector<double> &bar_v_bs, std::vector<double> &bar_p_bs);

void compute_the_next_r_boundaries(std::vector<double> &r_bs, std::vector<double> &bar_v_bs, int ghosts, double dt, int alpha,
				   std::vector<double> &next_r_bs, std::vector<double> &bar_A_bs);

void compute_the_next_consts(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &next_r_bs,
			     std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
			     std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms,
			     std::vector<double> &bar_A_bs, std::vector<double> &bar_v_bs, std::vector<double> &bar_p_bs,
			     const std::vector<double> &G0s, const std::vector<double> &G1s, const std::vector<double> &Phidots,
			     int ghosts, double dt, int alpha,
			     std::vector<double> &next_V_ms, std::vector<double> &next_S_ms, std::vector<double> &next_E_ms, std::vector<double> &next_M_ms);

void reconstruct_prims(std::vector<double> &r_bs, std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms,
		       int ghosts, int alpha, double gamma_ad,
		       std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs);

void floor_pressure(std::vector<double> &ps, double p_min);

// ----------------
//  Check the grid
// ----------------

int check_prims_integrity(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
			  int iter, int RANK, const std::string& message);

int check_consts_integrity(std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms,
			   int iter, int RANK, const std::string& message);

// ---------------
//  For debugging
// ---------------

void save_vector_to_file(std::vector<double>& Vs, const std::string& filename, int RANK, int iter);

#endif
