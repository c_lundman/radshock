//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "mhd_manager.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>

#include "main_helper_functions.h"
#include "dbg.h"

MHDManager::MHDManager(std::string sim_name_p) : sim_name(sim_name_p) {

  // Initializes the grid slices
  initialize_mpi();
  set_rank_and_num_proc(); // Sets RANK, NUM_PROC
  mhd_stopwatch.set_watch_number(2);
  mpi_stopwatch.set_watch_number(3);
}

void MHDManager::set_alpha(int alpha_p) {
  alpha = alpha_p;
}

void MHDManager::set_grid(std::vector<double> &m_bs_p, std::vector<double> &r_bs_p,
			  std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p) {

  // If grid has different size than previously given, resize the vectors first
  int n_bs_p = m_bs_p.size();
  if(n_bs != n_bs_p) {
    n_bs = n_bs_p;
    m_bs.resize(n_bs_p);
    r_bs.resize(n_bs_p);
    vs.resize(n_bs_p-1);
    rhos.resize(n_bs_p-1);
    ps.resize(n_bs_p-1);
    bs.resize(n_bs_p-1);

    V_ms.resize(n_bs_p-1);
    E_ms.resize(n_bs_p-1);
    S_ms.resize(n_bs_p-1);
    M_ms.resize(n_bs_p-1);

    G0s.resize(n_bs_p-1);
    G1s.resize(n_bs_p-1);
    dotPhis.resize(n_bs_p-1);

    // NEW: two-temp
    xis.resize(n_bs_p-1);
    dotxis.resize(n_bs_p-1);

  }

  // For parallel computing
  fill_mhd_grid_range(RANK, NUM_PROC, &i_min, &i_max);
  initialize_vector_slices();

  // Fill grids
  m_bs[0] = m_bs_p[0];
  r_bs[0] = r_bs_p[0];
  for(int j=0; j<n_bs-1; j++) {
    m_bs[j+1] = m_bs_p[j+1];
    r_bs[j+1] = r_bs_p[j+1];

    // Prims
    vs[j] = vs_p[j];
    rhos[j] = rhos_p[j];
    ps[j] = ps_p[j];
    bs[j] = bs_p[j];

    // NEW: two-temp
    xis[j] = 1.;
    dotxis[j] = 0.;

  }

  // Reconstructing the conserved variables
  construct_consts_wrapper();

  // Fixing ghost cells
  set_ghost_cells_wrapper();
}

void MHDManager::set_grid_full(std::vector<double> &m_bs_p, std::vector<double> &r_bs_p,
			       std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p,
			       std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p,
			       std::vector<double> &xis_p) {

  // If grid has different size than previously given, resize the vectors first
  int n_bs_p = m_bs_p.size();
  if(n_bs != n_bs_p) {
    n_bs = n_bs_p;
    m_bs.resize(n_bs_p);
    r_bs.resize(n_bs_p);
    vs.resize(n_bs_p-1);
    rhos.resize(n_bs_p-1);
    ps.resize(n_bs_p-1);
    bs.resize(n_bs_p-1);

    V_ms.resize(n_bs_p-1);
    E_ms.resize(n_bs_p-1);
    S_ms.resize(n_bs_p-1);
    M_ms.resize(n_bs_p-1);

    G0s.resize(n_bs_p-1);
    G1s.resize(n_bs_p-1);
    dotPhis.resize(n_bs_p-1);

    // NEW: two-temp
    xis.resize(n_bs_p-1);
    dotxis.resize(n_bs_p-1);

  }

  // For parallel computing
  fill_mhd_grid_range(RANK, NUM_PROC, &i_min, &i_max);
  // std::cout << "(mhd_manager.cpp) RANK = " << RANK << ", i_min = " << i_min << ", i_max = " << i_max << std::endl;
  initialize_vector_slices();

  // Fill grids
  m_bs[0] = m_bs_p[0];
  r_bs[0] = r_bs_p[0];
  for(int j=0; j<n_bs-1; j++) {
    m_bs[j+1] = m_bs_p[j+1];
    r_bs[j+1] = r_bs_p[j+1];

    // Prims
    vs[j] = vs_p[j];
    rhos[j] = rhos_p[j];
    ps[j] = ps_p[j];
    bs[j] = bs_p[j];

    // Consts
    V_ms[j] = V_ms_p[j];
    S_ms[j] = S_ms_p[j];
    E_ms[j] = E_ms_p[j];
    M_ms[j] = M_ms_p[j];

    // NEW: two-temp
    xis[j] = xis_p[j];
    dotxis[j] = 0.;

  }

  // // Fixing ghost cells
  // set_ghost_cells_wrapper();
}

void MHDManager::set_grid_mass(double R_i, std::vector<double> &m_bs_p,
			       std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p) {

  // If grid has different size than previously given, resize the vectors first
  int n_bs_p = m_bs_p.size();
  if(n_bs != n_bs_p) {
    n_bs = n_bs_p;
    m_bs.resize(n_bs_p);
    r_bs.resize(n_bs_p);
    vs.resize(n_bs_p-1);
    rhos.resize(n_bs_p-1);
    ps.resize(n_bs_p-1);
    bs.resize(n_bs_p-1);

    V_ms.resize(n_bs_p-1);
    E_ms.resize(n_bs_p-1);
    S_ms.resize(n_bs_p-1);
    M_ms.resize(n_bs_p-1);

    G0s.resize(n_bs_p-1);
    G1s.resize(n_bs_p-1);
    dotPhis.resize(n_bs_p-1);

    // NEW: two-temp
    xis.resize(n_bs_p-1);
    dotxis.resize(n_bs_p-1);

  }

  // For parallel computing
  fill_mhd_grid_range(RANK, NUM_PROC, &i_min, &i_max);
  initialize_vector_slices();

  // Fill grids
  m_bs = m_bs_p;
  vs = vs_p;
  rhos = rhos_p;
  ps = ps_p;
  bs = bs_p;

  // // Fill grids
  // m_bs[0] = m_bs_p[0];
  // // r_bs[0] = r_bs_p[0];
  // for(int j=0; j<n_bs-1; j++) {
  //   m_bs[j+1] = m_bs_p[j+1];
  //   // r_bs[j+1] = r_bs_p[j+1];

  //   // Prims
  //   vs[j] = vs_p[j];
  //   rhos[j] = rhos_p[j];
  //   ps[j] = ps_p[j];
  //   bs[j] = bs_p[j];
  // }

  // Reconstructing the conserved variables (this gets M_ms wrong, as it is a function of r)
  construct_consts_wrapper();

  // Now knows, V_ms, can compute r_bs
  compute_radius_vector_wrapper(R_i);

  // Reconstructing the conserved variables (here M_ms becomes correct, as r_bs is now known)
  construct_consts_wrapper();

  // Fixing ghost cells
  set_ghost_cells_wrapper();
}

void MHDManager::resize_all_grids(int n_bs_p) {
  // Resizes all grids except m_bs, r_bs, and consts

  // if(n_bs_p != n_bs) {

  n_bs = n_bs_p;
  // r_bs.resize(n_bs_p);
  vs.resize(n_bs_p-1);
  rhos.resize(n_bs_p-1);
  ps.resize(n_bs_p-1);
  bs.resize(n_bs_p-1);

  G0s.resize(n_bs_p-1);
  G1s.resize(n_bs_p-1);
  dotPhis.resize(n_bs_p-1);

  // For parallel computing
  fill_mhd_grid_range(RANK, NUM_PROC, &i_min, &i_max);
  initialize_vector_slices();

  // NEW: two-temp
  xis.resize(n_bs_p-1);
  dotxis.resize(n_bs_p-1);
  // }
}

void MHDManager::set_grid_consts(std::vector<double> &m_bs_p, std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p) {

  // If grid has different size than previously given, resize the vectors first
  int n_bs_p = m_bs_p.size();
  m_bs = m_bs_p;
  V_ms = V_ms_p;
  S_ms = S_ms_p;
  E_ms = E_ms_p;
  M_ms = M_ms_p;

  resize_all_grids(n_bs_p);

  // Computing the radius vector
  double R_i = r_bs[2];
  r_bs.resize(n_bs_p);
  compute_radius_vector_wrapper(R_i);

  // Reconstructing the primitive variables
  reconstruct_prims_wrapper();

  // Fixing ghost cells
  set_ghost_cells_wrapper();
}

void MHDManager::get_grid_consts(std::vector<double> &m_bs_p, std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p) {

  m_bs_p = m_bs;
  V_ms_p = V_ms;
  S_ms_p = S_ms;
  E_ms_p = E_ms;
  M_ms_p = M_ms;
  // // If grid has different size than previously given, resize the vectors first
  // int n_bs_p = m_bs_p.size();
  // if(n_bs != n_bs_p) {
  //   m_bs_p.resize(n_bs);
  //   r_bs_p.resize(n_bs);
  //   V_ms_p.resize(n_bs-1);
  //   S_ms_p.resize(n_bs-1);
  //   E_ms_p.resize(n_bs-1);
  //   M_ms_p.resize(n_bs-1);
  // }

  // // Fill grids
  // m_bs_p[0] = m_bs[0];
  // r_bs_p[0] = r_bs[0];
  // for(int j=0; j<n_bs-1; j++) {
  //   m_bs_p[j+1] = m_bs[j+1];
  //   r_bs_p[j+1] = r_bs[j+1];

  //   // Consts
  //   V_ms_p[j] = V_ms[j];
  //   S_ms_p[j] = S_ms[j];
  //   E_ms_p[j] = E_ms[j];
  //   M_ms_p[j] = M_ms[j];
  // }
}

void MHDManager::add_G_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p) {
  for(int i=0; i<n_bs-1; i++) {
    G0s[i] += G0s_p[i];
    G1s[i] += G1s_p[i];
  }
}

void MHDManager::add_all_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p, std::vector<double> &dotPhis_p) {
  for(int i=0; i<n_bs-1; i++) {
    G0s[i] += G0s_p[i];
    G1s[i] += G1s_p[i];
    dotPhis[i] += dotPhis_p[i];
  }
}

std::vector<double> MHDManager::get_linear_time_vector(double t_i, double t_f, int n_ts) {
  // Fills a vector with n_ts values between t_i and t_f, linearly spaced (first value is t_i; not used)

  std::vector<double> ts(n_ts);
  double dt = (t_f-t_i)/(n_ts-1);
  for(int k=0; k<n_ts; k++) {
    ts[k] = t_i + k*dt;
  }
  return ts;
}

std::vector<double> MHDManager::get_logarithmic_time_vector(double t_i, double t_f, int n_ts) {
  // Fills a vector with n_ts values between t_i and t_f, logarithmically spaced (first value is zero; not used)

  std::vector<double> ts(n_ts);

  // // First element is 0, second is t_i
  // double lnt, dlnt = log(t_f/t_i)/(n_ts-2);
  // ts[0] = 0.;
  // for(int k=1; k<n_ts; k++) {
  //   lnt = log(t_i) + (k-1)*dlnt;
  //   ts[k] = exp(lnt);
  // }

  // First element is t_i
  double lnt, dlnt = log(t_f/t_i)/(n_ts-1);
  for(int k=0; k<n_ts; k++) {
    lnt = log(t_i) + k*dlnt;
    ts[k] = exp(lnt);
  }
  return ts;
}

void MHDManager::print_progress(double t_f, int k, int n_ts) {
  if(RANK == 0) {
    std::cout << "   k = ";
    std::cout.width(3);
    std::cout << k << " / " << n_ts << "    t = " << t << " / " << t_f << "    n_bins = " << vs.size()-4 << "    iter_mhd = ";
    std::cout.width(6);
    std::cout << iter << "    ";
  }
}

void MHDManager::print_slices(int RANK_p) {
  if(RANK_p == -1 || RANK_p == RANK) {

    std::cout << "Printing slice (RANK=" << RANK << ", i_min=" << i_min << ", i_max=" << i_max << ")" << std::endl;

    // std::cout << "m_bs : ";
    // for(unsigned int i=0; i<m_bs_slice.size(); i++) std::cout << m_bs_slice[i] << " | ";
    // std::cout << std::endl;

    std::cout << "r_bs : ";
    for(unsigned int i=0; i<r_bs_slice.size(); i++) std::cout << r_bs_slice[i] << " | ";
    std::cout << std::endl;

    std::cout << "vs :       ";
    for(unsigned int i=0; i<vs_slice.size(); i++) std::cout << vs_slice[i] << " | ";
    std::cout << std::endl;

    std::cout << "rhos :     ";
    for(unsigned int i=0; i<rhos_slice.size(); i++) std::cout << rhos_slice[i] << " | ";
    std::cout << std::endl;

    std::cout << "ps :       ";
    for(unsigned int i=0; i<ps_slice.size(); i++) std::cout << ps_slice[i] << " | ";
    std::cout << std::endl << std::endl;

    // std::cout << "bs :       ";
    // for(unsigned int i=0; i<bs_slice.size(); i++) std::cout << bs_slice[i] << " | ";
    // std::cout << std::endl << std::endl;
  }

}

void MHDManager::print_grid(int RANK_p) {
  if(RANK_p == -1 || RANK_p == RANK) {

    std::cout << "Printing grid (RANK=" << RANK << ", i_min=" << i_min << ", i_max=" << i_max << ")" << std::endl;

    // std::cout << "m_bs : ";
    // for(unsigned int i=0; i<m_bs.size(); i++) std::cout << m_bs[i] << " | ";
    // std::cout << std::endl;

    std::cout << "r_bs : ";
    for(unsigned int i=0; i<r_bs.size(); i++) std::cout << r_bs[i] << " | ";
    std::cout << std::endl;

    std::cout << "vs :       ";
    for(unsigned int i=0; i<vs.size(); i++) std::cout << vs[i] << " | ";
    std::cout << std::endl;

    std::cout << "rhos :     ";
    for(unsigned int i=0; i<rhos.size(); i++) std::cout << rhos[i] << " | ";
    std::cout << std::endl;

    std::cout << "ps :       ";
    for(unsigned int i=0; i<ps.size(); i++) std::cout << ps[i] << " | ";
    std::cout << std::endl << std::endl;

    // std::cout << "bs :       ";
    // for(unsigned int i=0; i<bs.size(); i++) std::cout << bs[i] << " | ";
    // std::cout << std::endl << std::endl;
  }

}

void MHDManager::close() {
  // Close everything
  close_all_files();
  finalize_mpi();
}

void MHDManager::evolve_two_temp(double dt) {
  // NEW: two-temp
  // Assumes that dotxis have been set (it gets set in add_radiation_moment_sources_two_temp(...))
  double xi_0;
  for(unsigned int j=0; j<xis.size(); j++) {
    xi_0 = xis[j];
    xis[j] += dotxis[j]*dt;

    // If xi - 1 = theta_e/theta_p - 1 changes sign, instead set it to 1, since the change in sign could be due to
    // taking a too large time step (not resolving the Coulomb energy exchange time)
    if((xi_0 - 1.)*(xis[j] - 1.) < 0.) {
      xis[j] = 1.;
    }
  }
}

int MHDManager::update_single_step(double dt, int debug_start_iter, int debug_end_iter) {
  // Do an update here
  // The sources need to be set BEFORE calling this update
  // If multiple threads, each thread updates only part of the grid,
  // then the master collects grid information from all workers, sets
  // the boundary ghost cells, and finally distributes the complete
  // grid to all workers

  mhd_stopwatch.start();

  // MHD step
  set_slices_from_grid();
  int OK = take_mhd_step_wrapper(dt, debug_start_iter, debug_end_iter);
  put_slices_on_grid();

  // Iteration and time
  iter += 1;
  t += dt;

  // // MPI (and ghost cells)
  // OK = sync_grid(OK);

  // OK = check_if_code_crashed(OK);
  // collect_mhd_grid();
  // distribute_mhd_grid();
  // set_ghost_cells_wrapper();

  mhd_stopwatch.stop();

  return OK;
}

void MHDManager::check_if_bins_should_merge(double tau_limit) {
  // NEW: merge cells?

  // Never go below 32 bins
  int n_c = m_bs.size()-5;
  if(get_max_tau_column() < tau_limit && n_c >= 2*NUM_PROC && n_c >= 64) {
    if(is_master()) {
      std::cout << "Merging bins (" << m_bs.size()-5 << " -> " << (m_bs.size()-5)/2 << ")" << std::endl;
    }
    merge_neighbouring_bins();
  }
}

int MHDManager::sync_grid(int OK) {

  mhd_stopwatch.start();
  mpi_stopwatch.start();

  // MPI (and ghost cells)
  OK = check_if_code_crashed(OK);
  collect_mhd_grid();
  distribute_mhd_grid();
  set_ghost_cells_wrapper();

  mpi_stopwatch.stop();
  mhd_stopwatch.stop();

  return OK;
}

int MHDManager::update_until_time(double t_f) {
  // Updates the state until t = t_f
  // The sources need to be manually flushed and set BEFORE calling this update
  // The sources will be constant until t = t_f

  int OK=1;
  double dt;
  while(t < t_f) {
    dt = get_time_step_wrapper();
    if(t + dt > t_f) {
      dt = t_f - t;
    }

    mpi_stopwatch.start();
    OK = update_single_step(dt, 0, 0); // t += dt in this function
    mpi_stopwatch.stop();
    if(OK == 0) {t = t_f;}
  }

  return OK;
}

int MHDManager::update_until_time_and_save(double t_f) {
  int OK = update_until_time(t_f);
  save_data();
  return OK;
}

double MHDManager::get_max_tau_column() {
  // Computes the largest tau_column value on the grid and returns it
  double m_p = 1.6726231e-24;
  double sigma_T = 6.6524e-25;
  double kappa_T = sigma_T/m_p;

  double dm, r_avg, dtau=0, dtau_max=0;
  for(unsigned int i=2; i<m_bs.size()-4; i++) {
    dm = m_bs[i+1]-m_bs[i];
    r_avg = .5*(r_bs[i+1]+r_bs[i]);
    if(alpha == 2) {
      dtau = kappa_T*dm/r_avg/r_avg;
    } else if(alpha == 0) {
      dtau = kappa_T*dm;
    }
    if(dtau > dtau_max) {
      dtau_max = dtau;
    }
  }
  // std::cout << "dtau_max = " << dtau_max << std::endl;
  return dtau_max;
}

void MHDManager::merge_neighbouring_bins() {
  // Merges neighbouring bins, such that the total number of (non-ghost) bins are halved

  // ASSUMES THAT THE NUMBER OF BINS IS AN EVEN NUMBER
  if((m_bs.size() - 5) % 2 != 0) {
    std::cout << "TRYING TO MERGE AN UN-EVEN NUMBER OF BINS" << std::endl;
    return;
  }

  // New number of boundaries (incl. ghost cells)
  int n_bs_p = (m_bs.size() - 5)/2 + 5;
  std::vector<double> m_bs_p, r_bs_p, V_ms_p, E_ms_p, S_ms_p, M_ms_p;
  m_bs_p.resize(n_bs_p);
  r_bs_p.resize(n_bs_p);
  V_ms_p.resize(n_bs_p-1);
  E_ms_p.resize(n_bs_p-1);
  S_ms_p.resize(n_bs_p-1);
  M_ms_p.resize(n_bs_p-1);

  // Set ghosts
  for(int i=0; i<=2; i++) {
    m_bs_p[i] = m_bs[i];
    r_bs_p[i] = r_bs[i];
    if(i < 2) {
      V_ms_p[i] = V_ms[i];
      E_ms_p[i] = E_ms[i];
      S_ms_p[i] = S_ms[i];
      M_ms_p[i] = M_ms[i];
    }
  }

  for(int i=0; i<=2; i++) {
    m_bs_p[n_bs_p-1-i] = m_bs[m_bs.size()-1-i];
    r_bs_p[n_bs_p-1-i] = r_bs[r_bs.size()-1-i];
    if(i < 2) {
      V_ms_p[n_bs_p-2-i] = V_ms[V_ms.size()-1-i];
      E_ms_p[n_bs_p-2-i] = E_ms[V_ms.size()-1-i];
      S_ms_p[n_bs_p-2-i] = S_ms[V_ms.size()-1-i];
      M_ms_p[n_bs_p-2-i] = M_ms[V_ms.size()-1-i];
    }
  }

  // Loop over bins, except for ghosts
  for(int i=2; i<n_bs_p-3; i++) {
    if(i > 2) {
      m_bs_p[i] = m_bs[2*(i - 2) + 2];
      r_bs_p[i] = r_bs[2*(i - 2) + 2];
    }

    V_ms_p[i] = .5*(V_ms[2*(i - 2) + 2] + V_ms[2*(i - 2) + 3]);
    E_ms_p[i] = .5*(E_ms[2*(i - 2) + 2] + E_ms[2*(i - 2) + 3]);
    S_ms_p[i] = .5*(S_ms[2*(i - 2) + 2] + S_ms[2*(i - 2) + 3]);
    M_ms_p[i] = .5*(M_ms[2*(i - 2) + 2] + M_ms[2*(i - 2) + 3]);
  }

  // Set new grids
  m_bs = m_bs_p;
  r_bs = r_bs_p;
  V_ms = V_ms_p;
  E_ms = E_ms_p;
  S_ms = S_ms_p;
  M_ms = M_ms_p;

  // Resize all grids except r_bs, m_bs and consts
  resize_all_grids(n_bs_p);

  // Reconstructing the primitive variables
  reconstruct_prims_wrapper();

  // Fixing ghost cells
  set_ghost_cells_wrapper();

}
