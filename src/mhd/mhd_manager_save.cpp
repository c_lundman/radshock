//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "mhd_manager.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>

void MHDManager::open_all_files() {
  // Prepare the files
  if(RANK == 0) {

    // std::string sim_name = mhd_output_snapshot_file.substr(0, mhd_output_snapshot_file.find("."));

    mhd_file.open(mhd_output_snapshot_file, std::fstream::trunc);
    g0s_file.open(mhd_output_snapshot_file.substr(0, mhd_output_snapshot_file.find("."))+".g0s", std::fstream::trunc);
    g1s_file.open(mhd_output_snapshot_file.substr(0, mhd_output_snapshot_file.find("."))+".g1s", std::fstream::trunc);
    tim_file.open(mhd_output_snapshot_file.substr(0, mhd_output_snapshot_file.find("."))+".tim", std::fstream::trunc);

    // NEW: two-temp
    xis_file.open(mhd_output_snapshot_file.substr(0, mhd_output_snapshot_file.find("."))+".xis", std::fstream::trunc);
    xis_file << std::scientific;
    xis_file.precision(12);

    // Setting scientific notation and precision
    mhd_file << std::scientific;
    mhd_file.precision(12);
    g0s_file << std::scientific;
    g0s_file.precision(12);
    g1s_file << std::scientific;
    g1s_file.precision(12);
    tim_file << std::scientific;
    tim_file.precision(12);

    // Write file headers
    mhd_file << "_HEADER" << std::endl;
    mhd_file << "boundary_L = " << boundary_L << std::endl;
    mhd_file << "boundary_R = " << boundary_R << std::endl;
    mhd_file << "gamma_ad = " << gamma_ad << std::endl;
    mhd_file << "alpha = " << alpha << std::endl;
    mhd_file << std::endl;

    // Write output file legend
    mhd_file << "_LEGEND" << std::endl;
    mhd_file << "i (iteration number)" << std::endl;
    mhd_file << "t (current time)" << std::endl;
    mhd_file << "r[0] r[1] r[2] ... (radius vector)" << std::endl;
    mhd_file << "v[0] v[1] v[2] ... (speed vector)" << std::endl;
    mhd_file << "rho[0] rho[1] rho[2] ... (density vector)" << std::endl;
    mhd_file << "p[0] p[1] p[2] ... (pressure vector)" << std::endl;
    mhd_file << "b[0] b[1] b[2] ... (mag. field vector)" << std::endl;
    mhd_file << std::endl;
  }
}

void MHDManager::save_time_file_with_rad(int k, double wt, double wt_mhd, double wt_rad, double t, int iter_mhd, int iter_rad, double wt_mhd_mpi, double wt_rad_mpi, double wt_sav,
					 double wt_rad_prop, double wt_rad_pair, double wt_rad_smooth, double wt_rad_mom) {
  if(RANK == 0) {
    if(wt == 0.) tim_file << "NUM_PROC = " << NUM_PROC << std::endl;

    tim_file << " " << k;
    tim_file << " " << wt;
    tim_file << " " << wt_mhd;
    tim_file << " " << wt_rad;
    tim_file << " " << t;
    tim_file << " " << iter_mhd;
    tim_file << " " << iter_rad;
    tim_file << " " << wt_mhd_mpi;
    tim_file << " " << wt_rad_mpi;
    tim_file << " " << wt_sav;
    tim_file << " " << wt_rad_prop;
    tim_file << " " << wt_rad_pair;
    tim_file << " " << wt_rad_smooth;
    tim_file << " " << wt_rad_mom;
    tim_file << std::endl;

    tim_file.flush();
  }
}

void MHDManager::save_initial_conditions() {
  // Open the files
  open_all_files();

  // Saving mass vector
  if(RANK == 0) {

    // Saving the initial iteration, time, r, v, rho, p and b vectors
    mhd_file << "_DATA" << std::endl;
    save_data();
  }

}

void MHDManager::save_data() {
  // Save all MHD quantities (v, rho, p, b)
  if(RANK == 0) {

    mhd_file << " " << iter << std::endl;
    mhd_file << " " << t << std::endl;
    for(int i=0; i<n_bs; i++) {
      mhd_file << " " << m_bs[i];
      if(i == n_bs-1) {
    	mhd_file << std::endl;
      }
    }

    for(int i=0; i<n_bs; i++) {
      mhd_file << " " << r_bs[i];
      if(i == n_bs-1) {
	mhd_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      mhd_file << " " << vs[j];
      if(j == n_bs-2) {
	mhd_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      mhd_file << " " << rhos[j];
      if(j == n_bs-2) {
	mhd_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      mhd_file << " " << ps[j];
      if(j == n_bs-2) {
	mhd_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      mhd_file << " " << bs[j];
      if(j == n_bs-2) {
	mhd_file << std::endl << std::endl;
      }
    }

    mhd_file.flush();

    //

    for(int j=0; j<n_bs-1; j++) {
      g0s_file << " " << G0s[j];
      if(j == n_bs-2) {
	g0s_file << std::endl;
      }
    }

    g0s_file.flush();

    for(int j=0; j<n_bs-1; j++) {
      g1s_file << " " << G1s[j];
      if(j == n_bs-2) {
	g1s_file << std::endl;
      }
    }

    g1s_file.flush();

    // NEW: two-temp
    for(int j=0; j<n_bs-1; j++) {
      xis_file << " " << xis[j];
      if(j == n_bs-2) {
	xis_file << std::endl;
      }
    }

    xis_file.flush();

  }
}

void MHDManager::save_state() {

  if(RANK == 0) {

    save_state_file.open(mhd_output_state_file, std::fstream::trunc);

    // Setting scientific notation and precision
    save_state_file << std::scientific;
    save_state_file.precision(16);

    // Write file header
    save_state_file << "n_bs       " << n_bs << std::endl;
    save_state_file << "alpha      " << alpha << std::endl;
    save_state_file << "gamma_ad   " << gamma_ad << std::endl;
    save_state_file << "t          " << t << std::endl;
    save_state_file << "iter       " << iter << std::endl;
    save_state_file << "two_temp_version " << 1 << std::endl;
    save_state_file << std::endl;
    save_state_file << "---" << std::endl;
    save_state_file << std::endl;

    // Save grids
    for(int i=0; i<n_bs; i++) {
      save_state_file << " " << m_bs[i];
      if(i == n_bs-1) {
    	save_state_file << std::endl;
      }
    }

    for(int i=0; i<n_bs; i++) {
      save_state_file << " " << r_bs[i];
      if(i == n_bs-1) {
	save_state_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << vs[j];
      if(j == n_bs-2) {
	save_state_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << rhos[j];
      if(j == n_bs-2) {
	save_state_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << ps[j];
      if(j == n_bs-2) {
	save_state_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << bs[j];
      if(j == n_bs-2) {
	save_state_file << std::endl << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << V_ms[j];
      if(j == n_bs-2) {
    	// save_state_file << std::endl << std::endl;
    	save_state_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << S_ms[j];
      if(j == n_bs-2) {
    	// save_state_file << std::endl << std::endl;
    	save_state_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << E_ms[j];
      if(j == n_bs-2) {
    	// save_state_file << std::endl << std::endl;
    	save_state_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << M_ms[j];
      if(j == n_bs-2) {
    	save_state_file << std::endl << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << xis[j];
      if(j == n_bs-2) {
    	save_state_file << std::endl << std::endl;
      }
    }

    save_state_file.flush();

    // Close state file
    save_state_file.close();
  }

}

void MHDManager::load_state(std::string sim_name_of_state) {

  // Open state file
  load_state_file.open(sim_name_of_state, std::fstream::in);
  if(!load_state_file.is_open() && RANK == 0) {
    std::cout << std::endl << "ERROR: Could not load the file " << sim_name_of_state << std::endl << std::endl;
  }

  // Parse state file
  int n_bs_p = 0;
  int reading_header = 1;
  int two_temp_version = 0;

  std::string tmp;
  while(reading_header == 1) {

    load_state_file >> tmp;

    if(tmp == "n_bs") {
      load_state_file >> tmp;
      n_bs_p = std::stoi(tmp);

    } else if(tmp == "alpha") {
      load_state_file >> tmp;
      set_alpha(std::stoi(tmp));

    } else if(tmp == "gamma_ad") {
      load_state_file >> tmp;
      set_gamma_ad(std::stod(tmp));

    } else if(tmp == "t") {
      load_state_file >> tmp;
      set_t(std::stod(tmp));

    } else if(tmp == "iter") {
      load_state_file >> tmp;
      set_iter(std::stoi(tmp));

    } else if(tmp == "two_temp_version") {
      load_state_file >> tmp;
      two_temp_version = std::stoi(tmp);

    } else if(tmp == "---") {
      reading_header = 0;
    }

  }

  // Now, know n_bs (or rather, n_bs_p), so initiate vectors and fill them
  std::vector<double> m_bs_p(n_bs_p), r_bs_p(n_bs_p), vs_p(n_bs_p-1), rhos_p(n_bs_p-1), ps_p(n_bs_p-1), bs_p(n_bs_p-1);
  std::vector<double> V_ms_p(n_bs_p-1), S_ms_p(n_bs_p-1), E_ms_p(n_bs_p-1), M_ms_p(n_bs_p-1);
  std::vector<double> xis_p(n_bs_p-1);

  for(int i=0; i<n_bs_p; i++) {
    load_state_file >> tmp;
    m_bs_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p; i++) {
    load_state_file >> tmp;
    r_bs_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    vs_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    rhos_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    ps_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    bs_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    V_ms_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    S_ms_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    E_ms_p[i] = std::stod(tmp);
  }

  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    M_ms_p[i] = std::stod(tmp);
  }

  if(two_temp_version == 1) {
    for(int i=0; i<n_bs_p-1; i++) {
      load_state_file >> tmp;
      xis_p[i] = std::stod(tmp);
      // xis_p[i] = 1e-1;
    }
  } else {
    for(int i=0; i<n_bs_p-1; i++) {
      xis_p[i] = 1.;
    }
  }

  // Close state file
  load_state_file.close();

  // Set the grid
  set_grid_full(m_bs_p, r_bs_p, vs_p, rhos_p, ps_p, bs_p, V_ms_p, S_ms_p, E_ms_p, M_ms_p, xis_p);

  // // Set ghost cells
  // set_ghost_cells_wrapper();

}

void MHDManager::close_all_files() {
  // Closing all files
  if(RANK == 0) {
    mhd_file.close();
    g0s_file.close();
    g1s_file.close();
    tim_file.close();

    // NEW: two-temp
    xis_file.close();
  }
}
