//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "mhd_manager.h"
#include <string>
#include <vector>
#include <iostream>
#include <math.h>
#include "lagrangian_mhd_functions.h"
#include "physical_constants.h"
#include "main_helper_functions.h"

// Private functions (physics)

void MHDManager::flush_sources() {
  // Simply resetting all "forces" to zero
  std::fill(G0s.begin(), G0s.end(), 0);
  std::fill(G1s.begin(), G1s.end(), 0);
  std::fill(dotPhis.begin(), dotPhis.end(), 0);

  // NEW: two-temp
  std::fill(dotxis.begin(), dotxis.end(), 0);
}

void MHDManager::add_gravity_sources() {
  // Newtonian gravity from a point-source located at r = 0 with mass M = M_grav

  double r, v, Gamma, rho;
  for(int i=ghosts; i<n_bs-1-ghosts; i++) {
    r = .5*(r_bs[i+1]+r_bs[i]);
    v = vs[i];
    rho = rhos[i];
    Gamma = 1./sqrt(1.-v*v);
    G0s[i] += -(G/c/c*M_grav/r/r)*v*Gamma*rho;
    G1s[i] += -(G/c/c*M_grav/r/r)*Gamma*rho;
  }
}

// void make_pressure_hydrostatic(const double m_bs[], const double r_bs[], const double rhos[], double ps[], const double M_grav, const int n_bs) {
void MHDManager::make_pressure_hydrostatic(double norm) {

  // Assumes that the grid and M_grav have been set

  /* [dp/dr] = g / cm^4 */
  /* [(G/c^2)*M*m/r] = g */
  /* [(G/c^2)*M*rho/r^2] = g / cm^4 */
  /* dp/dr = (G/c^2)*M_NS*rho/r^2 */

  // double dm;
  double dr, r, rho, dp;

  for(int j=n_bs-2; j>=0; j--) {
    // dm = 4.*pi*(m_bs[j+1]-m_bs[j]);
    dr = r_bs[j+1]-r_bs[j];
    r = .5*(r_bs[j+1]+r_bs[j]);

    // dp = (G/c/c)*M_NS*dm/r/r*dr

    rho = rhos[j];
    dp = (G/c/c)*M_grav*rho/r/r*dr;
    if(j == n_bs-2) {
      ps[j] = dp;
    } else {
      ps[j] = ps[j+1] + dp;
    }
  }

  if(norm != 1) {
    for(unsigned int j=0; j<ps.size(); j++) {
      ps[j] *= norm;
    }
  }
}

// double MHDManager::get_theta(double rho, double p, double Z_pm, double gamma_ad, double f_heat) {
//   // WARNING: This function is a copy of the same function inside 'librad/rad_propagation.cpp'!
//   // Used only by the 'add_radiation_moment_sources' function below
//   // Allows MHD code to be "independent" on the radiation files

//   // Returns the electron temperature
//   // double theta = m_p/m_e*p/rho/(1.+Z_pm)/f_heat - (gamma_ad-1.)*(Z_pm-1.)/(Z_pm+1.)/f_heat;
//   double theta = m_p/m_e*p/rho/(1.+Z_pm)/f_heat;
//   return theta;
// }

double MHDManager::get_theta_e_two_temp(double rho, double p, double Z_pm, double xi, double f_heat) {
  // NEW: two-temp

  // WARNING: This function is a copy of the same function inside 'librad/rad_propagation.cpp'!
  // Used only by the 'add_radiation_moment_sources' function below
  // Allows MHD code to be "independent" on the radiation files

  // Returns the electron temperature
  double theta_e = (m_p/m_e)*(p/rho)*xi/(1.+Z_pm*xi)/f_heat;
  return theta_e;
}

// double MHDManager::get_p(double rho, double theta, double Z_pm, double gamma_ad, double f_heat) {
//   // Returns the gas pressure (same as the function above, but inverted)
//   double p = m_e*rho*(1.+Z_pm)*f_heat/m_p*theta;
//   return p;
// }

void MHDManager::floor_pressures(std::vector<double> &theta_Cs, std::vector<double> &Z_pms, double f_heat) {
  mhd_stopwatch.start();
  double p_floor;
  double theta_floor;
  for(int j=0; j<n_bs-1; j++) {
    // p_floor = get_p(rhos[j], xi*theta_Cs[j], Z_pms[j], gamma_ad, f_heat);
    theta_floor = .8*theta_Cs[j];
    p_floor = m_e*rhos[j]*(1.+Z_pms[j])*f_heat/m_p*theta_floor;
    if(ps[j] < p_floor) {
      ps[j] = p_floor;
    }

  }
  mhd_stopwatch.stop();
}

void MHDManager::add_radiation_moment_sources_two_temp(std::vector<double> &I_0s, std::vector<double> &I_1s, std::vector<double> &theta_Cs, std::vector<double> &Z_pms, double f_heat, double t,
						       int two_temp_plasma) {
  // NEW: two-temp
  // The two-temp algorithm assumes that MHDManager has a vector named xis (xi = theta_e/theta_p) which will be used and evolved inside this function,
  // and later passed to RADManager (which needs to compute theta_e)

  // Computes the G sources based on the radiation moments

  // Here, I_0s, I_1s, theta_Cs are smoothed across "tau_smoothing"
  // The hydro quantities are not smoothed
  // t is not used anymore; was used for printing information (debugging)

  mhd_stopwatch.start();

  // Fill the sources
  double y, x, beta, betaGamma_sq;
  double gamma, v, rho, p, Z_pm;
  double Pic, Mic;
  double H0_tilde, H1_tilde;

  // NEW: two-temp
  double theta_e, theta_p;
  double Pcc, xi;
  double lnLambda = 15.; // Coulomb logarithm
  double Acc = 1.5*sqrt(2./pi)*m_e*m_e/m_p/m_p*sigma_T*lnLambda; // A constant for the "Coulomb power"

  // for(int j=0; j<n_bs-1; j++) {
  for(int j=2; j<n_bs-3; j++) {
    v = vs[j];
    gamma = 1./sqrt(1.-v*v);
    rho = rhos[j];
    p = ps[j];
    Z_pm = Z_pms[j];
    xi = xis[j];

    // NEW: two-temp
    // Get two temperatures
    theta_e = get_theta_e_two_temp(rho, p, Z_pm, xi, f_heat);

    y = 3.*theta_e;
    x = y*(-.5*y + sqrt(1. + y*y/4.));
    beta = sqrt(x);
    betaGamma_sq = beta*beta/(1.-beta*beta);

    // Pic = Compton power, Pic = G^0 / n_pm (where G^0 is the comoving source term)
    // Mic = "momentum power", Mic = G^1 / n_pm (where G^1 is the comoving source term)
    Pic = (4./3.)*sigma_T*I_0s[j]*( 3.*theta_Cs[j] - betaGamma_sq );
    Mic = sigma_T*I_1s[j];

    // If temperature difference is small, kill Pic
    double small_fraction = 1e-1;
    if((1. - betaGamma_sq/3./theta_Cs[j])*(1. - betaGamma_sq/3./theta_Cs[j]) < small_fraction*small_fraction) {
      Pic = 0.;
    }

    // Lab frame specific (e.g. per mass) quantities
    H0_tilde = Z_pm/m_p*(Pic + v*Mic);
    H1_tilde = Z_pm/m_p*(Mic + v*Pic);

    G0s[j] += H0_tilde*gamma*rho;
    G1s[j] += H1_tilde*gamma*rho;

    // NEW: two-temp
    // Add to xi derivative: Pic = energy per comoving second gained by a single electron from radiation,
    //                       Pcc = energy per comoving second lost from a single electron to protons in Coulomb collisions
    Pcc = 0;
    theta_p = theta_e/xi;
    if(two_temp_plasma == 0) {
      // Enforcing single temperature (infinitely fast thermalization between protons and electrons)
      Pcc = Pic/(1. + Z_pm*xi);

    } else if(two_temp_plasma == 1) {
      // Thermalization via Coulomb coupling
      Pcc = Acc*Z_pm*rho*(theta_e-theta_p)/pow(theta_e + (m_e/m_p)*theta_p, 1.5);

    } else if(two_temp_plasma == 2) {
      // No energy exchange between electrons and protons
      Pcc = 0.;
    }

    dotxis[j] = 1./(gamma*theta_p*m_e)*(Pic - Pcc*(1.+Z_pm*xi));

  }

  mhd_stopwatch.stop();

}

// void MHDManager::add_radiation_moment_sources(std::vector<double> &I_0s, std::vector<double> &I_1s, std::vector<double> &theta_Cs, std::vector<double> &Z_pms, double f_heat, double t) {
//   // Computes the G sources based on the radiation moments

//   // Here, I_0s, I_1s, theta_Cs are smoothed across "tau_smoothing"
//   // The hydro quantities are not smoothed
//   // t is not used anymore; was used for printing information (debugging)

//   mhd_stopwatch.start();

//   // Fill the sources
//   double y, theta, x, beta, betaGamma_sq;
//   double gamma, v, rho, p, Z_pm;
//   double Pic, Mic;
//   double H0_tilde, H1_tilde;
//   for(int j=0; j<n_bs-1; j++) {
//     v = vs[j];
//     gamma = 1./sqrt(1.-v*v);
//     rho = rhos[j];
//     p = ps[j];
//     Z_pm = Z_pms[j];

//     theta = get_theta(rho, p, Z_pm, gamma_ad, f_heat);

//     y = 3.*theta;
//     x = y*(-.5*y + sqrt(1. + y*y/4.));
//     beta = sqrt(x);
//     betaGamma_sq = beta*beta/(1.-beta*beta);

//     // Pic = Compton power, Pic = G^0 / n_pm (where G^0 is the comoving source term)
//     // Mic = "momentum power", Mic = G^1 / n_pm (where G^1 is the comoving source term)
//     Pic = (4./3.)*sigma_T*I_0s[j]*( 3.*theta_Cs[j] - betaGamma_sq );
//     Mic = sigma_T*I_1s[j];

//     H0_tilde = Z_pm/m_p*(Pic + v*Mic);
//     H1_tilde = Z_pm/m_p*(Mic + v*Pic);

//     G0s[j] += H0_tilde*gamma*rho;
//     G1s[j] += H1_tilde*gamma*rho;
//   }

//   mhd_stopwatch.stop();

// }

void MHDManager::set_ghost_cells_wrapper() {
  set_ghost_cells(vs, rhos, ps, bs, r_bs, t, n_bs, boundary_L, boundary_R, gamma_ad, alpha, ghosts, piston_vgamma_L, piston_vgamma_R);

  // make ghosts also for consts
  double r;
  for(unsigned int j=0; j<2; j++) {
    r = .5*(r_bs[j] + r_bs[j+1]);
    V_ms[j] = get_V_m(vs[j], rhos[j]);
    S_ms[j] = get_S_m(vs[j], rhos[j], ps[j], bs[j], gamma_ad);
    E_ms[j] = get_E_m(vs[j], rhos[j], ps[j], bs[j], gamma_ad);
    M_ms[j] = get_M_m(rhos[j], bs[j], r, alpha);
  }

  for(unsigned int j=vs.size()-3; j<vs.size()-1; j++) {
    r = .5*(r_bs[j] + r_bs[j+1]);
    V_ms[j] = get_V_m(vs[j], rhos[j]);
    S_ms[j] = get_S_m(vs[j], rhos[j], ps[j], bs[j], gamma_ad);
    E_ms[j] = get_E_m(vs[j], rhos[j], ps[j], bs[j], gamma_ad);
    M_ms[j] = get_M_m(rhos[j], bs[j], r, alpha);
  }

}

void MHDManager::compute_radius_vector_wrapper(double R_i) {
  // Computing the radius vector, based on m_bs, V_ms, r_L and alpha
  get_radius_vector(R_i, m_bs, r_bs, V_ms, alpha);
  // compute_radius_vector(m_bs, r_bs, V_ms, r_L, alpha);
}

void MHDManager::reconstruct_prims_wrapper() {
  // Reconstructing the primitive variables from the conserved variables
  double r;

  for(int j=0; j<n_bs-1; j++) {
    r = get_avg_r(r_bs[j+1], r_bs[j], alpha);
    reconstruct_v_rho_p_b(V_ms[j], S_ms[j], E_ms[j], M_ms[j], &vs[j], &rhos[j], &ps[j], &bs[j], r, alpha, gamma_ad);
  }
}

void MHDManager::construct_consts_wrapper() {
  // Computing the conserved variables from the primitive variables
  double r;

  for(int j=0; j<n_bs-1; j++) {
    r = get_avg_r(r_bs[j+1], r_bs[j], alpha);
    V_ms[j] = get_V_m(vs[j], rhos[j]);
    S_ms[j] = get_S_m(vs[j], rhos[j], ps[j], bs[j], gamma_ad);
    E_ms[j] = get_E_m(vs[j], rhos[j], ps[j], bs[j], gamma_ad);
    M_ms[j] = get_M_m(rhos[j], bs[j], r, alpha);
  }
}

double MHDManager::get_time_step_wrapper() {
  // Computes the MHD time step
  mhd_stopwatch.start();
  double dt = get_time_step(vs, rhos, ps, bs, r_bs, m_bs, n_bs, ghosts, alpha, gamma_ad, epsilon);
  mhd_stopwatch.stop();
  return dt;
}

double MHDManager::get_cooling_time_step_wrapper(std::vector<double> &I_0s, std::vector<double> &I_1s, std::vector<double> &theta_Cs, std::vector<double> &Z_pms, double f_heat) {

  // Computes the cooling time step based on the Compton power (Pic)

  double y, theta, x, beta, betaGamma_sq;
  double rho, p, Z_pm, xi;
  double Pic;
  double dt_Pic, dt = 1e60;
  double xi_dt = 1e-2;
  // double mod = 1e-1; // Used when theta/theta_C > 3

  mhd_stopwatch.start();

  // double xi_2 = 1e-2, dt_2 = 1e60;
  for(int j=2; j<n_bs-3; j++) {
    rho = rhos[j];
    p = ps[j];
    Z_pm = Z_pms[j];
    xi = xis[j];
    // theta = get_theta(rho, p, Z_pm, gamma_ad, f_heat);
    theta = get_theta_e_two_temp(rho, p, Z_pm, xi, f_heat);

    y = 3.*theta;
    x = y*(-.5*y + sqrt(1. + y*y/4.));
    beta = sqrt(x);
    betaGamma_sq = beta*beta/(1.-beta*beta);

    // Pic = Compton power, Pic = G^0 / n_pm (where G^0 is the comoving source term)
    Pic = (4./3.)*sigma_T*I_0s[j]*( 3.*theta_Cs[j] - betaGamma_sq );
    if(Pic != 0.) {
      dt_Pic = -theta*m_e/Pic*f_heat;
    } else {
      dt_Pic = 1e60;
    }

    // // dt_Pic = m_e/4./sigma_T/I_0s[j];
    // if(betaGamma_sq/3. > 2*theta_Cs[j]) {
    //   dt_Pic /= mod;
    //   // std::cout << "MOD" << std::endl;
    // }
    if(dt_Pic > 0 && xi_dt*dt_Pic < dt) {
      dt = xi_dt*dt_Pic;
    }

    // // Mic = "momentum power", Mic = G^1 / n_pm (where G^1 is the comoving source term)
    // Mic = sigma_T*I_1s[j];
    // // dt = xi*m_p/Z_pm/sigma_T/I_1;
    // if(Mic != 0.) {
    //   dt_Mic = xi_2*m_p/Z_pm/Mic;
    // } else {
    //   dt_Mic = 1e60;
    // }
    // if((xi_2*dt_Mic)*(xi_2*dt_Mic) < dt_2*dt_2) {
    //   dt_2 = sqrt((xi_2*dt_Mic)*(xi_2*dt_Mic));
    // }
  }

  // std::cout << "dt_2 = " << dt_2 << std::endl;

  mhd_stopwatch.stop();

  return dt;
}

// void MHDManager::set_prims(std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p) {
//   // Fills the prim AND cons grids

//   double r;

//   for(int i=0; i<n_bs-1; i++) {
//     // Prims
//     vs[i] = vs_p[i];
//     rhos[i] = rhos_p[i];
//     ps[i] = ps_p[i];
//     bs[i] = bs_p[i];

//     // Consts
//     r = get_avg_r(r_bs[i+1], r_bs[i], alpha);
//     V_ms[i] = get_V_m(vs[i], rhos[i]);
//     S_ms[i] = get_S_m(vs[i], rhos[i], ps[i], bs[i], gamma_ad);
//     E_ms[i] = get_E_m(vs[i], rhos[i], ps[i], bs[i], gamma_ad);
//     M_ms[i] = get_M_m(rhos[i], bs[i], r, alpha);
//   }

//   set_ghost_cells_wrapper();
// }

void MHDManager::get_grid(std::vector<double> &m_bs_p, std::vector<double> &r_bs_p, std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p) {

  // Get grids
  m_bs_p = m_bs;
  r_bs_p = r_bs;
  vs_p = vs;
  rhos_p = rhos;
  ps_p = ps;
  bs_p = bs;

}

void MHDManager::get_xis(std::vector<double> &xis_p) {
  // NEW: two-temp
  xis_p = xis;
}

int MHDManager::take_mhd_step_wrapper(double dt, int debug_start_iter, int debug_end_iter) {

  if(dt < 0) {std::cout << "ERROR in 'take_mhd_step_wrapper()': dt < 0" << std::endl;}

  int OK = take_mhd_step(dt, m_bs_slice, r_bs_slice,
			 V_ms_slice, S_ms_slice, E_ms_slice, M_ms_slice,
			 vs_slice, rhos_slice, ps_slice, bs_slice,
  			 G0s_slice, G1s_slice, dotPhis_slice,
			 n_bs_slice, alpha, gamma_ad, boundary_L, boundary_R,
			 debug_start_iter, debug_end_iter,
			 wavespeed, iter, RANK);

  return OK;
}
