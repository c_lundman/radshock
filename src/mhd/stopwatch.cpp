//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "stopwatch.h"
#include <chrono>
#include <iostream>

void Stopwatch::start() {
  if(running == 1) {
    std::cout << "Warning: attempted to start an already running stopwatch! (watch_number = " << watch_number << ")" << std::endl;
  }
  running = 1;
  t_1 = std::chrono::high_resolution_clock::now();
}

void Stopwatch::stop() {
  // Get dt, then update t
  if(running == 0) {
    std::cout << "Warning: attempted to stop an already stopped stopwatch! (watch_number = " << watch_number << ")" << std::endl;
  }
  running = 0;
  t_2 = std::chrono::high_resolution_clock::now();
  time_span = t_2 - t_1;
  t += time_span.count();
}

double Stopwatch::get_t() {
  if(running) {
    stop();
    start();
  }
  return t;
}
