//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef PHYSICAL_CONSTANTS_H
#define PHYSICAL_CONSTANTS_H

const double pi = 3.141592653589793;
const double c = 2.9979e10;
const double m_e = 9.1094e-28;
const double m_p = 1.6726231e-24;
const double sigma_T = 6.6524e-25;
const double a = 7.5646e-15;
const double k_B = 1.380658e-16;
const double G = 6.674e-8;

#endif
