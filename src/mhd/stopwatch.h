//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <chrono>
#include <string>

class Stopwatch {

  // A simple class used for measuring (approximate) time (in seconds)

 private:
  std::chrono::high_resolution_clock::time_point t_1, t_2;
  std::chrono::duration<double> time_span;
  double t=0;
  int running=0;
  int watch_number=-1;

 public:
  void start();
  void stop();
  double get_t();

  void set_watch_number(int watch_number_p) {watch_number = watch_number_p;}
  int get_watch_number() {return watch_number;}
};

#endif
