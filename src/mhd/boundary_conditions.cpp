//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "boundary_conditions.h"
#include <vector>
#include <iostream>
#include <math.h>

// This file contains all MHD boundary condition functions

// -----------------------
//  Vacuum (boundary = 0)
// -----------------------

void boundary_vacuum_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs,
		       double t, int n_bs, double gamma_ad, int alpha) {

  int ghosts=2;
  double v_inner, rho_inner, p_inner, b_inner, f=1e-1;
  int j_inner, k;

  j_inner = ghosts;
  v_inner = vs[j_inner];
  rho_inner = rhos[j_inner];
  p_inner = ps[j_inner];
  b_inner = bs[j_inner];
  k = 2;
  for(int j=0; j<ghosts; j++) {
    vs[j] = v_inner;
    rhos[j] = pow(f, k)*rho_inner;
    ps[j] = pow(f, k)*p_inner;
    bs[j] = pow(f, k)*b_inner;
    k -= 1;
  }
}

void boundary_vacuum_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs,
		       double t, int n_bs, double gamma_ad, int alpha) {

  int ghosts=2;
  double v_inner, rho_inner, p_inner, b_inner, f=1e-1;
  int j_inner, k;

  j_inner = n_bs-2-ghosts;
  v_inner = vs[j_inner];
  rho_inner = rhos[j_inner];
  p_inner = ps[j_inner];
  b_inner = bs[j_inner];

  k = 1;
  for(int j=n_bs-1-ghosts; j<n_bs-1; j++) {
    vs[j] = v_inner;
    rhos[j] = pow(f, k)*rho_inner;
    ps[j] = pow(f, k)*p_inner;
    bs[j] = pow(f, k)*b_inner;
    k += 1;
  }
}

// ---------------------
//  Wall (boundary = 1)
// ---------------------

void boundary_wall_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha) {

  int ghosts=2;
  for(int j=0; j<ghosts; ++j) {
    vs[j] = -vs[ghosts + 1-j];
    rhos[j] = rhos[ghosts + 1-j];
    ps[j] = ps[ghosts + 1-j];
    bs[j] = bs[ghosts + 1-j];
  }
}

void boundary_wall_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha) {

  int ghosts=2;
  int k=0;
  for(int j=n_bs-1-ghosts; j<n_bs-1; j++) {
    vs[j] = -vs[n_bs-2-ghosts - k];
    rhos[j] = rhos[n_bs-2-ghosts - k];
    ps[j] = ps[n_bs-2-ghosts - k];
    bs[j] = bs[n_bs-2-ghosts - k];
    k += 1;
  }
}

// ------------------------------
//  Comoving wall (boundary = 2)
// ------------------------------

void boundary_comoving_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha) {

  int ghosts=2;
  for(int j=0; j<ghosts; j++) {
    vs[j] = vs[ghosts+1-j];
    rhos[j] = rhos[ghosts+1-j];
    ps[j] = ps[ghosts+1-j];
    bs[j] = bs[ghosts+1-j];
  }
}

void boundary_comoving_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha) {

  int k = 0;
  int ghosts=2;
  for(int j=n_bs-1-ghosts; j<n_bs-1; j++) {
    vs[j] = vs[n_bs-2-ghosts-k];
    rhos[j] = rhos[n_bs-2-ghosts-k];
    ps[j] = ps[n_bs-2-ghosts-k];
    bs[j] = bs[n_bs-2-ghosts-k];
    k += 1;
  }
}

// -------------------------
//  Periodic (boundary = 3)
// -------------------------

void boundary_periodic_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha) {

  // std::cout << "L: vs.size() = " << vs.size() << std::endl;

  int ghosts=2;
  for(int j=0; j<ghosts; j++) {
    // std::cout << "j = " << j << ", n_bs-1-2*ghosts+j = " << n_bs-1-2*ghosts+j << std::endl;
    vs[j] = vs[n_bs-1-2*ghosts+j];
    rhos[j] = rhos[n_bs-1-2*ghosts+j];
    ps[j] = ps[n_bs-1-2*ghosts+j];
    bs[j] = bs[n_bs-1-2*ghosts+j];
  }
}

void boundary_periodic_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha) {

  // std::cout << "R: vs.size() = " << vs.size() << std::endl;

  int ghosts=2;
  for(int j=n_bs-1-ghosts; j<n_bs-1; j++) {
    // std::cout << "j = " << j << ", -(n_bs-1-2*ghosts)+j = " << -(n_bs-1-2*ghosts)+j << std::endl;
    vs[j] = vs[-(n_bs-1-2*ghosts)+j];
    rhos[j] = rhos[-(n_bs-1-2*ghosts)+j];
    ps[j] = ps[-(n_bs-1-2*ghosts)+j];
    bs[j] = bs[-(n_bs-1-2*ghosts)+j];
  }
}

// ------------------------
//  Pistons (boundary = 4)
// ------------------------

void boundary_piston_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha, double piston_vgamma_L) {

  int ghosts=2;

  // double v_w, vGamma_w = piston_vgamma_L;
  // double v_b, v_b_prime, v_a, v_a_prime;
  // for(int j=0; j<ghosts; j++) {
  //   v_w = vGamma_w/sqrt(vGamma_w*vGamma_w + 1.);
  //   v_b = vs[ghosts+1-j];
  //   v_b_prime = (v_b - v_w)/(1. - v_b*v_w);
  //   v_a_prime = -v_b_prime;
  //   v_a = (v_a_prime + v_w)/(1. + v_a_prime*v_w);
  //   vs[j] = v_a;

  //   // std::cout << "v_w = " << v_w << ", v_a = " << v_a << ", v_b = " << v_b << std::endl;

  //   ps[j] = ps[ghosts+1-j];
  //   rhos[j] = rhos[ghosts+1-j];
  //   bs[j] = bs[ghosts+1-j];
  // }

  // An attempt at using both speed and pressure gradients
  double v_b, vGamma_b, f, vGamma_w = piston_vgamma_L;
  for(int j=0; j<ghosts; j++) {
    v_b = vs[ghosts+1-j];
    vGamma_b = v_b/sqrt(1.-v_b*v_b);

    if(vGamma_w > vGamma_b) {
      f = (vGamma_w - vGamma_b + 1.);
    } else {
      f = 1./(vGamma_b - vGamma_w + 1.);
    }

    // ps[j] = f*ps[ghosts+1-j];
    ps[j] = ps[ghosts+1-j];

    vGamma_b = f*vGamma_w;
    vs[j] = vGamma_b/sqrt(vGamma_b*vGamma_b + 1.);
    rhos[j] = rhos[ghosts+1-j];
    bs[j] = bs[ghosts+1-j];
  }

}

void boundary_piston_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha, double piston_vgamma_R) {

  int ghosts=2;
  double v_w, vGamma_w = piston_vgamma_R;
  double v_b, v_b_prime, v_a, v_a_prime;

  int k = 0;
  for(int j=n_bs-1-ghosts; j<n_bs-1; j++) {

    v_w = vGamma_w/sqrt(vGamma_w*vGamma_w + 1.);
    v_b = vs[n_bs-2-ghosts-k];
    v_b_prime = (v_b - v_w)/(1. - v_b*v_w);
    v_a_prime = -v_b_prime;
    v_a = (v_a_prime + v_w)/(1. + v_a_prime*v_w);
    vs[j] = v_a;

    ps[j] = ps[n_bs-2-ghosts];
    rhos[j] = rhos[n_bs-2-ghosts];
    bs[j] = bs[n_bs-2-ghosts];
  }
}

// ---------------------------
//  For tests (boundary = -1)
// ---------------------------

void boundary_test_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha) {
  // int ghosts=2;
}

void boundary_test_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha) {
  // int ghosts=2;
}
