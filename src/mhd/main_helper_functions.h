//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef MAIN_HELPER_FUNCTIONS_H
#define MAIN_HELPER_FUNCTIONS_H

// This file contains functions for finding parts of the initial MHD grids

#include <vector>
#include <string>

/* // -------------------------------- */
/* //  Finding mass or radius vectors */
/* // -------------------------------- */

/* void get_radius_vector(double R_i, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, double alpha); */
/* void get_mass_vector(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, double alpha); */
std::string get_current_time_string();
void print_current_time();
void print_vector(std::string name, std::vector<double> &Vs, int w);
void print_vector_int(std::string name, std::vector<int> &Vs, int w);
double get_vector_max(std::vector<double> &Vs);
double get_vector_min(std::vector<double> &Vs);

#endif
