//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef AMR_MANAGER_H
#define AMR_MANAGER_H

#include <vector>

double get_xi(double f_1, double f_2);

class MassBin {

  // A class used for storing mass bin information

 private:

  double m_L=0, m_R=0;
  double xi_min=0, xi_max=0;
  int lvl=0, max_lvl=0, have_children=0;
  std::vector<MassBin> mass_bins;

  // Conserved quantities
  double V_m=0, S_m=0, E_m=0, M_m=0, Z_pm=0;

  // Derivatives
  double dVdm=0, dSdm=0, dEdm=0, dMdm=0, dZpmdm=0;

  // The refining function
  double f=0, xi=0;

  void make_children();
  void merge_children();

 public:

  MassBin(double m_L_p, double m_R_p, int lvl_p, int max_lvl_p);

  void set_consts(double V_m_p, double S_m_p, double E_m_p, double M_m_p, double Z_pm_p);
  void get_consts(double &V_m_p, double &S_m_p, double &E_m_p, double &M_m_p, double &Z_pm_p);
  int set_consts_recursive(std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms, std::vector<double> &Z_pms, int counter);
  int get_consts_recursive(std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms, std::vector<double> &Z_pms, int counter);
  int set_refining_information_recursive(std::vector<double> &dVdms, std::vector<double> &dSdms, std::vector<double> &dEdms, std::vector<double> &dMdms, std::vector<double> &dZpmdms,
					 std::vector<double> &fs, std::vector<double> &xis, double xi_min_p, double xi_max_p, int counter);
  void clean_refining_information_recursive();
  double get_m_L() {return m_L;}
  double get_m_R() {return m_R;}
  double get_f() {return f;}
  int get_have_children() {return have_children;}
  int get_m_L_recursive(std::vector<double> &m_Ls, int counter);
  void check_if_cell_should_split_recursive();
  void check_if_cells_should_merge_recursive();
  int count_bins_recursive(int counter);

  void make_children_until_lvl_recursive(int lvl_p);
  void merge_children_until_lvl_recursive(int lvl_p);

};

//

class AMRManager {

  // A class used for real-time refinement of the MHD grid

 private:

  int n_outer_bins=0;
  int n_total_bins=0;
  int max_lvl=0;
  double xi_min=0, xi_max=0;

  std::vector<MassBin> mass_bins;
  std::vector<double> m_bs, V_ms, S_ms, E_ms, M_ms, Z_pms;
  std::vector<double> ms, dVdms, dSdms, dEdms, dMdms, dZpmdms;

  void make_derivatives(std::vector<double> &xs, std::vector<double> &fs, std::vector<double> &dfdxs);
  void fill_mass_and_consts();
  void fill_const_derivatives();
  void find_total_number_of_bins();
  void resize_vectors();

  // For first version
  std::vector<double> xis;
  void fill_xis(std::vector<double> &fs);
  void set_refining_information(std::vector<double> &fs);

  // For second version
  std::vector<int> lvls;
  void fill_lvls(std::vector<double> &ps, std::vector<double> &rhos, std::vector<int> &lvls);

  // For third version
  void fill_lvls_section(double m_1, double m_2, std::vector<int> &lvls);

 public:

  void set_max_lvl(int max_lvl_p) {max_lvl = max_lvl_p;}
  void set_xi_min(double xi_min_p) {xi_min = xi_min_p;}
  void set_xi_max(double xi_max_p) {xi_max = xi_max_p;}
  int get_n_total_bins() {return n_total_bins;}

  void set_initial_mass_grid(std::vector<double> &m_bs_p);
  void set_consts(std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p, std::vector<double> &Z_pms_p);
  void get_mass_grid(std::vector<double> &m_bs_p);
  void get_consts(std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p, std::vector<double> &Z_pms_p);

  void refine_grid(std::vector<double> &ps, std::vector<double> &rhos);
  void refine_grid_section(double m_1, double m_2);

  void resize_other_mhd_vectors(std::vector<double> &r_bs_p, std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p,
				std::vector<double> &G0s_p, std::vector<double> &G1s_p);
  void resize_other_rad_vectors(std::vector<double> &I_0s_p, std::vector<double> &I_1s_p, std::vector<double> &theta_Cs_p);
  void close() {};

};

#endif
