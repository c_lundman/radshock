//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef BOUNDARY_CONDITIONS_H
#define BOUNDARY_CONDITIONS_H

// This file contains all MHD boundary condition functions

#include <vector>

// -----------------------
//  Vacuum (boundary = 0)
// -----------------------

void boundary_vacuum_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);
void boundary_vacuum_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);

// ---------------------
//  Wall (boundary = 1)
// ---------------------

void boundary_wall_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);
void boundary_wall_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);

// ------------------------------
//  Comoving wall (boundary = 2)
// ------------------------------

void boundary_comoving_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);
void boundary_comoving_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);

// -------------------------
//  Periodic (boundary = 3)
// -------------------------

void boundary_periodic_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);
void boundary_periodic_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);

// ------------------------
//  Pistons (boundary = 4)
// ------------------------

void boundary_piston_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs,
		       double t, int n_bs, double gamma_ad, int alpha, double piston_vgamma_L);
void boundary_piston_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs,
		       double t, int n_bs, double gamma_ad, int alpha, double piston_vgamma_R);

// ---------------------------
//  For tests (boundary = -1)
// ---------------------------

void boundary_test_L(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);
void boundary_test_R(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs, double gamma_ad, int alpha);

#endif
