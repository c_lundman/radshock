//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef DBG_H
#define DBG_H

#include <iostream>
#include <string>
#include <vector>

class DBG {

  // A simple class used for printing debugging information

 private:
  std::string name;
  int dbg_number=-1;
  int iteration=0;
  void print_header();

 public:
  DBG();
  DBG(int dbg_number_p);
  DBG(std::string name_p);

  void iter();
  void mess(std::string message);
  void messi(std::string message, int integer);
  void messd(std::string message, double doub);

  void is_vector_positive(std::string message, std::vector<double> vec);
  void print_vector(std::string message, std::vector<double> vec);
  void print_vector_if_above(std::string message, std::vector<double> vec, double x);
};

#endif
