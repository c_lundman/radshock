//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "dbg.h"
// #include <iostream>
// #include <string>
// #include <vector>

DBG::DBG() {dbg_number=0;}
DBG::DBG(int dbg_number_p) {dbg_number=dbg_number_p;}
DBG::DBG(std::string name_p) {name=name_p;}

void DBG::print_header() {
  if(dbg_number >= 0) {
    std::cout << "  [DBG" << dbg_number;
  } else {
    std::cout << "  [" << name;
  }
  std::cout << " | " << iteration++ << "] ";
}

void DBG::iter() {
  print_header();
  std::cout << std::endl;
}

void DBG::mess(std::string message) {
  print_header();
  std::cout << message << std::endl;
}

void DBG::messi(std::string message, int integer) {
  print_header();
  std::cout << message << integer << std::endl;
}

void DBG::messd(std::string message, double doub) {
  print_header();
  std::cout << message << doub << std::endl;
}

void DBG::is_vector_positive(std::string message, std::vector<double> vec) {
  int positive = 1;
  for(unsigned int i=0; i<vec.size(); i++) {
    if(vec[i] < 0.) {
      positive = 0;
    }
  }
  print_header();
  if(positive == 1) {
    std::cout << message << ": no negative values found" << std::endl;
  } else {
    std::cout << message << ": CONTAINS NEGATIVE values!" << std::endl;
  }
}

void DBG::print_vector(std::string message, std::vector<double> vec) {
  for(unsigned int i=0; i<vec.size(); i++) {
    print_header();
    std::cout << message << "[" << i << "] = " << vec[i] << std::endl;
  }
}

void DBG::print_vector_if_above(std::string message, std::vector<double> vec, double x) {
  for(unsigned int i=0; i<vec.size(); i++) {
    if(vec[i] > x) {
      print_header();
      std::cout << message << "[" << i << "] = " << vec[i] << std::endl;
    }
  }
}
