//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "amr_manager.h"
#include <iostream>
#include "main_helper_functions.h"

double get_xi(double f_1, double f_2) {
  double xi = (f_2-f_1)/(f_2+f_1);
  return xi;
}

MassBin::MassBin(double m_L_p, double m_R_p, int lvl_p, int max_lvl_p) : m_L(m_L_p), m_R(m_R_p), lvl(lvl_p), max_lvl(max_lvl_p) {}

void MassBin::make_children() {
  have_children = 1;
  double m_c = .5*(m_R+m_L);

  MassBin mass_bin_L(m_L, m_c, lvl+1, max_lvl);
  MassBin mass_bin_R(m_c, m_R, lvl+1, max_lvl);

  double dm = .25*(m_R-m_L);
  // dm *= .5;
  // dm *= .75;
  // dm *= .25;
  dm = 0.;
  double V_m_L = V_m - dVdm*dm;
  double V_m_R = V_m + dVdm*dm;
  double S_m_L = S_m - dSdm*dm;
  double S_m_R = S_m + dSdm*dm;
  double E_m_L = E_m - dEdm*dm;
  double E_m_R = E_m + dEdm*dm;
  double M_m_L = M_m - dMdm*dm;
  double M_m_R = M_m + dMdm*dm;
  double Z_pm_L = Z_pm - dZpmdm*dm;
  double Z_pm_R = Z_pm + dZpmdm*dm;
  mass_bin_L.set_consts(V_m_L, S_m_L, E_m_L, M_m_L, Z_pm_L);
  mass_bin_R.set_consts(V_m_R, S_m_R, E_m_R, M_m_R, Z_pm_R);

  mass_bins.push_back(mass_bin_L);
  mass_bins.push_back(mass_bin_R);
}

void MassBin::merge_children() {
  have_children = 0;
  double V_m_tot = 0;
  double S_m_tot = 0;
  double E_m_tot = 0;
  double M_m_tot = 0;
  double Z_pm_tot = 0;

  double V_m_tmp, S_m_tmp, E_m_tmp, M_m_tmp, Z_pm_tmp;
  mass_bins[0].get_consts(V_m_tmp, S_m_tmp, E_m_tmp, M_m_tmp, Z_pm_tmp);
  V_m_tot += .5*V_m_tmp;
  S_m_tot += .5*S_m_tmp;
  E_m_tot += .5*E_m_tmp;
  M_m_tot += .5*M_m_tmp;
  Z_pm_tot += .5*Z_pm_tmp;
  mass_bins[1].get_consts(V_m_tmp, S_m_tmp, E_m_tmp, M_m_tmp, Z_pm_tmp);
  V_m_tot += .5*V_m_tmp;
  S_m_tot += .5*S_m_tmp;
  E_m_tot += .5*E_m_tmp;
  M_m_tot += .5*M_m_tmp;
  Z_pm_tot += .5*Z_pm_tmp;

  V_m = V_m_tot;
  S_m = S_m_tot;
  E_m = E_m_tot;
  M_m = M_m_tot;
  Z_pm = Z_pm_tot;

  // Delete children
  mass_bins.pop_back();
  mass_bins.pop_back();
}

void MassBin::make_children_until_lvl_recursive(int lvl_p) {
  if(have_children == 0 && lvl < lvl_p) {
    make_children();
  }
  if(lvl < lvl_p) {
    mass_bins[0].make_children_until_lvl_recursive(lvl_p);
    mass_bins[1].make_children_until_lvl_recursive(lvl_p);
  }
}

void MassBin::merge_children_until_lvl_recursive(int lvl_p) {
  if(have_children == 1) {
    mass_bins[0].merge_children_until_lvl_recursive(lvl_p);
    mass_bins[1].merge_children_until_lvl_recursive(lvl_p);
    if(lvl >= lvl_p) {
      merge_children();
    }
  }
}

//

void MassBin::set_consts(double V_m_p, double S_m_p, double E_m_p, double M_m_p, double Z_pm_p) {
  V_m = V_m_p;
  S_m = S_m_p;
  E_m = E_m_p;
  M_m = M_m_p;
  Z_pm = Z_pm_p;
}

void MassBin::get_consts(double &V_m_p, double &S_m_p, double &E_m_p, double &M_m_p, double &Z_pm_p) {
  V_m_p = V_m;
  S_m_p = S_m;
  E_m_p = E_m;
  M_m_p = M_m;
  Z_pm_p = Z_pm;
}

int MassBin::set_consts_recursive(std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms, std::vector<double> &Z_pms, int counter) {
  if(have_children == 1) {
    counter = mass_bins[0].set_consts_recursive(V_ms, S_ms, E_ms, M_ms, Z_pms, counter);
    counter = mass_bins[1].set_consts_recursive(V_ms, S_ms, E_ms, M_ms, Z_pms, counter);
  } else {
    V_m = V_ms[counter];
    S_m = S_ms[counter];
    E_m = E_ms[counter];
    M_m = M_ms[counter];
    Z_pm = Z_pms[counter];
    counter += 1;
  }
  return counter;
}

int MassBin::get_consts_recursive(std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms, std::vector<double> &Z_pms, int counter) {
  if(have_children == 1) {
    counter = mass_bins[0].get_consts_recursive(V_ms, S_ms, E_ms, M_ms, Z_pms, counter);
    counter = mass_bins[1].get_consts_recursive(V_ms, S_ms, E_ms, M_ms, Z_pms, counter);
  } else {
    V_ms[counter] = V_m;
    S_ms[counter] = S_m;
    E_ms[counter] = E_m;
    M_ms[counter] = M_m;
    Z_pms[counter] = Z_pm;
    counter += 1;
  }
  return counter;
}

int MassBin::set_refining_information_recursive(std::vector<double> &dVdms, std::vector<double> &dSdms, std::vector<double> &dEdms, std::vector<double> &dMdms, std::vector<double> &dZpmdms,
						std::vector<double> &fs, std::vector<double> &xis, double xi_min_p, double xi_max_p, int counter) {

  xi_min = xi_min_p;
  xi_max = xi_max_p;
  if(have_children == 1) {
    counter = mass_bins[0].set_refining_information_recursive(dVdms, dSdms, dEdms, dMdms, dZpmdms, fs, xis, xi_min_p, xi_max_p, counter);
    counter = mass_bins[1].set_refining_information_recursive(dVdms, dSdms, dEdms, dMdms, dZpmdms, fs, xis, xi_min_p, xi_max_p, counter);
  } else {
    dVdm = dVdms[counter];
    dSdm = dSdms[counter];
    dEdm = dEdms[counter];
    dMdm = dMdms[counter];
    dZpmdm = dZpmdms[counter];
    f = fs[counter];
    xi = xis[counter];
    counter += 1;
  }
  return counter;
}

void MassBin::clean_refining_information_recursive() {
  f = 0;
  xi = 0;
  xi_min = 0;
  xi_max = 0;
  dVdm = 0;
  dSdm = 0;
  dEdm = 0;
  dMdm = 0;
  dZpmdm = 0;
  if(have_children == 1) {
    mass_bins[0].clean_refining_information_recursive();
    mass_bins[1].clean_refining_information_recursive();
  }
}

int MassBin::get_m_L_recursive(std::vector<double> &m_Ls, int counter) {
  if(have_children == 1) {
    counter = mass_bins[0].get_m_L_recursive(m_Ls, counter);
    counter = mass_bins[1].get_m_L_recursive(m_Ls, counter);
  } else {
    m_Ls[counter] = m_L;
    counter += 1;
  }
  return counter;
}

void MassBin::check_if_cell_should_split_recursive() {
  if(have_children == 1) {
    mass_bins[0].check_if_cell_should_split_recursive();
    mass_bins[1].check_if_cell_should_split_recursive();
  } else {
    if(xi*xi > xi_max*xi_max && lvl != max_lvl) {
      make_children();
      // std::cout << "Make children!" << std::endl;
    } else if(xi*xi > xi_max*xi_max) {
      // std::cout << "attempted to make children, but max_lvl!" << std::endl;
    }
  }
}

void MassBin::check_if_cells_should_merge_recursive() {
  if(have_children == 1) {
    if(mass_bins[0].get_have_children() == 1 || mass_bins[1].get_have_children()) {
      mass_bins[0].check_if_cells_should_merge_recursive();
      mass_bins[1].check_if_cells_should_merge_recursive();
    } else {
      double f_1 = mass_bins[0].get_f();
      double f_2 = mass_bins[1].get_f();
      if(f_1 == 0. && f_2 == 0.) {
      } else {
	double xi_p = get_xi(f_1, f_2);
	if(xi_p*xi_p < xi_min*xi_min) {
	  // std::cout << "Merge children!" << std::endl;
	  merge_children();
	}
      }
    }
  }
}

int MassBin::count_bins_recursive(int counter) {
  if(have_children == 1) {
    counter = mass_bins[0].count_bins_recursive(counter);
    counter = mass_bins[1].count_bins_recursive(counter);
  } else {
    counter += 1;
  }
  return counter;
}














//

void AMRManager::set_initial_mass_grid(std::vector<double> &m_bs_p) {

  // std::cout << "set_initial_mass_grid" << std::endl;

  // Sets the initial grid. This grid is special, in that the resolution can never be lower than this

  double m_L, m_R;
  n_outer_bins = m_bs_p.size()-1;
  n_total_bins = n_outer_bins;
  mass_bins.reserve(n_outer_bins);
  lvls.reserve(n_outer_bins);
  for(int j=0; j<n_outer_bins; j++) {
    m_L = m_bs_p[j];
    m_R = m_bs_p[j+1];
    mass_bins.push_back(MassBin(m_L, m_R, 0, max_lvl));
  }
  resize_vectors();
}

void AMRManager::get_mass_grid(std::vector<double> &m_bs_p) {

  // std::cout << "get_mass_grid" << std::endl;

  // Fill mass grid

  m_bs_p.resize(n_total_bins+1);
  int counter = 0;
  for(int j=0; j<n_outer_bins; j++) {
    counter = mass_bins[j].get_m_L_recursive(m_bs_p, counter);
  }
  m_bs_p[counter] = mass_bins[n_outer_bins-1].get_m_R();
}

void AMRManager::set_consts(std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p, std::vector<double> &Z_pms_p) {

  // std::cout << "set_consts" << std::endl;

  // Set consts grid

  int n_p = V_ms_p.size();
  if(n_p != n_total_bins) {
    V_ms_p.resize(n_total_bins);
    S_ms_p.resize(n_total_bins);
    E_ms_p.resize(n_total_bins);
    M_ms_p.resize(n_total_bins);
    Z_pms_p.resize(n_total_bins);
  }
  int counter = 0;
  for(int j=0; j<n_outer_bins; j++) {
    counter = mass_bins[j].set_consts_recursive(V_ms_p, S_ms_p, E_ms_p, M_ms_p, Z_pms_p, counter);
  }
}

void AMRManager::get_consts(std::vector<double> &V_ms_p, std::vector<double> &S_ms_p, std::vector<double> &E_ms_p, std::vector<double> &M_ms_p, std::vector<double> &Z_pms_p) {

  // std::cout << "get_consts" << std::endl;

  // Get consts grid

  int n_p = V_ms_p.size();
  if(n_p != n_total_bins) {
    V_ms_p.resize(n_total_bins);
    S_ms_p.resize(n_total_bins);
    E_ms_p.resize(n_total_bins);
    M_ms_p.resize(n_total_bins);
    Z_pms_p.resize(n_total_bins);
  }
  int counter = 0;
  for(int j=0; j<n_outer_bins; j++) {
    counter = mass_bins[j].get_consts_recursive(V_ms_p, S_ms_p, E_ms_p, M_ms_p, Z_pms_p, counter);
  }
}

void AMRManager::make_derivatives(std::vector<double> &xs, std::vector<double> &fs, std::vector<double> &dfdxs) {

  // std::cout << "make_derivatives" << std::endl;

  double dx, df;
  int n = fs.size();
  for(int j=0; j<n; j++) {
    if(j == 0) {
      dx = xs[1]-xs[0];
      df = fs[1]-fs[0];
    } else if(j == n-1) {
      dx = xs[n-1]-xs[n-2];
      df = fs[n-1]-fs[n-2];
    } else {
      dx = .5*(xs[j+1]-xs[j-1]);
      df = .5*(fs[j+1]-fs[j-1]);
    }
    dfdxs[j] = df/dx;
    // std::cout << "dfdx = " << dfdxs[j] << ", f = " << fs[j] << ", x = " << xs[j] << std::endl;
    // std::cout << "df = " << df << ", dx = " << dx << std::endl;
    // std::cout << "xs[j] = " << xs[j] << std::endl;
  }
}

void AMRManager::fill_xis(std::vector<double> &fs) {

  double f_1, f_2;
  int n = fs.size();
  for(int j=0; j<n; j++) {
    if(j == 0) {
      f_1 = fs[0];
      f_2 = fs[1];
    } else if(j == n-1) {
      f_1 = fs[n-2];
      f_2 = fs[n-1];
    } else {
      f_1 = fs[j-1];
      f_2 = fs[j+1];
    }
    xis[j] = get_xi(f_1, f_2);
  }
}

// void AMRManager::fill_lvls(std::vector<double> &fs, std::vector<int> &lvls) {
//   // First, need to locate mass bin of maximum refinement...
//   int i_max = -1;
//   // double xi_trigger=0.125;
//   double m_trigger;

//   // Locate shock
//   // fill_xis(fs);
//   double f=0;
//   for(int i=2; i<n_total_bins-2; i++) {
//     // if(xis[i]*xis[i] > xi_trigger*xi_trigger) {
//     if(fs[i] > f) {
//       f = fs[i];
//       i_max = i;
//       m_trigger = m_bs[i];
//     }
//   }

//   // Find corresponding mass bin
//   int j_max = -1;
//   if(i_max != -1) {
//     for(int j=2; j<n_outer_bins-2; j++) {
//       if(m_trigger > mass_bins[j].get_m_L() && m_trigger <= mass_bins[j].get_m_R()) {
// 	j_max = j;
// 	// j_max = j+1;
//       }
//     }
//   }

//   int w = 3;
//   for(int j=2; j<n_outer_bins-2; j++) {
//     if(j_max != -1) {
//       if(j < j_max-(w-1)/2) {
// 	// lvls[j] = max_lvl - (j_max-j-(w-1)/2);
// 	// lvls[j] = max_lvl - 4*(j_max-j-(w-1)/2);
// 	lvls[j] = 0;
//       } else if(j > j_max+(w-1)/2) {
// 	lvls[j] = max_lvl - (j-j_max-(w-1)/2);
// 	// lvls[j] = max_lvl - 4*(j-j_max-(w-1)/2);
// 	// lvls[j] = 0;
//       } else {
// 	lvls[j] = max_lvl;
//       }

//       if(lvls[j] < 0) {
// 	lvls[j] = 0;
//       }
//     } else {
//       lvls[j] = 0;
//     }
//   }
// }

void AMRManager::fill_lvls(std::vector<double> &ps, std::vector<double> &rhos, std::vector<int> &lvls) {

  // First, need to locate mass bins of maximum refinement
  double f=3;

  // Find sub-shocks moving to the right
  std::vector<double> m_trigger_rights;
  std::vector<double> m_trigger_lefts;
  for(int i=4; i<n_total_bins-4; i++) {
    if(ps[i] > f*ps[i+2] || ps[i] > f*ps[i-2]) {
      if(rhos[i-2] > rhos[i+2]) {
	m_trigger_rights.push_back(m_bs[i]);
      } else {
	m_trigger_lefts.push_back(m_bs[i]);
      }
    }
  }

  // // Find sub-shocks moving to the left
  // std::vector<double> m_trigger_lefts;
  // for(int i=4; i<n_total_bins-4; i++) {
  //   if(ps[i] > f*ps[i-2]) {
  //     m_trigger_lefts.push_back(m_bs[i]);
  //   }
  // }

  // Find corresponding right mass bins
  std::vector<int> j_max_rights;
  for(unsigned int k=0; k<m_trigger_rights.size(); k++) {
    for(int j=2; j<n_outer_bins-2; j++) {
      if(m_trigger_rights[k] > mass_bins[j].get_m_L() && m_trigger_rights[k] <= mass_bins[j].get_m_R()) {
  	j_max_rights.push_back(j);
      }
    }
  }

  // Find corresponding left mass bins
  std::vector<int> j_max_lefts;
  for(unsigned int k=0; k<m_trigger_lefts.size(); k++) {
    for(int j=2; j<n_outer_bins-2; j++) {
      if(m_trigger_lefts[k] > mass_bins[j].get_m_L() && m_trigger_lefts[k] <= mass_bins[j].get_m_R()) {
  	j_max_lefts.push_back(j);
      }
    }
  }

  // Clean the lvls vector
  lvls.resize(n_outer_bins);
  std::fill(lvls.begin(), lvls.end(), 0);

  // Set levels for shocks propagating to the right
  int j_max, lvl, w = 1;
  for(unsigned int k=0; k<j_max_rights.size(); k++) {
    j_max = j_max_rights[k];
    for(int j=2; j<n_outer_bins-2; j++) {
      if(j < j_max-(w-1)/2) {
  	lvl = 0;
      } else if(j > j_max+(w-1)/2) {
  	lvl = max_lvl - (j-j_max-(w-1)/2);
      } else {
  	lvl = max_lvl;
      }
      if(lvls[j] < lvl) {
      	lvls[j] = lvl;
      }
    }
  }

  // Set levels for shocks propagating to the left
  for(unsigned int k=0; k<j_max_lefts.size(); k++) {
    j_max = j_max_lefts[k];
    for(int j=2; j<n_outer_bins-2; j++) {
      if(j < j_max-(w-1)/2) {
  	lvl = max_lvl - (j_max-j-(w-1)/2);
      } else if(j > j_max+(w-1)/2) {
  	lvl = 0;
      } else {
  	lvl = max_lvl;
      }
      if(lvls[j] < lvl) {
      	lvls[j] = lvl;
      }
    }
  }

}

void AMRManager::fill_mass_and_consts() {

  get_mass_grid(m_bs);
  get_consts(V_ms, S_ms, E_ms, M_ms, Z_pms);
}

void AMRManager::fill_const_derivatives() {

  // std::cout << "fill_const_derivatives" << std::endl;

  for(unsigned int i=0; i<m_bs.size()-1; i++) {
    ms[i] = .5*(m_bs[i+1]+m_bs[i]);
  }
  make_derivatives(ms, V_ms, dVdms);
  make_derivatives(ms, S_ms, dSdms);
  make_derivatives(ms, E_ms, dEdms);
  make_derivatives(ms, M_ms, dMdms);
  make_derivatives(ms, Z_pms, dZpmdms);
}

void AMRManager::set_refining_information(std::vector<double> &fs) {

  int counter = 0;
  for(int j=0; j<n_outer_bins; j++) {
    counter = mass_bins[j].set_refining_information_recursive(dVdms, dSdms, dEdms, dMdms, dZpmdms, fs, xis, xi_min, xi_max, counter);
  }
}

void AMRManager::find_total_number_of_bins() {
  int counter = 0;
  for(int j=0; j<n_outer_bins; j++) {
    counter = mass_bins[j].count_bins_recursive(counter);
  }
  n_total_bins = counter;
}

void AMRManager::resize_vectors() {
  m_bs.resize(n_total_bins+1);
  V_ms.resize(n_total_bins);
  S_ms.resize(n_total_bins);
  E_ms.resize(n_total_bins);
  M_ms.resize(n_total_bins);
  Z_pms.resize(n_total_bins);

  ms.resize(n_total_bins);
  dVdms.resize(n_total_bins);
  dSdms.resize(n_total_bins);
  dEdms.resize(n_total_bins);
  dMdms.resize(n_total_bins);
  dZpmdms.resize(n_total_bins);
  xis.resize(n_total_bins);
}

// void AMRManager::refine_grid(std::vector<double> &fs) {
//   // std::cout << "refine_grid" << std::endl;

//   // Needs to have filled the mass and const vectors first
//   fill_mass_and_consts();

//   // Computing derivatives
//   fill_const_derivatives();

//   // Computing xis
//   fill_xis(fs);

//   // Pass refining information to all children
//   set_refining_information(fs);

//   // Split cells
//   for(int j=2; j<n_outer_bins-3; j++) {
//     mass_bins[j].check_if_cell_should_split_recursive();
//   }

//   // Merge cells
//   for(int j=2; j<n_outer_bins-3; j++) {
//     mass_bins[j].check_if_cells_should_merge_recursive();
//   }

//   // Clean info
//   for(int j=0; j<n_outer_bins-1; j++) {
//     mass_bins[j].clean_refining_information_recursive();
//   }

//   // Update the current number of bins
//   int n_total_bins_old = n_total_bins;
//   find_total_number_of_bins();

//   // Resize all vectors
//   if(n_total_bins_old != n_total_bins) {
//     resize_vectors();
//   }
// }

void AMRManager::refine_grid(std::vector<double> &ps, std::vector<double> &rhos) {

  // Needs to have filled the mass and const vectors first
  fill_mass_and_consts();

  // Computing derivatives
  fill_const_derivatives();

  // Find the "max lvl function"
  fill_lvls(ps, rhos, lvls);

  // Merge or make children
  for(int j=2; j<n_outer_bins-2; j++) {
    mass_bins[j].merge_children_until_lvl_recursive(lvls[j]);
    mass_bins[j].make_children_until_lvl_recursive(lvls[j]);
  }

  // Update the current number of bins
  int n_total_bins_old = n_total_bins;
  find_total_number_of_bins();

  // Resize all vectors
  if(n_total_bins_old != n_total_bins) {
    resize_vectors();
  }
}

void AMRManager::fill_lvls_section(double m_1, double m_2, std::vector<int> &lvls) {
  // Sets lvl = max_lvl for all bins with m_1 < m < m_2

  // Clean the lvls vector
  lvls.resize(n_outer_bins);
  std::fill(lvls.begin(), lvls.end(), 0);

  // Filling lvls
  double m_L, m_R, m;
  for(int j=0; j<n_outer_bins; j++) {
    m_L = mass_bins[j].get_m_L();
    m_R = mass_bins[j].get_m_R();
    m = .5*(m_L+m_R);
    if(m_1 < m && m < m_2) {
      lvls[j] = max_lvl;
    } else {
      lvls[j] = 0;
    }
  }
}

void AMRManager::refine_grid_section(double m_1, double m_2) {

  // Needs to have filled the mass and const vectors first
  fill_mass_and_consts();

  // Computing derivatives
  fill_const_derivatives();

  // Find the "max lvl function"
  fill_lvls_section(m_1, m_2, lvls);

  // Merge or make children
  for(int j=2; j<n_outer_bins-2; j++) {
    mass_bins[j].merge_children_until_lvl_recursive(lvls[j]);
    mass_bins[j].make_children_until_lvl_recursive(lvls[j]);
  }

  // Update the current number of bins
  int n_total_bins_old = n_total_bins;
  find_total_number_of_bins();

  // Resize all vectors
  if(n_total_bins_old != n_total_bins) {
    resize_vectors();
  }
}

void AMRManager::resize_other_mhd_vectors(std::vector<double> &r_bs_p, std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p,
					  std::vector<double> &G0s_p, std::vector<double> &G1s_p) {

  r_bs_p.resize(n_total_bins+1);
  vs_p.resize(n_total_bins);
  rhos_p.resize(n_total_bins);
  ps_p.resize(n_total_bins);
  bs_p.resize(n_total_bins);
  G0s_p.resize(n_total_bins);
  G1s_p.resize(n_total_bins);
}

void AMRManager::resize_other_rad_vectors(std::vector<double> &I_0s_p, std::vector<double> &I_1s_p, std::vector<double> &theta_Cs_p) {

  I_0s_p.resize(n_total_bins);
  I_1s_p.resize(n_total_bins);
  theta_Cs_p.resize(n_total_bins);
}
