//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "main_helper_functions.h"
#include <vector>
#include <math.h>

#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string
#include <iostream>

// // --------------------------------
// //  Finding mass or radius vectors
// // --------------------------------

// void get_radius_vector(double R_i, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, double alpha) {
//   // Assumes that mass boundary vector and primitives are known, and computes the radius boundary vector
//   double Dm, r_1, r_2, gamma, rho;
//   r_bs[2] = R_i;
//   for(unsigned int i=3; i<r_bs.size(); i++) {
//     Dm = m_bs[i]-m_bs[i-1];
//     r_1 = r_bs[i-1];
//     gamma = 1./sqrt(1.-vs[i-1]*vs[i-1]);
//     rho = rhos[i-1];
//     r_bs[i] = pow(pow(r_1, alpha+1) + (alpha+1)*Dm/gamma/rho, 1./(alpha+1));
//   }
//   for(int i=1; i>=0; i--) {
//     Dm = m_bs[i+1]-m_bs[i];
//     r_2 = r_bs[i+1];
//     gamma = 1./sqrt(1.-vs[i]*vs[i]);
//     rho = rhos[i];
//     r_bs[i] = pow(pow(r_2, alpha+1) - (alpha+1)*Dm/gamma/rho, 1./(alpha+1));
//   }
// }

// void get_mass_vector(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, double alpha) {
//   // Assumes that radius boundary vector and primitives are known, and computes the mass boundary vector
//   double r_1, r_2, gamma, rho;
//   m_bs[2] = 0;
//   for(unsigned int i=3; i<m_bs.size(); i++) {
//     r_1 = r_bs[i-1];
//     r_2 = r_bs[i];
//     gamma = 1./sqrt(1.-vs[i-1]*vs[i-1]);
//     rho = rhos[i-1];
//     m_bs[i] = m_bs[i-1] + gamma*rho/(alpha+1)*(pow(r_2, alpha+1) - pow(r_1, alpha+1));
//   }
//   for(int i=1; i>=0; i--) {
//     r_1 = r_bs[i];
//     r_2 = r_bs[i+1];
//     gamma = 1./sqrt(1.-vs[i]*vs[i]);
//     rho = rhos[i];
//     m_bs[i] = m_bs[i+1] - gamma*rho/(alpha+1)*(pow(r_2, alpha+1) - pow(r_1, alpha+1));
//   }
// }

std::string get_current_time_string() {

  auto now = std::chrono::system_clock::now();
  auto in_time_t = std::chrono::system_clock::to_time_t(now);

  std::stringstream ss;
  // ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
  ss << std::put_time(std::localtime(&in_time_t), "%X");
  return "[" + ss.str() + "] ";
}

void print_current_time() {

  auto now = std::chrono::system_clock::now();
  auto in_time_t = std::chrono::system_clock::to_time_t(now);

  std::stringstream ss;
  // ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X");
  ss << std::put_time(std::localtime(&in_time_t), "%X");
  std::cout << "[" << ss.str() << "] ";
  std::cout.flush();
}

void print_vector(std::string name, std::vector<double> &Vs, int w) {
  std::string whitespace = "";
  for(unsigned int i=0; i<name.length()+2; i++) {
    whitespace += " ";
  }
  std::cout << name << " [";
  for(unsigned int i=0; i<Vs.size()-1; i++) {
    std::cout << Vs[i] << ", ";
    if(i != 0 && i%w == 0) {std::cout << std::endl << whitespace;}
  }
  std::cout << Vs[Vs.size()-1] << "]" << std::endl;
}

void print_vector_int(std::string name, std::vector<int> &Vs, int w) {
  std::string whitespace = "";
  for(unsigned int i=0; i<name.length()+2; i++) {
    whitespace += " ";
  }
  std::cout << name << " [";
  for(unsigned int i=0; i<Vs.size()-1; i++) {
    std::cout << Vs[i] << ", ";
    if(i != 0 && i%w == 0) {std::cout << std::endl << whitespace;}
  }
  std::cout << Vs[Vs.size()-1] << "]" << std::endl;
}

double get_vector_max(std::vector<double> &Vs) {
  double max_value = -1e60;
  for(unsigned int j=0; j<Vs.size(); j++) {
    if(Vs[j] > max_value) {max_value = Vs[j];}
  }
  return max_value;
}

double get_vector_min(std::vector<double> &Vs) {
  double min_value = +1e60;
  for(unsigned int j=0; j<Vs.size(); j++) {
    if(Vs[j] < min_value) {min_value = Vs[j];}
  }
  return min_value;
}
