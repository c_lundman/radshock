//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "parameter_parser.h"
#include <string>
#include <fstream>
#include <iostream>

int ParameterParser::read_parameter_file(std::string filename) {

  set_param_file_name(filename);

  param_file.open(filename, std::fstream::in);
  if(!param_file.is_open()) {
    return 0; // File not found
  }

  std::string tmp;
  std::string tmp0, tmp1, tmp2, tmp3; // For filling piston boundary parameters
  while(!param_file.eof()) {

    // Load next word
    param_file >> tmp;

    // if(tmp == "sim_name") {
    //   param_file >> tmp;
    //   param_file >> tmp;
    //   set_sim_name(tmp);

    if(tmp == "mhd_input_state_file") {
      param_file >> tmp;
      param_file >> tmp;
      set_mhd_input_state_file(tmp);

    } else if(tmp == "rad_input_state_file") {
      param_file >> tmp;
      param_file >> tmp;
      set_rad_input_state_file(tmp);

    } else if(tmp == "mhd_output_state_file") {
      param_file >> tmp;
      param_file >> tmp;
      set_mhd_output_state_file(tmp);

    } else if(tmp == "rad_output_state_file") {
      param_file >> tmp;
      param_file >> tmp;
      set_rad_output_state_file(tmp);

    } else if(tmp == "mhd_output_snapshot_file") {
      param_file >> tmp;
      param_file >> tmp;
      set_mhd_output_snapshot_file(tmp);

    } else if(tmp == "debug") {
      param_file >> tmp;
      param_file >> tmp;
      set_debug(std::stoi(tmp));

    } else if(tmp == "debug_start_mhd_iter") {
      param_file >> tmp;
      param_file >> tmp;
      set_debug_start_mhd_iter(std::stoi(tmp));

    } else if(tmp == "debug_end_mhd_iter") {
      param_file >> tmp;
      param_file >> tmp;
      set_debug_end_mhd_iter(std::stoi(tmp));

    } else if(tmp == "n_ts") {
      param_file >> tmp;
      param_file >> tmp;
      set_n_ts(std::stoi(tmp));

    } else if(tmp == "t_i") {
      param_file >> tmp;
      param_file >> tmp;
      set_t_i(std::stod(tmp));

    } else if(tmp == "t_f") {
      param_file >> tmp;
      param_file >> tmp;
      set_t_f(std::stod(tmp));

    } else if(tmp == "log_spaced_time") {
      param_file >> tmp;
      param_file >> tmp;
      set_log_spaced_time(std::stoi(tmp));

    } else if(tmp == "n_ss") {
      param_file >> tmp;
      param_file >> tmp;
      set_n_ss(std::stoi(tmp));

    } else if(tmp == "save_every_nth_mhd_iter") {
      param_file >> tmp;
      param_file >> tmp;
      set_save_every_nth_mhd_iter(std::stoi(tmp));

    } else if(tmp == "save_every_nth_rad_iter") {
      param_file >> tmp;
      param_file >> tmp;
      set_save_every_nth_rad_iter(std::stoi(tmp));

    } else if(tmp == "xi_rad") {
      param_file >> tmp;
      param_file >> tmp;
      set_xi_rad(std::stod(tmp));

    } else if(tmp == "xi_mhd") {
      param_file >> tmp;
      param_file >> tmp;
      set_xi_mhd(std::stod(tmp));

    } else if(tmp == "xi_mhd") {
      param_file >> tmp;
      param_file >> tmp;
      set_xi_mhd(std::stod(tmp));

    } else if(tmp == "seed") {
      param_file >> tmp;
      param_file >> tmp;
      set_seed(std::stoi(tmp));

    } else if(tmp == "load_photon_multiplier") {
      param_file >> tmp;
      param_file >> tmp;
      set_load_photon_multiplier(std::stoi(tmp));

    } else if(tmp == "save_photon_divider") {
      param_file >> tmp;
      param_file >> tmp;
      set_save_photon_divider(std::stoi(tmp));

    } else if(tmp == "m_grav") {
      param_file >> tmp;
      param_file >> tmp;
      set_m_grav(std::stod(tmp));

    } else if(tmp == "tau_smoothing") {
      param_file >> tmp;
      param_file >> tmp;
      set_tau_smoothing(std::stod(tmp));

    } else if(tmp == "use_pairs") {
      param_file >> tmp;
      param_file >> tmp;
      set_use_pairs(std::stoi(tmp));

    } else if(tmp == "rad_source_terms") {
      param_file >> tmp;
      param_file >> tmp;
      set_rad_source_terms(std::stoi(tmp));

    } else if(tmp == "ignore_crash_warning") {
      param_file >> tmp;
      param_file >> tmp;
      set_ignore_crash_warning(std::stoi(tmp));

    } else if(tmp == "boundary_L") {
      param_file >> tmp;
      param_file >> tmp;
      set_boundary_L(std::stoi(tmp));
      if(get_boundary_L() == 4) {
	// piston boundary condition, expects parameters
	param_file >> tmp;
	param_file >> tmp0;
	param_file >> tmp1;
	param_file >> tmp2;
	param_file >> tmp3;
	set_boundary_L_params(std::stod(tmp0), std::stod(tmp1), std::stod(tmp2), std::stod(tmp3));
      }

    } else if(tmp == "boundary_R") {
      param_file >> tmp;
      param_file >> tmp;
      set_boundary_R(std::stoi(tmp));
      if(get_boundary_R() == 4) {
	// piston boundary condition, expects parameters
	param_file >> tmp;
	param_file >> tmp0;
	param_file >> tmp1;
	param_file >> tmp2;
	param_file >> tmp3;
	set_boundary_R_params(std::stod(tmp0), std::stod(tmp1), std::stod(tmp2), std::stod(tmp3));
      }

    } else if(tmp == "two_temp_plasma") {
      param_file >> tmp;
      param_file >> tmp;
      set_two_temp_plasma(std::stoi(tmp));

    } else if(tmp == "uniform_spec_radius_bins") {
      param_file >> tmp;
      param_file >> tmp;
      set_uniform_spec_radius_bins(std::stoi(tmp));

    } else if(tmp == "tau_merge_limit") {
      param_file >> tmp;
      param_file >> tmp;
      set_tau_merge_limit(std::stod(tmp));

    }

  }

  param_file.close();
  return 1;
}

void ParameterParser::print_parameters() {

  std::cout << "Parameters loaded from file '" << param_file_name << "':" << std::endl;
  std::cout << "  mhd_input_state_file = " << mhd_input_state_file << std::endl;
  std::cout << "  rad_input_state_file = " << rad_input_state_file << std::endl;
  std::cout << "  mhd_output_state_file = " << mhd_output_state_file << std::endl;
  std::cout << "  rad_output_state_file = " << rad_output_state_file << std::endl;
  std::cout << "  mhd_output_snapshot_file = " << mhd_output_snapshot_file << std::endl;
  std::cout << "  debug = " << debug << std::endl;
  std::cout << "  debug_start_mhd_iter = " << debug_start_mhd_iter << std::endl;
  std::cout << "  debug_end_mhd_iter = " << debug_end_mhd_iter << std::endl;
  std::cout << "  n_ts = " << n_ts << std::endl;
  std::cout << "  t_i = " << t_i << std::endl;
  std::cout << "  t_f = " << t_f << std::endl;
  std::cout << "  log_spaced_time = " << log_spaced_time << std::endl;
  std::cout << "  n_ss = " << n_ss << std::endl;
  std::cout << "  save_every_nth_mhd_iter = " << save_every_nth_mhd_iter << std::endl;
  std::cout << "  save_every_nth_rad_iter = " << save_every_nth_rad_iter << std::endl;
  std::cout << "  xi_mhd = " << xi_mhd << std::endl;
  std::cout << "  xi_rad = " << xi_rad << std::endl;
  std::cout << "  seed = " << seed << std::endl;
  std::cout << "  load_photon_multiplier = " << load_photon_multiplier << std::endl;
  std::cout << "  save_photon_divider = " << save_photon_divider << std::endl;
  std::cout << "  m_grav = " << m_grav << std::endl;
  std::cout << "  tau_smoothing = " << tau_smoothing << std::endl;
  std::cout << "  use_pairs = " << use_pairs << std::endl;
  std::cout << "  rad_source_terms = " << rad_source_terms << std::endl;
  std::cout << "  ignore_crash_warning = " << ignore_crash_warning << std::endl;
  std::cout << "  boundary_L = " << boundary_L << std::endl;
  std::cout << "  boundary_R = " << boundary_R << std::endl;
  std::cout << "  two_temp_plasma = " << two_temp_plasma << std::endl;
  std::cout << "  uniform_spec_radius_bins = " << uniform_spec_radius_bins << std::endl;
  std::cout << "  tau_merge_limit = " << tau_merge_limit << std::endl;
  std::cout << std::endl;

}
