//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "riemann_hllc.h"
#include <math.h>
#include <iostream>

// -----------------------------------------------
// An HLLC Riemann solver, based on
// 
// Physical-constraints-preserving Lagrangian
// finite volume schemes for one- and two-
// dimensional special relativistic hydrodynamics
// 
// Ling, Duan & Tang (2019)
// 
// https://arxiv.org/pdf/1901.10625.pdf
// -----------------------------------------------

double findSoundSpeed(const double rho, const double p, const double b, const double gamma_ad) {

  double h, h_star, c_s, v_A, c_s_star;
  h = 1. + gamma_ad*p/(gamma_ad-1.)/rho;
  h_star = 1. + gamma_ad*p/(gamma_ad-1.)/rho + b*b/rho;
  c_s = sqrt(gamma_ad*p/h/rho);
  v_A = sqrt(b*b/h_star/rho);
  c_s_star = sqrt((1.-v_A*v_A)*c_s*c_s + v_A*v_A);
  return(c_s_star);

}

void findConstFromPrim(const double v, const double rho, const double p, const double b,
		       double *D, double *E, double *S, const double gamma_ad) {

  double gamma;
  gamma = 1./sqrt(1.-v*v);
  *D = 1./gamma/rho;
  *E = (*D)*(gamma*gamma*(rho + gamma_ad/(gamma_ad-1.)*p + b*b) - (p + .5*b*b));
  *S = (*D)*gamma*gamma*(rho + gamma_ad/(gamma_ad-1.)*p + b*b)*v;
}

void Riemann_problem(double p_L, double rho_L, double v_L, double b_L,
		     double p_R, double rho_R, double v_R, double b_R,
		     double *pstar, double *vstar,
		     const double gamma_ad, const int wavespeed,
		     const int iter, const int i) {

  // Remove 'wavespeed' from this function!

  // Finding the speeds s^\pm (s_m, s_p)
  double c_s_L, c_s_R; // Sound speeds
  c_s_L = findSoundSpeed(rho_L, p_L, b_L, gamma_ad);
  c_s_R = findSoundSpeed(rho_R, p_R, b_R, gamma_ad);
  double s_m_1 = (v_L - c_s_L)/(1. - v_L*c_s_L);
  double s_m_2 = (v_R - c_s_R)/(1. - v_R*c_s_R);
  double s_p_1 = (v_L + c_s_L)/(1. + v_L*c_s_L);
  double s_p_2 = (v_R + c_s_R)/(1. + v_R*c_s_R);
  double s_m, s_p;
  if(s_m_1 < s_m_2) {
    s_m = s_m_1;
  } else {
    s_m = s_m_2;
  }
  if(s_p_1 > s_p_2) {
    s_p = s_p_1;
  } else {
    s_p = s_p_2;
  }

  // Finding initial const states
  double D_L, E_L, S_L;
  double D_R, E_R, S_R;
  findConstFromPrim(v_L, rho_L, p_L, b_L, &D_L, &E_L, &S_L, gamma_ad);
  findConstFromPrim(v_R, rho_R, p_R, b_R, &D_R, &E_R, &S_R, gamma_ad);

  // Finding "his" E (e), m, which are energy and momentum per volume, not mass
  double m_L = S_L/D_L;
  double m_R = S_R/D_R;
  double e_L = E_L/D_L;
  double e_R = E_R/D_R;

  // Computing all constants, and finally vstar, pstar
  double A_m = s_m*e_L - m_L;
  double A_p = s_p*e_R - m_R;

  double B_m = m_L*(s_m - v_L) - p_L;
  double B_p = m_R*(s_p - v_R) - p_R;

  double C_0 = B_p-B_m;
  double C_1 = A_m + s_p*B_m - A_p - s_m*B_p;
  double C_2 = s_m*A_p - s_p*A_m;

  if(C_2 == 0) {
    *vstar = 0.;
  } else if(C_1*C_1 - 4.*C_0*C_2 < 0.) {
    *vstar = -.5*C_1/C_2;
  } else {
    *vstar = .5*(-C_1 - sqrt(C_1*C_1 - 4.*C_0*C_2))/C_2;
  }
  *pstar = (*vstar*A_m - B_m)/(1. - *vstar*s_m);

  // Did the code crash? If so, was it due to this routine, or because the initial conditions for the routine were bad?
  if(*vstar != *vstar) {
    std::cout << std::endl << "ERROR in 'riemann_hllc.cpp' at iter = " << iter << ", i = " << i << std::endl;
    std::cout << "v_L = " << v_L << ", rho_L = " << rho_L << ", p_L = " << p_L << ", b_L = " << b_L << std::endl;
    std::cout << "v_R = " << v_R << ", rho_R = " << rho_R << ", p_R = " << p_R << ", b_R = " << b_R << std::endl;
    // std::cout << "C_1*C_1 - 4.*C_0*C_2 = " << C_1*C_1 - 4.*C_0*C_2 << std::endl;
    // std::cout << "C_0 = " << C_0 << ", C_1 = " << C_1 << ", C_2 = " << C_2 << std::endl;
    // std::cout << "B_m = " << B_m << ", B_p = " << B_p << std::endl;
    // std::cout << "A_m = " << A_m << ", A_p = " << A_p << std::endl;
  }

}
