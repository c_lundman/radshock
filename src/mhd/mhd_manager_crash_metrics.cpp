//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "mhd_manager.h"
#include <string>
#include <vector>
#include <iostream>
#include <math.h>

// Public functions (crash metrics)

int MHDManager::is_code_about_to_crash(std::string &message) {

  // This function performs a number of checks of the MHD grid,
  // attempting to see if the code is about to evolve itself into a crash

  if(is_density_grid_too_spikey(message)) {return 1;}

  return 0;
}

int MHDManager::is_density_grid_too_spikey(std::string &message) {

  // This metric attempts to check if the density grid has developed a spike

  double xi = 3;

  for(unsigned int j=3; j<rhos.size()-3; j++) {
    if(rhos[j] > xi*rhos[j-1] && rhos[j] > xi*rhos[j+1]) {
      message = "density spike (factor 3 larger than both neighbours) found in bin j = " + std::to_string(j);
      return 1;
    }
  }

  return 0;
}
