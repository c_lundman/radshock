//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef PARAMETER_PARSER_H
#define PARAMETER_PARSER_H

#include <string>
#include <fstream>
#include <vector>

class ParameterParser {

  // A class that reads the parameter input file
  // and provides convenient access to the loaded parameters

  // When adding a new parameter, need to (in this file)
  // 1) define the (private) parameter (variable) name,
  // 2) define "set" (private) and "get" (public) functions for the parameter
  // in this file,
  // 3) in parameter_parser.cpp, add an "else if" statement that checks for the parameter and loads it
  // 4) in parameter_parser.cpp, add the parameter to the "print_parameters" function

 private:

  // All parameters here

  // The parameter file object
  std::string param_file_name;
  std::ifstream param_file;

  // General
  std::string mhd_input_state_file;
  std::string rad_input_state_file = "None";
  std::string mhd_output_state_file;
  std::string rad_output_state_file = "None";
  std::string mhd_output_snapshot_file;

  // Debug mode
  int debug = 0;
  int debug_start_mhd_iter = 0;
  int debug_end_mhd_iter = -1;

  // Times for saving data
  int n_ts = 10;
  double t_i = 0;
  double t_f = 1;
  int log_spaced_time = 0;

  // How many save states
  int n_ss = 0;

  // Save every n'th iteration
  int save_every_nth_mhd_iter = 0;
  int save_every_nth_rad_iter = 0;

  // Time steps
  double xi_rad = 1;
  double xi_mhd = 1;

  // Rng
  int seed = 0;

  // MC photon number modifications
  int load_photon_multiplier = 1;
  int save_photon_divider = 1;

  // Smoothing radiation quantities over this optical depth
  double tau_smoothing = 0;

  // Using pairs?
  int use_pairs = 1;

  // Gravity
  double m_grav = 0;

  // Boundaries
  int boundary_L = 0;
  int boundary_R = 0;

  // Radiation source terms
  int rad_source_terms = 1;

  // Ignore warnings of imminent crashes
  int ignore_crash_warning = 1;

  // Parameters for boundary_X = 4 (pistons)
  std::vector<double> boundary_L_params;
  std::vector<double> boundary_R_params;

  // Use two temperature plasma approximation?
  int two_temp_plasma = 0;

  // Is the spectrum saved in uniform mass or radius bins?
  int uniform_spec_radius_bins = 1;

  // Merge bins (N - > N/2) when highest column optical depth falls below this limit? (N > 100 enforced)
  double tau_merge_limit = 0;

  // Problem source terms? [TO DO]

  // Set parameters
  void set_param_file_name(std::string param_file_name_p) {param_file_name = param_file_name_p;}
  void set_mhd_input_state_file(std::string mhd_input_state_file_p) {mhd_input_state_file = mhd_input_state_file_p;}
  void set_rad_input_state_file(std::string rad_input_state_file_p) {rad_input_state_file = rad_input_state_file_p;}
  void set_mhd_output_state_file(std::string mhd_output_state_file_p) {mhd_output_state_file = mhd_output_state_file_p;}
  void set_rad_output_state_file(std::string rad_output_state_file_p) {rad_output_state_file = rad_output_state_file_p;}
  void set_mhd_output_snapshot_file(std::string mhd_output_snapshot_file_p) {mhd_output_snapshot_file = mhd_output_snapshot_file_p;}
  void set_debug(int debug_p) {debug = debug_p;}
  void set_debug_start_mhd_iter(int debug_start_mhd_iter_p) {debug_start_mhd_iter = debug_start_mhd_iter_p;}
  void set_debug_end_mhd_iter(int debug_end_mhd_iter_p) {debug_end_mhd_iter = debug_end_mhd_iter_p;}
  void set_n_ts(int n_ts_p) {n_ts = n_ts_p;}
  void set_t_i(double t_i_p) {t_i = t_i_p;}
  void set_t_f(double t_f_p) {t_f = t_f_p;}
  void set_log_spaced_time(int log_spaced_time_p) {log_spaced_time = log_spaced_time_p;}
  void set_n_ss(int n_ss_p) {n_ss = n_ss_p;}
  void set_save_every_nth_mhd_iter(int save_every_nth_mhd_iter_p) {save_every_nth_mhd_iter = save_every_nth_mhd_iter_p;}
  void set_save_every_nth_rad_iter(int save_every_nth_rad_iter_p) {save_every_nth_rad_iter = save_every_nth_rad_iter_p;}
  void set_xi_mhd(double xi_mhd_p) {xi_mhd = xi_mhd_p;}
  void set_xi_rad(double xi_rad_p) {xi_rad = xi_rad_p;}
  void set_seed(int seed_p) {seed = seed_p;}
  void set_load_photon_multiplier(int load_photon_multiplier_p) {load_photon_multiplier = load_photon_multiplier_p;}
  void set_save_photon_divider(int save_photon_divider_p) {save_photon_divider = save_photon_divider_p;}
  void set_m_grav(double m_grav_p) {m_grav = m_grav_p;}
  void set_rad_source_terms(int rad_source_terms_p) {rad_source_terms = rad_source_terms_p;}
  void set_tau_smoothing(double tau_smoothing_p) {tau_smoothing = tau_smoothing_p;}
  void set_use_pairs(int use_pairs_p) {use_pairs = use_pairs_p;}
  void set_ignore_crash_warning(int ignore_crash_warning_p) {ignore_crash_warning = ignore_crash_warning_p;}

  void set_boundary_L(int boundary_L_p) {boundary_L = boundary_L_p;}
  void set_boundary_R(int boundary_R_p) {boundary_R = boundary_R_p;}
  void set_boundary_L_params(double t1, double t2, double betagamma1, double betagamma2) {
    boundary_L_params.resize(4);
    boundary_L_params[0] = t1;
    boundary_L_params[1] = t2;
    boundary_L_params[2] = betagamma1;
    boundary_L_params[3] = betagamma2;
  }
  void set_boundary_R_params(double t1, double t2, double betagamma1, double betagamma2) {
    boundary_R_params.resize(4);
    boundary_R_params[0] = t1;
    boundary_R_params[1] = t2;
    boundary_R_params[2] = betagamma1;
    boundary_R_params[3] = betagamma2;
  }

  void set_two_temp_plasma(double two_temp_plasma_p) {two_temp_plasma = two_temp_plasma_p;}
  void set_uniform_spec_radius_bins(double uniform_spec_radius_bins_p) {uniform_spec_radius_bins = uniform_spec_radius_bins_p;}
  void set_tau_merge_limit(double tau_merge_limit_p) {tau_merge_limit = tau_merge_limit_p;}

 public:

  ParameterParser() {};
  int read_parameter_file(std::string filename);
  void print_parameters();

  // Get parameters
  std::string get_param_file_name() {return param_file_name;}
  std::string get_mhd_input_state_file() {return mhd_input_state_file;}
  std::string get_rad_input_state_file() {return rad_input_state_file;}
  std::string get_mhd_output_state_file() {return mhd_output_state_file;}
  std::string get_rad_output_state_file() {return rad_output_state_file;}
  std::string get_mhd_output_snapshot_file() {return mhd_output_snapshot_file;}
  int get_debug() {return debug;}
  int get_debug_start_mhd_iter() {return debug_start_mhd_iter;}
  int get_debug_end_mhd_iter() {return debug_end_mhd_iter;}
  int get_n_ts() {return n_ts;}
  double get_t_i() {return t_i;}
  double get_t_f() {return t_f;}
  int get_log_spaced_time() {return log_spaced_time;}
  int get_n_ss() {return n_ss;}
  int get_save_every_nth_mhd_iter() {return save_every_nth_mhd_iter;}
  int get_save_every_nth_rad_iter() {return save_every_nth_rad_iter;}
  double get_xi_mhd() {return xi_mhd;}
  double get_xi_rad() {return xi_rad;}
  int get_seed() {return seed;}
  int get_load_photon_multiplier() {return load_photon_multiplier;}
  int get_save_photon_divider() {return save_photon_divider;}
  double get_m_grav() {return m_grav;}
  int get_rad_source_terms() {return rad_source_terms;}
  double get_tau_smoothing() {return tau_smoothing;}
  int get_use_pairs() {return use_pairs;}
  int get_ignore_crash_warning() {return ignore_crash_warning;}

  int get_boundary_L() {return boundary_L;}
  int get_boundary_R() {return boundary_R;}
  std::vector<double> get_boundary_L_params() {return boundary_L_params;}
  std::vector<double> get_boundary_R_params() {return boundary_R_params;}

  int get_two_temp_plasma() {return two_temp_plasma;}
  int get_uniform_spec_radius_bins() {return uniform_spec_radius_bins;}
  double get_tau_merge_limit() {return tau_merge_limit;}

};

#endif
