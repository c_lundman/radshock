//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "lagrangian_mhd_functions.h"
#include <string>
#include <vector>
#include <math.h>
#include <iostream>
// #include "riemann_hll.h"
#include "riemann_hllc.h"
#include "boundary_conditions.h"
#include <fstream>

// --------------------------------------------------
//  Variable interpolation (PPM, Colella & Woodward)
// --------------------------------------------------

double get_dma_j(double a_jm1, double a_j, double a_jp1) {

  double da_j, dma_j, sign, A, B;

  if((a_jp1-a_j)*(a_j-a_jm1) <= 0.) {
    return 0.;
  } else {
    da_j = .5*(a_jp1 - a_jm1);
    if(da_j > 0.) {
      sign = 1.;
    } else {
      sign = -1.;
    }
    da_j = sqrt(da_j*da_j);
    A = 2.*sqrt((a_jp1-a_j)*(a_jp1-a_j));
    B = 2.*sqrt((a_j-a_jm1)*(a_j-a_jm1));
    if(da_j < A) {
      if(da_j < B) {
	dma_j = sign*sqrt(da_j*da_j);
      } else {
	dma_j = sign*B;
      }
    } else {
      if(A < B) {
	dma_j = sign*A;
      } else {
	dma_j = sign*B;
      }
    }
    return dma_j;
  }
}

double find_intermediate_a(double a_jm1, double a_j, double a_jp1, double a_jp2, double Dm_jm1, double Dm_j, double Dm_jp1, double Dm_jp2) {
  // a_jm1 = a_{j-1}, a_jp1 = a_{j+1} etc.
  // a_jphalf = a_{j+1/2}

  double a_jphalf, dma_j, dma_jp1;

  dma_j = get_dma_j(a_jm1, a_j, a_jp1);
  dma_jp1 = get_dma_j(a_j, a_jp1, a_jp2);

  // a_jphalf = .5*(a_jp1 + a_j) + (1./6.)*(dma_j - dma_jp1);
  double A, B, C, D, E, F;
  A = Dm_j/(Dm_j + Dm_jp1)*(a_jp1 - a_j);
  B = 1./(Dm_jm1 + Dm_j + Dm_jp1 + Dm_jp2);
  C = 2.*Dm_jp1*Dm_j/(Dm_j+Dm_jp1)*(a_jp1-a_j);
  D = (Dm_jm1 + Dm_j)/(2.*Dm_j + Dm_jp1) - (Dm_jp2 + Dm_jp1)/(2.*Dm_jp1 + Dm_j);
  E = Dm_j*(Dm_jm1 + Dm_j)/(2.*Dm_j + Dm_jp1)*dma_jp1;
  F = Dm_jp1*(Dm_jp1 + Dm_jp2)/(Dm_j + 2.*Dm_jp1)*dma_j;
  a_jphalf = a_j + A + B*(C*D - E + F);
  return a_jphalf;
}

void find_a_L_a_R(double a_jmhalf, double a_jphalf, double a_j, double *a_L, double *a_R) {

  *a_L = a_jmhalf;
  *a_R = a_jphalf;

  // Discontinuity detection!

  if((*a_R - a_j)*(a_j - *a_L) <= 0.) {
    *a_L = a_j;
    *a_R = a_j;
  } else if((*a_R - *a_L)*(a_j - .5*(*a_L + *a_R)) > (*a_R - *a_L)*(*a_R - *a_L)/6.) {
    *a_L = 3.*a_j - 2.*(*a_R);
  } else if(-(*a_R - *a_L)*(*a_R - *a_L)/6. > (*a_R - *a_L)*(a_j - .5*(*a_R + *a_L))) {
    *a_R = 3.*a_j - 2.*(*a_L);
  }
}

double get_a_6j(double a_j, double a_L, double a_R) {

  double a_6j;

  a_6j = 6.*(a_j -.5*(a_L + a_R));
  return a_6j;
}

double get_Delta_a_j(double a_L, double a_R) {

  double Delta_a_j;

  Delta_a_j = a_R - a_L;
  return Delta_a_j;
}

double find_a_pjphalf(double a_j, double a_L, double a_R, double x_j) {
  // The left Riemann state

  double a_pjphalf, Delta_a_j, a_6j;

  Delta_a_j = get_Delta_a_j(a_L, a_R);
  a_6j = get_a_6j(a_j, a_L, a_R);
  a_pjphalf = a_R - .5*x_j*(Delta_a_j - (1. - 2./3.*x_j)*a_6j);
  return a_pjphalf;
}

double find_a_mjmhalf(double a_j, double a_L, double a_R, double x_j) {
  // The right Riemann state

  double a_mjmhalf, Delta_a_j, a_6j;

  Delta_a_j = get_Delta_a_j(a_L, a_R);
  a_6j = get_a_6j(a_j, a_L, a_R);
  a_mjmhalf = a_L + .5*x_j*(Delta_a_j + (1. - 2./3.*x_j)*a_6j);
  return a_mjmhalf;
}

//

double get_avg_r(double r_1, double r_2, int alpha) {
  // If alpha = 2 or 1, returns average r, else returns unity
  // r_2 > r_1 needed!
  double r=0.;

  if(alpha == 2 || alpha == 1) {
    r = (r_2*r_2 - r_1*r_1)/2./(r_2-r_1);
  } else if(alpha == 0) {
    r = 1.;
  }
  return r;
}

double get_next_r_jhalf(double r_jhalf, double bar_v_jhalf, double Delta_t) {

  double next_r_jhalf;

  next_r_jhalf = r_jhalf + Delta_t*bar_v_jhalf;
  return next_r_jhalf;
}

double get_bar_A_jhalf(double r_jhalf, double next_r_jhalf, double bar_v_jhalf, double Delta_t, int alpha) {

  double bar_A_jhalf;

  if(alpha == 0) {
    bar_A_jhalf = 1.;
  } else if(bar_v_jhalf == 0.) {
    bar_A_jhalf = pow(r_jhalf, alpha);
  } else {
    bar_A_jhalf = (pow(next_r_jhalf, 1.+alpha) - pow(r_jhalf, 1.+alpha))/(1.+alpha)/bar_v_jhalf/Delta_t;
  }
  return bar_A_jhalf;
}

double get_next_V_m_j(double next_r_jhalf, double next_rjneghalf, double Delta_m, int alpha) {

  double next_V_m_j;

  next_V_m_j = (pow(next_r_jhalf, 1.+alpha) - pow(next_rjneghalf, 1.+alpha))/(1.+alpha)/Delta_m;
  return next_V_m_j;
}

double get_next_S_m_j(double S_m_j, double bar_A_jhalf, double bar_A_jneghalf, double bar_p_jhalf, double bar_p_jneghalf, double Delta_t, double Delta_m, double H1) {

  // H1 = G1/gamma/rho for planar symmetry
  // H1 = (G1+2p/r)/gamma/rho for spherical symmetry

  double next_S_m_j;

  next_S_m_j = S_m_j + Delta_t/Delta_m*(bar_A_jneghalf*bar_p_jneghalf - bar_A_jhalf*bar_p_jhalf) + Delta_t*H1;
  return next_S_m_j;
}

double get_next_E_m_j(double E_m_j, double bar_A_jhalf, double bar_A_jneghalf, double bar_v_jhalf, double bar_v_jneghalf, double bar_p_jhalf, double bar_p_jneghalf, double Delta_t, double Delta_m, double H0) {

  // H0 = G0/gamma/rho

  double next_E_m_j;

  next_E_m_j = E_m_j + Delta_t/Delta_m*(bar_A_jneghalf*bar_v_jneghalf*bar_p_jneghalf - bar_A_jhalf*bar_v_jhalf*bar_p_jhalf) + Delta_t*H0;
  return next_E_m_j;
}

double get_next_M_m_j(double M_m_j, double Delta_t, double HPhi) {

  // HPhi = Phidot/gamma/rho

  double next_M_m_j;

  next_M_m_j = M_m_j + Delta_t*HPhi;
  return next_M_m_j;
}

// --------------------------------------------------------------------------------
//  Functions for computing V_m, S_m, E_m and M_m from the v, rho, p and b vectors
// --------------------------------------------------------------------------------

double get_V_m(double v, double rho) {
  double Gamma = 1./sqrt(1.-v*v);
  double V_m = 1./Gamma/rho;
  return V_m;
}

double get_S_m(double v, double rho, double p, double b, const double gamma_ad) {
  double Gamma = 1./sqrt(1.-v*v);
  double w = gamma_ad/(gamma_ad-1.)*p/rho;
  double sigma = b*b/rho;
  double h_star = 1. + w + sigma;
  double V_m = 1./Gamma/rho;
  double S_m = V_m*Gamma*Gamma*rho*h_star*v;
  return S_m;
}

double get_E_m(double v, double rho, double p, double b, const double gamma_ad) {
  double Gamma = 1./sqrt(1.-v*v);
  double w = gamma_ad/(gamma_ad-1.)*p/rho;
  double sigma = b*b/rho;
  double p_b = .5*b*b;
  double h_star = 1. + w + sigma;
  double p_star = p + p_b;
  double V_m = 1./Gamma/rho;
  double E_m = V_m*(Gamma*Gamma*rho*h_star - p_star);
  return E_m;
}

double get_M_m(double rho, double b, double r, const int alpha) {
  double M_m = 0.;
  // if(alpha == 2.) {
  //   M_m = b/R_i/rho; // ASSUMING THAT R_i >> r
  // } else if(alpha == 1.) {
  //   // NOT IMPLEMENTED YET!
  //   M_m = b/pow(R_i, .5)/rho;
  // } else if(alpha == 0.) {
  //   M_m = b/rho;
  // }
  if(alpha == 2) {
    M_m = b/r/rho;
  } else if(alpha == 1) {
    // NOT IMPLEMENTED YET!
    M_m = b/pow(r, .5)/rho;
  } else if(alpha == 0) {
    M_m = b/rho;
  }
  return M_m;
}

// -------------------------------------------------------------------------------------
//  Functions for reconstructing v, rho, p and b from the V_m, S_m, E_m and M_m vectors
// -------------------------------------------------------------------------------------

void theEEquation(double w, double *f, double *dfdw, double V, double E, double S, double M, double r, int alpha, double gamma_ad) {

  double Gamma_r = gamma_ad/(gamma_ad-1.);
  double v;

  if(alpha == 2) {
    v = S*V/(w*V + r*r*M*M);
  } else if (alpha == 1) {
    v = S*V/(w*V + r*M*M); // NOT IMPLEMENTED YET!
  } else if (alpha == 0) {
    v = S*V/(w*V + M*M);
  } else {
    v = 0.;
  }

  double gamma = 1./sqrt(1.-v*v);
  double p = (w-gamma)/Gamma_r/gamma/gamma/V;

  if(alpha == 2) {
    *f = w - V*p + (1.-1./2./gamma/gamma)*r*r*M*M/V - E;
    *dfdw = 1. - 1./Gamma_r/gamma/gamma - v*v*v/S*(r*r*M*M/V - (gamma-2.*w)/Gamma_r);
  } else if(alpha == 1) {
    // NOT IMPLEMENTED YET
    *f = w - V*p + (1.-1./2./gamma/gamma)*r*M*M/V - E;
    *dfdw = 1. - 1./Gamma_r/gamma/gamma - v*v*v/S*(r*M*M/V - (gamma-2.*w)/Gamma_r);
  } else if(alpha == 0) {
    *f = w - V*p + (1.-1./2./gamma/gamma)*M*M/V - E;
    *dfdw = 1. - 1./Gamma_r/gamma/gamma - v*v*v/S*(M*M/V - (gamma-2.*w)/Gamma_r);
  } else {
    *f = 0.;
    *dfdw = 0.;
    printf("alpha != (0, 1, 2) according to theEEquation!\n");
  }
}

double rtnewt(double w_guess, double w_acc, double V, double E, double S, double M, double r, int alpha, double gamma_ad) {

  // Based on the numerical recipes implementation (but slightly modified)

  // Using the Newton-Raphson method, find the root of a function. The root rtnewt will be refined until
  // its accuracy is known within ± w_acc

  int j;
  double dfdw, dw, f, rtn;
  int JMAX=20;

  // Initial guess for the root
  rtn = w_guess;
  for(j=1; j<=JMAX; j++) {

    theEEquation(rtn, &f, &dfdw, V, E, S, M, r, alpha, gamma_ad);
    dw = f/dfdw;
    rtn -= dw;

    /* printf("j=%i: ", j); */
    /* printf("w_approx = %4.20f\n", rtn); */

    // Checking convergence to desired accuracy
    if(fabs(dw) < w_acc) {
      /* printf("j = %i\n", j); */
      return rtn;
    }
  }

  /* printf("Maximum number of iterations exceeded in rtnewt\n"); */
  /* printf("V = %4.20e\n", V); */
  /* printf("E = %4.20e\n", E); */
  /* printf("S = %4.20e\n", S); */
  /* printf("M = %4.20e\n", M); */
  /* printf("r = %4.20e\n", r); */
  /* printf("w_guess = %4.20e\n", w_guess); */
  /* printf("w_acc   = %4.20e\n", w_acc); */
  /* printf("rtn     = %4.20e\n", rtn); */
  return 0.; // Never get here!
}

int reconstruct_v_rho_p_b(double V_m, double S_m, double E_m, double M_m,
			  double *v, double *rho, double *p, double *b, double r, int alpha, double gamma_ad) {

  double Gamma_r, gamma, w, w_i;
  Gamma_r = gamma_ad/(gamma_ad-1.);

  double v_max = 0.999999944444; // Equals gamma_max = 3e3

  if(S_m != 0.) {
    if(alpha == 2) {
      /* w_i = E_m - r*r*M_m*M_m/V_m; */
      w_i = sqrt(S_m*S_m)/v_max - r*r*M_m*M_m/V_m;
    } else if(alpha == 1) {
      // NOT IMPLEMENTED YET!
      w_i = sqrt(S_m*S_m)/v_max - r*M_m*M_m/V_m;
    } else if(alpha == 0) {
      /* w_i = E_m - M_m*M_m/V_m; */
      w_i = E_m - M_m*M_m/V_m;
    } else {
      w_i = 1.;
      /* cout << "alpha != (0, 1, 2) according to reconstruct_v_rho_p_b!" << endl; */
    }

    // Finding the root
    double rel_acc = 1e-14;
    // double rel_acc = 1e-10;
    w = rtnewt(w_i, rel_acc*w_i, V_m, E_m, S_m, M_m, r, alpha, gamma_ad);
  } else {
    if(alpha == 2) {
      w = (E_m - r*r*M_m*M_m/V_m - 1./Gamma_r)/(1.-1./Gamma_r);
    } else if(alpha == 1) {
      // NOT IMPLEMENTED YET!
      w = (E_m - r*M_m*M_m/V_m - 1./Gamma_r)/(1.-1./Gamma_r);
    } else {
      w = (E_m - M_m*M_m/V_m - 1./Gamma_r)/(1.-1./Gamma_r);
    }
  }

  // Reconstructing the variables
  if(alpha == 2) {
    *v = S_m*V_m/(w*V_m + r*r*M_m*M_m);
  } else if(alpha == 1) {
    // NOT IMPLEMENTED YET!
    *v = S_m*V_m/(w*V_m + r*M_m*M_m);
  } else if(alpha == 0) {
    *v = S_m*V_m/(w*V_m + M_m*M_m);
  }

  if(*v > v_max) {
    *v = v_max;
  } else if(*v < -v_max) {
    *v = -v_max;
  }
  gamma = 1./sqrt(1.-(*v)*(*v));
  *rho = 1./gamma/V_m;
  *p = (w-gamma)/Gamma_r/gamma/gamma/V_m;

  if(alpha == 2) {
    *b = r*(*rho)*M_m;
  } else if(alpha == 1) {
    // NOT IMPLEMENTED YET!
    *b = pow(r, .5)*(*rho)*M_m;
  } else if(alpha == 0) {
    *b = (*rho)*M_m;
  }

  // Check for nan's
  if(*v != *v || *rho != *rho || *p != *p || *b != *b || rho <= 0 || p <= 0 || b <= 0) {
    return 0;
  }

  return 1;
}

// -----------------------
//  Finding the time step
// -----------------------

double get_time_step(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, std::vector<double> &m_bs,
		     int n_bs, int ghosts, int alpha, double gamma_ad, double epsilon) {

  // Finding the longest possible dt
  double h_j, h_star_j, c_s, c_s_star, v_A, A_j, gamma_j;
  double dt=1e60, Delta_t_j, Delta_m;
  double expansion_fraction=1e-2;

  for(int j=ghosts; j<n_bs-1-ghosts; ++j) {
    Delta_m = m_bs[j+1]-m_bs[j];

    A_j = 1./(1.+alpha)*(pow(r_bs[j+1], 1.+alpha) - pow(r_bs[j], 1.+alpha))/(r_bs[j+1] - r_bs[j]);

    h_j = 1. + gamma_ad*ps[j]/(gamma_ad-1.)/rhos[j];
    h_star_j = 1. + gamma_ad*ps[j]/(gamma_ad-1.)/rhos[j] + bs[j]*bs[j]/rhos[j];
    c_s = sqrt(gamma_ad*ps[j]/h_j/rhos[j]);
    v_A = sqrt(bs[j]*bs[j]/h_star_j/rhos[j]);
    c_s_star = sqrt((1.-v_A*v_A)*c_s*c_s + v_A*v_A);

    gamma_j = 1./sqrt(1.-vs[j]*vs[j]);
    Delta_t_j = epsilon*Delta_m/A_j/rhos[j]/c_s_star*gamma_j*(1.-vs[j]*c_s_star);
    if(Delta_t_j < dt) {
      dt = Delta_t_j;
    }
  }

  // If spherical geometry, check that the time step is smaller than the dynamical time
  if(alpha == 2 || alpha == 1) {
    for(int i=ghosts; i<n_bs-ghosts; ++i) {
      if(dt > expansion_fraction*r_bs[i]) {
	dt = expansion_fraction*r_bs[i];
      }
    }
  }

  return dt;
}

// --------------------------------------
//  Smoothing over nan's that may happen
// --------------------------------------

void smooth_over_nans(std::vector<double> &As, int n_bs, int ghosts) {

  // int smoothed = 0;
  int di = 2;
  for(int i=ghosts; i<n_bs-ghosts; i++) {
    if(As[i] != As[i]) {
      // smoothed = i;
      As[i] = .5*(As[i+di] + As[i-di]);
    }
  }
}

// --------------------
//  Setting boundaries
// --------------------

void set_ghost_cells(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, std::vector<double> &r_bs, double t, int n_bs,
		     int boundary_L, int boundary_R, double gamma_ad, int alpha, int ghosts, double piston_vgamma_L, double piston_vgamma_R) {

  // Vacuum
  if(boundary_L == 0) boundary_vacuum_L(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);
  if(boundary_R == 0) boundary_vacuum_R(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);

  // Wall
  if(boundary_L == 1) boundary_wall_L(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);
  if(boundary_R == 1) boundary_wall_R(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);

  // Comoving wall
  if(boundary_L == 2) boundary_comoving_L(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);
  if(boundary_R == 2) boundary_comoving_R(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);

  // Periodic boundaries
  if(boundary_L == 3) boundary_periodic_L(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);
  if(boundary_R == 3) boundary_periodic_R(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);

  // Piston
  if(boundary_L == 4) boundary_piston_L(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha, piston_vgamma_L);
  if(boundary_R == 4) boundary_piston_R(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha, piston_vgamma_R);

  // For tests
  if(boundary_L == -1) boundary_test_L(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);
  if(boundary_R == -1) boundary_test_R(vs, rhos, ps, bs, r_bs, t, n_bs, gamma_ad, alpha);

}

// --------------------------------
//  Finding mass or radius vectors
// --------------------------------

// void get_radius_vector(double R_i, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, int alpha) {
void get_radius_vector(double R_i, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &V_ms, int alpha) {
  // Assumes that mass boundary vector and primitives are known, and computes the radius boundary vector
  // double Dm, r_1, r_2, gamma, rho;
  double Dm, r_1, r_2, V_m;
  r_bs[2] = R_i;
  for(unsigned int i=3; i<r_bs.size(); i++) {
    Dm = m_bs[i]-m_bs[i-1];
    r_1 = r_bs[i-1];
    // gamma = 1./sqrt(1.-vs[i-1]*vs[i-1]);
    // rho = rhos[i-1];
    V_m = V_ms[i-1];
    r_bs[i] = pow(pow(r_1, alpha+1) + (alpha+1)*Dm*V_m, 1./(alpha+1));
  }
  for(int i=1; i>=0; i--) {
    Dm = m_bs[i+1]-m_bs[i];
    r_2 = r_bs[i+1];
    // gamma = 1./sqrt(1.-vs[i]*vs[i]);
    // rho = rhos[i];
    V_m = V_ms[i];
    r_bs[i] = pow(pow(r_2, alpha+1) - (alpha+1)*Dm*V_m, 1./(alpha+1));
  }
}

// void get_mass_vector(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, int alpha) {
void get_mass_vector(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &V_ms, int alpha) {
  // Assumes that radius boundary vector and primitives are known, and computes the mass boundary vector
  // double r_1, r_2, gamma, rho;
  double r_1, r_2, V_m;
  m_bs[2] = 0;
  for(unsigned int i=3; i<m_bs.size(); i++) {
    r_1 = r_bs[i-1];
    r_2 = r_bs[i];
    // gamma = 1./sqrt(1.-vs[i-1]*vs[i-1]);
    // rho = rhos[i-1];
    V_m = V_ms[i-1];
    m_bs[i] = m_bs[i-1] + 1./V_m/(alpha+1)*(pow(r_2, alpha+1) - pow(r_1, alpha+1));
  }
  for(int i=1; i>=0; i--) {
    r_1 = r_bs[i];
    r_2 = r_bs[i+1];
    // gamma = 1./sqrt(1.-vs[i]*vs[i]);
    // rho = rhos[i];
    V_m = V_ms[i];
    m_bs[i] = m_bs[i+1] - 1./V_m/(alpha+1)*(pow(r_2, alpha+1) - pow(r_1, alpha+1));
  }
}

// ----------------
//  Test functions
// ----------------

int check_prims_integrity(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
			  int iter, int RANK, const std::string& message) {

  // DEBUG: Check prims integrity
  int ghosts = 2;
  for(unsigned int j=ghosts; j<vs.size()-ghosts; j++) {
    if(vs[j] != vs[j] || rhos[j] != rhos[j] || ps[j] != ps[j] || bs[j] != bs[j]) {
      std::cout << message << " GRID BROKEN (prims, RANK=" << RANK << ") at iter = " << iter << ": vs[" << j << "] = " << vs[j] << ", rhos[" << j << "] = " << rhos[j] << ", ps[" << j << "] = " << ps[j] << std::endl;
      return 0;
    }

    if(ps[j] < 0 || rhos[j] < 0 || vs[j] < -1 || vs[j] > +1) {
      std::cout << message << " GRID NEGATIVE (prims, RANK=" << RANK << ", iter=" << iter << ")" << ": vs[" << j << "] = " << vs[j] << ", rhos[" << j << "] = " << rhos[j] << ", ps[" << j << "] = " << ps[j] << std::endl;
      return 0;
    }
  }
  return 1;
}

int check_consts_integrity(std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms,
			   int iter, int RANK, const std::string& message) {

  // DEBUG: Check grid integrity
  int ghosts = 2;
  for(unsigned int j=ghosts; j<V_ms.size()-ghosts; j++) {

    if(V_ms[j] <= 0 || E_ms[j] <= 0) {
      std::cout << message << " GRID NEGATIVE (consts, RANK=" << RANK << ", iter=" << iter << ")" << ": V_ms[" << j << "] = " << V_ms[j] << ", E_ms[" << j << "] = " << E_ms[j] << std::endl;
      return 0;
    }

  }
  return 1;
}

// -------------------
//  Taking a MHD step
// -------------------

void do_ppm(std::vector<double> &m_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, int ghosts,
	    std::vector<double> &v_bs, std::vector<double> &rho_bs, std::vector<double> &p_bs, std::vector<double> &b_bs) {

  // Use the piecewise parabolic method (PPM)

  // Finding v, rho and p at the boundaries by interpolation

  double Dm_jm1, Dm_j, Dm_jp1, Dm_jp2;
  for(unsigned int i=ghosts; i<m_bs.size()-ghosts; i++) {
    Dm_jm1 = m_bs[i-1]-m_bs[i-2];
    Dm_j = m_bs[i]-m_bs[i-1];
    Dm_jp1 = m_bs[i+1]-m_bs[i];
    Dm_jp2 = m_bs[i+2]-m_bs[i+1];
    v_bs[i] = find_intermediate_a(vs[i-2], vs[i-1], vs[i], vs[i+1], Dm_jm1, Dm_j, Dm_jp1, Dm_jp2);
    rho_bs[i] = find_intermediate_a(rhos[i-2], rhos[i-1], rhos[i], rhos[i+1], Dm_jm1, Dm_j, Dm_jp1, Dm_jp2);
    p_bs[i] = find_intermediate_a(ps[i-2], ps[i-1], ps[i], ps[i+1], Dm_jm1, Dm_j, Dm_jp1, Dm_jp2);
    b_bs[i] = find_intermediate_a(bs[i-2], bs[i-1], bs[i], bs[i+1], Dm_jm1, Dm_j, Dm_jp1, Dm_jp2);
  }

  // for(unsigned int i=ghosts; i<m_bs.size()-ghosts; i++) {
  //   v_bs[i] = find_intermediate_a(vs[i-2], vs[i-1], vs[i], vs[i+1], m_bs[i-2], m_bs[i-1], m_bs[i], m_bs[i+1]);
  //   rho_bs[i] = find_intermediate_a(rhos[i-2], rhos[i-1], rhos[i], rhos[i+1], m_bs[i-2], m_bs[i-1], m_bs[i], m_bs[i+1]);
  //   p_bs[i] = find_intermediate_a(ps[i-2], ps[i-1], ps[i], ps[i+1], m_bs[i-2], m_bs[i-1], m_bs[i], m_bs[i+1]);
  //   b_bs[i] = find_intermediate_a(bs[i-2], bs[i-1], bs[i], bs[i+1], m_bs[i-2], m_bs[i-1], m_bs[i], m_bs[i+1]);
  // }
}

void fix_ppm_edges(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs, int ghosts,
		   std::vector<double> &v_bs, std::vector<double> &rho_bs, std::vector<double> &p_bs, std::vector<double> &b_bs) {

  // Interpolation does not work at the boundaries between ghost cells, so using cell averages

  int n_bs = v_bs.size();
  v_bs[1] = .5*(vs[0] + vs[1]);
  rho_bs[1] = .5*(rhos[0] + rhos[1]);
  p_bs[1] = .5*(ps[0] + ps[1]);
  b_bs[1] = .5*(bs[0] + bs[1]);
  v_bs[n_bs-ghosts] = .5*(vs[n_bs-3] + vs[n_bs-2]);
  rho_bs[n_bs-ghosts] = .5*(rhos[n_bs-3] + rhos[n_bs-2]);
  p_bs[n_bs-ghosts] = .5*(ps[n_bs-3] + ps[n_bs-2]);
  b_bs[n_bs-ghosts] = .5*(bs[n_bs-3] + bs[n_bs-2]);

}

void fix_periodic_boundaries(int ghosts, int boundary_L, int boundary_R,
			     std::vector<double> &v_bs, std::vector<double> &rho_bs, std::vector<double> &p_bs, std::vector<double> &b_bs) {

  // If periodic boundaries, do a correction that ensures proper periodicity

  int n_bs = v_bs.size();
  if(boundary_L == 3 && boundary_R == 3) {
    v_bs[ghosts-1] = v_bs[n_bs-1-ghosts-1];
    v_bs[n_bs-ghosts] = v_bs[ghosts+1];
    rho_bs[ghosts-1] = rho_bs[n_bs-1-ghosts-1];
    rho_bs[n_bs-ghosts] = rho_bs[ghosts+1];
    p_bs[ghosts-1] = p_bs[n_bs-1-ghosts-1];
    p_bs[n_bs-ghosts] = p_bs[ghosts+1];
    b_bs[ghosts-1] = b_bs[n_bs-1-ghosts-1];
    b_bs[n_bs-ghosts] = b_bs[ghosts+1];
  }
}

void find_left_and_right_states_inside_each_bin(std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
						std::vector<double> &v_bs, std::vector<double> &rho_bs, std::vector<double> &p_bs, std::vector<double> &b_bs, int ghosts,
						std::vector<double> &vLs, std::vector<double> &vRs, std::vector<double> &rhoLs, std::vector<double> &rhoRs,
						std::vector<double> &pLs, std::vector<double> &pRs, std::vector<double> &bLs, std::vector<double> &bRs) {

  // Finding the left and right v, rho, p, b at each j (including closest ghost cells) [MAKE NEW FUNCTION]
  int n_bs = v_bs.size();
  double v_L, v_R, rho_L, rho_R, p_L, p_R, b_L, b_R;
  for(int j=ghosts-1; j<n_bs-1-ghosts+1; ++j) {
    find_a_L_a_R(v_bs[j], v_bs[j+1], vs[j], &v_L, &v_R);
    vLs[j] = v_L;
    vRs[j] = v_R;
    find_a_L_a_R(rho_bs[j], rho_bs[j+1], rhos[j], &rho_L, &rho_R);
    rhoLs[j] = rho_L;
    rhoRs[j] = rho_R;
    find_a_L_a_R(p_bs[j], p_bs[j+1], ps[j], &p_L, &p_R);
    pLs[j] = p_L;
    pRs[j] = p_R;
    find_a_L_a_R(b_bs[j], b_bs[j+1], bs[j], &b_L, &b_R);
    bLs[j] = b_L;
    bRs[j] = b_R;
  }
}


void find_left_and_right_states_at_each_boundary(std::vector<double> &m_bs, std::vector<double> &r_bs,
						 std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
						 std::vector<double> &vLs, std::vector<double> &vRs, std::vector<double> &rhoLs, std::vector<double> &rhoRs,
						 std::vector<double> &pLs, std::vector<double> &pRs, std::vector<double> &bLs, std::vector<double> &bRs,
						 int ghosts, double gamma_ad, int alpha, double dt,
						 std::vector<double> &vL_bs, std::vector<double> &vR_bs, std::vector<double> &rhoL_bs, std::vector<double> &rhoR_bs,
						 std::vector<double> &pL_bs, std::vector<double> &pR_bs, std::vector<double> &bL_bs, std::vector<double> &bR_bs) {

  // Finding a left and right v, rho, p and b at each boundary
  int n_bs = m_bs.size();
  double gamma_j, h_j, h_star_j, A_j, c_s, v_A, c_s_star, dm_j_l, dm_j_r;
  double Delta_m;
  for(int j=ghosts-1; j<n_bs-ghosts; ++j) {
    Delta_m = m_bs[j+1]-m_bs[j];
    h_j = 1. + gamma_ad*ps[j]/(gamma_ad-1.)/rhos[j];
    h_star_j = 1. + gamma_ad*ps[j]/(gamma_ad-1.)/rhos[j] + bs[j]*bs[j]/rhos[j];
    c_s = sqrt(gamma_ad*ps[j]/h_j/rhos[j]);
    v_A = sqrt(bs[j]*bs[j]/h_star_j/rhos[j]);
    c_s_star = sqrt((1.-v_A*v_A)*c_s*c_s + v_A*v_A);
    A_j = 1./(1.+alpha)*(pow(r_bs[j+1], 1.+alpha) - pow(r_bs[j], 1.+alpha))/(r_bs[j+1] - r_bs[j]);

    gamma_j = 1./sqrt(1.-vs[j]*vs[j]);
    dm_j_l = A_j*rhos[j]*c_s_star*dt/gamma_j/(1.-vs[j]*c_s_star);
    dm_j_r = A_j*rhos[j]*c_s_star*dt/gamma_j/(1.+vs[j]*c_s_star);

    vL_bs[j+1] = find_a_pjphalf(vs[j], vLs[j], vRs[j], dm_j_l/Delta_m);
    vR_bs[j] = find_a_mjmhalf(vs[j], vLs[j], vRs[j], dm_j_r/Delta_m);
    rhoL_bs[j+1] = find_a_pjphalf(rhos[j], rhoLs[j], rhoRs[j], dm_j_l/Delta_m);
    rhoR_bs[j] = find_a_mjmhalf(rhos[j], rhoLs[j], rhoRs[j], dm_j_r/Delta_m);
    pL_bs[j+1] = find_a_pjphalf(ps[j], pLs[j], pRs[j], dm_j_l/Delta_m);
    pR_bs[j] = find_a_mjmhalf(ps[j], pLs[j], pRs[j], dm_j_r/Delta_m);
    bL_bs[j+1] = find_a_pjphalf(bs[j], bLs[j], bRs[j], dm_j_l/Delta_m);
    bR_bs[j] = find_a_mjmhalf(bs[j], bLs[j], bRs[j], dm_j_r/Delta_m);

  }
}

void solve_the_Riemann_problem(std::vector<double> &vL_bs, std::vector<double> &vR_bs, std::vector<double> &rhoL_bs, std::vector<double> &rhoR_bs,
			       std::vector<double> &pL_bs, std::vector<double> &pR_bs, std::vector<double> &bL_bs, std::vector<double> &bR_bs,
			       int ghosts, double gamma_ad, int wavespeed, int iter,
			       std::vector<double> &bar_v_bs, std::vector<double> &bar_p_bs) {

  // Solving the Riemann problems to get bar(v) and bar(p) at the boundaries [MAKE NEW FUNCTION]
  int n_bs = vL_bs.size();
  double v_L, rho_L, p_L, b_L, v_R, rho_R, p_R, b_R;
  double bar_p, bar_v;
  for(int i=ghosts; i<n_bs-ghosts; ++i) {

    v_L = vL_bs[i];
    v_R = vR_bs[i];
    rho_L = rhoL_bs[i];
    rho_R = rhoR_bs[i];
    p_L = pL_bs[i];
    p_R = pR_bs[i];
    b_L = bL_bs[i];
    b_R = bR_bs[i];

    Riemann_problem(p_L, rho_L, v_L, b_L, p_R, rho_R, v_R, b_R, &bar_p, &bar_v, gamma_ad, wavespeed, iter, i);

    bar_v_bs[i] = bar_v;
    bar_p_bs[i] = bar_p;

  }
}


void compute_the_next_r_boundaries(std::vector<double> &r_bs, std::vector<double> &bar_v_bs, int ghosts, double dt, int alpha,
				   std::vector<double> &next_r_bs, std::vector<double> &bar_A_bs) {

  int n_bs = r_bs.size();
  for(int i=ghosts; i<n_bs-ghosts; ++i) {
    next_r_bs[i] = get_next_r_jhalf(r_bs[i], bar_v_bs[i], dt);
    bar_A_bs[i] = get_bar_A_jhalf(r_bs[i], next_r_bs[i], bar_v_bs[i], dt, alpha);
  }

  // Computing the next r-boundary also for the ghosts
  for(int i=0; i<ghosts; i++) {
    next_r_bs[i] = get_next_r_jhalf(r_bs[i], bar_v_bs[ghosts], dt);
    /* bar_A_bs[i] = get_bar_A_jhalf(r_bs[i], next_r_bs[i], bar_v_bs[i], dt); */
  }

  for(int i=n_bs-ghosts; i<n_bs; i++) {
    next_r_bs[i] = get_next_r_jhalf(r_bs[i], bar_v_bs[n_bs-ghosts-1], dt);
    /* bar_A_bs[i] = get_bar_A_jhalf(r_bs[i], next_r_bs[i], bar_v_bs[i], dt); */
  }
}

void compute_the_next_consts(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &next_r_bs,
			     std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
			     std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms,
			     std::vector<double> &bar_A_bs, std::vector<double> &bar_v_bs, std::vector<double> &bar_p_bs,
			     const std::vector<double> &G0s, const std::vector<double> &G1s, const std::vector<double> &Phidots,
			     int ghosts, double dt, int alpha,
			     std::vector<double> &next_V_ms, std::vector<double> &next_S_ms, std::vector<double> &next_E_ms, std::vector<double> &next_M_ms) {

  int n_bs = m_bs.size();
  double r, Delta_m, H0, H1, HPhi;

  for(int j=ghosts; j<n_bs-1-ghosts; j++) {
    // Computing forces
    if(alpha == 2) {
      r = get_avg_r(r_bs[j], r_bs[j+1], alpha);
      H1 = (G1s[j]+2.*ps[j]/r)*sqrt(1.-vs[j]*vs[j])/rhos[j];
    } else if(alpha == 1) {
      r = get_avg_r(r_bs[j], r_bs[j+1], alpha);
      H1 = (G1s[j]+1.*ps[j]/r)*sqrt(1.-vs[j]*vs[j])/rhos[j];
    } else {
      H1 = G1s[j]*sqrt(1.-vs[j]*vs[j])/rhos[j];
    }
    H0 = G0s[j]*sqrt(1.-vs[j]*vs[j])/rhos[j];
    HPhi = Phidots[j]*sqrt(1.-vs[j]*vs[j])/rhos[j];

    // // Correcting H0 if needed
    // double xi_H0 = 1e-1;
    // if(-H0*dt > xi_H0*E_ms[j]) {
    //   H0 = -xi_H0*E_ms[j]/dt;
    // }

    // Updating
    Delta_m = m_bs[j+1]-m_bs[j];
    next_V_ms[j] = get_next_V_m_j(next_r_bs[j+1], next_r_bs[j], Delta_m, alpha);
    next_S_ms[j] = get_next_S_m_j(S_ms[j], bar_A_bs[j+1], bar_A_bs[j], bar_p_bs[j+1], bar_p_bs[j], dt, Delta_m, H1);
    next_E_ms[j] = get_next_E_m_j(E_ms[j], bar_A_bs[j+1], bar_A_bs[j], bar_v_bs[j+1], bar_v_bs[j], bar_p_bs[j+1], bar_p_bs[j], dt, Delta_m, H0);
    next_M_ms[j] = get_next_M_m_j(M_ms[j], dt, HPhi);

    // if(next_E_ms[j] < 0) {
    //   std::cout << "xxx HYDRO ERROR: E_m < 0" << std::endl;
    //   std::cout << "j = " << j << std::endl;
    //   std::cout << "E_ms[" << j << "] = " << E_ms[j] << ", next_E_ms[" << j << "] = " << next_E_ms[j] << std::endl;
    //   std::cout << "H0 = " << H0 << ", H0*dt = " << H0*dt << ", dt = " << dt << std::endl;
    //   // std::cout << "SOURCE = " << SOURCE << ", SOURCE*dt = " << SOURCE*dt << std::endl;
    //   std::cout << "p = " << ps[j] << std::endl << std::endl;
    //   // std::cout << "iter = " << iter << std::endl << std::endl;
    // }

  }

}

void reconstruct_prims(std::vector<double> &r_bs, std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms,
		       int ghosts, int alpha, double gamma_ad,
		       std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs) {

  int OK = 1, n_bs = r_bs.size();
  double r;
  for(int j=ghosts; j<n_bs-1-ghosts; j++) {
    r = get_avg_r(r_bs[j], r_bs[j+1], alpha);
    OK = reconstruct_v_rho_p_b(V_ms[j], S_ms[j], E_ms[j], M_ms[j], &vs[j], &rhos[j], &ps[j], &bs[j], r, alpha, gamma_ad);
    if(OK == 0) {
    }
  }
}

void floor_pressure(std::vector<double> &ps, double p_min) {
  for(unsigned int j=0; j<ps.size(); j++) {
    if(ps[j] < p_min) {
      ps[j] = p_min;
    }
  }
}

void save_vector_to_file(std::vector<double>& Vs, const std::string& filename, int RANK, int iter) {

  std::string full_filename = filename + "_R" + std::to_string(RANK) + ".dbg";
  std::ofstream file;
  if(iter == 0) {
    file.open(full_filename, std::fstream::trunc);
    file << std::scientific;
    file.precision(12);
  } else {
    file.open(full_filename, std::fstream::app);
    file << std::scientific;
    file.precision(12);
  }

  for(unsigned int i=0; i<Vs.size(); i++) {file << " " << Vs[i];}
  file << std::endl;

  file.close();

}

int take_mhd_step(double dt, std::vector<double> &m_bs, std::vector<double> &r_bs,
		  std::vector<double> &V_ms, std::vector<double> &S_ms, std::vector<double> &E_ms, std::vector<double> &M_ms,
		  std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
		  const std::vector<double> &G0s, const std::vector<double> &G1s, const std::vector<double> &Phidots,
		  const int n_bs, const int alpha, const double gamma_ad, const int boundary_L, const int boundary_R,
		  const int debug_start_iter, const int debug_end_iter,
		  const int wavespeed, const int iter, const int RANK) {

  int ghosts = 2;
  double p_min = 1e-24;

  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(vs, "snapshot_files/_vs", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(rhos, "snapshot_files/_rhos", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(ps, "snapshot_files/_ps", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(vs, "snapshot_files/_bs", RANK, iter);}

  // DEBUG: Check grid integrity
  if(check_prims_integrity(vs, rhos, ps, bs, iter, RANK, "[start of take_mhd_step()]") == 0) {return 0;}
  if(check_consts_integrity(V_ms, S_ms, E_ms, M_ms, iter, RANK, "[start of take_mhd_step()]") == 0) {return 0;}

  // Use the piecewise parabolic method (PPM)
  std::vector<double> v_bs(n_bs), rho_bs(n_bs), p_bs(n_bs), b_bs(n_bs);
  do_ppm(m_bs, vs, rhos, ps, bs, ghosts, v_bs, rho_bs, p_bs, b_bs);
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(v_bs, "snapshot_files/_v_bs_A", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(rho_bs, "snapshot_files/_rho_bs_A", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(p_bs, "snapshot_files/_p_bs_A", RANK, iter);}
  fix_ppm_edges(vs, rhos, ps, bs, ghosts, v_bs, rho_bs, p_bs, b_bs);
  // fix_periodic_boundaries(ghosts, boundary_L, boundary_R, v_bs, rho_bs, p_bs, b_bs);


  // Finding the left and right v, rho, p, b at each j (including closest ghost cells)
  std::vector<double> vLs(n_bs-1), rhoLs(n_bs-1), pLs(n_bs-1), bLs(n_bs-1);
  std::vector<double> vRs(n_bs-1), rhoRs(n_bs-1), pRs(n_bs-1), bRs(n_bs-1);
  find_left_and_right_states_inside_each_bin(vs, rhos, ps, bs, v_bs, rho_bs, p_bs, b_bs, ghosts, vLs, vRs, rhoLs, rhoRs, pLs, pRs, bLs, bRs);

  // Finding a left and right v, rho, p and b at each boundary
  std::vector<double> vL_bs(n_bs), rhoL_bs(n_bs), pL_bs(n_bs), bL_bs(n_bs);
  std::vector<double> vR_bs(n_bs), rhoR_bs(n_bs), pR_bs(n_bs), bR_bs(n_bs);
  find_left_and_right_states_at_each_boundary(m_bs, r_bs, vs, rhos, ps, bs, vLs, vRs, rhoLs, rhoRs, pLs, pRs, bLs, bRs, ghosts,
					      gamma_ad, alpha, dt, vL_bs, vR_bs, rhoL_bs, rhoR_bs, pL_bs, pR_bs, bL_bs, bR_bs);

  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(vL_bs, "snapshot_files/_vL_bs", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(vR_bs, "snapshot_files/_vR_bs", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(pL_bs, "snapshot_files/_pL_bs", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(pR_bs, "snapshot_files/_pR_bs", RANK, iter);}

  // Solving the Riemann problems to get bar(v) and bar(p) at the boundaries
  std::vector<double> bar_v_bs(n_bs), bar_p_bs(n_bs);
  solve_the_Riemann_problem(vL_bs, vR_bs, rhoL_bs, rhoR_bs, pL_bs, pR_bs, bL_bs, bR_bs,
			    ghosts, gamma_ad, wavespeed, iter, bar_v_bs, bar_p_bs);
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bar_v_bs, "snapshot_files/_bar_v_bs", RANK, iter);}
  if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bar_p_bs, "snapshot_files/_bar_p_bs", RANK, iter);}

  floor_pressure(bar_p_bs, p_min);
  if(check_prims_integrity(bar_v_bs, rhos, bar_p_bs, bs, iter, RANK, "[after Riemann solver (checking bar_v_bs)]") == 0) {return 0;}

  // Computing next r-boundary vector and the A vector
  std::vector<double> next_r_bs(n_bs), bar_A_bs(n_bs);
  compute_the_next_r_boundaries(r_bs, bar_v_bs, ghosts, dt, alpha, next_r_bs, bar_A_bs);

  // Updating V_m, S_m, E_m, M_m
  std::vector<double> next_V_ms(n_bs-1), next_S_ms(n_bs-1), next_E_ms(n_bs-1), next_M_ms(n_bs-1);
  compute_the_next_consts(m_bs, r_bs, next_r_bs, vs, rhos, ps, bs, S_ms, E_ms, M_ms, bar_A_bs, bar_v_bs, bar_p_bs,
			  G0s, G1s, Phidots, ghosts, dt, alpha, next_V_ms, next_S_ms, next_E_ms, next_M_ms);

  if(check_consts_integrity(V_ms, S_ms, E_ms, M_ms, iter, RANK, "[old consts, before refreshing]") == 0) {return 0;}

  if(check_consts_integrity(next_V_ms, next_S_ms, next_E_ms, next_M_ms, iter, RANK, "[new consts, but before refreshing]") == 0) {

    // save_vector_to_file(vs, "snapshot_files/_vs", RANK, iter);
    // save_vector_to_file(rhos, "snapshot_files/_rhos", RANK, iter);
    // save_vector_to_file(ps, "snapshot_files/_ps", RANK, iter);

    // save_vector_to_file(v_bs, "snapshot_files/_v_bs", RANK, iter);
    // save_vector_to_file(rho_bs, "snapshot_files/_rho_bs", RANK, iter);
    // save_vector_to_file(p_bs, "snapshot_files/_p_bs", RANK, iter);

    // save_vector_to_file(vL_bs, "snapshot_files/_vL_bs", RANK, iter);
    // save_vector_to_file(vR_bs, "snapshot_files/_vR_bs", RANK, iter);
    // save_vector_to_file(rhoL_bs, "snapshot_files/_rhoL_bs", RANK, iter);
    // save_vector_to_file(rhoR_bs, "snapshot_files/_rhoR_bs", RANK, iter);
    // save_vector_to_file(pL_bs, "snapshot_files/_pL_bs", RANK, iter);
    // save_vector_to_file(pR_bs, "snapshot_files/_pR_bs", RANK, iter);

    // save_vector_to_file(bar_v_bs, "snapshot_files/_bar_v_bs", RANK, iter);
    // save_vector_to_file(bar_p_bs, "snapshot_files/_bar_p_bs", RANK, iter);

    return 0;
  }

  // Refreshing r_boundaries and consts
  r_bs = next_r_bs;
  V_ms = next_V_ms;
  S_ms = next_S_ms;
  E_ms = next_E_ms;
  M_ms = next_M_ms;

  // DEBUG: Check grid integrity
  if(check_consts_integrity(V_ms, S_ms, E_ms, M_ms, iter, RANK, "[after refreshing consts]") == 0) {return 0;}

  // Reconstructing v, rho, p and b from refreshed V_m, S_m, E_m and M_m
  reconstruct_prims(r_bs, V_ms, S_ms, E_ms, M_ms, ghosts, alpha, gamma_ad, vs, rhos, ps, bs);

  // // DEBUG: Check grid integrity
  // if(check_prims_integrity(vs, rhos, ps, bs, iter, RANK, "[after reconstructing prims]") == 0) {return 0;}

  // If needed, floor the pressure
  floor_pressure(ps, p_min);

  // DEBUG: Check grid integrity
  if(check_prims_integrity(vs, rhos, ps, bs, iter, RANK, "[end of take_mhd_step()]") == 0) {return 0;}
  if(check_consts_integrity(V_ms, S_ms, E_ms, M_ms, iter, RANK, "[end of take_mhd_step()]") == 0) {return 0;}

  // DEBUG: Save all itermediate data? [23 grids]

  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(v_bs, "snapshot_files/_v_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(rho_bs, "snapshot_files/_rho_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(p_bs, "snapshot_files/_p_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(b_bs, "snapshot_files/_b_bs", RANK, iter);}

  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(vLs, "snapshot_files/_vLs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(rhoLs, "snapshot_files/_rhoLs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(pLs, "snapshot_files/_pLs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bLs, "snapshot_files/_bLs", RANK, iter);}

  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(vRs, "snapshot_files/_vRs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(rhoRs, "snapshot_files/_rhoRs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(pRs, "snapshot_files/_pRs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bRs, "snapshot_files/_bRs", RANK, iter);}

  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(vL_bs, "snapshot_files/_vL_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(rhoL_bs, "snapshot_files/_rhoL_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(pL_bs, "snapshot_files/_pL_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bL_bs, "snapshot_files/_bL_bs", RANK, iter);}

  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(vR_bs, "snapshot_files/_vR_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(rhoR_bs, "snapshot_files/_rhoR_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(pR_bs, "snapshot_files/_pR_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bR_bs, "snapshot_files/_bR_bs", RANK, iter);}

  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bar_v_bs, "snapshot_files/_bar_v_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bar_p_bs, "snapshot_files/_bar_p_bs", RANK, iter);}
  // if(debug_start_iter <= iter && iter <= debug_end_iter) {save_vector_to_file(bar_A_bs, "snapshot_files/_bar_A_bs", RANK, iter);}

  return 1;
}
