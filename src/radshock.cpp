//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include "mhd/parameter_parser.h"
#include "mhd/stopwatch.h"
#include "mhd/dbg.h"
#include "mhd/main_helper_functions.h"
#include "mhd/mhd_manager.h"
#include "mhd/amr_manager.h"
#include "rad/rad_manager.h"

// -------------------
//  Piston boundaries
// -------------------

double get_linear_piston_boundary_momentum(double t, std::vector<double> params) {
  if(params.size() != 4) {

    std::cout << "Warning: piston boundary condition used, but no parameters given!" << std::endl;
    return 0;

  } else {

    double t1 = params[0];
    double t2 = params[1];
    double y1 = params[2];
    double y2 = params[3];

    if(t <= t1) {
      return y1;
    } else if(t >= t2) {
      return y2;
    } else {
      double x = 3.1415*((t-t1)/(t2-t1)-.5);
      return y1 + .5*(sin(x)+1.)*(y2-y1);
      // return y1 + (t-t1)/(t2-t1)*(y2-y1);
    }
  }
}

// ---------
//  Sources
// ---------

void set_problem_sources(std::vector<double> &G0s, std::vector<double> &G1s) {}

// -----------
//  Main loop
// -----------

int main(int argc, char* argv[]) {

  if(argc != 2) {
    std::cout << "ERROR: need to provide parameter file name." << std::endl;
    std::cout << "Example: $ mpirun -np 2 radshock <param_file.txt>" << std::endl;
    std::cout << "Exiting..." << std::endl;
    return 0;
  }

  // For handling crashes
  int save_every_nth_mhd_iter_when_approaching_crash = 30;
  int save_every_nth_rad_iter_when_approaching_crash = 10;
  int approaching_crash_by_too_many_iterations = 30;
  std::string crash_msg = "";

  // Loading the parameter file (filename = argv[1])
  int OK = 1;
  int mhd_iters_between_last_two_saves = 1e6;
  int mhd_iters_since_last_save = 0;
  int crash_mode = 0;
  ParameterParser parameter_parser;
  OK = parameter_parser.read_parameter_file(argv[1]);
  if(OK == 0) {
    std::cout << "ERROR: could not open parameter file." << std::endl;
    std::cout << "Exiting..." << std::endl;
    return 0;
  }

  // Parameters to implement in the parser

  // Importing the parameters
  std::string mhd_input_state_file = parameter_parser.get_mhd_input_state_file();
  std::string rad_input_state_file = parameter_parser.get_rad_input_state_file();
  std::string mhd_output_state_file = parameter_parser.get_mhd_output_state_file();
  std::string rad_output_state_file = parameter_parser.get_rad_output_state_file();
  std::string mhd_output_snapshot_file = parameter_parser.get_mhd_output_snapshot_file();
  int debug = parameter_parser.get_debug();
  int debug_start_mhd_iter = parameter_parser.get_debug_start_mhd_iter();
  int debug_end_mhd_iter = parameter_parser.get_debug_end_mhd_iter();
  int n_ts = parameter_parser.get_n_ts();
  double t_i = parameter_parser.get_t_i();
  double t_f = parameter_parser.get_t_f();
  int log_spaced_time = parameter_parser.get_log_spaced_time();
  int n_ss = parameter_parser.get_n_ss();
  int save_every_nth_rad_iter = parameter_parser.get_save_every_nth_rad_iter();
  int save_every_nth_mhd_iter = parameter_parser.get_save_every_nth_mhd_iter();
  double xi_mhd = parameter_parser.get_xi_mhd();
  double xi_rad = parameter_parser.get_xi_rad();
  int seed = parameter_parser.get_seed();
  int load_photon_multiplier = parameter_parser.get_load_photon_multiplier();
  int save_photon_divider = parameter_parser.get_save_photon_divider();
  double m_grav = parameter_parser.get_m_grav();
  double tau_smoothing = parameter_parser.get_tau_smoothing();
  int use_pairs = parameter_parser.get_use_pairs();
  int rad_source_terms = parameter_parser.get_rad_source_terms();
  int ignore_crash_warning = parameter_parser.get_ignore_crash_warning();
  int boundary_L = parameter_parser.get_boundary_L();
  int boundary_R = parameter_parser.get_boundary_R();
  std::vector<double> boundary_L_params = parameter_parser.get_boundary_L_params();
  std::vector<double> boundary_R_params = parameter_parser.get_boundary_R_params();
  int two_temp_plasma = parameter_parser.get_two_temp_plasma();
  int uniform_spec_radius_bins = parameter_parser.get_uniform_spec_radius_bins();
  double tau_merge_limit = parameter_parser.get_tau_merge_limit();
  int in_debug_range = 0;

  // Terminal output formatting
  std::string start_time = get_current_time_string();
  std::cout << std::scientific;
  std::cout.precision(8);

  // Declaring all vectors
  int n_bs = 1;
  std::vector<double> m_bs(n_bs), r_bs(n_bs), vs(n_bs-1), rhos(n_bs-1), ps(n_bs-1), bs(n_bs-1);
  std::vector<double> V_ms(n_bs-1), S_ms(n_bs-1), E_ms(n_bs-1), M_ms(n_bs-1);
  std::vector<double> G0s(n_bs-1), G1s(n_bs);
  std::vector<double> I_0s(n_bs-1), I_1s(n_bs), theta_Cs(n_bs);
  std::vector<double> Z_pms(n_bs-1, 1);

  // NEW: two-temp
  std::vector<double> xis(n_bs-1);

  //

  // ::: Initiating the MHD and RAD managers :::
  std::string sim_name = mhd_input_state_file.substr(0, mhd_input_state_file.find("."));
  if(sim_name.find("/") != 0) {
    sim_name = sim_name.substr(sim_name.find("/")+1);
  }

  MHDManager mhd_manager(sim_name);
  RADManager rad_manager(sim_name, seed+mhd_manager.get_rank());

  if(rad_input_state_file == "None") {
    rad_manager.set_disabled(1);
  }

  // Printing start message
  if(mhd_manager.is_master()) {
    if(debug) {
      parameter_parser.print_parameters();
    }

    // print_current_time();
    // std::cout << " Simulation name: " << sim_name << std::endl;
  }

  // Load states
  if(mhd_manager.is_master()) {
    print_current_time();
    std::cout << " Loading MHD state from '" << mhd_input_state_file << "'" << std::endl;
  }
  mhd_manager.load_state(mhd_input_state_file);
  mhd_manager.set_mhd_output_state_file(mhd_output_state_file);
  mhd_manager.set_mhd_output_snapshot_file(mhd_output_snapshot_file);
  mhd_manager.set_boundary_L(boundary_L);
  mhd_manager.set_boundary_R(boundary_R);
  if(mhd_manager.get_boundary_L() == 4) {
    if(boundary_L_params[2] == 0. && boundary_L_params[3] == 0) {
      mhd_manager.get_grid(m_bs, r_bs, vs, rhos, ps, bs);
      boundary_L_params[2] = vs[2]/sqrt(1.-vs[2]*vs[2]);
      boundary_L_params[3] = vs[2]/sqrt(1.-vs[2]*vs[2]);
    }
    mhd_manager.set_piston_L(get_linear_piston_boundary_momentum(mhd_manager.get_t(), boundary_L_params));
  }
  if(mhd_manager.get_boundary_R() == 4) {
    if(boundary_R_params[2] == 0. && boundary_R_params[3] == 0) {
      mhd_manager.get_grid(m_bs, r_bs, vs, rhos, ps, bs);
      boundary_R_params[2] = vs[vs.size()-3]/sqrt(1.-vs[vs.size()-3]*vs[vs.size()-3]);
      boundary_R_params[3] = vs[vs.size()-3]/sqrt(1.-vs[vs.size()-3]*vs[vs.size()-3]);
    }
    mhd_manager.set_piston_R(get_linear_piston_boundary_momentum(mhd_manager.get_t(), boundary_R_params));
  }
  mhd_manager.set_ghost_cells_wrapper();
  mhd_manager.set_grav_mass(m_grav);
  mhd_manager.get_grid(m_bs, r_bs, vs, rhos, ps, bs);
  mhd_manager.get_grid_consts(m_bs, V_ms, S_ms, E_ms, M_ms);
  if(rad_manager.is_enabled()) {
    if(mhd_manager.is_master()) {
      print_current_time();
      std::cout << " Loading RAD state from '" << rad_input_state_file << "'" << std::endl;
    }
    rad_manager.set_load_photon_multiplier(load_photon_multiplier);
    rad_manager.set_save_photon_divider(save_photon_divider);
    rad_manager.load_state(rad_input_state_file);
    rad_manager.set_rad_output_state_file(rad_output_state_file);
    rad_manager.set_rad_output_snapshot_file(mhd_output_snapshot_file);
    rad_manager.set_boundary_L(boundary_L);
    rad_manager.set_boundary_R(boundary_R);
    rad_manager.get_Z_pms(Z_pms);
    mhd_manager.get_xis(xis);
    rad_manager.set_grid(m_bs, r_bs, vs, rhos, ps, bs, Z_pms, xis);
    rad_manager.set_tau_smoothing(tau_smoothing);
    rad_manager.set_use_pairs(use_pairs);
    rad_manager.set_uniform_spec_radius_bins(uniform_spec_radius_bins);
  }

  if(mhd_manager.is_master()) {
    if(n_ss > 0) {
      print_current_time();
      std::cout << " Saving MHD state to '" << mhd_output_state_file << "'" << std::endl;
      if(rad_manager.is_enabled()) {
	print_current_time();
	std::cout << " Saving RAD state to '" << rad_output_state_file << "'" << std::endl;
      }
    }
    print_current_time();
    std::cout << " Saving snapshots to '" << mhd_output_snapshot_file.substr(0, mhd_output_snapshot_file.find(".")) << ".*'" << std::endl;
    print_current_time();
    std::cout << std::endl;
  }

  //

  // ::: Initiating the AMR manager :::
  AMRManager amr_manager;
  amr_manager.set_max_lvl(0);
  amr_manager.set_initial_mass_grid(m_bs);

  // Find current save time
  int k = 1;

  std::vector<double> ts;
  if(log_spaced_time == 1) {
    ts = mhd_manager.get_logarithmic_time_vector(t_i, t_f, n_ts);
  } else {
    ts = mhd_manager.get_linear_time_vector(t_i, t_f, n_ts);
  }
  while((1. + 1e-9)*mhd_manager.get_t() > ts[k]) {k += 1;}

  if(debug && mhd_manager.is_master()) {
    std::cout << "Save times:" << std::endl;
    for(int k=0; k<n_ts; k++) {
      std::cout << "ts[" << k << "] = " << ts[k];
      if(n_ss > 0) {
	if((k+1) % (n_ts/n_ss) == 0) {
	  std::cout << " [save state]";
	}
      }
      std::cout << std::endl;
    }
    std::cout << "mhd_manager.get_t() = " << mhd_manager.get_t() << std::endl;
  }

  // Stopwatches
  Stopwatch tot_stopwatch;
  Stopwatch sav_stopwatch;
  tot_stopwatch.set_watch_number(0);
  sav_stopwatch.set_watch_number(1);

  // Debugging printer
  //   Example usage:
  //   dbg.iter();
  //   dbg.mess("message");
  //   dbg.messint("message", int_number);
  //   dbg.messdoub("message", double_number);
  // DBG dbg;

  // // Save initial state
  // mhd_manager.save_initial_conditions();
  // if(rad_manager.is_enabled()) {
  //   rad_manager.save_initial_conditions();
  // }
  // mhd_manager.save_time_file_with_rad(k, 0., 0., 0., mhd_manager.get_t(), mhd_manager.get_iter(), rad_manager.get_iter(), 0., 0., 0., 0., 0., 0., 0.);

  // // Do a first computation of calI_primes, for the alpha_ggs computation
  // if(use_pairs == 1 && (rad_source_terms == 1 || rad_source_terms == 2)) {
  //   // Compute calI_primes, which is needed for pair production
  //   rad_manager.compute_calI_primes_and_sync();
  // }

  // // Print first iteration
  // if(mhd_manager.is_master()) {
  //   print_current_time();
  //   mhd_manager.print_progress(ts[n_ts-1], k, n_ts);
  //   if(rad_manager.is_enabled()) {
  //     std::cout << "iter_rad =    " << rad_manager.get_iter();
  //     std::cout << "    RAD/MHD =  " << "-----------";
  //     std::cout << "    n_phs/thread = " << rad_manager.get_n_phs() << "    max(Z_pms)-1 = " << std::fixed << get_vector_max(Z_pms)-1 << std::scientific;
  //   }
  //   std::cout << std::endl;
  // }

  // Save initial state
  if(rad_manager.is_disabled()) {
    mhd_manager.save_initial_conditions();
    mhd_manager.save_time_file_with_rad(k, 0., 0., 0., mhd_manager.get_t(), mhd_manager.get_iter(), rad_manager.get_iter(), 0., 0., 0., 0., 0., 0., 0.);

    // Print first iteration
    if(mhd_manager.is_master()) {
      print_current_time();
      mhd_manager.print_progress(ts[n_ts-1], k, n_ts);
      std::cout << std::endl;
    }
  }

  // Loopin'
  int first_iteration = 1;
  int save = 0;
  double dt_rad, dt_mhd;
  int mhd_has_caught_up = 0;
  tot_stopwatch.start();
  while(k < n_ts) {

    if(rad_manager.is_enabled()) {

      // Get radiation time step
      dt_rad = xi_rad*rad_manager.get_crossing_time_step_wrapper();

      // Check if save
      if(rad_manager.get_t() + dt_rad > ts[k]) {
	save = 1;
	dt_rad = ts[k]-rad_manager.get_t();
	k += 1;
      } else {
	save = 0;
      }

      // // If using moments, get sources from radiation BEFORE updating photons (else they can fly off the grid at large bulk speeds)
      // // if(rad_source_terms == 3) {
      // 	// If use_pairs == 1, this function also computes calI_primes, which is needed for pair production
      // rad_manager.get_radiation_moments_v2(I_0s, I_1s, theta_Cs, Z_pms, use_pairs); // MPI
      // // }

      // Update radiation
      rad_manager.update_single_step(dt_rad); // MPI
      rad_manager.get_Z_pms(Z_pms);

      // Save initial state? (Doing this after the radiation step, so that I_0s_prop etc. has "filled up" before the initial save
      if(first_iteration == 1) {
	first_iteration = 0;
	mhd_manager.save_initial_conditions();
	if(rad_manager.is_enabled()) {
	  rad_manager.save_initial_conditions();
	}
	int k_p = k;
	if(save == 1) {k_p -= 1;}
	mhd_manager.save_time_file_with_rad(k_p, 0., 0., 0., mhd_manager.get_t(), mhd_manager.get_iter(), rad_manager.get_iter(), 0., 0., 0., 0., 0., 0., 0.);
	// mhd_manager.save_time_file_with_rad(1, 0., 0., 0., mhd_manager.get_t(), mhd_manager.get_iter(), rad_manager.get_iter(), 0., 0., 0., 0., 0., 0., 0.);

	// Print first iteration
	if(mhd_manager.is_master()) {
	  print_current_time();
	  mhd_manager.print_progress(ts[n_ts-1], k_p, n_ts);
	  if(rad_manager.is_enabled()) {
	    std::cout << "iter_rad =    " << rad_manager.get_iter();
	    std::cout << "    RAD/MHD =  " << "-----------";
	    std::cout << "    n_phs/thread = " << rad_manager.get_n_phs() << "    max(Z_pms)-1 = " << std::fixed << get_vector_max(Z_pms)-1 << std::scientific;
	  }
	  std::cout << std::endl;
	}
      }

      // New: computing moments during radiation propagation!
      rad_manager.get_radiation_moments_from_prop(I_0s, I_1s, theta_Cs);

      for(unsigned int j=2; j<theta_Cs.size()-2; j++) {

	if(theta_Cs[j] <= 0) {
	  std::cout << "zero or negative theta_Cs[" << j << "] = " << theta_Cs[j] << std::endl;
	}

	if(I_0s[j] <= 0) {
	  std::cout << "zero or negative I_0s[" << j << "] = " << I_0s[j] << std::endl;
	}
      }

      // Get sources from radiation
      if(rad_source_terms == 1) {
	rad_manager.get_all_sources(G0s, G1s);
      } else if(rad_source_terms == 2) {
	rad_manager.get_G_sc_sources(G0s, G1s);
      }//  else if(rad_source_terms == 3) {
      // 	// If use_pairs == 1, this function also computes calI_primes, which is needed for pair production
      // 	rad_manager.get_radiation_moments_v2(I_0s, I_1s, theta_Cs, Z_pms, use_pairs); // MPI
      // }

      if(use_pairs == 1 && (rad_source_terms == 1 || rad_source_terms == 2)) {
      	// Compute calI_primes, which is needed for pair production
      	rad_manager.compute_calI_primes_and_sync();
      }

    } else {

      // Radiation disabled

      // Always save
      save = 1;
      dt_rad = ts[k]-mhd_manager.get_t();
      k += 1;

    }

    // Make MHD catch up to the radiation
    mhd_has_caught_up = 0;
    while(mhd_has_caught_up == 0) {

      // if(mhd_manager.get_iter() > 8000) {
      // 	debug = 1;
      // }

      // Get time step
      if(rad_manager.is_disabled() || rad_source_terms == 0 || rad_source_terms == 1 || rad_source_terms == 2) {
	dt_mhd = xi_mhd*mhd_manager.get_time_step_wrapper();
      } else {
	dt_mhd = xi_mhd*std::min(mhd_manager.get_time_step_wrapper(), mhd_manager.get_cooling_time_step_wrapper(I_0s, I_1s, theta_Cs, Z_pms, rad_manager.get_f_heat()));
      }

      // Decide if MHD has caught up to RAD
      if(rad_manager.is_enabled()) {

	if(mhd_manager.get_t() + dt_mhd > rad_manager.get_t()) {
	  dt_mhd = rad_manager.get_t() - mhd_manager.get_t();
	  mhd_has_caught_up = 1;
	}

      } else {

	if(mhd_manager.get_t() + dt_mhd > ts[k-1]) {
	  // k-1, since k was already updated
	  dt_mhd = ts[k-1] - mhd_manager.get_t();
	  mhd_has_caught_up = 1;
	}

      }

      // Add sources: choose between "real" sources or based on radiation moments
      mhd_manager.flush_sources();

      // "Real" sources
      if(rad_manager.is_enabled() && (rad_source_terms == 1 || rad_source_terms == 2)) {
	mhd_manager.add_G_sources(G0s, G1s);
      }

      // Compute sources each time step based on the intensity moments
      if(rad_manager.is_enabled() && rad_source_terms == 3) {
	// mhd_manager.add_radiation_moment_sources(I_0s, I_1s, theta_Cs, Z_pms, rad_manager.get_f_heat(), rad_manager.get_t());
	mhd_manager.add_radiation_moment_sources_two_temp(I_0s, I_1s, theta_Cs, Z_pms, rad_manager.get_f_heat(), rad_manager.get_t(), two_temp_plasma);
      }

      // Setting piston speed
      if(mhd_manager.get_boundary_L() == 4) {mhd_manager.set_piston_L(get_linear_piston_boundary_momentum(mhd_manager.get_t(), boundary_L_params));}
      if(mhd_manager.get_boundary_R() == 4) {mhd_manager.set_piston_R(get_linear_piston_boundary_momentum(mhd_manager.get_t(), boundary_R_params));}

      // Updating
      OK = mhd_manager.update_single_step(dt_mhd, debug_start_mhd_iter, debug_end_mhd_iter);
      mhd_iters_since_last_save += 1;

      if(debug_start_mhd_iter <= mhd_manager.get_iter()) {
	if(debug_end_mhd_iter == -1 || mhd_manager.get_iter() <= debug_end_mhd_iter) {
	  in_debug_range = 1;
	} else {
	  in_debug_range = 0;
	}
      }

      // Syncing grids over MPI
      OK = mhd_manager.sync_grid(OK);

      // Merge bins? (if optical depth is small)
      if(tau_merge_limit > 0) {
	mhd_manager.check_if_bins_should_merge(tau_merge_limit);
      }

      // NEW: two-temp
      if(rad_source_terms == 3) {
	// Only evolve two temperatures if using radiation moments as source terms
	mhd_manager.evolve_two_temp(dt_mhd);
      }

      // Flooring pressures (if needed)
      if(rad_manager.is_enabled()) {
	// if(rad_source_terms == 3) {
	mhd_manager.floor_pressures(theta_Cs, Z_pms, rad_manager.get_f_heat());
	// }
      }
      mhd_manager.set_ghost_cells_wrapper();

      // Exit if MHD crashed
      if(OK == 0) {
	mhd_has_caught_up = 1;
	k = n_ts;
	save = 1;
	if(mhd_manager.is_master()) {
	  std::cout << "Crash occurred on MHD iteration " << mhd_manager.get_iter() << ", RAD iteration " << rad_manager.get_iter() << std::endl;
	}
      }

      // Evaluate if the code is about to crash
      // if((mhd_manager.is_code_about_to_crash(crash_msg) && crash_mode == 0 && ignore_crash_warning == 0) || mhd_iters_since_last_save > approaching_crash_by_too_many_iterations*mhd_iters_between_last_two_saves) {

      if(crash_mode == 0 && ignore_crash_warning == 0) {
	// if((mhd_iters_since_last_save > approaching_crash_by_too_many_iterations*mhd_iters_between_last_two_saves) || (mhd_manager.is_code_about_to_crash(crash_msg))) {
	// if(mhd_manager.is_code_about_to_crash(crash_msg)) {
	if(mhd_iters_since_last_save > approaching_crash_by_too_many_iterations*mhd_iters_between_last_two_saves) {

	  crash_mode = 1;

	  if(mhd_manager.is_master()) {
	    print_current_time();
	    print_current_time();
	    std::cout << "Warning: " << crash_msg << std::endl;
	    print_current_time();
	    std::cout << "         Will now use:" << std::endl;
	    print_current_time();
	    std::cout << "  save_every_nth_mhd_iter = " << save_every_nth_mhd_iter_when_approaching_crash << std::endl;
	    if(rad_manager.is_enabled()) {
	      print_current_time();
	      std::cout << "  save_every_nth_rad_iter = " << save_every_nth_rad_iter_when_approaching_crash << std::endl;
	    }
	  }

	  if((mhd_iters_since_last_save > approaching_crash_by_too_many_iterations*mhd_iters_between_last_two_saves) && mhd_manager.is_master()) {
	    print_current_time();
	    // std::cout << "  (reason: MHD iterations became very slow)" << std::endl;
	    std::cout << "  reason: MHD became very slow" << std::endl;
	    std::cout << "  mhd_iters_since_last_save = " << mhd_iters_since_last_save << std::endl;
	    std::cout << "  approaching_crash_by_too_many_iterations = " << approaching_crash_by_too_many_iterations << std::endl;
	    std::cout << "  mhd_iters_between_last_two_saves = " << mhd_iters_between_last_two_saves << std::endl;
	  }

	  // Saves data every n'th iteration, to record the crash
	  if(save_every_nth_mhd_iter == 0 || save_every_nth_mhd_iter > save_every_nth_mhd_iter_when_approaching_crash) {
	    save_every_nth_mhd_iter = save_every_nth_mhd_iter_when_approaching_crash;
	  }
	  if(rad_manager.is_enabled()) {
	    if(save_every_nth_rad_iter == 0 || save_every_nth_rad_iter > save_every_nth_rad_iter_when_approaching_crash) {
	      save_every_nth_rad_iter = save_every_nth_rad_iter_when_approaching_crash;
	    }
	  }
	}
      }

      // Save on the n'th MHD iteration?
      if((debug == 1 && in_debug_range == 1) || (debug == 3 && in_debug_range == 1) || (save_every_nth_mhd_iter && !(mhd_manager.get_iter() % save_every_nth_mhd_iter))) {
	// Save snapshot
	sav_stopwatch.start();
	mhd_manager.save_data();
	if(rad_manager.is_enabled()) {
	  rad_manager.save_data(); // MPI
	}

	// Get "wall time" and save time file
	mhd_manager.save_time_file_with_rad(k, tot_stopwatch.get_t(), mhd_manager.get_mhd_stopwatch_t(), rad_manager.get_rad_stopwatch_t(), mhd_manager.get_t(), mhd_manager.get_iter(),
					    rad_manager.get_iter(), mhd_manager.get_mpi_stopwatch_t(), rad_manager.get_mpi_stopwatch_t(), sav_stopwatch.get_t(),
					    rad_manager.get_prop_stopwatch_t(), rad_manager.get_pair_stopwatch_t(), rad_manager.get_smooth_stopwatch_t(),
					    rad_manager.get_mom_stopwatch_t());
	sav_stopwatch.stop();

	if(mhd_manager.is_master()) {
	  print_current_time();
	  std::cout << "Saved after MHD iteration ";
	  if(debug == 1 && in_debug_range == 1) {
	    std::cout << "(mhd_iter = " << mhd_manager.get_iter() << ", debug = " << debug << ")" << std::endl;
	  } else {
	    std::cout << "(mhd_iter = " << mhd_manager.get_iter() << ")" << std::endl;
	  }
	}
      }

    }

    // // If using AMR, uncomment this code
    // // Get current MHD grid
    // mhd_manager.get_grid_consts(m_bs, V_ms, S_ms, E_ms, M_ms);

    // // Adaptive mesh refinement: set current MHD grid, refine it if needed, get the new grid
    // amr_manager.set_consts(V_ms, S_ms, E_ms, M_ms, Z_pms);

    // // Refine and resize
    // mhd_manager.get_grid(m_bs, r_bs, vs, rhos, ps, bs);

    // // amr_manager.refine_grid(ps, rhos);
    // amr_manager.refine_grid_section(3.17e26, 3.57e26);

    // amr_manager.get_mass_grid(m_bs);
    // amr_manager.get_consts(V_ms, S_ms, E_ms, M_ms, Z_pms);
    // amr_manager.resize_other_mhd_vectors(r_bs, vs, rhos, ps, bs, G0s, G1s);
    // amr_manager.resize_other_rad_vectors(I_0s, I_1s, theta_Cs);

    // // Update MHD and radiation grids
    // mhd_manager.set_grid_consts(m_bs, V_ms, S_ms, E_ms, M_ms);
    // mhd_manager.get_grid(m_bs, r_bs, vs, rhos, ps, bs);
    // rad_manager.set_grid(m_bs, r_bs, vs, rhos, ps, bs, Z_pms);
    // rad_manager.get_radiation_moments(I_0s, I_1s, theta_Cs, Z_pms);
    // mhd_manager.floor_pressures(theta_Cs, Z_pms, rad_manager.get_f_heat());

    // If not using the AMR, uncomment this code instead
    if(rad_manager.is_enabled()) {
      mhd_manager.get_grid(m_bs, r_bs, vs, rhos, ps, bs);
      mhd_manager.get_xis(xis);
      rad_manager.set_grid(m_bs, r_bs, vs, rhos, ps, bs, Z_pms, xis);
    }

    // Save on the n'th radiation iteration?
    if(rad_manager.is_enabled() && ((debug == 2 && in_debug_range == 1) || (debug == 3 && in_debug_range == 1) || (save_every_nth_rad_iter && !(rad_manager.get_iter() % save_every_nth_rad_iter)))) {
      // Save snapshot
      sav_stopwatch.start();
      mhd_manager.save_data();
      rad_manager.save_data(); // MPI

      // Get "wall time" and save time file
      mhd_manager.save_time_file_with_rad(k, tot_stopwatch.get_t(), mhd_manager.get_mhd_stopwatch_t(), rad_manager.get_rad_stopwatch_t(), mhd_manager.get_t(), mhd_manager.get_iter(),
					  rad_manager.get_iter(), mhd_manager.get_mpi_stopwatch_t(), rad_manager.get_mpi_stopwatch_t(), sav_stopwatch.get_t(),
					  rad_manager.get_prop_stopwatch_t(), rad_manager.get_pair_stopwatch_t(), rad_manager.get_smooth_stopwatch_t(),
					  rad_manager.get_mom_stopwatch_t());
      sav_stopwatch.stop();

      if(mhd_manager.is_master()) {
	print_current_time();
	std::cout << " Saved after RAD iteration ";
	if(debug == 2 && in_debug_range == 1) {
	  std::cout << "(rad_iter = " << rad_manager.get_iter() << ", debug = " << debug << ", t = " << rad_manager.get_t() << ")" << std::endl;
	} else {
	  std::cout << "(rad_iter = " << rad_manager.get_iter() << ")" << std::endl;
	}
      }
    }

    // Saving
    if(save == 1) {

      // Reset MHD iteration counter
      mhd_iters_between_last_two_saves = mhd_iters_since_last_save;
      mhd_iters_since_last_save = 0;

      // Save snapshot
      sav_stopwatch.start();
      mhd_manager.save_data();
      if(rad_manager.is_enabled()) {
	rad_manager.save_data(); // MPI
      }

      // Get "wall time" and save time file
      mhd_manager.save_time_file_with_rad(k, tot_stopwatch.get_t(), mhd_manager.get_mhd_stopwatch_t(), rad_manager.get_rad_stopwatch_t(), mhd_manager.get_t(), mhd_manager.get_iter(),
					  rad_manager.get_iter(), mhd_manager.get_mpi_stopwatch_t(), rad_manager.get_mpi_stopwatch_t(), sav_stopwatch.get_t(),
					  rad_manager.get_prop_stopwatch_t(), rad_manager.get_pair_stopwatch_t(), rad_manager.get_smooth_stopwatch_t(),
					  rad_manager.get_mom_stopwatch_t());
      sav_stopwatch.stop();

      // Print information
      if(mhd_manager.is_master()) {
	print_current_time();
	mhd_manager.print_progress(ts[n_ts-1], k, n_ts);
	if(rad_manager.is_enabled()) {
	  std::cout << "iter_rad =    " << rad_manager.get_iter() << "    ";
	  std::cout << "RAD/MHD =  " << std::fixed << rad_manager.get_rad_stopwatch_t()/mhd_manager.get_mhd_stopwatch_t() << std::scientific;
	  std::cout << "    n_phs/thread = " << rad_manager.get_n_phs() << "    max(Z_pms)-1 = " << std::fixed << get_vector_max(Z_pms)-1 << std::scientific;
	}
	std::cout << std::endl;
      }

      // Save state
      if(n_ss > 0) {
	if(k % (n_ts/n_ss) == 0 && OK == 1) {

	  sav_stopwatch.start();
	  mhd_manager.save_state();
	  if(mhd_manager.is_master()) {
	    print_current_time();
	    std::cout << "MHD state saved to '" << mhd_manager.get_mhd_output_state_file() << "'" << std::endl;
	  }

	  if(rad_manager.is_enabled()) {
	    rad_manager.save_state(); // MPI
	    if(mhd_manager.is_master()) {
	      print_current_time();
	      std::cout << "RAD state saved to '" << rad_manager.get_rad_output_state_file() << "'" << std::endl;
	    }
	  }
	  sav_stopwatch.stop();

	}
      }

    }
  }
  tot_stopwatch.stop();

  if(mhd_manager.is_master()) {

    print_current_time();
    std::cout << std::endl;
    print_current_time();
    std::cout << " MHD state was initially loaded from '" << mhd_input_state_file << "'" << std::endl;
    if(rad_manager.is_enabled()) {
      print_current_time();
      std::cout << " RAD state was initially loaded from '" << rad_input_state_file << "'" << std::endl;
    }
    print_current_time();
    std::cout << " Snapshots were saved to '" << mhd_output_snapshot_file.substr(0, mhd_output_snapshot_file.find(".")) << ".*'" << std::endl;

    print_current_time();
    std::cout << std::endl;
    print_current_time();
    std::cout << " Statistics:" << std::endl;
    print_current_time();
    std::cout << std::endl;
    print_current_time();
    std::cout << " MHD time:       " << mhd_manager.get_mhd_stopwatch_t() << " s" << std::endl;
    print_current_time();
    std::cout << " MHD MPI time:   " << mhd_manager.get_mpi_stopwatch_t() << " s" << std::endl;
    print_current_time();
    std::cout << " MHD + MPI time: " << mhd_manager.get_mhd_stopwatch_t() + mhd_manager.get_mpi_stopwatch_t() << " s" << std::endl;
    print_current_time();
    std::cout << std::endl;
    print_current_time();

    if(rad_manager.is_enabled()) {
      std::cout << " RAD time:       " << rad_manager.get_rad_stopwatch_t() << " s" << std::endl;
      print_current_time();
      std::cout << " RAD MPI time:   " << rad_manager.get_mpi_stopwatch_t() << " s" << std::endl;
      print_current_time();
      std::cout << " RAD + MPI time: " << rad_manager.get_rad_stopwatch_t() + rad_manager.get_mpi_stopwatch_t() << " s" << std::endl;
      print_current_time();
      std::cout << std::endl;
      print_current_time();
    }

    std::cout << " SAV time:       " << sav_stopwatch.get_t() << " s" << std::endl;
    print_current_time();
    std::cout << std::endl;
    print_current_time();
    std::cout << " All times:      " << sav_stopwatch.get_t() + mhd_manager.get_mhd_stopwatch_t() + mhd_manager.get_mpi_stopwatch_t() + rad_manager.get_rad_stopwatch_t() + rad_manager.get_mpi_stopwatch_t() << " s" << std::endl;
    print_current_time();
    std::cout << " TOT time:       " << tot_stopwatch.get_t() << " s" << std::endl;
    print_current_time();
    std::cout << std::endl;

    if(rad_manager.is_enabled()) {
      print_current_time();
      std::cout << " RAD/MHD:        " << std::fixed << rad_manager.get_rad_stopwatch_t()/mhd_manager.get_mhd_stopwatch_t() << std::scientific << std::endl;
      print_current_time();
      std::cout << std::endl;
    }
    std::cout << "----------" << std::endl;
    std::cout << start_time << "<--- start wall time" << std::endl;
    print_current_time();
    std::cout << "<--- end wall time" << std::endl;
  }

  // Finishin'
  mhd_manager.close();
  rad_manager.close();
  amr_manager.close();

}
