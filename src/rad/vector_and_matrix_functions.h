//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef VECTOR_AND_MATRIX_FUNCTIONS_H
#define VECTOR_AND_MATRIX_FUNCTIONS_H

/* ----------------------- */
/* Building photon vectors */
/* ----------------------- */

void make_unit_vector_3d(double theta, double phi, double v[3]);
void make_momentum_vector(double eps, double theta, double phi, double k[3]);
void make_four_vector(double k[3], double P[4]);
void get_momentum_vector(double P[4], double k[3]);
double get_eps(double P[4]);

/* -------------- */
/* Vector algebra */
/* -------------- */

double vector_dot_product_3d(double v1[3], double v2[3]);
void vector_cross_product_3d(double v1[3], double v2[3], double v_cross[3]);
void vector_scalar_multiplication_3d(double A, double v[3]);
void vector_norm_3d(double v[3], double v_norm[3]);

/* -------------- */
/* Matrix algebra */
/* -------------- */

void matrix_vector_multiplication_3d(double m[9], double v[3]);
void matrix_scalar_multiplication_4d(double A, double m[16]);
void matrix_vector_multiplication_4d(double m[16], double v[4]);

/* -------------------------------- */
/* Rotation and scattering matrices */
/* -------------------------------- */

void Rz(double phi, double k[3]);
void Ry(double theta, double k[3]);
void M(double phi, double S[4]);
void Lambda(double beta_vec[3], double P[4]);

/* ------------------------ */
/* Computing various angles */
/* ------------------------ */

double get_rotation_angle(double A[3], double B[3], double k[3]);
double get_polar_angle(double k[3]);
double get_azimuthal_angle(double k[3]);

#endif
