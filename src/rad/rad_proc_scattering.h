//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef RAD_PROC_SCATTERING_H
#define RAD_PROC_SCATTERING_H

#include "ran.h"

/* --------------------------------------- */
/* Functions for drawing scattering angles */
/* --------------------------------------- */

double PDF_mu(double mu, double eps_0);
double draw_polar_angle(double eps_0, double S[4], Ran *rng);
double PDF_phi(double phi, double theta, double eps_0, double eps, double S[4]);
double draw_azimuthal_angle(double theta, double eps_0, double eps, double S[4], Ran *rng);

/* ---------------------------------------  */
/* Mean free path and performing scattering */
/* ---------------------------------------  */

double get_eps_after_scattering(double eps_0, double theta);
double get_scattering_time(double v, double rho, double Z_pm);
double get_scattering_mean_free_path(double eps, double mu, double rho, double v, double Z_pm);
void perform_scattering(double v, double eps, double mu, double gamma_e, double *eps_f, double *mu_f, Ran *rng);

#endif
