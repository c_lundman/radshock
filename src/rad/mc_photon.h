//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef MC_PHOTON_H
#define MC_PHOTON_H

class MC_Photon {

  // A simple class for holding the MC photon properties

 private:

  double r, mu, eps, w, s;
  int alive=1;

 public:

  MC_Photon() {}

  int get_alive() {return alive;}
  double get_r() {return r;}
  double get_mu() {return mu;}
  double get_eps() {return eps;}
  double get_w() {return w;}
  double get_s() {return s;}

  void set_alive(int alive_p) {alive = alive_p;}
  void set_r(double r_p) {r = r_p;}
  void set_mu(double mu_p) {mu = mu_p;}
  void set_eps(double eps_p) {eps = eps_p;}
  void set_w(double w_p) {w = w_p;}
  void set_s(double s_p) {s = s_p;}

  void print();

};

#endif
