//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "rad_manager.h"
#include <vector>
#include <iostream>
#include "math.h"
#include "mc_photon.h"
#include "num_distributions.h"
#include "rad_propagation.h"
#include "../mhd/physical_constants.h"

void RADManager::load_photons(int n_phs, double Z_gamma) {

  // This code fills the photon vector
  // It assumes that the MHD grid has first been set,
  // and that the radiation is locally isotropic, with a Wien spectrum, and the
  // local photon-to-proton ratio (Z_gamma) is given

  // For now, the photon-to-proton vector is computed inside this function (fix)

  // Photon-to-proton ratio
  std::vector<double> Z_gammas(vs.size(), Z_gamma);

  // Photon-to-proton ratio
  photons.resize(n_phs/NUM_PROC);

  // Photons have a Wien spectrum (locally)
  NumDistribution Wien_spectrum_norm(300);
  Wien_spectrum_norm.load_Wien_norm();

  // Computing radial photon distribution
  NumDistribution r_dist(vs.size());
  double N_gamma = r_dist.load_r_dist(r_bs, vs, rhos, Z_gammas, alpha);

  // Computing the photon weight
  // double w = N_gamma/n_phs/NUM_PROC;
  double w = N_gamma/photons.size()/NUM_PROC;

  int l;
  double Z_pm, gamma, v, rho, p, r, u, mu, theta, eps_prime, eps, eps_sum=0;
  for(unsigned int i=0; i<photons.size(); i++) {

    // Finding r, and spatial bin
    r = r_dist.sample(rng.doub());
    l = bisect_without_ghosts(r_bs, r);
    Z_pm = Z_pms[l];
    v = vs[l];
    rho = rhos[l];
    p = ps[l];
    gamma = 1./sqrt(1.-v*v);

    // Finding mu
    if(v == 0.) {
      mu = 2*rng.doub()-1;
    } else {
      u = sqrt(1/gamma/gamma/gamma/gamma/((1-v)*(1-v) + 4*v*rng.doub()));
      mu = (1.-u)/v;
    }

    // Finding eps
    theta = get_theta(rho, p, Z_pm, gamma_ad, f_heat);
    eps_prime = theta*Wien_spectrum_norm.sample(rng.doub());
    eps = eps_prime/gamma/(1-v*mu);
    eps_sum += eps;

    // Set drawn properties
    photons[i].set_r(r);
    photons[i].set_mu(mu);
    photons[i].set_eps(eps);
    photons[i].set_w(w);

  }

  // Find properties for photon splitting
  double eps_i = 5*(eps_sum/photons.size());
  w_i = w;
  for(unsigned k=0; k<20; k++) {
    eps_splits.push_back(eps_i*pow(2, k));
  }

}
