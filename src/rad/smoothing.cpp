//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "smoothing.h"
#include <vector>
#include <iostream>

// Smoothing function(s), used for the intensity moments
// Ignores the ghost cells
// If periodic, should "connects" endpoints to make the smoothed values periodic (NOT FIXED YET)

#include "../mhd/physical_constants.h"

void moving_window_smoothing(std::vector<double> &xs, int W) {

  // W is the "smoothing window size"
  // W should be _uneven_ to have the window properly centered

  // xs_s is the smoothed vector
  std::vector<double> xs_s(xs.size());

  // Moving window average
  int l, n=xs.size();
  double x_avg;
  for(int j=2; j<n-3; j++) {
    l = 0;
    x_avg = 0;
    for(int k=-(W-1)/2; k<=+(W-1)/2; k++) {
      if(j+k >= 2 && j+k <= n-3) {
	l += 1;
	x_avg += xs[j+k];
      }
    }
    if(l > 0) {
      x_avg /= l;
    }
    xs_s[j] = x_avg;
  }

  // Copying the smoothed vector back to the original vector
  for(int j=2; j<n-3; j++) {
    xs[j] = xs_s[j];
  }

}

void average_over_optical_depth(std::vector<double> &xs, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &Z_pms, double tau_smoothing, int alpha,
				int boundary_L, int boundary_R) {
  if(tau_smoothing != 0) {
    // average_over_optical_depth_v1(xs, m_bs, r_bs, Z_pms, tau_smoothing, alpha);
    average_over_optical_depth_v2(xs, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  }
}

void average_over_optical_depth_v1(std::vector<double> &xs, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &Z_pms, double tau_smoothing, int alpha) {

  if(tau_smoothing != 0) {

    double kappa_T = sigma_T/m_p;
    // double dtau_avg = .2;
    double dtau_avg = tau_smoothing;
    double dtau_half, tau_bin=0, tau_tmp=0;
    double x_sum, n, dm, r, Z_pm;
    int i, n_bs = m_bs.size();
    std::vector<double> xs_avg(xs.size());

    // if(alpha != 0) {std::cout << "average_over_optical_depth() has no implementation for alpha != 0 yet!" << std::endl;}

    for(int j=0; j<n_bs-1; j++) {
      dm = m_bs[j+1]-m_bs[j];
      Z_pm = Z_pms[j];
      if(alpha == 0) {
	tau_bin = kappa_T*dm*Z_pm;
      } else if(alpha == 2) {
	r = .5*(r_bs[j+1]+r_bs[j]);
	// tau_bin = kappa_T*4.*pi*dm*Z_pm/r/r;
	tau_bin = kappa_T*dm*Z_pm/r/r;
      } else {
	std::cout << "average_over_optical_depth() has no implementation for alpha != 0, 2 (in smoothing.cpp)" << std::endl;
      }

      // Checking left
      x_sum = xs[j]*dm;
      n = 1*dm;
      dtau_half = .5*tau_bin;
      i = j-1;
      while(dtau_half < .5*dtau_avg && i >= 2) {
	dm = m_bs[i+1]-m_bs[i];
	if(alpha == 0) {
	  tau_tmp = kappa_T*dm*Z_pm;
	} else if(alpha == 2) {
	  r = .5*(r_bs[i+1]+r_bs[i]);
	  // tau_tmp = kappa_T*4.*pi*dm*Z_pm/r/r;
	  tau_tmp = kappa_T*dm*Z_pm/r/r;
	}
	dtau_half += tau_tmp;
	if(dtau_half < .5*dtau_avg) {
	  x_sum += xs[i]*dm;
	  n += 1*dm;
	}
	i -= 1;
      }

      // Checking right
      dtau_half = .5*tau_bin;
      i = j+1;
      while(dtau_half < .5*dtau_avg && i <= n_bs-4) {
	dm = m_bs[i+1]-m_bs[i];
	if(alpha == 0) {
	  tau_tmp = kappa_T*dm*Z_pm;
	} else if(alpha == 2) {
	  r = .5*(r_bs[i+1]+r_bs[i]);
	  // tau_tmp = kappa_T*4.*pi*dm*Z_pm/r/r;
	  tau_tmp = kappa_T*dm*Z_pm/r/r;
	}
	dtau_half += tau_tmp;
	if(dtau_half < .5*dtau_avg) {
	  x_sum += xs[i]*dm;
	  n += 1*dm;
	}
	i += 1;
      }

      // Average
      xs_avg[j] = x_sum/n;
    }
    xs = xs_avg;
  }
}

double get_tau_bin(int j, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &Z_pms, double tau_smoothing, int alpha) {

  double kappa_T = sigma_T/m_p;
  double r, dm = m_bs[j+1]-m_bs[j];
  double Z_pm = Z_pms[j];
  double tau_bin = 0;
  if(alpha == 0) {
    tau_bin = kappa_T*dm*Z_pm;
  } else if(alpha == 2) {
    r = .5*(r_bs[j+1]+r_bs[j]);
    tau_bin = kappa_T*dm*Z_pm/r/r;
  } else {
    std::cout << "average_over_optical_depth() has no implementation for alpha != 0, 2 (in smoothing.cpp)" << std::endl;
  }
  return tau_bin;
}

double get_weight(double x, double x_max) {
  // Assumptions: 0 <= x <= x_max

  // // Constant
  // return 1;

  // Triangle
  return 1. - x/x_max;

}

void average_over_optical_depth_v2(std::vector<double> &xs, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &Z_pms, double tau_smoothing, int alpha,
				   int boundary_L, int boundary_R) {

  double dtau_avg = tau_smoothing;
  double dtau_half, dtau_j=0, dtau_i=0;
  double weighted_x_sum, n, w;
  int i, n_bs = m_bs.size();
  int i_min = 2;
  int i_max = m_bs.size() - 4;
  std::vector<double> xs_avg(xs.size());

  for(int j=0; j<n_bs-1; j++) {

    // Find optical depth of the current bin
    dtau_j = get_tau_bin(j, m_bs, r_bs, Z_pms, tau_smoothing, alpha);
    w = get_weight(0, dtau_avg);
    weighted_x_sum = w*xs[j]*dtau_j;
    n = w*dtau_j;

    // Checking left
    dtau_half = .5*dtau_j;
    i = j-1;
    while(dtau_half < .5*dtau_avg && i >= i_min) {
      dtau_i = get_tau_bin(i, m_bs, r_bs, Z_pms, tau_smoothing, alpha);
      dtau_half += dtau_i;

      // Accumulate
      if(dtau_half < .5*dtau_avg) {
	w = get_weight(dtau_half-.5*dtau_i, .5*dtau_avg);
	weighted_x_sum += w*xs[i]*dtau_i;
	n += w*dtau_i;
	i -= 1;

	// If periodic boundaries, go to i_max
	if(boundary_L == 3 && i < i_min) {
	  i = i_max;
	}

      }

    }

    // Checking right
    dtau_half = .5*dtau_j;
    i = j+1;
    while(dtau_half < .5*dtau_avg && i <= i_max) {
      dtau_i = get_tau_bin(i, m_bs, r_bs, Z_pms, tau_smoothing, alpha);
      dtau_half += dtau_i;

      // Accumulate
      if(dtau_half < .5*dtau_avg) {
	w = get_weight(dtau_half-.5*dtau_i, .5*dtau_avg);
	weighted_x_sum += w*xs[i]*dtau_i;
	n += w*dtau_i;
	i += 1;

      	// If periodic boundaries, go to i_min
	if(boundary_R == 3 && i > i_max) {
	  i = i_min;
	}

      }

    }

    // Average
    xs_avg[j] = weighted_x_sum/n;
  }
  xs = xs_avg;
}
