//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "num_distributions.h"
#include <vector>
#include <math.h>
#include "../mhd/physical_constants.h"
#include "ran.h"
#include <iostream>
#include <fstream>

// The "load_XX" functions are not very elegant, as lots of code is re-used. Could be better if I pass function pointers perhaps?
// But diff. number of arguments...

NumDistribution::NumDistribution(int n) : Ps(n), xs(n) {}

// -----------------
//  Loading spectra
// -----------------

void NumDistribution::load_Planck_norm() {

  // Creating a cumulative Planck spectrum

  int N = xs.size();
  double x, x_max = 100;
  double dx = x_max/N;

  // "Integrating"
  xs[0] = 0;
  Ps[0] = 0;
  for(int i=1; i<N; i++) {
    xs[i] = i*dx;
    x = .5*(xs[i]+xs[i-1]);
    Ps[i] = Ps[i-1] + Planck_norm(x)*dx;
  }

  // Normalizing
  double P_max = Ps[N-1];
  for(int i=0; i<N; i++) {Ps[i] /= P_max;}

}

void NumDistribution::load_Wien_norm() {

  // Creating a cumulative Wien spectrum

  int N = xs.size();
  double x, x_max = 100;
  double dx = x_max/N;

  // "Integrating"
  xs[0] = 0;
  Ps[0] = 0;
  for(int i=1; i<N; i++) {
    xs[i] = i*dx;
    x = .5*(xs[i]+xs[i-1]);
    Ps[i] = Ps[i-1] + Wien_norm(x)*dx;
  }

  // Normalizing
  double P_max = Ps[N-1];
  for(int i=0; i<N; i++) {Ps[i] /= P_max;}

}

void NumDistribution::load_Maxwellian_nonrel_norm() {

  // Creating a cumulative (non-relativistic) Maxwellian spectrum (x = gamma) [NOT VERIFIED YET]

  int N = xs.size();
  double x, x_max = 100;
  double dx = x_max/(double)N;

  // "Integrating"
  xs[0] = 0;
  Ps[0] = 0;
  for(int i=1; i<N; i++) {
    xs[i] = i*dx;
    x = .5*(xs[i]+xs[i-1]);
    Ps[i] = Ps[i-1] + Maxwellian_nonrel_norm(x)*dx;
  }

  // Normalizing
  double P_max = Ps[N-1];
  for(int i=0; i<N; i++) {Ps[i] /= P_max;}

}

void NumDistribution::load_Maxwellian(double theta) {

  // Creating a cumulative Maxwellian spectrum (x = gamma)

  double gamma_max, theta_prime=0.02;
  int N = xs.size();
  double theta_min = 1e-6;
  double gamma_minus_1_mininum = 1e-8;

  // Set the lowest possible temperature
  if(theta < theta_min) {theta = theta_min;}

  // Using Asafs old algorithm,
  // gamma_max: (empirically), theta<0.2: gamma_max = 1+10*theta;
  //                           theta>0.2 -> gamma_max = 15*theta.

  if(theta > 10*theta_prime) {
    gamma_max = 15*theta + 1e-10;
  } else if(theta > theta_prime) {
    gamma_max = 1 + 20*theta;
  } else {
    gamma_max = 1 + 10*theta_prime;
  }
  double dgamma = (gamma_max-1)/N;

  // "Integrating"
  xs[0] = 1 + gamma_minus_1_mininum;
  Ps[0] = 0;
  for(int i=0; i<N-1; i++) {
    xs[i+1] = 1 + (i+1)*dgamma;
    if(theta < theta_prime) {
      Ps[i+1] = Ps[i] + Maxwellian(xs[i+1], theta_prime)*dgamma;
      xs[i+1] = 1 + theta/theta_prime*(xs[i+1]-1);
    } else {
      Ps[i+1] = Ps[i] + Maxwellian(xs[i+1], theta)*dgamma;
    }
  }

  // Normalizing
  double P_max = Ps[N-1];
  for(int i=0; i<N; i++) {Ps[i] /= P_max;}

}

// ---------------------------
//  Initial photon properties
// ---------------------------

double NumDistribution::load_r_dist(std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &Z_gammas, int alpha) {

  // Z_gamma is the photon-to-proton ratio

  int n_bs = r_bs.size();
  double r_min = r_bs[2];
  double r_max = r_bs[n_bs-3];
  double r_avg, A = 1, dr = (r_max-r_min)/(Ps.size()-1);
  int l;
  double gamma, v, rho, Z_gamma, f;

  // Filling cumulative distribution
  xs[0] = r_min;
  Ps[0] = 0.;

  for(unsigned int i=0; i<Ps.size()-1; i++) {
    // Next location
    xs[i+1] = xs[i] + dr;

    // Next probability
    r_avg = .5*(xs[i+1] + xs[i]);
    if(alpha == 2) {A = r_avg*r_avg;
    } else if(alpha == 1) {A = r_avg;
    } else if(alpha == 0) {A = 1.;}

    // Find corresponding bin
    l = bisect(r_bs, r_avg);

    // Find bin properties
    v = vs[l];
    rho = rhos[l];
    Z_gamma = Z_gammas[l];
    gamma = 1./sqrt(1.-v*v);

    // Integrating the cumulative distribution
    f = gamma*rho*A*Z_gamma/m_p; // Really N_gamma*f, where f is prob. dens. of r
    Ps[i+1] = Ps[i]+f*dr;
  }

  // Normalizing the distribution
  double N_gamma = Ps[Ps.size()-1];
  for(unsigned int i=0; i<Ps.size(); i++) {Ps[i] = Ps[i]/N_gamma;}

  // This specific loading function returns the total number of physical photons
  return N_gamma;

}

double NumDistribution::sample(double P_p) {

  // Find cumulative P
  int l = bisect(Ps, P_p);

  // Linear interpolation
  double x, x_l, x_r, P_l, P_r;
  x_l = xs[l];
  x_r = xs[l+1];
  P_l = Ps[l];
  P_r = Ps[l+1];
  x = x_l + (P_p-P_l)/(P_r-P_l)*(x_r-x_l);
  return x;
}

void NumDistribution::get_Ps(std::vector<double> &Ps_p) {
  for(unsigned int i=0; i<Ps.size(); i++) {
    Ps_p[i] = Ps[i];
  }
}

void NumDistribution::get_xs(std::vector<double> &xs_p) {
  for(unsigned int i=0; i<xs.size(); i++) {
    xs_p[i] = xs[i];
  }
}

void NumDistribution::save_dist_to_file(std::ofstream &dist_file, int n_samples, int seed, std::string comment) {
  // Saves the loaded cumulative distribution to file, along with 'n_samples' drawn samples from the distribution

  // dist_file.open(file_name);
  dist_file << std::scientific;
  dist_file.precision(12);

  // Writing comment and seed
  dist_file << "Comments: " << comment << std::endl;
  dist_file << "seed = " << seed << std::endl << std::endl;

  // Write xs vector
  int n = xs.size();
  for(int i=0; i<n; i++) {
    dist_file << " " << xs[i];
    if(i == n-1) {
      dist_file << std::endl;
    }
  }

  // Write Ps vector
  for(int i=0; i<n; i++) {
    dist_file << " " << Ps[i];
    if(i == n-1) {
      dist_file << std::endl << std::endl;
    }
  }

  // Fill with samples
  Ran rng(seed);
  double s;
  for(int i=0; i<n_samples; i++) {
    s = sample(rng.doub());
    dist_file << s << std::endl;
  }

}

void NumDistribution::print_Ps() {
  std::string name = "Ps";
  std::string whitespace = "";
  int w = 11;
  for(unsigned int i=0; i<name.length()+2; i++) {
    whitespace += " ";
  }
  std::cout << name << " [";
  for(unsigned int i=0; i<Ps.size()-1; i++) {
    std::cout << Ps[i] << ", ";
    if(i != 0 && i%w == 0) {std::cout << std::endl << whitespace;}
  }
  std::cout << Ps[Ps.size()-1] << "]" << std::endl;
}

void NumDistribution::print_xs() {
  std::string name = "xs";
  std::string whitespace = "";
  int w = 11;
  for(unsigned int i=0; i<name.length()+2; i++) {
    whitespace += " ";
  }
  std::cout << name << " [";
  for(unsigned int i=0; i<xs.size()-1; i++) {
    std::cout << xs[i] << ", ";
    if(i != 0 && i%w == 0) {std::cout << std::endl << whitespace;}
  }
  std::cout << xs[xs.size()-1] << "]" << std::endl;
}

int bisect(const std::vector<double> &xs, double x) {

  // Returns l, such that xs[l] < x < xs[l+1]
  // Assumes that xs is ordered, such that xs[i] < xs[i+1] for all i

  int m, l=0, h=xs.size()-1;
  while(h-l != 1) {
    m = (l+h)/2;
    if(xs[m] > x) {
      h = m;
    } else {
      l = m;
    }
  }
  return l;
}

// ---------
//  Spectra
// ---------

double Planck_norm(double x) {

  // A Planck spectrum, written in terms of the variable x = E/kT (not normalized)

  double f = x*x/(exp(x) - 1.);
  return f;
}

double Wien_norm(double x) {

  // Wien spectrum, written in terms of the variable x = E/kT

  double f = .5*x*x*exp(-x);
  return f;
}

double Maxwellian_nonrel_norm(double x) {

  // Non-relativistic Maxwellian, written in terms of the variable x = E/kT

  double f = 2.*sqrt(x/pi)*exp(-x);
  return f;
}

double Maxwellian(double gamma, double theta) {

  // The Maxwellian distribution (not normalized)

  double f = gamma*sqrt(gamma*gamma-1.)*exp(-gamma/theta);
  return f;
}

// ----------------
//  Test functions
// ----------------

void test_Planck_norm(std::string file_name, int n, int n_samples, int seed) {
  // n = number of bins
  // n_samples = number of samples drawn
  // seed = seed for the random number generator

  NumDistribution Planck_test(n);
  Planck_test.load_Planck_norm();

  std::ofstream dist_file;
  dist_file.open(file_name);
  Planck_test.save_dist_to_file(dist_file, n_samples, seed, "load_Planck");
  dist_file.close();
}

void test_Wien_norm(std::string file_name, int n, int n_samples, int seed) {
  // n = number of bins
  // n_samples = number of samples drawn
  // seed = seed for the random number generator

  NumDistribution Wien_test(n);
  Wien_test.load_Wien_norm();

  std::ofstream dist_file;
  dist_file.open(file_name);
  Wien_test.save_dist_to_file(dist_file, n_samples, seed, "load_Wien");
  dist_file.close();
}

void test_Maxwellian(std::string file_name, int n, int n_samples, int seed, double theta) {
  // n = number of bins
  // n_samples = number of samples drawn
  // seed = seed for the random number generator
  // theta = temperature of the Maxwellian

  NumDistribution Maxwellian_test(n);
  Maxwellian_test.load_Maxwellian(theta);

  std::ofstream dist_file;
  dist_file.open(file_name);
  Maxwellian_test.save_dist_to_file(dist_file, n_samples, seed, "load_Maxwellian, theta = 1e-2");
  dist_file.close();
}

void test_Maxwellian_nonrel_norm(std::string file_name, int n, int n_samples, int seed) {
  // n = number of bins
  // n_samples = number of samples drawn
  // seed = seed for the random number generator

  NumDistribution Maxwellian_nonrel_norm_test(n);
  Maxwellian_nonrel_norm_test.load_Maxwellian_nonrel_norm();

  std::ofstream dist_file;
  dist_file.open(file_name);
  Maxwellian_nonrel_norm_test.save_dist_to_file(dist_file, n_samples, seed, "load_Maxwellian_nonrel_norm");
  dist_file.close();
}
