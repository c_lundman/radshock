//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef RAD_PROPAGATION_H
#define RAD_PROPAGATION_H

#include <vector>
#include "mc_photon.h"
#include "num_distributions.h"
#include "ran.h"

double get_radiation_time_step_cr(std::vector<double> &r_bs, std::vector<double> &vs);
double get_radiation_time_step_sc(std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &Z_pms, double gamma_ad, int alpha);
double get_radiation_time_step_sc_without_pairs(std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, double gamma_ad, int alpha);
double get_theta(double rho, double p, double Z_pm, double gamma_ad, double f_heat);
double propagate_mu(double mu_i, double r_i, double r_f, int alpha);
void perform_propagation(double r_b_L, double r_b_R, double v_b_L, double v_b_R,
			 double r_i, double mu_i, double ds, double dt_remaining,
			 double &r_f, double &mu_f, double &dt_propagated, int &event_type, int alpha, int print_history=0);
int find_interaction_type(std::vector<double> inv_lambdas, double inv_lambda_tot, double P_p);
int bisect_without_ghosts(const std::vector<double> &xs, double x);
int find_grid_location(std::vector<double> &r_bs, MC_Photon &photon);
int propagate_and_interact_until_time(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
				      std::vector<double> &Z_pms, std::vector<double> &G0_scs, std::vector<double> &G1_scs,
				      std::vector<double> &G0_ggs, std::vector<double> &G1_ggs, std::vector<double> &dotZ_pms,
				      std::vector<double> &eps_p_bs, std::vector<double> &mu_p_bs, std::vector<double> &alpha_ggs,
				      std::vector<NumDistribution> &electron_energy_dists, MC_Photon &photon, int boundary_L, int boundary_R, double Dt, int alpha, int using_pairs,
				      std::vector<double> &skip_pair_calcs, std::vector<double> &skip_pair_calcs_prev, Ran *rng, int RANK, int iter, int i,
				      std::vector<double> &I_0s_prop, std::vector<double> &I_1s_prop, std::vector<double> &calJ_primes_prop, std::vector<double> &p_gammas_prop,
				      std::vector<double> &u_gammas_lab_prop, std::vector<double> &zetas_prop, std::vector<double> &calI_primes_prop, std::vector<double> &eps_bs, double dlneps,
				      std::vector<double> &eps_bs_Compton, double dlneps_Compton);

// NEW: two-temp
double get_theta_e_two_temp(double rho, double p, double Z_pm, double xi, double f_heat);

#endif
