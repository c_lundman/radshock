//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef RAD_MANAGER_H
#define RAD_MANAGER_H

#include <string>
#include <vector>
#include <fstream>
#include "mc_photon.h"
#include "num_distributions.h"
#include "ran.h"
#include "../mhd/stopwatch.h"
#include "../mhd/dbg.h"

// The function implementations are found in:
// (1) rad_manager.cpp
// (2) rad_manager_physics.cpp
// (3) rad_manager_load_photons.cpp
// (4) rad_manager_mpi.cpp
// (5) rad_manager_save.cpp

class RADManager {

  // A class that stores and manages the Monte Carlo photons

 private:

  int disabled=0;

  std::string sim_name;
  int n_bs;
  int alpha;
  int RANK;
  int NUM_PROC;
  int NUM_MPI_CYCLES_POWER_OF_TWO;

  int boundary_L=1;
  int boundary_R=1;
  int save_spc_file=1;
  int n_bs_spec=0;
  double gamma_ad=4./3.;
  double epsilon=5e-2;
  int iter=0;
  double t=0;
  double f_heat=1;
  double tau_smoothing=0.2;
  int use_pairs=1;
  int uniform_spec_radius_bins;

  int load_photon_multiplier=1;
  int save_photon_divider=1;

  Stopwatch rad_stopwatch;
  Stopwatch mpi_stopwatch;
  Stopwatch prop_stopwatch; // subset of rad_stopwatch timing
  Stopwatch pair_stopwatch; // subset of rad_stopwatch timing
  Stopwatch smooth_stopwatch; // subset of rad_stopwatch timing
  Stopwatch mom_stopwatch; // subset of rad_stopwatch timing

  std::string run_dir;

  // Photon vector
  std::vector<MC_Photon> photons;

  // Electron energy distributions
  int n_el_spec_bins;
  std::vector<NumDistribution> electron_energy_dists;

  // Random number generator
  Ran rng;

  // Grid for photon propagation
  std::vector<double> m_bs, r_bs;
  std::vector<double> vs, rhos, ps, bs;
  std::vector<double> G0_scs, G1_scs, dotZ_pms;
  std::vector<double> G0_ggs, G1_ggs;

  // Pairs
  std::vector<double> Z_pms;
  std::vector<double> skip_pair_calcs;
  std::vector<double> skip_pair_calcs_prev;

  // Radiation spectrum grid
  double dlneps;
  std::vector<double> eps_bs;

  // Radiation spectrum grid used only for the Compton temperature calculation
  double dlneps_Compton;
  std::vector<double> eps_bs_Compton;

  // Integrated radiation quantities
  std::vector<double> p_gammas;
  std::vector<double> I_0s, I_1s;
  std::vector<double> zetas;
  std::vector<double> theta_Cs, As, Bs; // As and Bs are used to compute the Compton temperature
  std::vector<double> calJ_primes; // A matrix, flattened to a single vector
  std::vector<double> dudlnepss; // A matrix, flattened to a single vector
  std::vector<double> dudlneps_ints; // Radiation spectrum, integrated over the whole grid
  std::vector<double> m_bs_spec, r_bs_spec; // Grid for radiation spectrum
  std::vector<double> dudlneps_specs; // Radiation spectrum inside the m_bs_spec, r_bs_spec grid

  // For new integration, during photon propagation
  std::vector<double> I_0s_prop, I_1s_prop, calJ_primes_prop;
  std::vector<double> p_gammas_prop, u_gammas_lab_prop, zetas_prop, calI_primes_prop;

  // Vectors for pair production
  /* std::vector<double> lambda_ggs; // A 3D matrix, flattened to a single vector, containing mean free paths */
  std::vector<double> alpha_ggs; // A 3D matrix, flattened to a single vector, containing inverse mean free paths
  std::vector<double> calI_primes; // A 3D matrix, flattened to a single vector, used for pairs
  std::vector<double> eps_p_bs; // Binning (in comoving frame) for the calI_primes vector
  std::vector<double> mu_p_bs; // Binning (in comoving frame) for the calI_primes vector
  std::vector<double> psis; // Integral that can be pre-computed (and is automatically resized when computed)

  // Save files
  std::ofstream zpm_file;
  std::ofstream pre_file;
  std::ofstream ugl_file;
  std::ofstream cte_file;
  std::ofstream spc_file;
  std::ofstream ispc_file;

  // Save state file name
  std::string rad_output_state_file;

  // Snapshot file name
  std::string rad_output_snapshot_file;

  // For the moments
  std::ofstream i0s_file;
  std::ofstream i1s_file;
  std::ofstream zet_file;

  // For the save and load state file names
  std::ofstream save_state_file;
  std::ifstream load_state_file;

  // For photon splitting
  std::vector<double> eps_splits;
  double w_i;
  /* int photons_per_split=3; */
  /* int photons_per_split=2; */
  int photons_per_split=1;

  int check_if_photon_should_split(MC_Photon photon);
  int propagate_and_interact_until_time_wrapper(double dt);

  void flush_sources();
  void open_file(std::ofstream &file, std::string prefix);
  void open_all_files();
  void save_single_line(std::ofstream &file, std::vector<double> &Vs);
  void close_all_files();

  // Pairs
  void initiate_pairprod_vectors();
  void compute_pair_mean_free_path_wrapper();
  void evolve_Z_pm(double dt);

  // For printing information when debugging
  /* DBG dbg("RAD"); */
  DBG dbg;
  /* DBG dbg(99); */

 public:

  /* RADManager(std::string sim_name_p, int n_phs_p, int n_el_spec_bins, int alpha_p, int RANK_p, int NUM_PROC_p, int SEED_p); */
  /* RADManager(std::string sim_name_p, int n_phs_p, int n_el_spec_bins, int alpha_p, int SEED_p); */
  RADManager(std::string sim_name_p, int SEED);

  void set_boundary_L(int boundary_L_p) {boundary_L = boundary_L_p;}
  void set_boundary_R(int boundary_R_p) {boundary_R = boundary_R_p;}
  void set_save_spc_file(int save_spc_file_p) {save_spc_file = save_spc_file_p;}
  void set_n_bs_spec(int n_bs_spec_p) {n_bs_spec = n_bs_spec_p;}
  void set_gamma_ad(double gamma_ad_p) {gamma_ad = gamma_ad_p;}
  void set_epsilon(double epsilon_p) {epsilon = epsilon_p;}
  void set_f_heat(double f_heat_p) {f_heat = f_heat_p;}
  void set_iter(int iter_p) {iter = iter_p;}
  void set_t(double t_p) {t = t_p;}
  void set_grid(std::vector<double> &m_bs_p, std::vector<double> &r_bs_p,
		std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p, std::vector<double> &Z_pms_p,
		std::vector<double> &xis_p);
  void set_Z_pms(std::vector<double> &Z_pms_p);
  void set_photons_per_split(int photons_per_split_p) {photons_per_split = photons_per_split_p;}

  void set_alpha(int alpha_p);
  void set_n_el_spec_bins(int n_el_spec_bins_p) {n_el_spec_bins = n_el_spec_bins_p;}

  void set_load_photon_multiplier(int load_photon_multiplier_p) {load_photon_multiplier = load_photon_multiplier_p;}
  void set_save_photon_divider(int save_photon_divider_p) {save_photon_divider = save_photon_divider_p;}
  void set_tau_smoothing(double tau_smoothing_p) {tau_smoothing = tau_smoothing_p;}
  void set_use_pairs(int use_pairs_p) {use_pairs = use_pairs_p;}
  void set_uniform_spec_radius_bins(int uniform_spec_radius_bins_p) {uniform_spec_radius_bins = uniform_spec_radius_bins_p;}

  void set_run_dir(std::string run_dir_p) {run_dir = run_dir_p;}
  void set_disabled(int disabled_p) {disabled = disabled_p;}

  bool is_master() {return RANK == 0;}
  bool is_disabled() {return disabled == 1;}
  bool is_enabled() {return disabled == 0;}
  void print_grid(int RANK_p);

  double get_rad_stopwatch_t() {return rad_stopwatch.get_t();}
  double get_mpi_stopwatch_t() {return mpi_stopwatch.get_t();}
  double get_prop_stopwatch_t() {return prop_stopwatch.get_t();}
  double get_pair_stopwatch_t() {return pair_stopwatch.get_t();}
  double get_smooth_stopwatch_t() {return smooth_stopwatch.get_t();}
  double get_mom_stopwatch_t() {return mom_stopwatch.get_t();}

  int get_n_phs() {return photons.size();}
  int get_iter() {return iter;}
  double get_f_heat() {return f_heat;}
  double get_t() {return t;}
  double get_crossing_time_step_wrapper();
  double get_time_step_wrapper();
  double get_time_step_without_pairs_wrapper();
  double get_time_step_momentum(std::vector<double> &I_1s, std::vector<double> &Z_pms);
  void load_photons(int n_phs, double Z_gamma); // Inside rad_manager_load_photons.cpp
  void get_Z_pms(std::vector<double> &Z_pms_p) {Z_pms_p = Z_pms;}
  void get_G_sc_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p);
  void get_G_gg_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p);
  void get_all_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p);
  void get_all_and_pair_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p, std::vector<double> &dotZ_pms_p);
  int get_photons_per_split() {return photons_per_split;}
  void split_all_photons();
  void update_single_step(double dt);
  void update_single_step_fake(double dt);
  void save_initial_conditions();
  void save_data();
  void close();

  void copy_file_if_it_exists();
  void set_rad_output_state_file(std::string rad_output_state_file_p) {rad_output_state_file = rad_output_state_file_p;}
  void set_rad_output_snapshot_file(std::string rad_output_snapshot_file_p) {rad_output_snapshot_file = rad_output_snapshot_file_p;}
  std::string get_rad_output_state_file() {return rad_output_state_file;}
  std::string get_rad_output_snapshot_file() {return rad_output_snapshot_file;}
  void save_state();
  void load_state(std::string sim_name_of_state);

  // MPI
  void set_rank_and_num_proc();

  void collect_vector(std::vector<double> &Vs);
  void collect_vector_old(std::vector<double> &Vs);
  void collect_vector_power_of_two(std::vector<double> &Vs);

  void distribute_vector(std::vector<double> &Vs);
  void distribute_vector_old(std::vector<double> &Vs);
  void distribute_vector_power_of_two(std::vector<double> &Vs);

  void collect_int(int &N);
  void distribute_int(int &N);
  void wait_for_all_threads();

  // Spectrum
  void set_spec_grid(int n_eps_bins, double eps_min, double eps_max);

  // Computing integrated properties (summing over MC photons)
  void compute_rad_pressure();
  double get_s(double eps);
  double get_q(double eps);
  void compute_calJ_prime_and_lab_frame_spectrum(); // Comoving, "isotropic" number intensity
  void compute_Compton_temperature_AB_vectors();
  void compute_Compton_temperature();
  double apply_empirical_Compton_temperature_correction(double theta_C);
  void get_radiation_moments(std::vector<double> &I_0s_p, std::vector<double> &I_1s_p, std::vector<double> &theta_Cs_p, std::vector<double> &Z_pms_p);

  // New functions, for better speed
  void get_radiation_moments_v2(std::vector<double> &I_0s_p, std::vector<double> &I_1s_p, std::vector<double> &theta_Cs_p, std::vector<double> &Z_pms_p, int use_pairs);
  void compute_I_0s_I_1s_calJ_primes(int use_pairs);
  void compute_calI_primes();
  void compute_calI_primes_and_sync();

  // Even newer functions: collecting radiation moments as radiation is propagating
  void get_radiation_moments_from_prop(std::vector<double> &I_0s_p, std::vector<double> &I_1s_p, std::vector<double> &theta_Cs_p);
  void compute_Compton_temperature_AB_vectors_prop();

  // Printing
  void print_vector(std::string name, std::vector<double> &Vs, int w);
  void print_G_sc_sources(int RANK_p=-1);
  void print_all_sources(int RANK_p=-1);
};

#endif
