//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include <iostream>
#include <vector>

// --------------------------------------------- //
//  Full treatment of gamma-gamma cross-section  //
// --------------------------------------------- //

// The comoving frame cross-section

double get_sigma_gg_full(double x);

// Functions for pre-computation

void fill_mu_p_bs(std::vector<double> &mu_p_bs);
void fill_eps_p_bs(std::vector<double> &eps_p_bs, double eps_min, double eps_max);
void pre_compute_psi(std::vector<double> &mu_p_bs, std::vector<double> &eps_p_bs, std::vector<double> &psis);

// Computing the alpha_gg matrix (for each time step)

void compute_alpha_ggs(std::vector<double> &r_bs, std::vector<double> &eps_p_bs, std::vector<double> &mu_p_bs, std::vector<double> &psis, std::vector<double> &calI_primes,
		       std::vector<double> &alpha_ggs, std::vector<double> &skip_pair_calcs, int RANK, int NUM_PROC);

// ---------------- //
//  Mean free path  //
// ---------------- //

double get_pairprod_mean_free_path(double r, double eps, double mu, double v, std::vector<double> &r_bs, std::vector<double> &eps_p_bs, std::vector<double> &mu_p_bs,
				   std::vector<double> &alpha_ggs);
