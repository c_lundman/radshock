//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef SMOOTHING_H
#define SMOOTHING_H

#include <vector>

void moving_window_smoothing(std::vector<double> &xs, int W);
void average_over_optical_depth(std::vector<double> &xs, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &Z_pms, double tau_smoothing, int alpha,
				int boundary_L, int boundary_R);

double get_tau_bin(int j, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &Z_pms, double tau_smoothing, int alpha);
double get_weight(double x, double x_max);
void average_over_optical_depth_v1(std::vector<double> &xs, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &Z_pms, double tau_smoothing, int alpha);
void average_over_optical_depth_v2(std::vector<double> &xs, std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &Z_pms, double tau_smoothing, int alpha,
				   int boundary_L, int boundary_R);

#endif
