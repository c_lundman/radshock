//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "rad_manager.h"
#include <vector>
#include <mpi.h>
#include <iostream>
#include <math.h>

// This file includes all MPI functions used by the RADManager class; all functions are private

void RADManager::set_rank_and_num_proc() {
  MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
  MPI_Comm_size(MPI_COMM_WORLD, &NUM_PROC);
  if(NUM_PROC > 0 && ((NUM_PROC & (NUM_PROC-1)) == 0)) {
    NUM_MPI_CYCLES_POWER_OF_TWO = log2(NUM_PROC);
  }
}

void RADManager::collect_vector(std::vector<double> &Vs) {

  // A wrapper, which selects the algorithm to use

  // The below statement checks if NUM_PROC is an even power of two
  if(NUM_PROC > 0 && ((NUM_PROC & (NUM_PROC-1)) == 0)) {
    collect_vector_power_of_two(Vs);
  } else {
    collect_vector_old(Vs);
  }

}

void RADManager::distribute_vector(std::vector<double> &Vs) {

  // A wrapper, which selects the algorithm to use

  // The below statement checks if NUM_PROC is an even power of two
  if(NUM_PROC > 0 && ((NUM_PROC & (NUM_PROC-1)) == 0)) {
    distribute_vector_power_of_two(Vs);
  } else {
    distribute_vector_old(Vs);
  }

}

void RADManager::collect_vector_old(std::vector<double> &Vs) {

  // Collects a vector from all workers

  int n = Vs.size();
  MPI_Status stat;

  if(RANK == 0) {
    // Collect the vector
    std::vector<double> Vs_p(n);
    for(int worker=1; worker<NUM_PROC; worker++) {
      MPI_Recv(&Vs_p[0], n, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      for(int i=0; i<n; i++) {
  	Vs[i] += Vs_p[i];
      }
    }

  } else {
    // Send the vector
    MPI_Send(&Vs[0], n, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
  }

}

void RADManager::distribute_vector_old(std::vector<double> &Vs) {

  // Distributes a vector to all workers

  int n = Vs.size();
  MPI_Status stat;

  if(RANK == 0) {
    // Send the vector
    for(int worker=1; worker<NUM_PROC; worker++) {
      MPI_Send(&Vs[0], n, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD);
    }

  } else {
    // Collect the vector
    MPI_Recv(&Vs[0], n, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &stat);
  }

}

void RADManager::collect_int(int &N) {

  // Collects (sums) integers from all workers

  MPI_Status stat;

  if(RANK == 0) {
    // Collect the integer
    int N_p;
    for(int worker=1; worker<NUM_PROC; worker++) {
      MPI_Recv(&N_p, 1, MPI_INT, worker, 1, MPI_COMM_WORLD, &stat);
      // MPI_Recv(&Vs_p[0], n, MPI_DOUBLE, worker, 1, MPI_COMM_WORLD, &stat);
      N += N_p;
    }

  } else {
    // Send the integer
    MPI_Send(&N, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
  }
}

void RADManager::distribute_int(int &N) {

  // Distributes an integer to all workers
  MPI_Status stat;

  if(RANK == 0) {
    // Send the vector
    for(int worker=1; worker<NUM_PROC; worker++) {
      MPI_Send(&N, 1, MPI_INT, worker, 1, MPI_COMM_WORLD);
    }

  } else {
    // Collect the vector
    MPI_Recv(&N, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &stat);
  }
}

void RADManager::wait_for_all_threads() {
  MPI_Barrier(MPI_COMM_WORLD);
}


void RADManager::collect_vector_power_of_two(std::vector<double> &Vs) {

  // Collects a vector from all workers
  // Uses a faster algorithm, which assumes that the number of threads (NUM_PROC) are a power of two
  // The wall time of this algorithm should scale as log(NUM_PROC), while the old algorithm scales as NUM_PROC

  MPI_Status stat;
  int a, b, n = Vs.size();
  std::vector<double> Vs_p(n);
  
  int rank_to_talk_to;

  // A cycle is a "communication step"
  for(int i=0; i<NUM_MPI_CYCLES_POWER_OF_TWO; i++) {
    a = pow(2, i);
    b = pow(2, i+1);

    // Should the thread communicate?
    if(RANK % a == 0) {

      // Is the thread a "receiver"?
      if(RANK % b == 0) {

	// Collect the vector from "rank_to_talk_to"
	rank_to_talk_to = RANK + a;
	MPI_Recv(&Vs_p[0], n, MPI_DOUBLE, rank_to_talk_to, 1, MPI_COMM_WORLD, &stat);
	for(int i=0; i<n; i++) {
	  Vs[i] += Vs_p[i];
	}

      } else {

	// Send the vector to "rank_to_talk_to"
	rank_to_talk_to = RANK - a;
	MPI_Send(&Vs[0], n, MPI_DOUBLE, rank_to_talk_to, 1, MPI_COMM_WORLD);

      }
    }
  }
  
}

void RADManager::distribute_vector_power_of_two(std::vector<double> &Vs) {

  // Distributes a vector to all workers
  // Uses a faster algorithm, which assumes that the number of threads (NUM_PROC) are a power of two
  // The wall time of this algorithm should scale as log(NUM_PROC), while the old algorithm scales as NUM_PROC

  MPI_Status stat;
  int a, b, n = Vs.size();

  int rank_to_talk_to;

  // A cycle is a "communication step"
  for(int i=NUM_MPI_CYCLES_POWER_OF_TWO-1; i>=0; i--) {
    a = pow(2, i);
    b = pow(2, i+1);

    // Should the thread communicate?
    if(RANK % a == 0) {

      // Is the thread a "sender"?
      if(RANK % b == 0) {

	// Send the vector to "rank_to_talk_to"
	rank_to_talk_to = RANK + a;
	MPI_Send(&Vs[0], n, MPI_DOUBLE, rank_to_talk_to, 1, MPI_COMM_WORLD);

      } else {

	// Collect the vector from "rank_to_talk_to"
	rank_to_talk_to = RANK - a;
	MPI_Recv(&Vs[0], n, MPI_DOUBLE, rank_to_talk_to, 1, MPI_COMM_WORLD, &stat);

      }
    }
  }
  
}
