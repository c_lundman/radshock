//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef NUM_DISTRIBUTIONS_H
#define NUM_DISTRIBUTIONS_H

#include <string>
#include <vector>
#include <fstream>

class NumDistribution {

  // A class for holding and drawing from numerical distributions

 private:

  std::vector<double> Ps; // The cumulative distribution (0 <= P <= 1)
  std::vector<double> xs; // The independent variable
  double label;

 public:

  NumDistribution() {};
  NumDistribution(int n);

  void load_Planck_norm();  // Normalized blackbody (returns x = E/kT)
  void load_Wien_norm();    // Normalized Wien (returns x = E/kT)
  void load_Maxwellian_nonrel_norm();  // Normalized Maxwellian (returns x = E/kT)
  void load_Maxwellian(double theta);  // NOT normalized Maxwellian, because it can't be normalized (returns eps = E/m_e c^2)

  double load_r_dist(std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &Z_gammas, int alpha); // returns N_gamma

  double sample(double P_p); // P_p is supposed to be sampled from an uniform distribution (0 <= P_p <= 1)

  void get_Ps(std::vector<double> &Ps_p);
  void get_xs(std::vector<double> &xs_p);
  void set_label(double label_p) {label = label_p;}
  double get_label() {return label;}

  void print_Ps();
  void print_xs();
  void save_dist_to_file(std::ofstream &dist_file, int samples, int seed, std::string comment);

};

int bisect(const std::vector<double> &xs, double x);

// ---------
//  Spectra
// ---------

double Planck_norm(double x);
double Wien_norm(double x);
double Maxwellian_nonrel_norm(double x);
double Maxwellian(double gamma, double theta);

// ----------------
//  Test functions
// ----------------

void test_Planck_norm(std::string file_name, int n, int n_samples, int seed);
void test_Wien_norm(std::string file_name, int n, int n_samples, int seed);
void test_Maxwellian(std::string file_name, int n, int n_samples, int seed, double theta);
void test_Maxwellian_nonrel_norm(std::string file_name, int n, int n_samples, int seed);

#endif
