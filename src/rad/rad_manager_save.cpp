//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "rad_manager.h"
#include <fstream>
#include <string>
#include <vector>
#include "smoothing.h"

// #include "../libmhd/main_helper_functions.h"

// When adding a new save file, do this:
// 1) Declare it in the 'rad_manager.h' file
// 2) Add it to the 'open_all_files' function
// 3) Add it to the 'save_data' function
// 4) Add it to the 'close_all_files' function

void RADManager::open_file(std::ofstream &file, std::string prefix) {
  // if(alpha == 0) {
  //   file.open("output/pla_sym/"+sim_name+prefix, std::fstream::trunc);
  // } else if(alpha == 1) {
  //   file.open("output/cyl_sym/"+sim_name+prefix, std::fstream::trunc);
  // } else if(alpha == 2) {
  //   file.open("output/sph_sym/"+sim_name+prefix, std::fstream::trunc);
  // }
  // file.open("snapshot_files/"+sim_name+prefix, std::fstream::trunc);
  file.open(rad_output_snapshot_file.substr(0, rad_output_snapshot_file.find("."))+prefix, std::fstream::trunc);

  // Setting scientific notation and precision
  file << std::scientific;
  file.precision(12);
}

void RADManager::save_single_line(std::ofstream &file, std::vector<double> &Vs) {
  int n = Vs.size();
  for(int i=0; i<n; i++) {
    file << " " << Vs[i];
    if(i == n-1) {
      file << std::endl;
    }
  }
  file.flush();
}

void RADManager::open_all_files() {
  // Prepare the files
  if(RANK == 0) {
    open_file(zpm_file, ".zpm");
    open_file(pre_file, ".pre");
    open_file(ugl_file, ".ugl");
    open_file(cte_file, ".cte");
    open_file(spc_file, ".spc");
    open_file(ispc_file, ".ispc");
    open_file(i0s_file, ".i0s");
    open_file(i1s_file, ".i1s");
    open_file(zet_file, ".zet");
  }
}

void RADManager::save_initial_conditions() {
  // Open the snapshot files
  open_all_files();

  // Adding the 'f_heat' information to the pressure file
  pre_file << "f_heat = " << f_heat << std::endl;

  // Adding the energy grid to the spectrum files
  save_single_line(ispc_file, eps_bs);
  if(save_spc_file == 1) {save_single_line(spc_file, eps_bs);}
  save_data();
}

void RADManager::save_data() {
  // Save everything

  // Pairs
  save_single_line(zpm_file, Z_pms);

  // Spectrum inside each spatial bin
  compute_calJ_prime_and_lab_frame_spectrum();
  // collect_vector(dudlnepss);
  collect_vector(dudlneps_ints);
  save_single_line(ispc_file, dudlneps_ints);
  if(save_spc_file == 1) {
    collect_vector(dudlneps_specs);
    save_single_line(spc_file, m_bs_spec);
    save_single_line(spc_file, r_bs_spec);
    save_single_line(spc_file, dudlneps_specs);
  }

  // If no radiation propagation has happened yet, then I_0s_prop is empty, and we use the "old" code for computing the
  // Compton temperature, I_0 and I_1
  if(I_0s_prop[I_0s_prop.size()/2] == 0) {

    // Radiation pressure
    compute_rad_pressure();
    collect_vector(p_gammas);
    smooth_stopwatch.start();
    average_over_optical_depth(p_gammas, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
    smooth_stopwatch.stop();
    save_single_line(pre_file, p_gammas);

    // Compton temperature (uses calJ_primes)
    compute_Compton_temperature_AB_vectors();
    collect_vector(As);
    collect_vector(Bs);
    smooth_stopwatch.start();
    average_over_optical_depth(As, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
    average_over_optical_depth(Bs, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
    smooth_stopwatch.stop();
    compute_Compton_temperature();
    save_single_line(cte_file, theta_Cs);

    // Moments of the intensity
    collect_vector(I_0s);
    collect_vector(I_1s);
    collect_vector(zetas);
    smooth_stopwatch.start();
    average_over_optical_depth(I_0s, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
    average_over_optical_depth(I_1s, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
    smooth_stopwatch.stop();
    save_single_line(i0s_file, I_0s);
    save_single_line(i1s_file, I_1s);
    save_single_line(zet_file, zetas);

  } else {

    // Moments of the intensity TEST
    save_single_line(pre_file, p_gammas_prop);
    save_single_line(ugl_file, u_gammas_lab_prop);
    save_single_line(zet_file, zetas_prop);
    save_single_line(i0s_file, I_0s_prop);
    save_single_line(i1s_file, I_1s_prop);
    save_single_line(cte_file, theta_Cs);
  }

}

void RADManager::copy_file_if_it_exists() {

  // std::ifstream ifile("state_files/"+sim_name+".rsf");
  std::ifstream ifile(rad_output_state_file);
  if((bool)ifile) {
    ifile.close();
    load_state_file.open(rad_output_state_file, std::ios::binary);
    // save_state_file.open("state_files/"+sim_name+"_prev.rsf", std::ios::binary);
    save_state_file.open(rad_output_state_file.substr(0, rad_output_state_file.find("."))+"_prev.rsf", std::ios::binary);
    save_state_file << load_state_file.rdbuf();
    save_state_file.flush();
    save_state_file.close();
    load_state_file.close();
  }

}

void RADManager::save_state() {
  // Collect photon number

  int n_phs = photons.size();
  int n_phs_mod = n_phs/save_photon_divider;
  if(n_phs % save_photon_divider != 0) {
    n_phs_mod += 1;
  }
  collect_int(n_phs_mod);

  // if(is_master()) {
  //   copy_file_if_it_exists();
  //   // copy_file_if_it_exists();
  // }

  if(is_master()) {
    // Open state file
    // if(alpha == 0) {
    //   // save_state_file.open("output/pla_sym/"+sim_name+".rad_state", std::fstream::trunc);
    //   save_state_file.open("output/pla_sym/"+sim_name+".rsf", std::fstream::trunc);
    // } else if(alpha == 1) {
    //   // save_state_file.open("output/cyl_sym/"+sim_name+".rad_state", std::fstream::trunc);
    //   save_state_file.open("output/cyl_sym/"+sim_name+".rsf", std::fstream::trunc);
    // } else if(alpha == 2) {
    //   // save_state_file.open("output/sph_sym/"+sim_name+".rad_state", std::fstream::trunc);
    //   save_state_file.open("output/sph_sym/"+sim_name+".rsf", std::fstream::trunc);
    // }

    // save_state_file.open("state_files/"+sim_name+".rsf", std::fstream::trunc);
    save_state_file.open(rad_output_state_file, std::fstream::trunc);

    // Setting scientific notation and precision
    save_state_file << std::scientific;
    save_state_file.precision(12);

    // Write file header
    // save_state_file << "n_phs          " << n_phs << std::endl;
    save_state_file << "n_phs          " << n_phs_mod << std::endl;
    save_state_file << "n_bs           " << m_bs.size() << std::endl;
    save_state_file << "eps_min        " << eps_bs[0] << std::endl;
    save_state_file << "eps_max        " << eps_bs[eps_bs.size()-1] << std::endl;
    save_state_file << "n_bs_spec      " << n_bs_spec << std::endl;
    save_state_file << "n_eps_bs       " << eps_bs.size() << std::endl;
    save_state_file << "n_el_spec_bins " << n_el_spec_bins << std::endl;
    save_state_file << "alpha          " << alpha << std::endl;
    save_state_file << "f_heat         " << f_heat << std::endl;
    save_state_file << "gamma_ad       " << gamma_ad << std::endl;
    // save_state_file << "boundary_L     " << boundary_L << std::endl;
    // save_state_file << "boundary_R     " << boundary_R << std::endl;
    save_state_file << "t              " << t << std::endl;
    save_state_file << "iter           " << iter << std::endl;
    // save_state_file << "NUM_PROC       " << NUM_PROC << std::endl;
    save_state_file << "photons_per_split " << photons_per_split << std::endl;
    save_state_file << "w_i            " << w_i << std::endl;
    save_state_file << "n_eps_splits_bins " << eps_splits.size() << std::endl;
    save_state_file << std::endl;
    save_state_file << "---" << std::endl;
    save_state_file << std::endl;

    for(unsigned int j=0; j<eps_splits.size(); j++) {
      save_state_file << " " << eps_splits[j];
      if(j == eps_splits.size()-1) {
	save_state_file << std::endl;
      }
    }

    for(int j=0; j<n_bs-1; j++) {
      save_state_file << " " << Z_pms[j];
      if(j == n_bs-2) {
	save_state_file << std::endl;
      }
    }

    save_state_file.flush();
    save_state_file.close();
  }

  for(int current_rank=0; current_rank<NUM_PROC; current_rank++) {
    wait_for_all_threads();
    if(current_rank == RANK) {

      // Do worker (and also master) save

      // // Open state file
      // if(alpha == 0) {
      // 	// save_state_file.open("output/pla_sym/"+sim_name+".rad_state", std::fstream::out | std::fstream::app);
      // 	save_state_file.open("output/pla_sym/"+sim_name+".rsf", std::fstream::out | std::fstream::app);
      // } else if(alpha == 1) {
      // 	// save_state_file.open("output/cyl_sym/"+sim_name+".rad_state", std::fstream::out | std::fstream::app);
      // 	save_state_file.open("output/cyl_sym/"+sim_name+".rsf", std::fstream::out | std::fstream::app);
      // } else if(alpha == 2) {
      // 	// save_state_file.open("output/sph_sym/"+sim_name+".rad_state", std::fstream::out | std::fstream::app);
      // 	save_state_file.open("output/sph_sym/"+sim_name+".rsf", std::fstream::out | std::fstream::app);
      // }

      // save_state_file.open("state_files/"+sim_name+".rsf", std::fstream::out | std::fstream::app);
      // save_state_file.open(rad_output_state_file.substr(0, rad_output_state_file.find("."))+"_prev.rsf", std::ios::binary);
      save_state_file.open(rad_output_state_file, std::fstream::out | std::fstream::app);

      // Setting scientific notation and precision
      save_state_file << std::scientific;
      save_state_file.precision(12);

      // Save all photons
      for(unsigned int i=0; i<photons.size(); i++) {
	if(i % save_photon_divider == 0) {
	  save_state_file << photons[i].get_r() << " ";
	  save_state_file << photons[i].get_mu() << " ";
	  save_state_file << photons[i].get_eps() << " ";
	  save_state_file << photons[i].get_w()*save_photon_divider << std::endl;
	}
      }

      // Close the file
      save_state_file.flush();
      save_state_file.close();

    }
  }
}

void RADManager::load_state(std::string sim_name_of_state) {

  // // Open state file
  // if(alpha == 0) {
  //   load_state_file.open("output/pla_sym/"+sim_name_of_state+".rad_state", std::fstream::in);
  // } else if(alpha == 1) {
  //   load_state_file.open("output/cyl_sym/"+sim_name_of_state+".rad_state", std::fstream::in);
  // } else if(alpha == 2) {
  //   load_state_file.open("output/sph_sym/"+sim_name_of_state+".rad_state", std::fstream::in);
  // }

  // Open state file
  // load_state_file.open("output/"+sim_name_of_state+".rad_state", std::fstream::in);
  // load_state_file.open("output/"+sim_name_of_state, std::fstream::in);
  load_state_file.open(sim_name_of_state, std::fstream::in);
  if(!load_state_file.is_open() && RANK == 0) {
    // std::cout << std::endl << "ERROR: Could not load the file " << sim_name_of_state << ".rad_state" << std::endl << std::endl;
    std::cout << std::endl << "ERROR: Could not load the file " << sim_name_of_state << std::endl << std::endl;
  }

  // Parse state file
  int n_phs = 0, n_bs_p = 0;
  double eps_min = 0, eps_max = 0;
  double n_eps_splits_bins = 0;
  int n_eps_bs = 0;
  int reading_header = 1;

  std::string tmp;
  while(reading_header == 1) {

    load_state_file >> tmp;

    if(tmp == "n_phs") {
      load_state_file >> tmp;
      n_phs = std::stoi(tmp);

    } else if(tmp == "n_bs") {
      load_state_file >> tmp;
      n_bs_p = std::stoi(tmp);

    } else if(tmp == "eps_min") {
      load_state_file >> tmp;
      eps_min = std::stod(tmp);

    } else if(tmp == "eps_max") {
      load_state_file >> tmp;
      eps_max = std::stod(tmp);

    } else if(tmp == "n_bs_spec") {
      load_state_file >> tmp;
      set_n_bs_spec(std::stoi(tmp));

    } else if(tmp == "n_eps_bs") {
      load_state_file >> tmp;
      n_eps_bs = std::stoi(tmp);

    } else if(tmp == "n_el_spec_bins") {
      load_state_file >> tmp;
      set_n_el_spec_bins(std::stoi(tmp));

    } else if(tmp == "alpha") {
      load_state_file >> tmp;
      set_alpha(std::stoi(tmp));

    } else if(tmp == "f_heat") {
      load_state_file >> tmp;
      set_f_heat(std::stod(tmp));

    } else if(tmp == "gamma_ad") {
      load_state_file >> tmp;
      set_gamma_ad(std::stod(tmp));

    // } else if(tmp == "boundary_L") {
    //   load_state_file >> tmp;
    //   set_boundary_L(std::stoi(tmp));

    // } else if(tmp == "boundary_R") {
    //   load_state_file >> tmp;
    //   set_boundary_R(std::stoi(tmp));

    } else if(tmp == "t") {
      load_state_file >> tmp;
      set_t(std::stod(tmp));

    } else if(tmp == "iter") {
      load_state_file >> tmp;
      set_iter(std::stoi(tmp));

    } else if(tmp == "photons_per_split") {
      load_state_file >> tmp;
      set_photons_per_split(std::stoi(tmp));

    } else if(tmp == "w_i") {
      load_state_file >> tmp;
      w_i = std::stod(tmp);

    } else if(tmp == "n_eps_splits_bins") {
      load_state_file >> tmp;
      n_eps_splits_bins = std::stoi(tmp);

    } else if(tmp == "---") {
      reading_header = 0;
    }

  }

  // Set spec grid
  set_spec_grid(n_eps_bs, eps_min, eps_max);

  // Load the eps_splits grid
  eps_splits.resize(n_eps_splits_bins);
  for(int i=0; i<n_eps_splits_bins; i++) {
    load_state_file >> tmp;
    eps_splits[i] = std::stod(tmp);
  }

  // Load the Z_pm grid
  std::vector<double> Z_pms_p(n_bs_p-1);
  for(int i=0; i<n_bs_p-1; i++) {
    load_state_file >> tmp;
    Z_pms_p[i] = std::stod(tmp);
  }
  set_Z_pms(Z_pms_p);

  // Load the photons
  // - Find the range i_min to i_max for this given rank
  // - Resize the photon vector
  // - Loop over all lines in the file, and set photon properties only when in between i_min and i_max

  int n_phs_thread = int(floor(double(n_phs)/double(NUM_PROC)));
  int n_phs_this_thread = n_phs_thread;
  if(RANK == NUM_PROC-1) {
    n_phs_this_thread += n_phs-NUM_PROC*n_phs_thread;
  }

  int i_min = RANK*n_phs_thread;
  int i_max = i_min + n_phs_this_thread - 1;

  photons.resize(n_phs_this_thread*load_photon_multiplier);
  int j, broken = 0;
  double r, mu, eps, w;
  for(int i=0; i<n_phs; i++) {

    load_state_file >> tmp;
    r = std::stod(tmp);
    load_state_file >> tmp;
    mu = std::stod(tmp);
    load_state_file >> tmp;
    eps = std::stod(tmp);
    load_state_file >> tmp;
    w = std::stod(tmp);

    if(i >= i_min && i <= i_max) {
      // j = i-i_min;
      // photons[j].set_r(r);
      // photons[j].set_mu(mu);
      // photons[j].set_eps(eps);
      // photons[j].set_w(w);

      j = (i-i_min)*load_photon_multiplier;
      for(int k=0; k<load_photon_multiplier; k++) {
	photons[j+k].set_r(r);
	photons[j+k].set_mu(mu);
	photons[j+k].set_eps(eps);
	photons[j+k].set_w(w/load_photon_multiplier);
      }

      if(mu > 1 || mu < -1 || eps < 0 || w < 0) {
	broken += 1;
	for(int k=0; k<load_photon_multiplier; k++) {
	  photons[j+k].set_alive(0);
	}
	
      }
    }
  }

  if(RANK == 0 && broken > 0) {
    std::cout << std::endl << "ERROR: " << broken << " photons in the 'save_state' are broken..." << std::endl << std::endl;
  }
  load_state_file.close();

}

void RADManager::close_all_files() {
  // Closing all files
  if(RANK == 0) {
    zpm_file.close();
    pre_file.close();
    ugl_file.close();
    cte_file.close();
    spc_file.close();
    ispc_file.close();
    i0s_file.close();
    i1s_file.close();
    zet_file.close();
  }
}
