//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "rad_manager.h"
#include <vector>
#include <iostream>
#include "math.h"
#include "rad_propagation.h"
#include "num_distributions.h"
#include "smoothing.h"
#include "../mhd/physical_constants.h"

void RADManager::set_spec_grid(int n_eps_bs, double eps_min, double eps_max) {

  // Produce the (lab frame) energy grid for the spectrum
  eps_bs.resize(n_eps_bs);
  dlneps = log(eps_max/eps_min)/(n_eps_bs-1);
  double lneps;
  for(int k=0; k<n_eps_bs; k++) {
    lneps = log(eps_min) + k*dlneps;
    eps_bs[k] = exp(lneps);
  }

  // Produce the (comoving frame) energy grid for computing the Compton temperature
  int n_eps_bs_Compton = 1001;
  double eps_min_Compton = 1e-10;
  double eps_max_Compton = 1e0;
  eps_bs_Compton.resize(n_eps_bs_Compton);
  dlneps_Compton = log(eps_max_Compton/eps_min_Compton)/(n_eps_bs_Compton-1);
  for(int k=0; k<n_eps_bs_Compton; k++) {
    lneps = log(eps_min_Compton) + k*dlneps_Compton;
    eps_bs_Compton[k] = exp(lneps);
  }

}

// --------------------
//  Radiation pressure
// --------------------

void RADManager::compute_rad_pressure() {
  // Computes the radiation pressure from the MC photons
  // Also computes the comoving radiation energy density
  // Also computes the local photon-to-proton ratio

  // Flush the pressure vector
  std::fill(p_gammas.begin(), p_gammas.end(), 0);
  std::fill(I_0s.begin(), I_0s.end(), 0);
  std::fill(zetas.begin(), zetas.end(), 0);

  int j;
  double r, mu, eps, w;
  double gamma, v, D, mu_p;
  double Dr, r_avg, A=1;
  for(unsigned int k=0; k<photons.size(); k++) {
    if(photons[k].get_alive() == 1) {

      // Photon properties
      r = photons[k].get_r();
      mu = photons[k].get_mu();
      eps = photons[k].get_eps();
      w = photons[k].get_w();

      // Find r bin
      j = bisect_without_ghosts(r_bs, r);

      v = vs[j];
      gamma = 1./sqrt(1.-v*v);
      D = 1./gamma/(1.-v*mu);
      Dr = r_bs[j+1]-r_bs[j];
      r_avg = .5*(r_bs[j+1]+r_bs[j]);
      if(alpha == 2) {
	A = r_avg*r_avg;
      } else if(alpha == 1) {
	A = r_avg;
      } else if(alpha == 0) {
	A = 1;
      }

      // Add to pressure
      mu_p = (mu-v)/(1.-v*mu);
      p_gammas[j] += mu_p*mu_p*eps*w*m_e/D/D/A/Dr;
      I_0s[j] += w*eps*m_e/D/D/A/Dr;
      // zetas[j] += w/D/D/A/Dr/(rhos[j]/m_p);
      zetas[j] += w/D/A/Dr/(rhos[j]/m_p);
    }
  }

}

// ---------------------
//  Compton temperature
// ---------------------

double RADManager::get_s(double eps) {

  double s, A, B, C, D, E;
  double eps_t = 1e-4;
  if(eps > eps_t) {
    A = 3./8./eps;
    B = 1./eps - 2./eps/eps - 3./eps/eps/eps;
    C = log(1.+2.*eps);
    D = 2.*(-10.*eps*eps*eps*eps + 51.*eps*eps*eps + 93.*eps*eps + 51.*eps + 9.);
    E = 3.*eps*eps*(1.+2.*eps)*(1.+2.*eps)*(1.+2.*eps);
    s = A*(B*C + D/E);
  } else {
    s = 1.;
  }
  return s;

}

double RADManager::get_q(double eps) {
  double q, A, B, C, D, E;
  double eps_t = 1e-4;
  if(eps > eps_t) {
    A = 3./32.;
    B = 3./2./eps - 2./eps/eps - 13./2./eps/eps/eps;
    C = log(1.+2.*eps);
    D = -216.*eps*eps*eps*eps*eps*eps + 476.*eps*eps*eps*eps*eps + 2066.*eps*eps*eps*eps + 2429.*eps*eps*eps + 1353.*eps*eps + 363.*eps + 39.;
    E = 3.*eps*eps*(1.+2.*eps)*(1.+2.*eps)*(1.+2.*eps)*(1.+2.*eps)*(1.+2.*eps);
    q = A*(B*C + D/E);
  } else {
    q = 1.;
  }
  return(q);
}

void RADManager::compute_calJ_prime_and_lab_frame_spectrum() {

  // The lab frame spectrum is defined as eps(du_gamma/deps) = du_gamma/dlneps
  // calJ_primes is used for computing the Compton temperature, and for the "isotropic" pair production algorithm
  // Computes the comoving photon number spectrum (caligraphy J, not I, since its considered isotropic)
  // Assuming the comoving spectrum is isotropic (which it is not) to reduce the dimensionality of the pair production problem

  int j_spec = 2; // Spatial counter for the "real" grid

  // Initializing spectrum grid (wide)
  m_bs_spec.resize(n_bs_spec);
  r_bs_spec.resize(n_bs_spec);
  dudlneps_specs.resize((n_bs_spec-1)*(eps_bs.size()-1));

  if(uniform_spec_radius_bins == 1) {
    // Uniform bins in the radial coordinate (better for making observed spectrum movie, because radius translates to observer time)

    // Computing the radial boundaries
    double Dr_spec_tmp = (r_bs[r_bs.size()-3] - r_bs[2])/(n_bs_spec-1);
    for(int i_spec=0; i_spec<n_bs_spec; i_spec++) {
      r_bs_spec[i_spec] = r_bs[2] + i_spec*Dr_spec_tmp;
    }

    // Computing the corresponding mass coordinates
    m_bs_spec[0] = m_bs[2];
    double dm_spec_tmp, r_1, r_2, r_l;
    for(int i_spec=1; i_spec<n_bs_spec; i_spec++) {
      r_1 = r_bs_spec[i_spec-1];
      r_2 = r_bs_spec[i_spec];

      dm_spec_tmp = 0;
      r_l = r_1;
      while(r_bs[j_spec+1] < r_2) {
	// Add to bin
	dm_spec_tmp = m_bs[j_spec+1]-m_bs_spec[i_spec-1];
	r_l = r_bs[j_spec+1];
	j_spec += 1;
      }

      // Add final part to bin
      if(alpha == 0) {
	dm_spec_tmp += rhos[j_spec]/sqrt(1.-vs[j_spec]*vs[j_spec])*(r_2 - r_l);
      } else if(alpha == 1) {
	std::cout << "ERROR: alpha = 1 is not supported by function 'void RADManager::compute_calJ_prime_and_lab_frame_spectrum()'" << std::endl;
      } else if(alpha == 2) {
	dm_spec_tmp += rhos[j_spec]/sqrt(1.-vs[j_spec]*vs[j_spec])/3.*(r_2*r_2*r_2 - r_l*r_l*r_l);
      }

      m_bs_spec[i_spec] = m_bs_spec[i_spec-1] + dm_spec_tmp;
    }
    // Correcting to make the total grid mass "exactly" the same
    m_bs_spec[m_bs_spec.size()-1] = m_bs[m_bs.size()-3];

  } else { // if(uniform_spec_radius_bins != 1)
    // Uniform bins in the mass coordinate (better for plotting spectra during the optically thick phase)

    // Computing the mass boundaries
    double Dm_spec_tmp = (m_bs[m_bs.size()-3] - m_bs[2])/(n_bs_spec-1);
    for(int i_spec=0; i_spec<n_bs_spec; i_spec++) {
      m_bs_spec[i_spec] = m_bs[2] + i_spec*Dm_spec_tmp;
    }

    //

    // Computing the corresponding radial coordinates
    r_bs_spec[0] = r_bs[2];
    double dr_spec_tmp, m_1, m_2, m_l;
    for(int i_spec=1; i_spec<n_bs_spec; i_spec++) {
      m_1 = m_bs_spec[i_spec-1];
      m_2 = m_bs_spec[i_spec];

      dr_spec_tmp = 0;
      m_l = m_1;
      while(m_bs[j_spec+1] < m_2) {
	// Add to bin
	dr_spec_tmp = r_bs[j_spec+1]-r_bs_spec[i_spec-1];
	m_l = m_bs[j_spec+1];
	j_spec += 1;
      }

      // Add final part to bin
      if(alpha == 0) {
	dr_spec_tmp += sqrt(1.-vs[j_spec]*vs[j_spec])/rhos[j_spec]*(m_2 - m_l);
      } else if(alpha == 1) {
	std::cout << "ERROR: alpha = 1 is not supported by function 'void RADManager::compute_calJ_prime_and_lab_frame_spectrum()'" << std::endl;
      } else if(alpha == 2) {
	// dr_spec_tmp += rhos[j_spec]/sqrt(1.-vs[j_spec]*vs[j_spec])/3.*(m_2*m_2*m_2 - m_l*m_l*m_l); // CHANGE
	dr_spec_tmp += pow(pow(r_bs[j_spec+1], 3) + 3.*(m_2 - m_l)*sqrt(1.-vs[j_spec]*vs[j_spec])/rhos[j_spec], 1./3.) - r_bs[j_spec+1];
      }

      r_bs_spec[i_spec] = r_bs_spec[i_spec-1] + dr_spec_tmp;
    }
    // Correcting to make the total grid mass "exactly" the same
    r_bs_spec[r_bs_spec.size()-1] = r_bs[r_bs.size()-3];

    //

  }

  // Initiate spectrum
  std::fill(calJ_primes.begin(), calJ_primes.end(), 0);
  std::fill(I_1s.begin(), I_1s.end(), 0);
  std::fill(dudlnepss.begin(), dudlnepss.end(), 0);
  std::fill(dudlneps_ints.begin(), dudlneps_ints.end(), 0);
  std::fill(dudlneps_specs.begin(), dudlneps_specs.end(), 0);

  // Resizing and initiate comoving intensity
  calI_primes.resize(( r_bs.size()-1 )*( eps_p_bs.size()-1 )*( mu_p_bs.size()-1 ));
  std::fill(calI_primes.begin(), calI_primes.end(), 0);

  int n_eps_bs = eps_bs.size();
  int nr_eps_bins = eps_p_bs.size()-1;
  int nr_mu_bins = mu_p_bs.size()-1;
  int j, m, q, l;
  double r, eps, w;
  double Dr, r_avg, A=1;
  double Dr_full = r_bs[r_bs.size()-3]-r_bs[2];
  double D, mu_prime, dmu_prime, eps_prime, deps_prime, gamma, v, mu;
  int l_spec;
  double Dr_spec, r_avg_spec, A_spec=1;
  for(unsigned int k=0; k<photons.size(); ++k) {
    if(photons[k].get_alive() == 1) {
      r = photons[k].get_r();
      mu = photons[k].get_mu();
      eps = photons[k].get_eps();
      w = photons[k].get_w();
      // if(mu > 1) {
      // 	std::cout << "r = " << r << ", mu = " << mu << ", eps = " << eps << ", w = " << w << std::endl;
      // }

      // Find r bin
      j = bisect_without_ghosts(r_bs, r);
      Dr = r_bs[j+1]-r_bs[j];
      r_avg = .5*(r_bs[j+1]+r_bs[j]);
      if(alpha == 2) {
	A = r_avg*r_avg;
      } else if(alpha == 1) {
	A = r_avg;
      } else if(alpha == 0) {
	A = 1;
      }

      // Find r bin (wide spec)
      j_spec = bisect(r_bs_spec, r);
      Dr_spec = r_bs_spec[j_spec+1]-r_bs_spec[j_spec];
      r_avg_spec = .5*(r_bs_spec[j_spec+1]+r_bs_spec[j_spec]);
      if(alpha == 2) {
	A_spec = r_avg_spec*r_avg_spec;
      } else if(alpha == 1) {
	A_spec = r_avg_spec;
      } else if(alpha == 0) {
	A_spec = 1;
      }

      // Find comoving energy
      v = vs[j];
      gamma = 1./sqrt(1.-v*v);
      eps_prime = eps*gamma*(1.-v*mu);

      // Find eps_prime bin
      // m = bisect(eps_bs, eps_prime);
      m = bisect(eps_bs_Compton, eps_prime);

      // Add to spectrum
      // l = m + (n_eps_bs-1)*j;
      l = m + (eps_bs_Compton.size()-1)*j;
      deps_prime = eps_prime*dlneps_Compton;
      calJ_primes[l] += w/deps_prime/4./pi/A/Dr;

      // Find eps bin
      m = bisect(eps_bs, eps);

      // Add to spectrum
      l = m + (n_eps_bs-1)*j;
      if(eps > eps_bs[0]) {
	dudlnepss[l] += eps*w*m_e/dlneps/A/Dr;
	dudlneps_ints[m] += eps*w*m_e/dlneps/A/Dr_full;

	// Add to wide spectrum
	l_spec = m + (n_eps_bs-1)*j_spec;
	if(eps > eps_bs[0]) {
	  dudlneps_specs[l_spec] += eps*w*m_e/dlneps/A_spec/Dr_spec;
	}
      }

      // Add to first intensity moment
      mu_prime = (mu - v)/(1. - mu*v);
      D = 1./gamma/(1.-v*mu);
      I_1s[j] += w*eps_prime*mu_prime*m_e/D/A/Dr;

      // Add to "full" intensity
      // eps_p_bs is used for pair production, which needs bins centered around eps ~ 1 mostly, while eps_bs is used by the other spectra
      m = bisect(eps_p_bs, eps_prime);
      q = bisect(mu_p_bs, mu_prime);

      dmu_prime = mu_p_bs[q+1]-mu_p_bs[q];
      deps_prime = eps_p_bs[m+1]-eps_p_bs[m];
      l = m + nr_eps_bins*(q + nr_mu_bins*j);
      calI_primes[l] += w/2./pi/A/Dr/deps_prime/dmu_prime;

    }
  }

}

void RADManager::compute_Compton_temperature_AB_vectors() {
  // Computes the Compton temperature from the MC photons using the formula from Andrei's 1995 paper

  int l;
  double A, B, eps, s, q;
  for(int j=2; j<n_bs-3; j++) {
    A = 0;
    B = 0;
    for(unsigned int i=0; i<eps_bs_Compton.size()-1; i++) {
      eps = sqrt(eps_bs_Compton[i+1]*eps_bs_Compton[i]); // Geometric mean
      s = get_s(eps);
      q = get_q(eps);
      // s = 1.;
      // q = 1.;
      // l = i + (n_eps_bs-1)*j;
      l = i + (eps_bs_Compton.size()-1)*j;
      A += eps*eps*eps*calJ_primes[l]*s*dlneps_Compton;
      B += eps*eps*calJ_primes[l]*q*dlneps_Compton;
    }
    As[j] = A;
    Bs[j] = B;
  }
}

double RADManager::apply_empirical_Compton_temperature_correction(double theta_C) {

  // My Compton temperature calculation becomes inaccurate when eps (or theta_C) start to approach unity,
  // even though I use leading order correcting factors (get_s(eps), get_q(eps)). For some of my problems,
  // I need to work with theta_C <~ 0.1. The mismatch can then become important.

  // This function is based on empirical tests: freezing the electron temperature to a value, letting photons
  // interact with the electrons, then checking the Compton temperature of the radiation at steady state. Doing this
  // for several electron temperatures, then finding an analytical function that fits the discrepancy.

  // This correction provides a really good fit up to temperatures of 0.1, but _only for a thermal radiation spectrum_.

  double dtheta = -.25*pow(theta_C, 1.35);
  return theta_C + dtheta;
}

void RADManager::compute_Compton_temperature() {
  // Computes the Compton temperature from the MC photons using the formula from Andrei's 1995 paper

  for(int j=2; j<n_bs-3; j++) {
    if(Bs[j] != 0.) {
      theta_Cs[j] = As[j]/4./Bs[j];
      theta_Cs[j] = apply_empirical_Compton_temperature_correction(theta_Cs[j]);
    } else {
      theta_Cs[j] = 0;
    }
  }
}

void RADManager::compute_Compton_temperature_AB_vectors_prop() {
  // Computes the Compton temperature from the MC photons using the formula from Andrei's 1995 paper

  int l;
  double A, B, eps, s, q;
  for(int j=2; j<n_bs-3; j++) {
    A = 0;
    B = 0;
    for(unsigned int i=0; i<eps_bs_Compton.size()-1; i++) {
      eps = sqrt(eps_bs_Compton[i+1]*eps_bs_Compton[i]); // Geometric mean
      s = get_s(eps);
      q = get_q(eps);
      // s = 1.;
      // q = 1.;
      l = i + (eps_bs_Compton.size()-1)*j;
      A += eps*eps*eps*calJ_primes_prop[l]*s*dlneps_Compton;
      B += eps*eps*calJ_primes_prop[l]*q*dlneps_Compton;
    }
    As[j] = A;
    Bs[j] = B;
  }
}

// void RADManager::compute_I_0s_I_1s_calJ_primes() {

//   // In this function I will compute I_0s, I_1s, calJ_primes

//   // Clear the vectors
//   std::fill(I_0s.begin(), I_0s.end(), 0);
//   std::fill(I_1s.begin(), I_1s.end(), 0);
//   std::fill(calJ_primes.begin(), calJ_primes.end(), 0);

//   // Pre-compute bin volumes
//   std::vector<double> DVs;
//   DVs.resize(vs.size());
//   double r_avg, Dr, A = 1;
//   for(unsigned int j=2; j<vs.size()-2; j++) {
//     Dr = r_bs[j+1] - r_bs[j];
//     r_avg = .5*(r_bs[j+1]+r_bs[j]);
//     if(alpha == 2) {
//       A = r_avg*r_avg;
//     } else if(alpha == 1) {
//       A = r_avg;
//     } else if(alpha == 0) {
//       A = 1;
//     }
//     DVs[j] = A*Dr;
//   }

//   // Loop over all photons
//   int j, m, l;
//   double r, mu, eps, w;
//   double v, gamma, D, DV, mu_prime, eps_prime, deps_prime;

//   for(unsigned int k=0; k<photons.size(); k++) {
//     if(photons[k].get_alive() == 1) {

//       // Photon properties
//       r = photons[k].get_r();
//       mu = photons[k].get_mu();
//       eps = photons[k].get_eps();
//       w = photons[k].get_w();

//       // Find r bin
//       j = bisect_without_ghosts(r_bs, r);

//       // Find bin properties
//       DV = DVs[j];
//       v = vs[j];
//       gamma = 1./sqrt(1.-v*v);
//       D = 1./gamma/(1.-v*mu);

//       // Add to I_0s
//       I_0s[j] += w*eps*m_e/D/D/DV;

//       // Find comoving energy
//       eps_prime = eps*gamma*(1.-v*mu);

//       // Add to I_1s
//       mu_prime = (mu - v)/(1. - mu*v);
//       I_1s[j] += w*eps_prime*mu_prime*m_e/D/DV;

//       // Add to spectrum
//       m = bisect(eps_bs, eps_prime);
//       l = m + (eps_bs.size()-1)*j;
//       deps_prime = eps_prime*dlneps;
//       calJ_primes[l] += w/deps_prime/4./pi/DV;

//     }
//   }

// }

void RADManager::compute_I_0s_I_1s_calJ_primes(int use_pairs) {

  // In this function I will compute I_0s, I_1s, calJ_primes, used for radiation moment sources
  // If using pairs, compute also calI_primes (using the same loop, for speed), which will be used later

  // Clear the vectors
  std::fill(I_0s.begin(), I_0s.end(), 0);
  std::fill(I_1s.begin(), I_1s.end(), 0);
  std::fill(calJ_primes.begin(), calJ_primes.end(), 0);

  if(use_pairs == 1) {
    // Resizing and initiate comoving intensity
    unsigned int s = ( r_bs.size()-1 )*( eps_p_bs.size()-1 )*( mu_p_bs.size()-1 );
    if(calI_primes.size() != s) {
      calI_primes.resize(s);
    }
    std::fill(calI_primes.begin(), calI_primes.end(), 0);
  }

  // Pre-compute bin volumes
  std::vector<double> DVs;
  DVs.resize(vs.size());
  double r_avg, Dr, A = 1;
  for(unsigned int j=2; j<vs.size()-2; j++) {
    Dr = r_bs[j+1] - r_bs[j];
    r_avg = .5*(r_bs[j+1]+r_bs[j]);
    if(alpha == 2) {
      A = r_avg*r_avg;
    } else if(alpha == 1) {
      A = r_avg;
    } else if(alpha == 0) {
      A = 1;
    }
    DVs[j] = A*Dr;
  }

  // Loop over all photons
  int nr_eps_bins = eps_p_bs.size()-1;
  int nr_mu_bins = mu_p_bs.size()-1;
  int j, m, l, q;
  double r, mu, eps, w;
  double v, gamma, D, DV;
  double mu_prime, dmu_prime, eps_prime, deps_prime;

  for(unsigned int k=0; k<photons.size(); k++) {
    if(photons[k].get_alive() == 1) {

      // Photon properties
      r = photons[k].get_r();
      mu = photons[k].get_mu();
      eps = photons[k].get_eps();
      w = photons[k].get_w();

      // Find r bin
      j = bisect_without_ghosts(r_bs, r);

      // Find bin properties
      DV = DVs[j];
      v = vs[j];
      gamma = 1./sqrt(1.-v*v);
      D = 1./gamma/(1.-v*mu);

      // Add to I_0s
      I_0s[j] += w*eps*m_e/D/D/DV;

      // Find comoving energy
      eps_prime = eps*gamma*(1.-v*mu);

      // Add to I_1s
      mu_prime = (mu - v)/(1. - mu*v);
      I_1s[j] += w*eps_prime*mu_prime*m_e/D/DV;

      // Add to spectrum
      m = bisect(eps_bs_Compton, eps_prime);
      l = m + (eps_bs_Compton.size()-1)*j;
      deps_prime = eps_prime*dlneps_Compton;
      calJ_primes[l] += w/deps_prime/4./pi/DV;

      // If pairs, add to calI_primes
      if(use_pairs == 1) {
	if(skip_pair_calcs[j] > 0) {

	  // Add to calI_primes
	  m = bisect(eps_p_bs, eps_prime);
	  q = bisect(mu_p_bs, mu_prime);
	  l = m + nr_eps_bins*(q + nr_mu_bins*j);
	  dmu_prime = mu_p_bs[q+1]-mu_p_bs[q];
	  deps_prime = eps_p_bs[m+1]-eps_p_bs[m];
	  calI_primes[l] += w/2./pi/DV/deps_prime/dmu_prime;
	}

      }

    }
  }

}

void RADManager::compute_calI_primes() {

  // In this function I will compute calI_primes

  // Resizing and initiate comoving intensity
  calI_primes.resize(( r_bs.size()-1 )*( eps_p_bs.size()-1 )*( mu_p_bs.size()-1 ));
  std::fill(calI_primes.begin(), calI_primes.end(), 0);

  // Pre-compute bin volumes
  std::vector<double> DVs;
  DVs.resize(vs.size());
  double r_avg, Dr, A = 1;
  for(unsigned int j=2; j<vs.size()-2; j++) {
    Dr = r_bs[j+1] - r_bs[j];
    r_avg = .5*(r_bs[j+1]+r_bs[j]);
    if(alpha == 2) {
      A = r_avg*r_avg;
    } else if(alpha == 1) {
      A = r_avg;
    } else if(alpha == 0) {
      A = 1;
    }
    DVs[j] = A*Dr;
  }

  // Looping over all photons
  int nr_eps_bins = eps_p_bs.size()-1;
  int nr_mu_bins = mu_p_bs.size()-1;
  int j, m, q, l;
  double r, mu, eps, w;
  double v, gamma, DV;
  double mu_prime, dmu_prime, eps_prime, deps_prime;

  for(unsigned int k=0; k<photons.size(); k++) {
    if(photons[k].get_alive() == 1) {

      // Photon properties
      r = photons[k].get_r();
      mu = photons[k].get_mu();
      eps = photons[k].get_eps();
      w = photons[k].get_w();

      // Find r bin
      j = bisect_without_ghosts(r_bs, r);

      // Find bin properties
      DV = DVs[j];
      v = vs[j];
      gamma = 1./sqrt(1.-v*v);

      // Find comoving energy bin
      eps_prime = eps*gamma*(1.-v*mu);
      m = bisect(eps_p_bs, eps_prime);

      // Find comoving mu bin
      mu_prime = (mu - v)/(1. - mu*v);
      q = bisect(mu_p_bs, mu_prime);

      // Add to calI_primes
      l = m + nr_eps_bins*(q + nr_mu_bins*j);
      dmu_prime = mu_p_bs[q+1]-mu_p_bs[q];
      deps_prime = eps_p_bs[m+1]-eps_p_bs[m];
      calI_primes[l] += w/2./pi/DV/deps_prime/dmu_prime;

    }
  }

}

void RADManager::compute_calI_primes_and_sync() {

  // Starting rad stopwatch
  rad_stopwatch.start();

  // Compute calI_primes
  mom_stopwatch.start();
  compute_calI_primes();
  mom_stopwatch.stop();

  // MPI
  mpi_stopwatch.start();
  collect_vector(calI_primes);
  distribute_vector(calI_primes);
  mpi_stopwatch.stop();

  // Stopping rad stopwatch
  rad_stopwatch.stop();

}

void RADManager::get_radiation_moments_from_prop(std::vector<double> &I_0s_p, std::vector<double> &I_1s_p, std::vector<double> &theta_Cs_p) {
  // average_over_optical_depth(I_0s_prop, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  // average_over_optical_depth(I_1s_prop, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  // compute_Compton_temperature_AB_vectors_prop();
  // compute_Compton_temperature();
  // average_over_optical_depth(theta_Cs, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  I_0s_p = I_0s_prop;
  I_1s_p = I_1s_prop;
  theta_Cs_p = theta_Cs;
}

void RADManager::get_radiation_moments_v2(std::vector<double> &I_0s_p, std::vector<double> &I_1s_p, std::vector<double> &theta_Cs_p, std::vector<double> &Z_pms_p, int use_pairs) {

  // A new function, which should replace the old, slower 'get_radiation_moments'

  // Starting rad stopwatch
  rad_stopwatch.start();

  // Compute I_0s, I_1s, calJ_primes
  mom_stopwatch.start();
  compute_I_0s_I_1s_calJ_primes(use_pairs);
  compute_Compton_temperature_AB_vectors();
  mom_stopwatch.stop();

  // MPI
  mpi_stopwatch.start();
  collect_vector(I_0s);
  collect_vector(I_1s);
  collect_vector(As);
  collect_vector(Bs);
  if(use_pairs == 1) collect_vector(calI_primes);
  distribute_vector(I_0s);
  distribute_vector(I_1s);
  distribute_vector(As);
  distribute_vector(Bs);
  if(use_pairs == 1) distribute_vector(calI_primes);
  mpi_stopwatch.stop();

  // Smoothing
  smooth_stopwatch.start();
  average_over_optical_depth(I_0s, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(I_1s, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(As, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(Bs, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  smooth_stopwatch.stop();

  // Fill vectors
  mom_stopwatch.start();
  compute_Compton_temperature();
  I_0s_p = I_0s;
  I_1s_p = I_1s;
  theta_Cs_p = theta_Cs;
  Z_pms_p = Z_pms;
  mom_stopwatch.stop();

  // Stopping rad stopwatch
  rad_stopwatch.stop();

}

void RADManager::get_radiation_moments(std::vector<double> &I_0s_p, std::vector<double> &I_1s_p, std::vector<double> &theta_Cs_p, std::vector<double> &Z_pms_p) {

  // Starting rad stopwatch
  rad_stopwatch.start();

  // Compute I_0s
  mom_stopwatch.start();
  compute_rad_pressure();
  mom_stopwatch.stop();

  // Switching to mpi stopwatch

  // MPI
  mpi_stopwatch.start();
  collect_vector(I_0s);
  distribute_vector(I_0s);
  mpi_stopwatch.stop();

  smooth_stopwatch.start();
  average_over_optical_depth(I_0s, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  smooth_stopwatch.stop();

  // Compute I_1s
  mom_stopwatch.start();
  compute_calJ_prime_and_lab_frame_spectrum();
  mom_stopwatch.stop();

  // MPI
  mpi_stopwatch.start();
  collect_vector(I_1s);
  distribute_vector(I_1s);
  mpi_stopwatch.stop();

  smooth_stopwatch.start();
  average_over_optical_depth(I_1s, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  smooth_stopwatch.stop();

  // Compute theta_Cs (uses calJ_primes)
  mom_stopwatch.start();
  compute_Compton_temperature_AB_vectors();
  mom_stopwatch.stop();

  // MPI
  mpi_stopwatch.start();
  collect_vector(As);
  collect_vector(Bs);
  distribute_vector(As);
  distribute_vector(Bs);
  mpi_stopwatch.stop();

  smooth_stopwatch.start();
  average_over_optical_depth(As, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(Bs, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  smooth_stopwatch.stop();

  mom_stopwatch.start();
  compute_Compton_temperature();

  // Fill vectors
  I_0s_p = I_0s;
  I_1s_p = I_1s;
  theta_Cs_p = theta_Cs;
  Z_pms_p = Z_pms;
  mom_stopwatch.stop();
  rad_stopwatch.stop();

}
