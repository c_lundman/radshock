//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "rad_propagation.h"
#include <vector>
#include <iostream>
#include "math.h"
#include "../libmhd/physical_constants.h"
#include "mc_photon.h"
#include "num_distributions.h"
#include "rad_proc_scattering.h"
#include "rad_proc_pairprod.h"
#include "ran.h"

// To do:
// - Add pair production mean free path (fix: do this later...)

double get_radiation_time_step(std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &Z_pms, double gamma_ad, int alpha) {

  // Returns a fraction of the shortest scattering time
  // For now, fraction is hard coded to xi_sc=1e-1 (fix?)

  double dt_sc, dt=1e60;
  double xi_sc = 1e-1;
  for(unsigned int j=2; j<r_bs.size()-3; j++) {
    dt_sc = get_scattering_time(vs[j], rhos[j], Z_pms[j]);
    if(xi_sc*dt_sc < dt) {dt = xi_sc*dt_sc;}
  }

  // Limit to fraction of dynamical time scale
  double xi_curv = 1e-1;
  if(alpha != 0) {
    double r = r_bs[2];
    if(dt > xi_curv*r) {dt = xi_curv*r;}
  }
  return dt;
}

double get_theta(double rho, double p, double Z_pm, double gamma_ad, double f_heat) {
  // Returns the electron temperature
  // double theta = m_p/m_e*p/rho/(1.+Z_pm)/f_heat - (gamma_ad-1.)*(Z_pm-1.)/(Z_pm+1.)/f_heat;
  double theta = m_p/m_e*p/rho/(1.+Z_pm)/f_heat;
  return theta;
}

double propagate_mu(double mu_i, double r_i, double r_f, int alpha) {

  // Modifying the angle to the local r direction as the photon propagates (in non-planar geometries)

  double mu_f;

  if(alpha == 0) {
    mu_f = mu_i;
  } else {
    mu_f = sqrt(1. - r_i*r_i/r_f/r_f*(1.-mu_i*mu_i));
    if(r_f < r_i) {mu_f *= -1.;}
  }
  if(mu_f != mu_f) {
    // std::cout << "mu_i = " << mu_i << std::endl;
    // std::cout << "r_i = " << r_i << std::endl;
    // std::cout << "r_f = " << r_f << std::endl;
    // std::cout << "r_i^2/r_f^2 = " << r_i*r_i/r_f/r_f << std::endl;
    // std::cout << "(1.-mu_i*mu_i) = " << (1.-mu_i*mu_i) << std::endl;
    // std::cout << "r_i*r_i/r_f/r_f*(1.-mu_i*mu_i) = " << r_i*r_i/r_f/r_f*(1.-mu_i*mu_i) << std::endl;
    mu_f = 0.;
  }
  return mu_f;
}

void perform_propagation(double r_b_L, double r_b_R, double v_b_L, double v_b_R,
			 double r_i, double mu_i, double ds, double dt_remaining,
			 double &r_f, double &mu_f, double &dt_propagated, int &event_type, int alpha, int print_history) {

  // Propagate the photon until the next event. The following are defined as events:
  // 
  // [event_type = 0] The time is up (no interaction, photon simply propagates along straight line)
  // [event_type = 1 or 2 (left or right)] The photon crosses into a neighbouring grid cell before the time is up or interacting
  // [event_type = 3] The photon makes an "interaction" (e.g. scattering, gamma-gamma annihilation etc.)
  //
  // The code signals what happened by modifying the "event_type" variable.

  // Note: r_bs must be given at the same time as r_i!
  // dt_min is the time the photon has to cross a boundary before the time is up or it interacts

  double dt_min = ds;
  if(dt_remaining < dt_min) {dt_min = dt_remaining;}
  double r_new = r_i + mu_i*dt_min;

  if(print_history == 1) {
    std::cout.precision(6);
    std::cout << std::endl << "_perform_propagation:" << std::endl;
    std::cout << "ds = " << ds << std::endl;
    std::cout << "dt_min = " << dt_min << std::endl;
    std::cout << "dt_remaining = " << dt_remaining << std::endl << std::endl;
  }

  if(r_new > r_b_R + v_b_R*dt_min) {
    // Crossing to the right
    event_type = 2;
    dt_propagated = (r_b_R - r_i)/(mu_i - v_b_R); // Time of crossing

    if(print_history == 1) {
      std::cout << "_Crossing right" << std::endl;
      std::cout << "dt_propagated = " << dt_propagated << std::endl;
    }

  } else if(r_new < r_b_L + v_b_L*dt_min) {
    // Crossing to the left
    event_type = 1;
    dt_propagated = (r_b_L - r_i)/(mu_i - v_b_L); // Time of crossing

    if(print_history == 1) {
      std::cout << "_Crossing left" << std::endl;
      std::cout << "dt_propagated = " << dt_propagated << std::endl;
      std::cout << "r_b_L = " << r_b_L << std::endl;
      std::cout << "v_b_L = " << v_b_L << std::endl;
      std::cout << "r_i = " << r_i << std::endl;
      std::cout << "mu_i = " << mu_i << std::endl;
    }

  } else if(dt_remaining < ds) {
    // If not crossing any boundaries and time is up, no interaction occurs
    event_type = 0;
    dt_propagated = dt_remaining;

    if(print_history == 1) {
      std::cout << "_No event" << std::endl;
      std::cout << "dt_propagated = " << dt_propagated << std::endl;
    }

  } else {
    // Interaction!
    event_type = 3;
    dt_propagated = ds;

    if(print_history == 1) {
      std::cout << "_Interaction" << std::endl;
      std::cout << "dt_propagated = " << dt_propagated << std::endl;
    }

  }

  // Correct mu for curvilinear coordinates (if alpha != 0)
  r_f = r_i + mu_i*dt_propagated;
  mu_f = propagate_mu(mu_i, r_i, r_f, alpha);
  if(print_history == 1) {
    std::cout << "r_f = " << r_f << std::endl << std::endl;
  }

}

int find_interaction_type(std::vector<double> inv_lambdas, double inv_lambda_tot, double P_p) {

  // P_p is here drawn from a uniform distribution
  int n_interactions = inv_lambdas.size();
  std::vector<double> Ps(n_interactions);

  // Filling the cumulative interaction probability vector
  Ps[0] = inv_lambdas[0]/inv_lambda_tot;
  for(int i=1; i<n_interactions; i++) {Ps[i] = Ps[i-1] + inv_lambdas[i]/inv_lambda_tot;}

  // Finding the interaction type
  int interaction_type = 0;
  for(int i=0; i<n_interactions; i++) {
    if(P_p < Ps[i]) {
      interaction_type = i;
      break;
    }
  }
  return interaction_type;
}

int bisect_without_ghosts(const std::vector<double> &xs, double x) {

  // A copy of the bisect function from num_distributions.cpp, but where the ghosts are not iterated over

  // Returns l, such that xs[l] < x < xs[l+1]
  // Assumes that xs is ordered, such that xs[i] < xs[i+1] for all i

  int m, l=2, h=xs.size()-3;
  while(h-l != 1) {
    m = (l+h)/2;
    if(xs[m] > x) {
      h = m;
    } else {
      l = m;
    }
  }
  return l;
}

int find_grid_location(std::vector<double> &r_bs, MC_Photon &photon) {

  // Nudge if needed
  double r_i = photon.get_r();
  double eps_nudge = 1e-20;
  int n_bs = r_bs.size();

  if(r_i < r_bs[2]) {
    photon.set_r(r_bs[2] + eps_nudge);
    r_i = photon.get_r();
  } else if(r_i > r_bs[n_bs-3]) {
    photon.set_r(r_bs[n_bs-3] - eps_nudge);
    r_i = photon.get_r();
  }

  // Bisect
  int l = bisect_without_ghosts(r_bs, r_i);
  return l;
}

int propagate_and_interact_until_time(std::vector<double> &m_bs, std::vector<double> &r_bs, std::vector<double> &vs, std::vector<double> &rhos, std::vector<double> &ps, std::vector<double> &bs,
				      std::vector<double> &Z_pms, std::vector<double> &G0_scs, std::vector<double> &G1_scs,
				      std::vector<double> &G0_ggs, std::vector<double> &G1_ggs, std::vector<double> &dotZ_pms,
				      std::vector<double> &eps_p_bs, std::vector<double> &mu_p_bs, std::vector<double> &alpha_ggs,
				      std::vector<NumDistribution> &electron_energy_dists, MC_Photon &photon, int boundary_L, int boundary_R, double Dt, int alpha, Ran *rng,
				      int RANK, int iter, int i) {

  // For debugging
  int RANK_tracking = -1;
  int iter_tracking = -1;
  int i_tracking = -1;

  // int RANK_tracking = 1;
  // int iter_tracking = 218;
  // int i_tracking = 19195;
  // int iter_loop_tracking = -1;
  // if(iter >= 218 && RANK == 1) {
  //   std::cout << "RANK = " << RANK << ", iter = " << iter << ", i = " << i << std::endl;
  // }

  // This piece of code assumes that there are two ghost cells on either side of the grid

  // Number of interactions
  int interaction_type, n_interactions = 1;
  std::vector<double> inv_lambdas(n_interactions);
  double inv_lambda_tot;

  // The following interaction types exist right now:
  // interaction_type == 0: scattering
  // The (inverse of the) interaction mean free paths must be added to the
  // total (inverse) mean free path in the same order as shown here

  // Photon properties
  double r_i, mu_i, eps_i, w_i;
  double r_f, mu_f, eps_f, w_f;

  // Grid location, number of boundaries
  int n_bs = r_bs.size();

  // Elapsed time for this photon
  double t = 0;

  // Local grid properties
  double r_b_L, r_b_R, v_b_L, v_b_R, gamma_b_L, gamma_b_R;
  double eps_p, mu_p;
  double gamma_e;
  double v, rho, Z_pm;
  double r_avg, A = 1, Delta_r;
  double Delta_m;

  // Inverse of total mean free path
  double dt_propagated, dt_remaining;
  double ds;
  int event_type;
  double eps_nudge = 1e-20;

  // Nudge back if out of grid, and find current grid location
  int j = find_grid_location(r_bs, photon);

  if(RANK == RANK_tracking && iter == iter_tracking && i == i_tracking) {
    std::cout << "j   = " << j << std::endl;
    std::cout << "v   = " << vs[j] << std::endl;
    std::cout << "rho = " << rhos[j] << std::endl;
    std::cout << "p   = " << ps[j] << std::endl;
    std::cout << "b   = " << bs[j] << std::endl;
  }

  // Loop until time is up
  int iter_loop = 0;
  if(iter == iter_tracking && i == i_tracking) {
    
  }
  while(t < Dt && photon.get_alive()) {
    if(iter == iter_tracking && i == i_tracking) {
      std::cout << std::endl << std::endl << std::endl << "iter_loop = " << iter_loop << ": ";
      // std::cout << "iter_loop = " << iter_loop << std::endl;
      photon.print();
    }

    // if(iter == iter_tracking && i == i_tracking && iter_loop == iter_loop_tracking) {
    // if(iter == iter_tracking && i == i_tracking && iter_loop >= 2 && iter_loop <= 4) {
    //   break;
    // }
    iter_loop += 1;

    // Photon properties
    r_i = photon.get_r();
    mu_i = photon.get_mu();
    eps_i = photon.get_eps();

    // Find grid location properties
    v_b_L = .5*(vs[j-1]+vs[j]); // Simple average of bin speeds
    v_b_R = .5*(vs[j]+vs[j+1]); // Simple average of bin speeds

    // The speed of the outmost boundaries must be the same for periodic simulations
    double v_grid_boundaries = .5*(vs[2] + vs[n_bs-4]);
    if(boundary_L == 3) {
      if(j == 2) {v_b_L = v_grid_boundaries;}
      if(j == n_bs-4) {v_b_R = v_grid_boundaries;}
    }

    // Speed correction for periodic bins
    r_b_L = r_bs[j] + v_b_L*t;
    r_b_R = r_bs[j+1] + v_b_R*t;
    v = vs[j];
    rho = rhos[j];
    // p = ps[j];
    Z_pm = Z_pms[j];

    // All individual mean free paths (must be same as n_interactions)
    inv_lambdas[0] = 1./get_scattering_mean_free_path(eps_i, mu_i, rho, v, Z_pm);
    inv_lambdas[1] = 1./get_pairprod_mean_free_path(r_i, eps_i, mu_i, v, r_bs, eps_p_bs, mu_p_bs, alpha_ggs);

    // Find total mean free path and draw a propagation distance
    inv_lambda_tot = 0.;
    for(int i=0; i<n_interactions; i++) {inv_lambda_tot += inv_lambdas[i];};
    ds = -(1./inv_lambda_tot)*log(rng->doub());

    if(RANK == RANK_tracking && iter == iter_tracking && i == i_tracking) {
      std::cout.precision(6);
      std::cout << "__before__" << std::endl;
      std::cout << "j = " << j << std::endl;
      std::cout << "mu_i = " << mu_i << std::endl;
      std::cout << "r_i = " << r_i << std::endl;
      std::cout << "r_b_L = " << r_b_L << std::endl;
      std::cout << "r_b_R = " << r_b_R << std::endl;
      std::cout << "v_b_L = " << v_b_L << std::endl;
      std::cout << "v_b_R = " << v_b_R << std::endl;
      std::cout << "ds = " << ds << std::endl;
      std::cout << "v_grid_boundaries = " << v_grid_boundaries << std::endl;
    }

    // Perform a single propagation (the perform_propagation function updates: r_f, mu_f, dt_propagated, event_type)
    dt_remaining = Dt-t;
    // perform_propagation(r_b_L, r_b_R, v_b_L, v_b_R, r_i, mu_i, ds, dt_remaining, r_f, mu_f, dt_propagated, event_type, alpha);

    if(iter == iter_tracking && i == i_tracking) {
      perform_propagation(r_b_L, r_b_R, v_b_L, v_b_R, r_i, mu_i, ds, dt_remaining, r_f, mu_f, dt_propagated, event_type, alpha, 1);
    } else {
      perform_propagation(r_b_L, r_b_R, v_b_L, v_b_R, r_i, mu_i, ds, dt_remaining, r_f, mu_f, dt_propagated, event_type, alpha);
    }

    if(iter == iter_tracking && i == i_tracking) {
      std::cout << "__after__" << std::endl;
      std::cout << "photon.get_alive() = " << photon.get_alive() << std::endl;
      std::cout << "r_f = " << r_f << std::endl;
      std::cout << "mu_f = " << mu_f << std::endl;
      std::cout << "dt_propagated = " << dt_propagated << std::endl;
      std::cout << "event_type = " << event_type << std::endl;
    }

    // Update time and photon properties
    t += dt_propagated;
    photon.set_r(r_f);
    photon.set_mu(mu_f);

    // If event_type != 0, processing the event
    if(event_type == 1) {

      // Entering left neighbour
      if(j == 2) {

	// Perform boundary condition
	if(boundary_L != 3) {
	  // Reflect in comoving frame of boundary
	  mu_i = photon.get_mu();
	  gamma_b_L = 1./sqrt(1.-v_b_L*v_b_L);
	  eps_p = eps_i*gamma_b_L*(1.-v_b_L*mu_i);
	  mu_p = (mu_i-v_b_L)/(1.-v_b_L*mu_i);
	  mu_p = -mu_p;
	  photon.set_eps(eps_p*gamma_b_L*(1.+v_b_L*mu_p));
	  photon.set_mu((mu_p + v_b_L)/(1.+v_b_L*mu_p));

	} else {
	  // Periodic boundaries
	  r_f = r_bs[n_bs-3] + v_grid_boundaries*t - eps_nudge;
	  photon.set_r(r_f);
	  j = n_bs-4;
	}

      } else {j -= 1;}

    } else if(event_type == 2) {

      // Entering right neighbour
      if(j == n_bs-4) {

	// Perform boundary condition
	if(boundary_R != 3) {
	  // Reflect in comoving frame of boundary
	  mu_i = photon.get_mu();
	  gamma_b_R = 1./sqrt(1.-v_b_R*v_b_R);
	  eps_p = eps_i*gamma_b_R*(1.-v_b_R*mu_i);
	  mu_p = (mu_i-v_b_R)/(1.-v_b_R*mu_i);
	  mu_p = -mu_p;
	  photon.set_eps(eps_p*gamma_b_R*(1.+v_b_R*mu_p));
	  photon.set_mu((mu_p + v_b_R)/(1.+v_b_R*mu_p));
	  if(boundary_R == 4 && alpha == 2) {
	    // WARNING; TMP; FIX!!!
	    photon.set_alive(0);
	  }
	} else {
	  // Periodic boundaries
	  r_f = r_bs[2] + v_grid_boundaries*t + eps_nudge;
	  photon.set_r(r_f);
	  j = 2;
	}

      } else {j += 1;}

    } else if(event_type == 3) {

      if(iter == iter_tracking && i == i_tracking) {
	std::cout << "starting event_type 3" << std::endl;
      }

      // Determine which interaction has occured
      interaction_type = find_interaction_type(inv_lambdas, inv_lambda_tot, rng->doub());

      if(iter == iter_tracking && i == i_tracking) {
	std::cout << "interaction_type = " << interaction_type << std::endl;
      }

      // Stuff needed for filling the source terms (essentially, need to know dV)
      Delta_r = r_bs[j+1]-r_bs[j];
      r_avg = .5*(r_bs[j+1]+r_bs[j]);
      if(alpha == 0) {
	A = 1;
      } else if(alpha == 1) {
	A = r_avg;
      } else if(alpha == 2) {
	A = r_avg*r_avg;
      }

      if(iter == iter_tracking && i == i_tracking) {
	std::cout << "Delta_r = " << Delta_r << std::endl;
      }

      // If scattering, determine gamma_e, then perform the scattering
      if(interaction_type == 0) {

	// Find the electron gamma
	gamma_e = electron_energy_dists[j].sample(rng->doub());
	if(gamma_e < 1.) {std::cout << "gamma_e < 1! (gamma_e - 1 = " << gamma_e - 1. << ")" << std::endl;}
	if(gamma_e == 1.) {
	  std::cout << "gamma_e = 1... eps_i = " << eps_i << std::endl;
	  // std::cout << "label = " << electron_energy_dists[j].get_label() << std::endl;
	  // gamma_e = 1 + 1e-8;
	  electron_energy_dists[j].print_xs();
	  electron_energy_dists[j].print_Ps();
	}

	// Perform scattering on the electron
	eps_i = photon.get_eps();
	mu_i = photon.get_mu();
	w_i = photon.get_w();

	if(iter == iter_tracking && i == i_tracking) {
	  std::cout << "gamma_e - 1 = " << gamma_e - 1. << std::endl;
	  std::cout << "v = " << v << std::endl;
	  std::cout << "eps_i = " << eps_i << std::endl;
	  std::cout << "mu_i = " << mu_i << std::endl;
	  std::cout << "w_i = " << w_i << std::endl;
	}

	perform_scattering(v, eps_i, mu_i, gamma_e, &eps_f, &mu_f, rng);
	photon.set_eps(eps_f);
	photon.set_mu(mu_f);

	if(iter == iter_tracking && i == i_tracking) {
	  std::cout << "mu_f = " << mu_f << std::endl;
	  std::cout << "eps_f = " << eps_f << std::endl;
	}

	// Fill the source terms
	w_f = w_i;
	G0_scs[j] += (w_i*eps_i - w_f*eps_f)*m_e/A/Delta_r/Dt;
	G1_scs[j] += (w_i*eps_i*mu_i - w_f*eps_f*mu_f)*m_e/A/Delta_r/Dt;

      } else if(interaction_type == 1) {

      	// Pair production
	eps_i = photon.get_eps();
	mu_i = photon.get_mu();
	w_i = photon.get_w();
      	photon.set_alive(0);

      	G0_ggs[j] += w_i*eps_i*m_e/A/Delta_r/Dt;
      	G1_ggs[j] += w_i*eps_i*mu_i*m_e/A/Delta_r/Dt;
      	Delta_m = m_bs[j+1]-m_bs[j];
      	dotZ_pms[j] += w_i*m_p/Delta_m/Dt;

      }

    }
  }

  return 1;
}
