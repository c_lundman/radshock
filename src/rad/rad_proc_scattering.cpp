//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "rad_proc_scattering.h"
#include <math.h>
#include "vector_and_matrix_functions.h"
#include "../mhd/physical_constants.h"
#include "ran.h"

/* --------------------------------------- */
/* Functions for drawing scattering angles */
/* --------------------------------------- */

double PDF_mu(double mu, double eps_0) {

  /* Compton scattering cross section integrated over azimuthal angle
     as measured by devices not sensitive to polarization (D = [1, 0, 0, 0]).
     This expression is normalized such that the function returns a
     value between 0 and 1.

     Also called the 'marginal' probability density function (Salvat et al. 2001),
     q(mu) = int_0^2pi p(mu, phi) dphi. */

  /* WARNING WARNING WARNING: The electron spin has been assumed to be isotropic
     and then averaged over! This means that all terms with P3 (circular polarization)
     vanishes from the PDFs for phi and theta. If the electron spin is indeed
     isotropic, this method is advantageous (see comments in function T_N). */

  return (.5*(mu*mu + eps_0*(1.-mu) + 1./(1.+eps_0*(1.-mu)))/(1.+eps_0*(1.-mu))/(1.+eps_0*(1.-mu)));
}

double draw_polar_angle(double eps_0, double S[4], Ran *rng) {

  /* Assumes PDF_mu to have values between (or equal to) 0 and 1 */

  double mu, p;

  while(0 < 1) {
    /* mu = 2.*ran.doub()-1.; */
    /* p = ran.doub(); */
    mu = 2.*rng->doub()-1.;
    p = rng->doub();
    if(p <= PDF_mu(mu, eps_0)) {
      return acos(mu);
    }
  }
}

double PDF_phi(double phi, double theta, double eps_0, double eps, double S[4]) {

  /* The 'conditional' probability density function for phi (Salvat et al. 2001,
     Jeffrey & Kontar 2011), p(phi|mu) = p(mu, phi)/q(mu) where
     q(mu) = int_0^2pi p(mu, phi) dphi. Requires theta to be already drawn. */

  /* WARNING WARNING WARNING: The electron spin has been assumed to be isotropic
     and then averaged over! This means that all terms with P3 (circular polarization)
     vanishes from the PDFs for phi and theta. If the electron spin is indeed
     isotropic, this method is advantageous (see comments in function T_N). */

  double A, B;
  double P1, P2;

  P1 = S[1];
  P2 = S[2];

  A = eps_0/eps + eps/eps_0;
  B = sin(theta)*sin(theta);
  return (1./2./pi)*(A - B*(1. - P1*cos(2.*phi) + P2*sin(2.*phi)))/(A - B);
}

double draw_azimuthal_angle(double theta, double eps_0, double eps, double S[4], Ran *rng) {

  /* Requires PDF_phi to have values between (or equal to) 0 and P_max */

  double phi, p, P_max;
  double A, B;

  double P1, P2;

  P1 = S[1];
  P2 = S[2];

  A = eps_0/eps + eps/eps_0;
  B = sin(theta)*sin(theta);
  P_max = (1./2./pi)*(A - B*(1. - sqrt(P1*P1 + P2*P2)))/(A - B);

  while(0 < 1) {
    /* phi = 2.*pi*ran.doub(); */
    /* p = P_max*ran.doub(); */
    phi = 2.*pi*rng->doub();
    p = P_max*rng->doub();
    if(p <= PDF_phi(phi, theta, eps_0, eps, S)) {
      return(phi);
    }
  }
}

/* ---------------------------------------  */
/* Mean free path and performing scattering */
/* ---------------------------------------  */

double get_eps_after_scattering(double eps_0, double theta) {

  /* Returns the energy of a photon after scattering on an electron
     (in units of the electron rest mass) */

  return eps_0/(1 + eps_0*(1 - cos(theta)));
}

double get_scattering_time(double v, double rho, double Z_pm) {

  // Returns the lab frame scattering time for a "typical" photon in the Thompson regime

  double t_sc, gamma = 1/sqrt(1-v*v);
  t_sc = gamma*m_p/Z_pm/rho/sigma_T;
  return t_sc;
}

double get_scattering_mean_free_path(double eps, double mu, double rho, double v, double Z_pm) {

  // Returns the scattering mean free path

  double gamma = 1./sqrt(1.-v*v);
  double x = eps*gamma*(1.-v*mu);
  double x_break = 3e-2;
  double sigma;

  if(x > x_break) {
    double A = (1.+x)/x/x/x;
    double B = 2.*x*(1.+x)/(1.+2.*x) - log(1.+2.*x);
    double C = 1./2./x*log(1.+2.*x);
    double D = (1.+3.*x)/(1.+2.*x)/(1.+2.*x);
    sigma = 3./4.*(A*B + C - D)*sigma_T;
  } else {
    sigma = sigma_T;
  }

  /* log_1_plus_2x_approx = 2.*x - 2.*x*x + 8.*x*x*x/3. - 4.*x*x*x*x + 32.*x*x*x*x*x/5.; */
  double l = m_p/sigma/Z_pm/rho/gamma/(1.-v*mu);
  return l;
}

void perform_scattering(double v, double eps, double mu, double gamma_e, double *eps_f, double *mu_f, Ran *rng) {

  // Scattering on an isotropic electron with Lorentz factor gamma_e, Klein-Nishina effects included

  double v_e, phi_dum, mu_dum;
  double beta_lc[3], beta_cl[3], beta_ce[3], beta_ec[3];
  double P[4], S[4], k[3], k_0[3];
  double theta_e0, phi_e0;
  double eps_0, eps_sc, theta_sc, phi_sc;

  // No polarization
  S[0] = 1.;
  S[1] = 0.;
  S[2] = 0.;
  S[3] = 0.;

  // Making photon four vector in the lab frame
  phi_dum = 2.*pi*rng->doub();
  make_momentum_vector(eps, acos(mu), phi_dum, k_0);
  make_four_vector(k_0, P);

  // Finding beta_lc (and beta_cl = -beta_lc)
  beta_lc[0] = 0.;
  beta_lc[1] = 0.;
  beta_lc[2] = v;
  beta_cl[0] = 0.;
  beta_cl[1] = 0.;
  beta_cl[2] = -v;

  /* GO TO COMOVING FRAME */

  // Transform four-vector from C_l to C_c
  // Get momentum vector in this frame
  // Find the vector angles
  Lambda(beta_lc, P);
  get_momentum_vector(P, k_0);
  theta_e0 = get_polar_angle(k_0);
  phi_e0 = get_azimuthal_angle(k_0);

  /* GO TO ELECTRON FRAME */

  v_e = sqrt(1.-1./gamma_e/gamma_e);
  mu_dum = 1./v_e*(1. - sqrt(1. + v_e*v_e + 2.*v_e - 4.*v_e*rng->doub()));
  phi_dum = 2.*pi*rng->doub();

  make_momentum_vector(v_e, acos(mu_dum), phi_dum, beta_ce); // "Abusing" this function
  Ry(-theta_e0, beta_ce);
  Rz(-phi_e0, beta_ce);

  beta_ec[0] = -beta_ce[0];
  beta_ec[1] = -beta_ce[1];
  beta_ec[2] = -beta_ce[2];

  // Transform four-vector from C_c to C_e
  // Get momentum vector in this frame
  Lambda(beta_ce, P);
  get_momentum_vector(P, k_0);

  /* PERFORM SCATTERING */

  // Finding photon direction angles in C_e
  theta_e0 = get_polar_angle(k_0);
  phi_e0 = get_azimuthal_angle(k_0);

  // Find the current photon energy from the four-vector
  // Draw polar scattering angle in C_e^{z k_0}
  // Determine energy after scattering
  // Draw azimuthal scattering angle in C_e^{z k_0}
  eps_0 = get_eps(P);
  theta_sc = draw_polar_angle(eps_0, S, rng);
  eps_sc = get_eps_after_scattering(eps_0, theta_sc);
  phi_sc = draw_azimuthal_angle(theta_sc, eps_0, eps, S, rng);

  // Make new photon momentum vector
  // Rotate k from C_e^{z k_0} to C_e
  // Construct new photon four-vector in C_e
  make_momentum_vector(eps_sc, theta_sc, phi_sc, k);
  Ry(-theta_e0, k);
  Rz(-phi_e0, k);
  make_four_vector(k, P);

  /* GO TO COMOVING FRAME */

  Lambda(beta_ec, P);

  /* GO TO LAB FRAME */

  // Transform four-vector from local comoving frame to lab frame
  Lambda(beta_cl, P);

  // Get the energy and direction
  *eps_f = P[0];
  *mu_f = P[3]/P[0];
}
