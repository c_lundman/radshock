//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "rad_manager.h"
#include <vector>
#include <iostream>
#include "math.h"
#include "rad_propagation.h"
#include "rad_proc_pairprod.h"
#include "../mhd/physical_constants.h"

void RADManager::flush_sources() {

  // Simply resetting all "forces" to zero
  std::fill(G0_scs.begin(), G0_scs.end(), 0);
  std::fill(G1_scs.begin(), G1_scs.end(), 0);
  std::fill(G0_ggs.begin(), G0_ggs.end(), 0);
  std::fill(G1_ggs.begin(), G1_ggs.end(), 0);
  std::fill(dotZ_pms.begin(), dotZ_pms.end(), 0);
}

void RADManager::initiate_pairprod_vectors() {

  // These things should not be hard coded!

  double eps_max = 3e1;
  eps_p_bs.resize(31);
  mu_p_bs.resize(11);
  fill_mu_p_bs(mu_p_bs);
  fill_eps_p_bs(eps_p_bs, 1./eps_max, eps_max);
  pre_compute_psi(mu_p_bs, eps_p_bs, psis);

}

void RADManager::compute_pair_mean_free_path_wrapper() {

  // pair_stopwatch.start();
  // compute_calJ_prime_and_lab_frame_spectrum(); // Do I need this?
  // compute_calI_primes(); // <-- NEW: use this function instead!
  // pair_stopwatch.stop();

  // mpi_stopwatch.start();
  // collect_vector(calI_primes);
  // distribute_vector(calI_primes);
  // mpi_stopwatch.stop();

  // This is the heavy function (most likely)...
  pair_stopwatch.start();
  compute_alpha_ggs(r_bs, eps_p_bs, mu_p_bs, psis, calI_primes, alpha_ggs, skip_pair_calcs, RANK, NUM_PROC);
  pair_stopwatch.stop();

  mpi_stopwatch.start();
  collect_vector(alpha_ggs);
  distribute_vector(alpha_ggs);
  mpi_stopwatch.stop();


}

void RADManager::evolve_Z_pm(double dt) {

  // Assumes that the sources (dotZ_pms) are already computed

  double Z_pm, dZ_pm_add, dZ_pm_ann, n, Gamma, dotZ_pm;

  for(unsigned int j=2; j<m_bs.size()-3; j++) {
    Z_pm = Z_pms[j];
    n = rhos[j]/m_p;
    Gamma = 1./sqrt(1.-vs[j]*vs[j]);
    dotZ_pm = dotZ_pms[j];

    dZ_pm_add = dotZ_pm*dt;
    dZ_pm_ann = (3./16.)*sigma_T*n/Gamma*(Z_pm*Z_pm - 1.)*dt;

    Z_pms[j] = Z_pm + dZ_pm_add - dZ_pm_ann;
    if(Z_pms[j] < 1.) {
      Z_pms[j] = 1.;
    }
  }

}

void RADManager::set_Z_pms(std::vector<double> &Z_pms_p) {
  Z_pms = Z_pms_p;
}

void RADManager::set_grid(std::vector<double> &m_bs_p, std::vector<double> &r_bs_p,
			  std::vector<double> &vs_p, std::vector<double> &rhos_p, std::vector<double> &ps_p, std::vector<double> &bs_p, std::vector<double> &Z_pms_p,
			  std::vector<double> &xis_p) {

  // // Decouple the calJ size (used to compute the Compton temperature) from the output spectrum size
  // int n_eps_bs_for_calJ = 301;

  // If grid has different size than previously given, resize the vectors first
  int n_bs_p = m_bs_p.size();
  if(n_bs != n_bs_p) {
    n_bs = n_bs_p;
    m_bs.resize(n_bs_p);
    r_bs.resize(n_bs_p);
    vs.resize(n_bs_p-1);
    rhos.resize(n_bs_p-1);
    ps.resize(n_bs_p-1);
    bs.resize(n_bs_p-1);

    Z_pms.resize(n_bs_p-1);

    G0_scs.resize(n_bs_p-1);
    G1_scs.resize(n_bs_p-1);
    G0_ggs.resize(n_bs_p-1);
    G1_ggs.resize(n_bs_p-1);
    dotZ_pms.resize(n_bs_p-1);

    skip_pair_calcs.resize(n_bs_p-1);
    skip_pair_calcs_prev.resize(n_bs_p-1);
    std::fill(skip_pair_calcs.begin(), skip_pair_calcs.end(), 0);
    std::fill(skip_pair_calcs_prev.begin(), skip_pair_calcs_prev.end(), 0);

    p_gammas.resize(n_bs_p-1);
    I_0s.resize(n_bs_p-1);
    I_1s.resize(n_bs_p-1);
    zetas.resize(n_bs_p-1);
    theta_Cs.resize(n_bs_p-1);
    As.resize(n_bs_p-1);
    Bs.resize(n_bs_p-1);
    // calJ_primes.resize((n_bs_p-1)*(eps_bs.size()-1));
    calJ_primes.resize((n_bs_p-1)*(eps_bs_Compton.size()-1));
    dudlnepss.resize((n_bs_p-1)*(eps_bs.size()-1));
    dudlneps_ints.resize(eps_bs.size()-1);
    electron_energy_dists.resize(n_bs_p-1, n_el_spec_bins);

    // New!
    I_0s_prop.resize(n_bs_p-1);
    I_1s_prop.resize(n_bs_p-1);
    // calJ_primes_prop.resize((n_bs_p-1)*(eps_bs.size()-1));
    calJ_primes_prop.resize((n_bs_p-1)*(eps_bs_Compton.size()-1));
    p_gammas_prop.resize(n_bs_p-1);
    u_gammas_lab_prop.resize(n_bs_p-1);
    zetas_prop.resize(n_bs_p-1);
    calI_primes_prop.resize(( r_bs.size()-1 )*( eps_p_bs.size()-1 )*( mu_p_bs.size()-1 ));
  }

  // Fill grids
  m_bs[0] = m_bs_p[0];
  r_bs[0] = r_bs_p[0];
  for(int j=0; j<n_bs-1; j++) {
    m_bs[j+1] = m_bs_p[j+1];
    r_bs[j+1] = r_bs_p[j+1];
    vs[j] = vs_p[j];
    rhos[j] = rhos_p[j];
    ps[j] = ps_p[j];
    bs[j] = bs_p[j];
    Z_pms[j] = Z_pms_p[j];
  }

  // Make corresponding electron energy distributions
  double theta;
  for(int j=2; j<n_bs-3; j++) {
    theta = get_theta_e_two_temp(rhos[j], ps[j], Z_pms[j], xis_p[j], f_heat);
    if(theta != theta) {
      std::cout << "WARNING: thetas[" << j << "] = " << theta << std::endl;
      std::cout << "         rhos[" << j << "] = " << rhos[j] << std::endl;
      std::cout << "         ps[" << j << "] = " << ps[j] << std::endl;
      std::cout << "         Z_pms[" << j << "] = " << Z_pms[j] << std::endl;
      std::cout << "         xis_p[" << j << "] = " << xis_p[j] << std::endl;
    }
    electron_energy_dists[j].load_Maxwellian(theta);
    electron_energy_dists[j].set_label(theta);
  }
}

double RADManager::get_crossing_time_step_wrapper() {
  // Computes the radiation time step
  double dt = get_radiation_time_step_cr(r_bs, vs);
  return dt;
}

double RADManager::get_time_step_wrapper() {
  // Computes the radiation time step
  double dt = get_radiation_time_step_sc(r_bs, vs, rhos, ps, Z_pms, gamma_ad, alpha);
  return dt;
}

double RADManager::get_time_step_without_pairs_wrapper() {
  // Computes the radiation time step
  double dt = get_radiation_time_step_sc_without_pairs(r_bs, vs, rhos, ps, gamma_ad, alpha);
  return dt;
}

double RADManager::get_time_step_momentum(std::vector<double> &I_1s, std::vector<double> &Z_pms) {
  // Computes the time for the first intensity moment to change the speed of a bin
  double xi = 3e-2;
  double Mic, dt_Mic, dt = 1e60;
  for(unsigned int j=2; j<I_1s.size()-2; j++) {
    Mic = sigma_T*I_1s[j];
    if(Mic != 0.) {
      dt_Mic = m_p/Z_pms[j]/Mic;
      // std::cout << "Z_pms[" << j << "] = " << Z_pms[j] << ", Mic = " << Mic << ", dt_Mic = " << dt_Mic << std::endl;
      // std::cout << "I_1s[" << j << "] = " << I_1s[j] << std::endl;
    } else {
      dt_Mic = 1e60;
    }

    if(xi*xi*dt_Mic*dt_Mic < dt*dt) {
      dt = sqrt(xi*xi*dt_Mic*dt_Mic);
    }
  }
  return dt;
}

int RADManager::check_if_photon_should_split(MC_Photon photon) {
  // Have vector of splitting energies and the initial weight

  double eps = photon.get_eps();
  double w = photon.get_w();
  for(unsigned int k=0; k<eps_splits.size(); k++) {
    if(eps > eps_splits[k]) {
      if(w > w_i/pow(photons_per_split, k+1)) {
	return 1;
      }
    } else {
      return 0;
    }
  }
  return 0;
}

void RADManager::split_all_photons() {
  for(unsigned int i=0; i<photons.size(); i++) {
    if(photons[i].get_alive()) {
      photons[i].set_w(photons[i].get_w()/2);
      photons.push_back(photons[i]);
    }
  }
}

int RADManager::propagate_and_interact_until_time_wrapper(double dt) {

  // Iterating over all photons
  if(dt < 0) {std::cout << "ERROR in 'propagate_and_interact_until_time_wrapper()': dt < 0" << std::endl;}
  int OK = 1, OK_all = 1;

  // For checking if pair calculation can be skipped inside spatial bins
  if(use_pairs == 1) {
    skip_pair_calcs_prev = skip_pair_calcs;
    std::fill(skip_pair_calcs.begin(), skip_pair_calcs.end(), 0);
  }

  for(unsigned int i=0; i<photons.size(); i++) {

    // Propagate
    if(photons[i].get_alive()) {
      OK = propagate_and_interact_until_time(m_bs, r_bs, vs, rhos, ps, bs, Z_pms, G0_scs, G1_scs, G0_ggs, G1_ggs, dotZ_pms, eps_p_bs, mu_p_bs, alpha_ggs,
					     electron_energy_dists, photons[i], boundary_L, boundary_R, dt, alpha, use_pairs, skip_pair_calcs, skip_pair_calcs_prev, &rng, RANK, iter, i,
					     I_0s_prop, I_1s_prop, calJ_primes_prop, p_gammas_prop, u_gammas_lab_prop, zetas_prop, calI_primes_prop, eps_bs, dlneps, eps_bs_Compton, dlneps_Compton);
      if(OK == 0) {}
    }

    if(photons[i].get_alive()) {
      // Check if photon should split into two
      if(check_if_photon_should_split(photons[i]) == 1) {

	// Divide weight by two and copy the photon
	photons[i].set_w(photons[i].get_w()/photons_per_split);
	for(int k=0; k<photons_per_split-1; k++) {
	  photons.push_back(photons[i]);
	}
      }

    }
  }

  return OK_all;
}
