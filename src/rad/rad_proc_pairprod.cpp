//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include <iostream>
#include <vector>
#include "math.h"
#include "../mhd/physical_constants.h"
#include "num_distributions.h" // For "bisect"
#include "rad_propagation.h" // For "bisect_without_ghosts"

// --------------------------------------------- //
//  Full treatment of gamma-gamma cross-section  //
// --------------------------------------------- //

// The comoving frame cross-section

double get_sigma_gg_full(double x) {

  // Cross-section can be found in Indrek's thesis
  // x = eps = E/m_e c^2 in the center of momentum frame

  if(x <= 1.) {
    return 0;
  }

  double beta, p, A, B, C, D, s;

  beta = sqrt(1.-1./x/x);
  p = sqrt(x*x-1.);

  A = 3./8.*p/x/x/x;
  B = 1./beta*(1.+1./x/x-1./2./x/x/x/x);
  C = log((1.+beta)/(1.-beta));
  D = 1./x/x + 1.;

  s = A*(B*C - D);
  return sigma_T*s;
}

// Functions for pre-computation

void fill_mu_p_bs(std::vector<double> &mu_p_bs) {

  // Building comoving mu grid

  int nr_mu_bins = mu_p_bs.size()-1;
  double dtheta_p = pi/(double)(nr_mu_bins);
  for(int i=0; i<nr_mu_bins+1; i++) {
    mu_p_bs[i] = cos(pi - dtheta_p*(double)i);
  }
}

void fill_eps_p_bs(std::vector<double> &eps_p_bs, double eps_min, double eps_max) {

  // Building the comoving eps grid

  int nr_spec_bins = eps_p_bs.size()-1;
  double dlneps = (log(eps_max)-log(eps_min))/(double)(nr_spec_bins);
  for(int i=0; i<nr_spec_bins+1; i++) {
    eps_p_bs[i] = exp(log(eps_min) + dlneps*(double)i);
  }
}

void pre_compute_psi(std::vector<double> &mu_p_bs, std::vector<double> &eps_p_bs, std::vector<double> &psis) {

  // The psi grid is simply a part of the pair production integral that can be pre-computed

  int l, nr_phi_bins=100;
  double phi, mu_tilde, eps, eps_0, eps_cm, sigma_gg, psi;
  double sint, cost, sint_0, cost_0;
  double dphi = pi/(double)nr_phi_bins;
  int nr_mu_bins = mu_p_bs.size()-1;
  int nr_eps_bins = eps_p_bs.size()-1;

  // Finding the size of the psi grid

  int nr_psi_bins = nr_eps_bins*nr_mu_bins*nr_eps_bins*nr_mu_bins;
  int psis_size = psis.size();
  if(psis_size != nr_psi_bins) {psis.resize(nr_psi_bins);}

  // Computing the psi grid

  for(int i_mu_0=0; i_mu_0<nr_mu_bins; i_mu_0++) {
    cost_0 = .5*(mu_p_bs[i_mu_0]+mu_p_bs[i_mu_0+1]);
    sint_0 = sin(acos(.5*(mu_p_bs[i_mu_0]+mu_p_bs[i_mu_0+1])));
    for(int i_eps_0=0; i_eps_0<nr_eps_bins; i_eps_0++) {
      eps_0 = eps_p_bs[i_eps_0];
      for(int i_mu=0; i_mu<nr_mu_bins; i_mu++) {
	cost = .5*(mu_p_bs[i_mu]+mu_p_bs[i_mu+1]);
	sint = sin(acos(.5*(mu_p_bs[i_mu]+mu_p_bs[i_mu+1])));
	for(int i_eps=0; i_eps<nr_eps_bins; i_eps++) {
	  eps = eps_p_bs[i_eps];
	  psi = 0.;
	  for(int i_phi=0; i_phi<nr_phi_bins; i_phi++) {
	    phi = (i_phi+.5)*dphi;
	    mu_tilde = sint_0*sint*cos(phi) + cost_0*cost;
	    eps_cm = sqrt((1.-mu_tilde)*eps_0*eps);
	    sigma_gg = get_sigma_gg_full(eps_cm);
	    psi += 2.*(1.-mu_tilde)*sigma_gg*dphi;
	  }

	  l = i_eps + nr_eps_bins*(i_mu + nr_mu_bins*(i_eps_0 + nr_eps_bins*(i_mu_0)));
	  psis[l] = psi;

	}
      }
    }
  }

}

// Computing the alpha_gg matrix (for each time step)

void compute_alpha_ggs(std::vector<double> &r_bs, std::vector<double> &eps_p_bs, std::vector<double> &mu_p_bs, std::vector<double> &psis, std::vector<double> &calI_primes,
		       std::vector<double> &alpha_ggs, std::vector<double> &skip_pair_calcs, int RANK, int NUM_PROC) {

  // Assumes that:
  // 1) The eps_p_bs, mu_p_bs vectors have been precomputed
  // 2) The psis vector has been precomputed
  // 3) The calI_primes vector has been computed and shared between workers (using the eps_p_bs, mu_p_bs, r_bs grids)
  // 5) That skip_pair_calcs has been computed and shared between workers
  // 4) That the alpha_ggs vector will be collected in the appropriate way afterwards

  // Splitting the work-load over the workers
  int i_r_0_min, i_r_0_max;
  int nr_r_bins = r_bs.size()-5; // Cutting ghosts
  int bins_per_thread = nr_r_bins / NUM_PROC;
  int remaining_bins = nr_r_bins % NUM_PROC;
  i_r_0_min = 2 + RANK*bins_per_thread;
  i_r_0_max = 2 + (RANK+1)*bins_per_thread-1;
  if(RANK == NUM_PROC-1) {
    i_r_0_max += remaining_bins;
  }

  // Computing the gamma-gamma mean free path for photons
  int l_0, l;
  int nr_mu_bins = mu_p_bs.size()-1;
  int nr_eps_bins = eps_p_bs.size()-1;
  if(alpha_ggs.size() != calI_primes.size()) {
    alpha_ggs.resize(calI_primes.size());
  }
  std::fill(alpha_ggs.begin(), alpha_ggs.end(), 0);

  double dmu_p, deps_p;
  double alpha_gg, I, psi;
  for(int i_r_0=i_r_0_min; i_r_0<i_r_0_max; i_r_0++) {
    if(skip_pair_calcs[i_r_0] > 0) {
      for(int i_mu_0=0; i_mu_0<nr_mu_bins; i_mu_0++) {
	for(int i_eps_0=0; i_eps_0<nr_eps_bins; i_eps_0++) {
	  alpha_gg = 1e-60;

	  for(int i_mu=0; i_mu<nr_mu_bins; i_mu++) {
	    for(int i_eps=0; i_eps<nr_eps_bins; i_eps++) {
	      l = i_eps + nr_eps_bins*(i_mu + nr_mu_bins*i_r_0);
	      I = calI_primes[l];

	      l = i_eps + nr_eps_bins*(i_mu + nr_mu_bins*(i_eps_0 + nr_eps_bins*i_mu_0));
	      psi = psis[l];

	      deps_p = eps_p_bs[i_eps+1]-eps_p_bs[i_eps];
	      dmu_p = mu_p_bs[i_mu+1]-mu_p_bs[i_mu];
	      alpha_gg += I*psi*dmu_p*deps_p;
	    }
	  }

	  l_0 = i_eps_0 + nr_eps_bins*(i_mu_0 + nr_mu_bins*i_r_0);
	  alpha_ggs[l_0] = alpha_gg;
	  // std::cout << "alpha_ggs = " << alpha_gg << std::endl;

	}
      }
    }
  }
}

// ---------------- //
//  Mean free path  //
// ---------------- //

double get_pairprod_mean_free_path(double r, double eps, double mu, double v, std::vector<double> &r_bs, std::vector<double> &eps_p_bs, std::vector<double> &mu_p_bs,
				   std::vector<double> &alpha_ggs) {

  double gamma = 1./sqrt(1.-v*v);
  double eps_p = eps*gamma*(1.-mu*v);
  double mu_p = (mu-v)/(1.-mu*v);

  int i_r = bisect_without_ghosts(r_bs, r);
  int i_eps = bisect(eps_p_bs, eps_p);
  int i_mu = bisect(mu_p_bs, mu_p);

  int nr_eps_bins = eps_p_bs.size()-1;
  int nr_mu_bins = mu_p_bs.size()-1;
  int l = i_eps + nr_eps_bins*(i_mu + nr_mu_bins*i_r);
  double lambda_gg = 1./alpha_ggs[l]/gamma/(1.-v*mu);
  return lambda_gg;

}
