//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "rad_manager.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "math.h"
#include "smoothing.h"
#include "../mhd/dbg.h"

// RADManager::RADManager(std::string sim_name_p, int n_phs_p, int n_el_spec_bins_p, int alpha_p, int RANK_p, int NUM_PROC_p, int SEED_p) : sim_name(sim_name_p), n_phs(n_phs_p/NUM_PROC_p),
// 																	 alpha(alpha_p), RANK(RANK_p), NUM_PROC(NUM_PROC_p),
// 																	 photons(n_phs_p/NUM_PROC_p), n_el_spec_bins(n_el_spec_bins_p),
// 																	 rng(RANK_p+SEED_p) {

// RADManager::RADManager(std::string sim_name_p, int n_phs_p, int n_el_spec_bins_p, int alpha_p, int SEED_p) : sim_name(sim_name_p), n_phs(n_phs_p/NUM_PROC_p),
// 													     alpha(alpha_p),
// 													     photons(n_phs_p/NUM_PROC_p), n_el_spec_bins(n_el_spec_bins_p) {

RADManager::RADManager(std::string sim_name_p, int SEED) : sim_name(sim_name_p), rng(SEED) {

  // Initialize things here
  set_rank_and_num_proc();
  initiate_pairprod_vectors();

  rad_stopwatch.set_watch_number(4);
  mpi_stopwatch.set_watch_number(5);
  prop_stopwatch.set_watch_number(6);
  pair_stopwatch.set_watch_number(7);
  smooth_stopwatch.set_watch_number(8);
  mom_stopwatch.set_watch_number(9);

}

// void RADManager::initiate_rng(int SEED) {
//   rng = Ran(RANK+SEED);
// }

void RADManager::set_alpha(int alpha_p) {
  alpha = alpha_p;
  // open_all_files();
}

void RADManager::close() {
  // Close everything
  close_all_files();
}

void RADManager::update_single_step_fake(double dt) {
  // This function simply fakes an update; no photons are propagated, no sources collected,
  // but iter and t are updated

  // Iteration and time
  iter += 1;
  t += dt;
}

void RADManager::update_single_step(double dt) {
  // The MHD grid must be set BEFORE calling this function
  // Setting the MHD grid also computes the electron energy distributions

  rad_stopwatch.start();

  // Flushing the sources
  mom_stopwatch.start();
  flush_sources();
  mom_stopwatch.stop();

  // Computing calI_primes, alpha_ggs (for pair production)
  // (stopwatches start and stop within the function below)
  if(use_pairs == 1) {
    compute_pair_mean_free_path_wrapper(); // MAKE FASTER [PAIRS]
  }

  // New vectors, to fill during propagation
  std::fill(I_0s_prop.begin(), I_0s_prop.end(), 0);
  std::fill(I_1s_prop.begin(), I_1s_prop.end(), 0);
  std::fill(p_gammas_prop.begin(), p_gammas_prop.end(), 0);
  std::fill(u_gammas_lab_prop.begin(), u_gammas_lab_prop.end(), 0);
  std::fill(zetas_prop.begin(), zetas_prop.end(), 0);
  std::fill(calJ_primes_prop.begin(), calJ_primes_prop.end(), 0);
  std::fill(calI_primes_prop.begin(), calI_primes_prop.end(), 0);

  // Radiation step
  prop_stopwatch.start();
  int OK = propagate_and_interact_until_time_wrapper(dt);
  if(OK == 0) {} // Handle any issues
  prop_stopwatch.stop();

  // MPI
  mpi_stopwatch.start();

  // New: collecting sources while radiation is propagating
  compute_Compton_temperature_AB_vectors_prop();
  collect_vector(I_0s_prop);
  collect_vector(I_1s_prop);
  collect_vector(p_gammas_prop);
  collect_vector(u_gammas_lab_prop);
  collect_vector(zetas_prop);
  collect_vector(As);
  collect_vector(Bs);
  distribute_vector(I_0s_prop);
  distribute_vector(I_1s_prop);
  distribute_vector(p_gammas_prop);
  distribute_vector(u_gammas_lab_prop);
  distribute_vector(zetas_prop);
  distribute_vector(As);
  distribute_vector(Bs);

  mpi_stopwatch.stop();
  compute_Compton_temperature();
  average_over_optical_depth(I_0s_prop, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(I_1s_prop, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(p_gammas_prop, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(u_gammas_lab_prop, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(zetas_prop, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  average_over_optical_depth(theta_Cs, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R);
  mpi_stopwatch.start();
// -- End new --

  collect_vector(G0_scs);
  collect_vector(G1_scs);
  collect_vector(G0_ggs);
  collect_vector(G1_ggs);
  if(use_pairs == 1) {
    collect_vector(dotZ_pms);
    collect_vector(skip_pair_calcs);
  }
  distribute_vector(G0_scs);
  distribute_vector(G1_scs);
  distribute_vector(G0_ggs);
  distribute_vector(G1_ggs);
  if(use_pairs == 1) {
    distribute_vector(dotZ_pms);
    distribute_vector(skip_pair_calcs);
  }
  mpi_stopwatch.stop();

  // Smooth dotZ_pms?
  smooth_stopwatch.start();
  average_over_optical_depth(dotZ_pms, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R); // WARNING: TEMPORARY! THIS DOES NOT CONSERVE THE NUMBER OF PAIRS, AND IS WRONG!
  average_over_optical_depth(G0_ggs, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R); // WARNING: TEMPORARY! THIS DOES NOT CONSERVE THE NUMBER OF PAIRS, AND IS WRONG!
  average_over_optical_depth(G1_ggs, m_bs, r_bs, Z_pms, tau_smoothing, alpha, boundary_L, boundary_R); // WARNING: TEMPORARY! THIS DOES NOT CONSERVE THE NUMBER OF PAIRS, AND IS WRONG!
  smooth_stopwatch.stop();

  // Add functions to evolve Z_pm here!
  pair_stopwatch.start();
  if(use_pairs == 1) {
    evolve_Z_pm(dt);
  }
  pair_stopwatch.stop();

  // Emission here?

  // Iteration and time
  iter += 1;
  t += dt;

  rad_stopwatch.stop();

}

void RADManager::get_G_sc_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p) {
  G0s_p = G0_scs;
  G1s_p = G1_scs;
}

void RADManager::get_G_gg_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p) {
  G0s_p = G0_ggs;
  G1s_p = G1_ggs;
}

void RADManager::get_all_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p) {
  G0s_p.resize(G0_scs.size());
  G1s_p.resize(G1_scs.size());
  for(unsigned int i=0; i<G0_scs.size(); i++) {
    G0s_p[i] = G0_scs[i] + G0_ggs[i];
    G1s_p[i] = G1_scs[i] + G1_ggs[i];
  }
}

void RADManager::get_all_and_pair_sources(std::vector<double> &G0s_p, std::vector<double> &G1s_p, std::vector<double> &dotZ_pms_p) {
  G0s_p.resize(G0_scs.size());
  G1s_p.resize(G1_scs.size());
  dotZ_pms_p.resize(dotZ_pms.size());
  for(unsigned int i=0; i<G0_scs.size(); i++) {
    G0s_p[i] = G0_scs[i] + G0_ggs[i];
    G1s_p[i] = G1_scs[i] + G1_ggs[i];
    dotZ_pms_p[i] = dotZ_pms[i];
  }
}

void RADManager::print_vector(std::string name, std::vector<double> &Vs, int w) {

  // A copy of the same function in the "main_helper_functions.cpp" file

  std::string whitespace = "";
  for(unsigned int i=0; i<name.length()+2; i++) {
    whitespace += " ";
  }
  std::cout << name << " [";
  for(unsigned int i=0; i<Vs.size()-1; i++) {
    std::cout << Vs[i] << ", ";
    if(i != 0 && i%w == 0) {std::cout << std::endl << whitespace;}
  }
  std::cout << Vs[Vs.size()-1] << "]" << std::endl;
}

void RADManager::print_grid(int RANK_p) {
  int w = 17;
  if(RANK_p == -1 || RANK == RANK_p) {
    print_vector("m_bs =", m_bs, w);
    print_vector("r_bs =", r_bs, w);
    print_vector("vs =", vs, w);
    print_vector("rhos =", rhos, w);
    print_vector("ps =", ps, w);
    // print_vector("bs =", bs, w);
  }
}

void RADManager::print_G_sc_sources(int RANK_p) {
  int w = 17;
  if(RANK_p == -1 || RANK == RANK_p) {
    print_vector("G0_scs =", G0_scs, w);
    print_vector("G1_scs =", G1_scs, w);
  }
}

void RADManager::print_all_sources(int RANK_p) {
  int w = 17;
  if(RANK_p == -1 || RANK == RANK_p) {
    print_vector("G0_scs   =", G0_scs, w);
    print_vector("G1_scs   =", G1_scs, w);
    print_vector("dotZ_pms =", dotZ_pms, w);
  }
}
