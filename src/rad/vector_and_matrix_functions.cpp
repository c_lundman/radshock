//
//    Radshock: Simulating radiation mediated shocks from first principles.
//    Copyright (C) 2019, Christoffer Lundman.
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "vector_and_matrix_functions.h"
#include <math.h>
#include <iostream>
#include "../mhd/physical_constants.h"

/* ----------------------- */
/* Building photon vectors */
/* ----------------------- */

void make_unit_vector_3d(double theta, double phi, double v[3]) {

  /* Builds a unit vector from the directional angles,
     MODIFYING the 3-vector v */

  v[0] = sin(theta)*cos(phi);
  v[1] = sin(theta)*sin(phi);
  v[2] = cos(theta);
}

void make_momentum_vector(double eps, double theta, double phi, double k[3]) {

  /* Builds the photon momentum vector from the photon energy and
     directional angles, MODIFYING the 3-vector k */

  k[0] = eps*sin(theta)*cos(phi);
  k[1] = eps*sin(theta)*sin(phi);
  k[2] = eps*cos(theta);
}

void make_four_vector(double k[3], double P[4]) {

  /* Builds the photon 4-vector from the photon momentum vector,
     MODIFYING the 4-vector P */

  double eps;

  eps = sqrt(vector_dot_product_3d(k, k));

  P[0] = eps;
  P[1] = k[0];
  P[2] = k[1];
  P[3] = k[2];
}

void get_momentum_vector(double P[4], double k[3]) {

  /* Finds the photon momentum vector from the photon
     4-vector, MODIFYING the momentum vector k */

  k[0] = P[1];
  k[1] = P[2];
  k[2] = P[3];
}

double get_eps(double P[4]) {

  /* Finds the photon energy from the photon 4-vector */

  double eps;

  eps = P[0];

  return(eps);
}

/* -------------- */
/* Vector algebra */
/* -------------- */

double vector_dot_product_3d(double v1[3], double v2[3]) {

  /* The dot product of two 3-vectors */

  return(v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2]);
}

void vector_cross_product_3d(double v1[3], double v2[3], double v_cross[3]) {

  /* The cross product of two 3-vectors, v_cross = v1 x v2 */

  v_cross[0] = v1[1]*v2[2] - v1[2]*v2[1];
  v_cross[1] = v1[2]*v2[0] - v1[0]*v2[2];
  v_cross[2] = v1[0]*v2[1] - v1[1]*v2[0];
}

void vector_scalar_multiplication_3d(double A, double v[3]) {

  /* Multiplies a 3-vector with a scalar,
     MODIFYING the input vector */

  v[0] *= A;
  v[1] *= A;
  v[2] *= A;
}

void vector_norm_3d(double v[3], double v_norm[3]) {

  /* Normalizes a 3-vector, v_norm = v/abs(v) */

  double A;

  A = sqrt(vector_dot_product_3d(v, v));

  v_norm[0] = v[0]/A;
  v_norm[1] = v[1]/A;
  v_norm[2] = v[2]/A;
}

/* -------------- */
/* Matrix algebra */
/* -------------- */

void matrix_vector_multiplication_3d(double m[9], double v[3]) {

  /* Multiplies a 3x3 matrix with a 3-vector,
     MODIFYING the input vector */

  double v_tmp[3];

  v_tmp[0] = v[0];
  v_tmp[1] = v[1];
  v_tmp[2] = v[2];

  v[0] = m[0]*v_tmp[0] + m[1]*v_tmp[1] + m[2]*v_tmp[2];
  v[1] = m[3]*v_tmp[0] + m[4]*v_tmp[1] + m[5]*v_tmp[2];
  v[2] = m[6]*v_tmp[0] + m[7]*v_tmp[1] + m[8]*v_tmp[2];
}

void matrix_scalar_multiplication_4d(double A, double m[16]) {

  /* Multiplies a 4x4 matrix with a scalar,
     MODIFYING the input matrix */

  int k;

  for(k=0; k<16; k++) {
    m[k] *= A;
  }
}

void matrix_vector_multiplication_4d(double m[16], double v[4]) {

  /* Multiplies a 4x4 matrix with a 4-vector,
     MODIFYING the input vector */

  double v_tmp[4];

  v_tmp[0] = v[0];
  v_tmp[1] = v[1];
  v_tmp[2] = v[2];
  v_tmp[3] = v[3];

  v[0] = m[0]*v_tmp[0] + m[1]*v_tmp[1] + m[2]*v_tmp[2] + m[3]*v_tmp[3];
  v[1] = m[4]*v_tmp[0] + m[5]*v_tmp[1] + m[6]*v_tmp[2] + m[7]*v_tmp[3];
  v[2] = m[8]*v_tmp[0] + m[9]*v_tmp[1] + m[10]*v_tmp[2] + m[11]*v_tmp[3];
  v[3] = m[12]*v_tmp[0] + m[13]*v_tmp[1] + m[14]*v_tmp[2] + m[15]*v_tmp[3];
}

/* -------------------------------- */
/* Rotation and scattering matrices */
/* -------------------------------- */

void Rz(double phi, double k[3]) {

  /* Rotates the coordinate system in which the 3-vector k is
     expressed an angle phi counter-clockwise around the z-axis,
     which is pointing towards the reader */

  double r[9];

  r[0] = cos(phi);
  r[1] = sin(phi);
  r[2] = 0.;

  r[3] = -sin(phi);
  r[4] = cos(phi);
  r[5] = 0.;

  r[6] = 0.;
  r[7] = 0.;
  r[8] = 1.;

  matrix_vector_multiplication_3d(r, k);

}

void Ry(double theta, double k[3]) {

  /* Rotates the coordinate system in which the 3-vector k is
     expressed an angle theta counter-clockwise around the y-axis,
     which is pointing towards the reader */

  double r[9];

  r[0] = cos(theta);
  r[1] = 0.;
  r[2] = -sin(theta);

  r[3] = 0.;
  r[4] = 1.;
  r[5] = 0.;

  r[6] = sin(theta);
  r[7] = 0.;
  r[8] = cos(theta);

  matrix_vector_multiplication_3d(r, k);

}

void M(double phi, double S[4]) {

  /* Rotates the coordinate system in which the Stokes vector (I, P1, P2, P3)
     is expressed an angle phi counter-clockwise around the z-axis,
     which is pointing towards the reader */

  double m[16];

  m[0] = 1.;
  m[1] = 0.;
  m[2] = 0.;
  m[3] = 0.;

  m[4] = 0.;
  m[5] = cos(2.*phi);
  m[6] = -sin(2.*phi);
  m[7] = 0.;

  m[8] = 0.;
  m[9] = sin(2.*phi);
  m[10] = cos(2.*phi);
  m[11] = 0.;

  m[12] = 0.;
  m[13] = 0.;
  m[14] = 0.;
  m[15] = 1.;

  matrix_vector_multiplication_4d(m, S);
}

void Lambda(double beta_vec[3], double P[4]) {

  /* Lorentz transforms the 4-vector P into the frame which moves
     with velocity beta in the current frame */

  double lambda[16];
  double beta_x, beta_y, beta_z;
  double beta, Gamma;

  beta_x = beta_vec[0];
  beta_y = beta_vec[1];
  beta_z = beta_vec[2];

  beta = sqrt(vector_dot_product_3d(beta_vec, beta_vec));

  if(beta == 0.) {
    return;
  }

  Gamma = 1./sqrt(1.-beta*beta);

  lambda[0] = Gamma;
  lambda[1] = -Gamma*beta_x;
  lambda[2] = -Gamma*beta_y;
  lambda[3] = -Gamma*beta_z;

  lambda[4] = -Gamma*beta_x;
  lambda[5] = 1. + (Gamma-1.)*beta_x*beta_x/beta/beta;
  lambda[6] = (Gamma-1.)*beta_x*beta_y/beta/beta;
  lambda[7] = (Gamma-1.)*beta_x*beta_z/beta/beta;

  lambda[8] = -Gamma*beta_y;
  lambda[9] = (Gamma-1.)*beta_y*beta_x/beta/beta;
  lambda[10] = 1. + (Gamma-1.)*beta_y*beta_y/beta/beta;
  lambda[11] = (Gamma-1.)*beta_y*beta_z/beta/beta;

  lambda[12] = -Gamma*beta_z;
  lambda[13] = (Gamma-1.)*beta_z*beta_x/beta/beta;
  lambda[14] = (Gamma-1.)*beta_z*beta_y/beta/beta;
  lambda[15] = 1. + (Gamma-1.)*beta_z*beta_z/beta/beta;

  matrix_vector_multiplication_4d(lambda, P);
}

/* ------------------------ */
/* Computing various angles */
/* ------------------------ */

double get_rotation_angle(double A[3], double B[3], double k[3]) {

  /* Finds the angle needed to rotate from C^{Ak} to C^{Bk}
     around k (as described in Update 3). Returns a value
     of phi in the range -pi <= phi <= pi. */

  double phi;
  double sign;
  double x_A_hat[3], y_A_hat[3], z_A_hat[3];
  double y_B_hat[3];
  double y_A_tmp[3], y_B_tmp[3];
  double y_A_hat_dot_y_B_hat;
  double shift=1e-32;
  double accuracy=1e-12;

  double A_copy[3], B_copy[3];

  /* Copying the vector so the original input vectors are not modified */

  A_copy[0] = A[0];
  A_copy[1] = A[1];
  A_copy[2] = A[2];

  B_copy[0] = B[0];
  B_copy[1] = B[1];
  B_copy[2] = B[2];

  /* Since the vector defined by A x k has zero length if A is parallel
     or anti-parallel to k, I shift A and B a little. The shift is less
     than the accuracy of the constants defined in the code. */

  A_copy[0] -= shift;
  B_copy[0] -= shift;

  /* Obtaining unit vectors for C^{Ak} */

  vector_norm_3d(k, z_A_hat);                         // z_A_hat = k/abs(k)
  vector_cross_product_3d(A_copy, k, y_A_tmp);        // y_A = A x k
  vector_norm_3d(y_A_tmp, y_A_hat);                   // y_A_hat = y_A_tmp/abs(y_A_tmp)
  vector_cross_product_3d(y_A_hat, z_A_hat, x_A_hat); // x_A_hat = y_A_hat x z_A_hat

  /* Printing warning message */

  if(abs(vector_dot_product_3d(y_A_hat, y_A_hat)-1.)>accuracy) {
    std::cout << std::endl << "--- ERROR in get_rotation_angle(A, B, k):" << std::endl;
    std::cout << "    A is parallel to +-k. Therefore, A x k returns a vector of zero length." << std::endl;
    std::cout << "    y_A_hat = [" << y_A_hat[0] << ", " << y_A_hat[1] << ", " << y_A_hat[2] << "]" << std::endl;
    std::cout << "    y_A_hat^2 - 1 = " << vector_dot_product_3d(y_A_hat, y_A_hat)-1. << std::endl << std::endl;
    // printf("\n--- ERROR in get_rotation_angle(A, B, k):\n");
    // printf("    A is parallel to +-k. Therefore, A x k returns a vector of zero length.\n");
    // printf("    y_A_hat = [%4.2e, %4.2e, %4.2e]\n", y_A_hat[0], y_A_hat[1], y_A_hat[2]);
    // printf("    y_A_hat^2 - 1 = %4.2e\n\n", vector_dot_product_3d(y_A_hat, y_A_hat)-1.);
  }

  /* Obtaining y-unit vector for C^{Bk} */

  vector_cross_product_3d(B_copy, k, y_B_tmp);        // y_B_tmp = B x k
  vector_norm_3d(y_B_tmp, y_B_hat);                   // y_B_hat = y_B_tmp/abs(y_B_tmp)

  /* Printing warning message */

  if(abs(vector_dot_product_3d(y_B_hat, y_B_hat)-1.)>accuracy) {
    std::cout << std::endl << "--- ERROR in get_rotation_angle(A, B, k):" << std::endl;
    std::cout << "    B is parallel to +-k. Therefore, B x k returns a vector of zero length." << std::endl;
    std::cout << "    y_B_hat = [" << y_B_hat[0] << ", " << y_B_hat[1] << ", " << y_B_hat[2] << "]" << std::endl;
    std::cout << "    y_B_hat^2 - 1 = " << vector_dot_product_3d(y_B_hat, y_B_hat)-1. << std::endl << std::endl;
    // printf("\n--- ERROR in get_rotation_angle(A, B, k):\n");
    // printf("    B is parallel to +-k. Therefore, B x k returns a vector of zero length.\n");
    // printf("    y_B_hat = [%4.2e, %4.2e, %4.2e]\n", y_B_hat[0], y_B_hat[1], y_B_hat[2]);
    // printf("    y_B_hat^2 - 1 = %4.2e\n\n", vector_dot_product_3d(y_B_hat, y_B_hat)-1.);
  }

  if(vector_dot_product_3d(x_A_hat, y_B_hat) > 0) {
    sign = 1.;
  } else {
    sign = -1.;
  }

  y_A_hat_dot_y_B_hat = vector_dot_product_3d(y_A_hat, y_B_hat);

  /* Due to numerical accuracy, sometimes the dot product of two unit vectors
     is slightly outside the range [-1, 1] */

  if(y_A_hat_dot_y_B_hat > 1.) {
    y_A_hat_dot_y_B_hat -= accuracy;
  } else if(y_A_hat_dot_y_B_hat < -1.) {
    y_A_hat_dot_y_B_hat += accuracy;
  }

  phi = -sign*acos(y_A_hat_dot_y_B_hat);

  if(phi != phi) {
    std::cout << "A_copy = [" << A_copy[0] << ", " << A_copy[1] << ", " << A_copy[2] << "]" << std::endl;
    std::cout << "B_copy = [" << B_copy[0] << ", " << B_copy[1] << ", " << B_copy[2] << "]" << std::endl;
    std::cout << "k      = [" << k[0] << ", " << k[1] << ", " << k[2] << "]" << std::endl;
    std::cout << "A_hat dot B_hat = " << vector_dot_product_3d(y_A_hat, y_B_hat) << std::endl;
    std::cout << "acos(A_hat dot B_hat) = " << acos(vector_dot_product_3d(y_A_hat, y_B_hat)) << std::endl;
    std::cout << "(A_hat dot B_hat) - 1.0 = " << vector_dot_product_3d(y_A_hat, y_B_hat)-1. << std::endl;
    // printf("A_copy = [%4.2e, %4.2e, %4.2e]\n", A_copy[0], A_copy[1], A_copy[2]);
    // printf("B_copy = [%4.2e, %4.2e, %4.2e]\n", B_copy[0], B_copy[1], B_copy[2]);
    // printf("k      = [%4.2e, %4.2e, %4.2e]\n", k[0], k[1], k[2]);
    // printf("A_hat dot B_hat = %4.2e\n", vector_dot_product_3d(y_A_hat, y_B_hat));
    // printf("acos(A_hat dot B_hat) = %4.2e\n", acos(vector_dot_product_3d(y_A_hat, y_B_hat)));
    // printf("(A_hat dot B_hat) - 1.0 = %4.2e\n", vector_dot_product_3d(y_A_hat, y_B_hat)-1.);
  }

  return(phi);
}

double get_polar_angle(double k[3]) {

  /* Returns the polar angle of k,
     0. <= theta <= pi */

  double theta;
  double kz;

  kz = k[2];

  theta = acos(kz/sqrt(vector_dot_product_3d(k, k)));
  return(theta);
}

double get_azimuthal_angle(double k[3]) {

  /* Returns the azimuthal angle of k,
     0. <= phi <= 2.pi */

  /* Returns phi = 0 if kx = ky = 0 */

  /* The atan function gives the correct angle when kx > 0, else an
     angle of pi needs to be added to get to the angle in the correct quadrant. */

  double phi;
  double kx, ky;

  kx = k[0];
  ky = k[1];

  if(kx == 0.) {
    if(ky == 0.) {
      phi = 0.;
    } else {
      if(ky > 0.) {
	phi = pi/2.;
      } else {
	phi = 3.*pi/2.;
      }
    }
  } else {
    phi = atan(ky/kx);
    if(kx < 0.) {
      phi += pi;
    }
  }

  if(phi < 0.) {
    phi += 2.*pi;
  }

  return(phi);
}
