![](doc/images/radshock_banner.png)

# **1. Instructions**

- TBD

# **2. Resources**

- [Link to the BitBucket repository](https://bitbucket.org/c_lundman/radshock/src/master/)
- [A great Git resource](https://git-scm.com/book/en/v2)
- [Git Cheat Sheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf)
- [Mastering Git Markdown](https://guides.github.com/features/mastering-markdown/)
- [How to use the astronomy department HPC](https://ttt.astro.su.se/computers/UN-60.html)
- [How to book the astronomy department HPC](https://confluence.it.su.se/confluence/display/DA/Booking+system)

# **3. Random**

- When using intensity moment sources, it seems that ~1000 MC photons per bin is the lowest number for not crashing when Z_gamma = 1e5, f_heat = 1
- When using intensity moment sources, moothing over lengths wider than the original bin sizes, and p_gamma ~ rho or larger, a "zig-zag" instability can develop in rho
- Ideas for making pair production algorithm faster:
  1) when computing alpha_gg, slice the work load in eps (or mu?) so that the computed results are adjacent in memory
  2) when collecting alpha_gg, collect only the the relevant slices
- Idea for pair production: compute the 'dotZ_pms' using moments, instead of using the direct sources? Avantages/disadvantages?
- If I could kill both the V_m < 0 and E_m < 0 bugs, then I'd be one happy postdoc
- Tests indicate that time scales as t propto n_phs, t propto sqrt(n_bs) (???), and somewhat linear scaling with number of nodes
- My new HLLC solver works only without magnetic fields for now. Can I fix this?
- Idea: on clusters, NEVER make code changes, ONLY run parameter and state files that are prepared elsewhere (like locally). The output snapshots can then be pushed to BitBucket, pulled to my local machine and plotted.
- radshock does ~ 200 iterations (bin crossings) per minute, per core, per 10^6 MC photons

# ** Licensing **

Radshock is available under The GNU General Public License v3.0.