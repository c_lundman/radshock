The parameter file has a parameter called 'debug'. If non-zero, the simulation runs in
debugging mode. Below are the available debugging modes:

debug = 0 	no debugging
debug = 1	print extra information, save a snapshot each mhd iteration
debug = 2	print extra information, save a snapshot each radiation iteration
debug = 3	print extra information, save a snapshot each mhd and radiation iteration
