# Code for plotting with text inline (or above/below the line)

# Initial python setup
from numpy import pi, sin, cos, arctan2, rad2deg, log10
import matplotlib.pyplot as plt
from matplotlib import pylab

def get_theta(ax, dxs, dys, aspect_ratio):

    # Finds angle for text rotation when loglog=0
    #xscreen = ax.transData.transform(zip(dxs, dys))
    #theta = np.rad2deg(np.arctan2(*np.abs(np.gradient(xscreen)[0][0][::-1])))
    #dx = dxs[1]-dxs[0]
    #dy = dys[1]-dys[0]
    #if dx*dy < 0.:
    #    theta *= -1.

    dx = dxs[1]-dxs[0]
    dy = dys[1]-dys[0]
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()
    Dy = ymax-ymin
    Dx = xmax-xmin
    # dy *= 3./4.*Dx/Dy
    dy *= Dx/Dy/aspect_ratio
    theta = 180./pi*arctan2(dy, dx)
    if theta < -90.:
        theta += 180.
    elif theta > 90.:
        theta -= 180.
    return theta

def get_theta_loglog(ax, dxs, dys, aspect_ratio):
    # Finds angle for text rotation when loglog=1
    alpha = (log10(dys[1]) - log10(dys[0]))/(log10(dxs[1]) - log10(dxs[0]))
    dx = .1
    dy = alpha*dx
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()
    f = log10(ymax/ymin)/log10(xmax/xmin)
    dy /= f
    dy /= aspect_ratio
    theta = rad2deg(arctan2(dy, dx))
    return theta

def shift_origin(p, theta, ax, text_size, aspect_ratio, above, loglog):
    # Shifts text plotting location, if text is supposed to be plotted above/below the line
    if not loglog:
        d = .05
        modx = d*text_size/22.
        mody = d*text_size/22.*aspect_ratio
        xshift = -sin(pi/180.*theta)*modx*above
        yshift = +cos(pi/180.*theta)*mody*above
        p = p[0]+xshift, p[1]+yshift
    else:
        ymin, ymax = ax.get_ylim()
        xmin, xmax = ax.get_xlim()
        dec_y = log10(ymax/ymin)
        dec_x = log10(xmax/xmin)
        d = .07
        modx = d*(text_size/22.)*(dec_x/3.)
        mody = d*(text_size/22.)*(dec_y/3.)*aspect_ratio
        xshift = -sin(pi/180.*theta)*modx*above
        yshift = +cos(pi/180.*theta)*mody*above
        p = p[0]*(10**(xshift)), p[1]*(10**(yshift))
    return p

def find_bbox(above, bbox_color, bbox_alpha, bbox_round):
    # Finds the bbox for the text; the bbox will cover any text background
    if above == 0 or bbox_color != None or bbox_round != None:
        if bbox_color == None:
            bbox_color = '1'
        bbox = dict(facecolor=bbox_color, edgecolor=bbox_color, alpha=bbox_alpha)
        if bbox_round:
            bbox['boxstyle'] = 'round4'
    else:
        bbox = None
    return bbox

def find_i_l_and_i_r(xs, x):
    i_l = 0
    i_r = len(xs)-1
    if x < xs[0]:
        return 0, 1
    elif x > xs[-1]:
        return len(xs)-2, len(xs)-1
    else:
        while(i_r-i_l != 1):
            i_m = (i_l+i_r)/2
            if xs[i_m] > x:
                i_r = i_m
            else:
                i_l = i_m
        return i_l, i_r

def interpolate(x_loc, x_l, x_r, y_l, y_r):
    Dx = x_r-x_l
    Dy = y_r-y_l
    f = (x_loc-x_l)/Dx
    y_loc = y_l + f*Dy
    return y_loc

def plot_with_inline_label(xs, ys, label, i_loc=None, x_loc=None, y_loc=None, c='k', ls='-', lw=None, marker='', fontsize=None, above=0, alpha=1.,
                           bbox_color=None, bbox_alpha=1., bbox_round=None, no_rotation=False, ax=None):
    """ Line defined by xs, ys will be plotted, with a text label centered on xs[i], ys[i], rotated along the line
        Needs x and y-axis limits, as well as loglog(), to be set BEFORE calling this function, to ensure proper text rotations
        If above=1, text lies above the line instead of cutting the line (above=-1 => below the line, above=3 => 3 times higher above the line)
    """

    """ New modification: can give x_loc (or y_loc) instead of i_loc; if so, code finds closest i_loc
    """

    # dx, dy = pylab.rcParams['figure.figsize']
    # aspect_ratio = float(dx)/dy

    # bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    bbox = ax.get_window_extent()
    dx, dy = bbox.width, bbox.height
    aspect_ratio = float(dx)/dy

    if not fontsize:
        fontsize = pylab.rcParams['font.size']
    if ax == None:
        ax = plt.gca()
    if ax.get_yscale() == 'log':
        # Assuming that plot is not logged on one axis only; not supported yet
        loglog = 1
    else:
        loglog = 0

    # Select center value for i, if nothing else is provided
    if i_loc == None and x_loc == None and y_loc == None:
        i_loc = len(xs)/2

    # Find center point, slope
    if i_loc is not None:
        x_loc = xs[i_loc]
        y_loc = ys[i_loc]
        i_l = i_loc-1
        i_r = i_loc+1
    if i_loc == None and x_loc != None:
        # Find corresponding i_l, i_r, y_interpolated in linear or log
        if xs[0] > xs[1]:
            i_l, i_r = find_i_l_and_i_r(xs[::-1], x_loc)
            i_l = len(xs)-1-i_l
            i_r = len(xs)-1-i_r
        else:
            i_l, i_r = find_i_l_and_i_r(xs, x_loc)
        if not loglog:
            y_loc = interpolate(x_loc, xs[i_l], xs[i_r], ys[i_l], ys[i_r])
        else:
            # Fix this!
            y_loc = interpolate(x_loc, xs[i_l], xs[i_r], ys[i_l], ys[i_r])
    elif i_loc == None and y_loc != None:
        if ys[0] > ys[1]:
            i_l, i_r = find_i_l_and_i_r(ys[::-1], y_loc)
            i_l = len(ys)-1-i_l
            i_r = len(ys)-1-i_r
        else:
            i_l, i_r = find_i_l_and_i_r(ys, y_loc)
        if not loglog:
            x_loc = interpolate(y_loc, ys[i_l], ys[i_r], xs[i_l], xs[i_r])
        else:
            # Fix this!
            x_loc = interpolate(y_loc, ys[i_l], ys[i_r], xs[i_l], xs[i_r])

    p = x_loc, y_loc
    dxs = [xs[i_l], xs[i_r]]
    dys = [ys[i_l], ys[i_r]]

    l, = ax.plot(xs, ys, color=c, linestyle=ls, alpha=alpha, lw=lw, marker=marker)
    if loglog:
        theta = get_theta_loglog(ax, dxs, dys, aspect_ratio)
    else:
        theta = get_theta(ax, dxs, dys, aspect_ratio)

    if no_rotation:
        theta = 0.

    p = shift_origin(p, theta, ax, fontsize, aspect_ratio, above, loglog)
    bbox = find_bbox(above, bbox_color, bbox_alpha, bbox_round)
    ax.text(p[0], p[1], label, size=fontsize, rotation=theta, color=l.get_color(), ha="center", va="center", bbox=bbox)

#

def example_usage():
    N = 200
    dx = 1./N
    xs = [2.*(i+1)*dx for i in xrange(N)]
    ys = [.1+(x-1.)**2 - (x-1.)**3. for x in xs]
    label = r'$\mathrm{SUP}$'

    # Linear plot
    plt.ylim(-1, +2)
    plot_with_inline_label(xs, ys, label, i_loc=3*N/4, c='k', ls='-', fontsize=24)
    plt.show()

    # Loglog plot
    plt.ylim(1e-2, 1e1)
    plt.xlim(1e-1, 1e1)
    plt.loglog()
    plot_with_inline_label(xs, ys, label, i_loc=N/3, c='k', ls='-', fontsize=24)
    plt.show()

#

def testing_ax():
    N = 200
    dx = 1./N
    xs = [2.*(i+1)*dx for i in xrange(N)]
    ys = [.1+(x-1.)**2 - (x-1.)**3. for x in xs]
    label = r'$\mathrm{SUP}$'

    scale = 9
    n_panels = 2
    fig, axs = plt.subplots(n_panels, 1, sharex=True, figsize=(scale, scale*n_panels*.5))
    fig.subplots_adjust(hspace=0.1)
    ax1 = axs[0]
    ax2 = axs[1]

    # Linear plot
    ax1.set_ylim(-1, +2)
    plot_with_inline_label(xs, ys, label, i_loc=1*N/4, c='k', ls='-', fontsize=24, ax=ax1, above=1)

    # Loglog plot
    ax2.set_ylim(1e-2, 1e1)
    ax2.set_xlim(1e-1, 1e1)
    ax2.set_yscale('log')
    ax2.set_xscale('log')
    plot_with_inline_label(xs, ys, label, i_loc=N/3, c='k', ls='-', fontsize=24, ax=ax2)
    plt.show()

if __name__ == '__main__':
    testing_ax()
