#!/usr/bin/env python

import os
import sys
import argparse
from numpy import nan
from math import pi, sqrt
from matplotlib import pyplot as plt
# from generate_state_files import get_theta
from generate_state_files import get_theta_e, get_theta_p, bisect, bisect_rev
from shock_finder import find_shocks
from inline_label_plots import plot_with_inline_label as pwil

def sci_notation(num, decimal_digits=1, precision=None, exponent=None, remove_leading_ones=False):

    # Returns a string representation of the scientific
    # notation of the given number formatted for use with
    # LaTeX or Mathtext, with specified number of significant
    # decimal digits and precision (number of decimal digits
    # to show). The exponent to be used can also be specified
    # explicitly.

    from numpy import round, abs, floor, log10

    if num == 0.:
        exponent = 0
    if exponent == None:
        exponent = int(floor(log10(abs(num))))
    coeff = round(num / float(10**exponent), decimal_digits)
    if not precision:
        precision = decimal_digits
    if remove_leading_ones and coeff == 1.:
        return r"$10^{{{1:d}}}$".format(coeff, exponent, precision)
    else:
        return r"${0:.{2}f}\times\,10^{{{1:d}}}$".format(coeff, exponent, precision)

def get_kappa_T():
    m_p = 1.6726231e-24
    sigma_T = 6.6524e-25
    kappa_T = sigma_T/m_p
    return kappa_T

def get_dtauss(m_bss, rss, alpha, Z_pmss=None):

    # This is the column tau per bin at a fixed time

    # Consider pairs?
    if Z_pmss == None:
        Z_pmss = [[1. for r in rs] for rs in rss]
    dmss = [[m_bs[j+1] - m_bs[j] for j in xrange(len(m_bs)-1)] for m_bs in m_bss]
    kappa_T = get_kappa_T()
    if alpha == 0:
        dtauss = [[dm*kappa_T*Z_pm for dm, Z_pm in zip(dms, Z_pms)] for dms, Z_pms in zip(dmss, Z_pmss)]
    elif alpha == 1:
        dtauss = [[dm/2./pi/r*kappa_T*Z_pm for dm, r, Z_pm in zip(dms, rs, Z_pms)] for dms, rs, Z_pms in zip(dmss, rss, Z_pmss)]
    elif alpha == 2:
        dtauss = [[dm/4./pi/r/r*kappa_T*Z_pm for dm, r, Z_pm in zip(dms, rs, Z_pms)] for dms, rs, Z_pms in zip(dmss, rss, Z_pmss)]
    return dtauss

def get_tauss(m_bss, rss, alpha, Z_pmss=None, show_ghosts=False, reverse=False):

    # This is the integrated tau across bins

    dtauss = get_dtauss(m_bss, rss, alpha, Z_pmss=Z_pmss)
    tau_bss = []
    for dtaus in dtauss:
        tau_bs = [0.]
        for dtau in dtaus:
            tau_bs.append(tau_bs[-1] + dtau)
        tau_bss.append(tau_bs)

    if reverse:
        tau_maxs = [tau_bs[-1] for tau_bs in tau_bss]
        tau_bss = [[tau_max-tau_b for tau_b in tau_bs] for tau_bs, tau_max in zip(tau_bss, tau_maxs)]

    tauss = [[.5*(tau_bs[i+1]+tau_bs[i]) for i in xrange(len(tau_bs)-1)] for tau_bs in tau_bss]

    if show_ghosts:
        tau_is = [taus[2] for taus in tauss]
        tauss = [[tau-tau_i for tau in taus] for taus, tau_i in zip(tauss, tau_is)]
    return tauss

def get_t_scss(vss, rhoss, Z_pmss):

    # This is the lab frame time between scatterings

    kappa_T = get_kappa_T()
    gammass = [[1./sqrt(1.-v*v) for v in vs] for vs in vss]
    t_scss = [[gamma/Z_pm/rho/kappa_T for gamma, Z_pm, rho in zip(gammas, Z_pms, rhos)] for gammas, Z_pms, rhos in zip(gammass, Z_pmss, rhoss)]
    return t_scss

def get_t_crss(r_bss, vss):

    # This is the lab frame time of a typical crossing of a bin for isotropic comoving frame photons

    gammass = [[1./sqrt(1.-v*v) for v in vs] for vs in vss]
    t_crss = [[gamma*gamma*(r_bs[i+1]-r_bs[i]) for i, gamma in enumerate(gammas)] for gammas, r_bs in zip(gammass, r_bss)]
    return t_crss

def get_derivatives(ys, xs):
    dydxs = []
    for i in xrange(len(xs)):
        if i == len(xs)-1:
            dy = ys[i]-ys[i-1]
            dx = xs[i]-xs[i-1]
        elif i == 0:
            dy = ys[i+1]-ys[i]
            dx = xs[i+1]-xs[i]
        else:
            dy = ys[i+1]-ys[i-1]
            dx = xs[i+1]-xs[i-1]
        if dx != 0.:
            dydx = dy/dx
        else:
            dydx = nan
        dydxs.append(dydx)
    return dydxs

def get_compression_times(vss, rss):
    c = 2.99e10
    t_compss = []
    for vs, rs in zip(vss, rss):
        t_comps = []
        for i in xrange(len(vs)):
            if i == len(vs)-1:
                i -= 1
            dv = (vs[i+1]-vs[i])*c
            dr = rs[i+1]-rs[i]
            if dv != 0.:
                t_comp = dr/dv
                t_comp = sqrt(t_comp*t_comp)
            else:
                t_comp = nan
            t_comps.append(t_comp)
        t_compss.append(t_comps)

    return t_compss

def read_mhd_file(input_file, n_max=None, show_ghosts=False, keep_nth=None):
    if input_file[-3:] != 'mhd':
        print '  error: wrong file format!'
        raise SystemExit
    iterations = []
    ts = []
    m_bss = []
    r_bss = []
    vss = []
    rhoss = []
    pss = []
    bss = []

    reading_data = 0
    with open(input_file, 'r') as f:
        i = 0
        lines = f.readlines()
        for line in lines:
            i += 1
            if line[:5] == 'alpha':
                alpha = int(line.split()[-1])
            if line[:8] == 'gamma_ad':
                gamma_ad = float(line.split()[-1])
            if line == '_DATA\n':
                reading_data = 1
                break
        lines = lines[i:]
        n = len(lines)/9

        if n_max:
            if n > n_max:
                n = n_max

        # w = 200
        # lines_short = []
        # for line in lines:
        #     if len(line) > w:
        #         lines_short.append(line[:w])
        #     else:
        #         lines_short.append(line[:-2])

        # lines_short = lines_short[:15*9-1] + ['', ' 1'] + lines_short[15*9-1:]
        # lines = lines[:15*9-1] + ['', ' 1'] + lines[15*9-1:]

        # for j in xrange(16*9):
        #     print lines_short[j]
        # # print lines_short[15*9-1]
        # raise SystemExit

        for j in xrange(n):

            sub_lines = lines[9*j:9*j+9]

            iterations.append(int(sub_lines[0]))
            ts.append(float(sub_lines[1]))

            if show_ghosts:
                m_bss.append([float(x) for x in sub_lines[2].split()])
                r_bss.append([float(x) for x in sub_lines[3].split()])
                vss.append([float(x) for x in sub_lines[4].split()])
                rhoss.append([float(x) for x in sub_lines[5].split()])
                pss.append([float(x) for x in sub_lines[6].split()])
                bss.append([float(x) for x in sub_lines[7].split()])
            else:
                m_bss.append([float(x) for x in sub_lines[2].split()[2:-2]])
                r_bss.append([float(x) for x in sub_lines[3].split()[2:-2]])
                vss.append([float(x) for x in sub_lines[4].split()][2:-2])
                rhoss.append([float(x) for x in sub_lines[5].split()][2:-2])
                pss.append([float(x) for x in sub_lines[6].split()][2:-2])
                bss.append([float(x) for x in sub_lines[7].split()][2:-2])

    if alpha == 2:
        m_bss = [[4.*pi*m for m in m_bs] for m_bs in m_bss]
    elif alpha == 1:
        m_bss = [[2.*pi*m for m in m_bs] for m_bs in m_bss]

    if keep_nth:
        if not (len(ts)-1) % keep_nth:
            ts = ts[::keep_nth]
            iterations = iterations[::keep_nth]
            m_bss = m_bss[::keep_nth]
            r_bss = r_bss[::keep_nth]
            vss = vss[::keep_nth]
            rhoss = rhoss[::keep_nth]
            pss = pss[::keep_nth]
            bss = bss[::keep_nth]
        else:
            ts = ts[::keep_nth] + [ts[-1]]
            iterations = iterations[::keep_nth] + [iterations[-1]]
            m_bss = m_bss[::keep_nth] + [m_bss[-1]]
            r_bss = r_bss[::keep_nth] + [r_bss[-1]]
            vss = vss[::keep_nth] + [vss[-1]]
            rhoss = rhoss[::keep_nth] + [rhoss[-1]]
            pss = pss[::keep_nth] + [pss[-1]]
            bss = bss[::keep_nth] + [bss[-1]]
    return iterations, ts, m_bss, r_bss, vss, rhoss, pss, bss, alpha, gamma_ad

def read_pre_file(input_file, n_max=None, show_ghosts=False, keep_nth=None):
    p_gammass = []
    with open(input_file, 'r') as f:
        f_heat = float(f.readline().split()[-1])
        n = 0
        for line in f.readlines():
            if show_ghosts:
                p_gammass.append([float(x) for x in line.split()])
            else:
                p_gammass.append([float(x) for x in line.split()][2:-2])
            n += 1
            if n_max:
                if n == n_max:
                    break

    if keep_nth:
        if not (len(p_gammass)-1) % keep_nth:
            p_gammass = p_gammass[::keep_nth]
        else:
            p_gammass = p_gammass[::keep_nth] + [p_gammass[-1]]

    return p_gammass, f_heat

def read_zpm_file(input_file, n_max=None, show_ghosts=False, keep_nth=None):
    Z_pmss = []
    with open(input_file, 'r') as f:
        n = 0
        for line in f.readlines():
            if show_ghosts:
                Z_pmss.append([float(x) for x in line.split()])
            else:
                Z_pmss.append([float(x) for x in line.split()][2:-2])
            n += 1
            if n_max:
                if n == n_max:
                    break

    if keep_nth:
        if not (len(Z_pmss)-1) % keep_nth:
            Z_pmss = Z_pmss[::keep_nth]
        else:
            Z_pmss = Z_pmss[::keep_nth] + [Z_pmss[-1]]

    return Z_pmss

def get_colors(N, cmap='magma', cmin=0, cmax=1):
    dc = (cmax-cmin)/float(N-1)
    cs = [cmin + i*dc for i in xrange(N)]
    return plt.get_cmap(cmap)(cs)

def import_mplstyle(mplstyle):
    mplstyle_path = ''
    for part in os.getcwd().split('/'):
        if part:
            mplstyle_path = mplstyle_path + '/'+part
            if part == 'radshock':
                # Found the "base" folder
                break
    mplstyle_path += '/scripts'
    sys.path.append(mplstyle_path)
    plt.style.use(mplstyle_path+'/'+mplstyle)

def plot_data(xss, ysss, ns, xlabel, ylabels, xlog=False, ylog=True, xlims=None, ylims=None, watermark=False, legend_loc=1, legend_font_size=None, marker=None, zero_line=None, track=None, save_name=None,
              no_show=False, separate=None, suppress_warning=False, single_ylabel=None, final_line_is_black=True, ylabels_for_same_line=None, cmaps=None, zorder_reverse=False,
              no_tight_layout=False, find_all_shocks=False, multiple_panels=False, multiple_ylims=None, legend_labels_in_plot=None, no_rotation_of_text=False, vlines=None):

    import_mplstyle('radshock.mplstyle')
    if multiple_panels:
        n_panels = len(ysss)
        scale = 9
        fig, axs = plt.subplots(n_panels, 1, sharex=True, figsize=(scale, scale*n_panels*.75))
        fig.subplots_adjust(hspace=0.1)
        no_tight_layout = True
    else:
        fig, ax = plt.subplots()
        axs = [ax]

    if not cmaps:
        cmaps = ['plasma', 'viridis', 'magma']

    if multiple_panels:
        lss = ['-' for yss in ysss]
    else:
        lss = ['-', '--', ':']

    if len(ysss) == 1:
        colors_base = ['k', 'r', 'DodgerBlue']
    else:
        # n_colors = max(3, len(ysss))
        n_colors = len(ysss)
        colors_base = get_colors(n_colors, cmap='plasma_r', cmin=.1, cmax=.9)
    # colors_base = ['k', 'r', 'DodgerBlue']
    # colors_base = ['r', 'DodgerBlue', 'k']

    separate_factor = len(ns)
    if separate:
        if single_ylabel:
            single_ylabel = single_ylabel + r'$\, \mathrm{(arb.\, norm.)}$'
        else:
            ylabels = [ylabel+r'$\, \mathrm{(arb.\, norm.)}$' for ylabel in ylabels]

    if watermark:
        # fig, ax = plt.subplots()
        for ax in axs:
            ax.text(.05, .025, watermark, ha='left', va='bottom', fontsize=40, color='k', alpha=.1, rotation=0, transform=ax.transAxes)

    if zero_line:
        for ax in axs:
            ax.plot([min([min(xs) for xs in xss]), max([max(xs) for xs in xss])], [0, 0], 'k', alpha=.2, ls=':')

    #

    # Finding ymin, ymax
    if not multiple_panels:
        ysss_plot = []
        for yss in ysss:
            yss_plot = []
            for n in ns:
                yss_plot.append(yss[n])
            ysss_plot.append(yss_plot)
        if separate:
            ysss_plot = [[[y*(separate_factor**i) for y in ys] for i, ys in enumerate(yss)] for yss in ysss_plot]

        # Find indices for minimum and maximum x-values
        i_min = 0
        i_max = -1
        if xlims:
            if xss[0][1] > xss[0][0]:
                # i_min = min([bisect(xs, xlims[0], no_ghosts=True) for xs in xss])
                # i_max = max([bisect(xs, xlims[1], no_ghosts=True) for xs in xss])
                i_min = min([bisect(xs, xlims[0], no_ghosts=False) for xs in xss])
                i_max = max([bisect(xs, xlims[1], no_ghosts=False) for xs in xss])
            else:
                # i_min = min([bisect_rev(xs, xlims[1], no_ghosts=True) for xs in xss])
                # i_max = max([bisect_rev(xs, xlims[0], no_ghosts=True) for xs in xss])
                i_min = min([bisect_rev(xs, xlims[1], no_ghosts=False) for xs in xss])
                i_max = max([bisect_rev(xs, xlims[0], no_ghosts=False) for xs in xss])

        ymin = 1e60
        for yss in ysss_plot:
            ymin_trial = min([min(ys[i_min:i_max]) for ys in yss])
            if ymin_trial < ymin:
                ymin = ymin_trial
        # ymax = 1e-60
        ymax = -1e60
        for yss in ysss_plot:
            ymax_trial = max([max(ys[i_min:i_max]) for ys in yss])
            if ymax_trial > ymax:
                ymax = ymax_trial

        if ylog:
            if ymin > 0:
                R = ymax/ymin
                if R > 1.1:
                    if len(ylabels) > 1:
                        ax.set_ylim(ymin/(R**(1./10.)), ymax*(R**(1./2.)))
                    else:
                        ax.set_ylim(ymin/(R**(1./10.)), ymax*(R**(1./10.)))
                else:
                    ax.set_ylim(.5*ymin, 2.*ymax)
            else:
                if not suppress_warning:
                    if ymin == 0:
                        print '  WARNING: attempting to plot zero valued y-values in log scale'
                    else:
                        print '  WARNING: attempting to plot negative y-values in log scale'
            ax.set_yscale('log')

        else:
            if 1e50 > ymin > 0:
                if (ymax-ymin)/(ymax+ymin) < 1e-2:
                    ax.set_ylim(.5*ymax, 1.5*ymax)
            else:
                dy = ymax-ymin
                ax.set_ylim(ymin-.2*dy, ymax+.2*dy)
    else:
        if ylog:
            i = 0
            for ax in axs:
                ax.set_yscale('log')
                i += 1

    #

    for ax, ylabel in zip(axs, ylabels):
        ax.set_xlim(min([min(xss[n]) for n in ns]), max([max(xss[n]) for n in ns]))

    for ax in axs:
        if xlog:
            ax.set_xscale('log')
        if xlims:
            ax.set_xlim(xlims[0], xlims[1])
        if ylims and not multiple_ylims:
            ax.set_ylim(ylims[0], ylims[1])

    if multiple_panels and multiple_ylims:
        i = 0
        for ax in axs:
            ylims = multiple_ylims[i], multiple_ylims[i+1]
            ax.set_ylim(ylims[0], ylims[1])
            i += 2

    cmin = .1
    cmax = .9

    # # --- Hack, to get nice ticks for a plot with multiple panels ---
    # from matplotlib.ticker import FuncFormatter, FixedLocator
    # formatter = FuncFormatter(lambda value, tick_number: r'$%2.f$' % (value))
    # ax = axs[1]
    # ax.yaxis.set_minor_locator(FixedLocator([2, 3, 4, 5]))
    # ax.yaxis.set_minor_formatter(formatter)
    # cmin = 0
    # cmax = .8

    #

    ax_nr = 0
    while len(lss) < len(ysss):
        lss.append('-')
    while len(cmaps) < len(ysss):
        cmaps.append('plasma')

    if vlines:
        ylims = axs[ax_nr].get_ylim()
        for vline in vlines:
            axs[ax_nr].vlines(vline, ylims[0], ylims[1], 'r')

    for yss, ylabel, cmap, ls, color_base in zip(ysss, ylabels, cmaps, lss, colors_base):
        colors = [color for color in get_colors(max(len(ns), 2), cmap=cmap, cmin=cmin, cmax=cmax)]
        if final_line_is_black:
            colors[-1] = 'k'
        if len(ns) == 1:
            colors = [color_base]
            ls = '-'
        # colors = colors_base
        i = 0
        for n, color in zip(ns, colors):
            xs = xss[n]
            ys = yss[n]
            if separate:
                ys = [y*(separate_factor**i) for y in ys]
            i += 1
            if zorder_reverse:
                zorder = -i
            else:
                zorder = i - len(ns)-2

            if ylabels_for_same_line:
                label = ylabels_for_same_line[i-1]
                # if multiple_panels and ax_nr > 0:
                if multiple_panels and ax_nr != 1:
                    label = None
            elif n == ns[0] and not multiple_panels:
                label = ylabel
            else:
                label = None

            if legend_labels_in_plot and label != None:
                pwil(xs, ys, label, x_loc=legend_labels_in_plot[i-1], c=color, ls=ls, fontsize=legend_font_size, ax=axs[ax_nr], no_rotation=no_rotation_of_text, above=1.25)
            else:
                axs[ax_nr].plot(xs, ys, color=color, ls=ls, marker=marker, markersize=10, label=label, zorder=zorder)

            if track != None:
                axs[ax_nr].plot([xs[track]], [ys[track]], color=color, marker='o', markeredgewidth=2.5, mfc='none', markersize=13)

            if find_all_shocks:
                #Ns = find_shocks(ys)

                # Calibrated for betagamma
                dAA_lim = .001
                W = 15

                # dAA_lim = .015
                # W = 300

                Ns = find_shocks(ys, dAA_min=dAA_lim, dAA_max=dAA_lim, W=W)
                if len(Ns) > 2:
                    Ns = [Ns[0], Ns[-1]]

                # if len(Ns) > 1:
                #     Ns = [Ns[0]]

                for N in Ns:
                    axs[ax_nr].plot([xs[int(N)]], [ys[int(N)]], color='r', marker='o', markeredgewidth=2.5, mfc='none', markersize=24)
        if multiple_panels:
            ax_nr += 1

    #

    axs[-1].set_xlabel(xlabel)
    for ax, ylabel in zip(axs, ylabels):
        # ax.set_xlim(min([min(xs) for xs in xss]), max([max(xs) for xs in xss]))
        # ax.set_xlabel(xlabel)

        if single_ylabel:
            ax.set_ylabel(single_ylabel)
            ax.legend(loc=legend_loc, fontsize=legend_font_size)
        elif len(ylabels) == 1:
            ax.set_ylabel(ylabels[0])
            if ylabels_for_same_line:
                ax.legend(loc=legend_loc, fontsize=legend_font_size)
        elif multiple_panels:
            ax.set_ylabel(ylabel)
            ax.legend(loc=legend_loc, fontsize=legend_font_size)
        else:
            ax.legend(loc=legend_loc, fontsize=legend_font_size)

    #

    if not no_tight_layout:
        fig.tight_layout()
    if save_name:
        if not multiple_panels:
            plt.savefig(save_name, bbox_inches='tight', pad_inches=0)
        else:
            plt.savefig(save_name, bbox_inches='tight', pad_inches=0.03)

    if not no_show and not save_name:
        plt.show()
    plt.close()

#

# def plot_data(xss, ysss, ns, xlabel, ylabels, xlog=False, ylog=True, xlims=None, ylims=None, watermark=False, legend_loc=1, legend_font_size=None, marker=None, zero_line=None, track=None, save_name=None,
#               no_show=False, separate=None, suppress_warning=False, single_ylabel=None, final_line_is_black=True, ylabels_for_same_line=None, cmaps=None, zorder_reverse=False,
#               no_tight_layout=False, find_all_shocks=False):

#     import_mplstyle('radshock.mplstyle')

#     if not cmaps:
#         cmaps = ['plasma', 'viridis', 'magma']
#     lss = ['-', '--', ':']
#     # colors_base = ['k', 'r', 'DodgerBlue']
#     if len(ysss) == 1:
#         colors_base = ['k', 'r', 'DodgerBlue']
#     else:
#         colors_base = get_colors(3, cmap='plasma_r', cmin=.1, cmax=.9)

#     separate_factor = len(ns)
#     if separate:
#         if single_ylabel:
#             single_ylabel = single_ylabel + r'$\, \mathrm{(arb.\, norm.)}$'
#         else:
#             ylabels = [ylabel+r'$\, \mathrm{(arb.\, norm.)}$' for ylabel in ylabels]

#     if watermark:
#         fig, ax = plt.subplots()
#         plt.text(.05, .025, watermark, ha='left', va='bottom', fontsize=40, color='k', alpha=.1, rotation=0, transform=ax.transAxes)

#     if zero_line:
#         plt.plot([min([min(xs) for xs in xss]), max([max(xs) for xs in xss])], [0, 0], 'k', alpha=.2, ls=':')

#     for yss, ylabel, cmap, ls, color_base in zip(ysss, ylabels, cmaps, lss, colors_base):
#         # colors = [color for color in get_colors(max(len(ns), 2), cmap=cmap, cmin=.05, cmax=.95)]
#         colors = [color for color in get_colors(max(len(ns), 2), cmap=cmap, cmin=.1, cmax=.9)]
#         if final_line_is_black:
#             colors[-1] = 'k'
#         if len(ns) == 1:
#             colors = [color_base]
#             ls = '-'
#         i = 0
#         for n, color in zip(ns, colors):
#             xs = xss[n]
#             ys = yss[n]
#             if separate:
#                 ys = [y*(separate_factor**i) for y in ys]
#             i += 1
#             if zorder_reverse:
#                 zorder = -i
#             else:
#                 zorder = i - len(ns)-2

#             if ylabels_for_same_line:
#                 label = ylabels_for_same_line[i-1]
#             elif n == ns[0]:
#                 label = ylabel
#             else:
#                 label = None
#             plt.plot(xs, ys, color=color, ls=ls, marker=marker, markersize=10, label=label, zorder=zorder)

#             if track != None:
#                 plt.plot([xs[track]], [ys[track]], color=color, marker='o', markeredgewidth=2.5, mfc='none', markersize=13)

#             if find_all_shocks:
#                 #Ns = find_shocks(ys)

#                 # Calibrated for betagamma
#                 dAA_lim = .001
#                 W = 15
#                 Ns = find_shocks(ys, dAA_min=dAA_lim, dAA_max=dAA_lim, W=W)
#                 if len(Ns) > 2:
#                     Ns = [Ns[0], Ns[-1]]

#                 for N in Ns:
#                     plt.plot([xs[int(N)]], [ys[int(N)]], color='r', marker='o', markeredgewidth=2.5, mfc='none', markersize=24)

#     plt.xlim(min([min(xs) for xs in xss]), max([max(xs) for xs in xss]))
#     plt.xlabel(xlabel)

#     if single_ylabel:
#         plt.ylabel(single_ylabel)
#         plt.legend(loc=legend_loc, fontsize=legend_font_size)
#     elif len(ylabels) == 1:
#         plt.ylabel(ylabels[0])
#         if ylabels_for_same_line:
#             plt.legend(loc=legend_loc, fontsize=legend_font_size)
#     else:
#         plt.legend(loc=legend_loc, fontsize=legend_font_size)

#     # Finding ymin, ymax
#     ysss_plot = []
#     for yss in ysss:
#         yss_plot = []
#         for n in ns:
#             yss_plot.append(yss[n])
#         ysss_plot.append(yss_plot)
#     if separate:
#         ysss_plot = [[[y*(separate_factor**i) for y in ys] for i, ys in enumerate(yss)] for yss in ysss_plot]

#     ymin = 1e60
#     for yss in ysss_plot:
#         ymin_trial = min([min(ys) for ys in yss])
#         if ymin_trial < ymin:
#             ymin = ymin_trial
#     ymax = 1e-60
#     for yss in ysss_plot:
#         ymax_trial = max([max(ys) for ys in yss])
#         if ymax_trial > ymax:
#             ymax = ymax_trial

#     if ylog:
#         if ymin > 0:
#             R = ymax/ymin
#             if R > 1.1:
#                 if len(ylabels) > 1:
#                     plt.ylim(ymin/(R**(1./10.)), ymax*(R**(1./2.)))
#                 else:
#                     plt.ylim(ymin/(R**(1./10.)), ymax*(R**(1./10.)))
#             else:
#                 plt.ylim(.5*ymin, 2.*ymax)
#         else:
#             if not suppress_warning:
#                 if ymin == 0:
#                     print '  WARNING: attempting to plot zero valued y-values in log scale'
#                 else:
#                     print '  WARNING: attempting to plot negative y-values in log scale'
#         plt.yscale('log')

#     else:
#         if (ymax-ymin)/(ymax+ymin) < 1e-2:
#             plt.ylim(.5*ymax, 1.5*ymax)

#     if xlog:
#         plt.xscale('log')
#     if xlims:
#         plt.xlim(xlims[0], xlims[1])
#     if ylims:
#         plt.ylim(ylims[0], ylims[1])
#     if not no_tight_layout:
#         plt.tight_layout()
#     if save_name:
#         # plt.subplots_adjust(left=-0.1, right=0, top=0, bottom=-0.1)
#         plt.subplots_adjust(left=0.2, bottom=0.175, right=0.95)
#         plt.savefig(save_name)
#     if not no_show and not save_name:
#         plt.show()
#     plt.close()

#

def animate_update(m, lines, text, labels, xsss, ysss):
    for xss, yss, line in zip(xsss, ysss, lines):
        xs = xss[m]
        ys = yss[m]
        line.set_xdata(xs)
        line.set_ydata(ys)
    if labels is not None:
        text.set_text(labels[m])

def make_movie(xss, ysss, ns, xlabel, ylabels, xlog=False, ylog=True, xlims=None, ylims=None, watermark=False, legend_loc=1, marker=None, zero_line=None, track=None, save_name=None,
               texts=None, colors=None, lss=None, text_loc=1, endbuffer=0, freeze_first=0, FPS=60, suppress_warning=False, single_ylabel=None, find_all_shocks=False, vlines=None):

    import_mplstyle('radshock.mplstyle')
    import matplotlib.animation as animation

    ysss = [[yss[n] for n in ns] for yss in ysss]
    xsss = [[xss[n] for n in ns] for yss in ysss]

    markers = [marker for yss in ysss]

    aniFPS = FPS

    if endbuffer:
        for i in xrange(endbuffer):
            for xss in xsss:
                xss.append(xss[-1])
            for yss in ysss:
                yss.append(yss[-1])
            texts.append(texts[-1])
    fig, ax = plt.subplots(1)

    if not ylabels:
        ylabels = [None for yss in ysss]

    # Make my "base colors"
    cmaps = ['plasma', 'viridis', 'magma']
    color_list = get_colors(40, cmap='plasma')
    base_colors = [color_list[0], color_list[29], color_list[30]]

    color_list = get_colors(40, cmap='viridis')
    base_colors[1] = color_list[27]

    # base_colors = ['#575757', 'Dodgerblue', 'r']
    base_lss = ['-', '--', ':']
    if not colors:
        colors = base_colors[:len(ysss)]
    if not lss:
        lss = base_lss[:len(ysss)]
    is_trackeds = [False for yss in ysss]

    # NEW FOR PLOTTING SPECTRA
    if len(colors) < len(ysss):
        colors = get_colors(len(ysss), cmap='plasma')
        lss = ['-' for color in colors]
    # /NEW FOR PLOTTING SPECTRA

    if track != None:
        xsss_track = [[[xs[track]] for xs in xss] for xss in xsss]
        ysss_track = [[[ys[track]] for ys in yss] for yss in ysss]
        xsss += xsss_track
        ysss += ysss_track

        for i, xss in enumerate(xsss_track):
            markers.append('o')
            colors.append(base_colors[i])
            lss.append('-')
            ylabels.append(None)
            is_trackeds.append(True)

    if find_all_shocks:

        xsss_track = []
        ysss_track = []
        for xss, yss in zip(xsss, ysss):
            xss_track = []
            yss_track = []
            for xs, ys in zip(xss, yss):
                xs_track = []
                ys_track = []

                # Calibrated for betagamma
                dAA_lim = .001
                W = 15
                Ns = find_shocks(ys, dAA_min=dAA_lim, dAA_max=dAA_lim, W=W)
                if len(Ns) > 2:
                    Ns = [Ns[0], Ns[-1]]

                if Ns:
                    for N in Ns:
                        N = int(N)
                        xs_track.append(xs[N])
                        ys_track.append(ys[N])
                else:
                    xs_track.append(nan)
                    ys_track.append(nan)
                xss_track.append(xs_track)
                yss_track.append(ys_track)
            xsss_track.append(xss_track)
            ysss_track.append(yss_track)

        # xsss_track = [[[xs[track]] for xs in xss] for xss in xsss]
        # ysss_track = [[[ys[track]] for ys in yss] for yss in ysss]
        xsss += xsss_track
        ysss += ysss_track

        for i, xss in enumerate(xsss_track):
            markers.append('o')
            colors.append('r')
            lss.append('')
            ylabels.append(None)
            is_trackeds.append(True)

        # Ns = find_shocks(ys)
        # for N in Ns:
        #     plt.plot([xs[int(N)]], [ys[int(N)]], color='r', marker='o', markeredgewidth=2.5, mfc='none', markersize=8)

    alphas = [1 for yss in ysss]

    if zero_line:
        yss_zero = [[0 for y in ys] for ys in ysss[0]]
        ysss.append(yss_zero)
        xsss.append(xsss[0])
        ylabels.append('')
        markers.append(None)
        colors.append('k')
        alphas.append(.2)
        lss.append(':')
        is_trackeds.append(False)

    lines = []
    # if freeze_first:
    #     for xss, yss, color, ls, marker in zip(xsss, ysss, colors, lss, markers):
    #         plt.plot(xss[0], yss[0], marker=marker, ls=ls, color=color, alpha=.15)
    for xss, yss, label, color, ls, marker, alpha, is_tracked in zip(xsss, ysss, ylabels, colors, lss, markers, alphas, is_trackeds):
        if not is_tracked:
            line, = plt.plot(xss[0], yss[0], marker=marker, ls=ls, color=color, label=label, alpha=alpha)
        else:
            line, = plt.plot(xss[0], yss[0], marker=marker, ls=ls, color=color, label=label, alpha=alpha, markeredgewidth=2.5, mfc='none', markersize=13)
        lines.append(line)

    if watermark:
        plt.text(.05, .025, watermark, ha='left', va='bottom', fontsize=40, color='k', alpha=.1, rotation=0, transform=ax.transAxes)

    if texts is not None:
        dx = .04
        dy = .03
        if text_loc == 1:
            x = 1.-dx
            y = 1.-dy
            ha = 'right'
            va = 'top'
        elif text_loc == 2:
            x = dx
            y = 1.-dy
            ha = 'left'
            va = 'top'
        elif text_loc == 3:
            x = dx
            y = dy
            ha = 'left'
            va = 'bottom'
        elif text_loc == 4:
            x = 1.-dx
            y = dy
            ha = 'right'
            va = 'bottom'

        text = plt.text(x, y, r'$ $', ha=ha, va=va, transform=ax.transAxes)
        # text = plt.text(1.-.025, 1.-.035, r'$ $', ha='right', va='top', transform=ax.transAxes)
    else:
        text = None

    ani = animation.FuncAnimation(fig, animate_update, len(ysss[0]), interval=1000/aniFPS, repeat=True,
                                  fargs=([lines, text, texts, xsss, ysss]))

    #

    plt.xlim(min([min(xs) for xs in xsss[0]]), max([max(xs) for xs in xsss[0]]))

    if single_ylabel:
        plt.ylabel(single_ylabel)
        plt.legend(loc=legend_loc)
    elif len(ylabels) == 1:
        plt.ylabel(ylabels[0])
    else:
        plt.legend(loc=legend_loc)

    # if len(ylabels) == 1:
    #     plt.ylabel(ylabels[0])
    # else:
    #     plt.legend(loc=legend_loc)

    # Find indices for minimum and maximum x-values
    i_min = 0
    i_max = -1
    if xlims:
        if xss[0][1] > xss[0][0]:
            i_min = min([bisect(xs, xlims[0], no_ghosts=True) for xs in xss])
            i_max = max([bisect(xs, xlims[1], no_ghosts=True) for xs in xss])
        else:
            i_min = min([bisect_rev(xs, xlims[1], no_ghosts=True) for xs in xss])
            i_max = max([bisect_rev(xs, xlims[0], no_ghosts=True) for xs in xss])

    ymin = 1e60
    for yss in ysss:
        ymin_trial = min([min(ys[i_min:i_max]) for ys in yss])
        if ymin_trial < ymin:
            ymin = ymin_trial
    ymax = 1e-60
    for yss in ysss:
        ymax_trial = max([max(ys[i_min:i_max]) for ys in yss])
        if ymax_trial > ymax:
            ymax = ymax_trial

    # ymin = 1e60
    # for yss in ysss:
    #     ymin_trial = min([min(ys) for ys in yss])
    #     if ymin_trial < ymin:
    #         ymin = ymin_trial
    # ymax = 1e-60
    # for yss in ysss:
    #     ymax_trial = max([max(ys) for ys in yss])
    #     if ymax_trial > ymax:
    #         ymax = ymax_trial

    #

    if ylog:
        if ymin > 0:
            R = ymax/ymin
            if R > 1.1:
                if len(ylabels) > 1:
                    plt.ylim(ymin/(R**(1./10.)), ymax*(R**(1./2.)))
                else:
                    plt.ylim(ymin/(R**(1./10.)), ymax*(R**(1./10.)))
            else:
                plt.ylim(.5*ymin, 2.*ymax)
        else:
            if not suppress_warning:
                print '  WARNING: attempting to plot negative y-values in log scale'
        plt.yscale('log')

    else:
        if ymin > 0 and (ymax-ymin)/(ymax+ymin) < 1e-2:
            plt.ylim(.5*ymax, 1.5*ymax)
        else:
            dy = ymax-ymin
            ax.set_ylim(ymin-.2*dy, ymax+.2*dy)

    #

    if xlog:
        plt.xscale('log')

    if xlims:
        plt.xlim(xlims[0], xlims[1])
    if ylims:
        plt.ylim(ylims[0], ylims[1])

    #

    if vlines:
        ylims = plt.ylim()
        for vline in vlines:
            plt.vlines(vline, ylims[0], ylims[1], 'r')

    #

    if xlabel:
        plt.xlabel(xlabel)
    # if len(ylabels) > 1:
    #     plt.legend(loc=legend_loc)
    # else:
    #     plt.ylabel(ylabels[0])
    plt.tight_layout()

    # Save movie
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=FPS, metadata=dict(artist='c_lundman'), bitrate=None)
    ani.save(save_name, writer=writer, dpi=300)

#

def get_xss_ysss_and_labels(args):

    # Load the file
    c = 2.9979e10
    iterations, ts, m_bss, r_bss, vss, rhoss, pss, bss, alpha, gamma_ad = read_mhd_file(args.INPUT_MHD_FILE, args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
    mss = [[.5*(m_bs[i] + m_bs[i+1]) for i in xrange(len(m_bs)-1)] for m_bs in m_bss]
    rss = [[.5*(r_bs[i] + r_bs[i+1]) for i in xrange(len(r_bs)-1)] for r_bs in r_bss]

    # Shift r?
    if args.v_shift:
        v = args.v_shift
        t_i = ts[0]
        # r_bss = [[r_b - v*(t - t_i) for r_b in r_bs] for r_bs, t in zip(r_bss, ts)]
        rss = [[r - v*(t - t_i) for r in rs] for rs, t in zip(rss, ts)]

    try:
        # Print timing information?
        if args.print_times:
            v_i = vss[0][-1]
            r_i = rss[0][-1]
            ct_i = r_i/v_i
            c = 2.9979e10
            dt = (ct_i - r_i)/c
            gamma_i = 1./sqrt(1. - v_i*v_i)

            v_i_inner = vss[0][0]
            r_i_inner = rss[0][0]
            ct_i_inner = r_i_inner/v_i_inner
            #print 'gamma_i = %4.2f' % (gamma_i)
            #print 'r_i = %4.8e' % (r_i)
            #print 'dt = %4.8f s (time between gws and outer boundary, based on ct_i from this file)' % (dt)
            print 'ct_i_inner = %4.8e cm (estimate from motion of inner boundary)' % (ct_i_inner)
            print 'ct_i = %4.8e cm (estimate from motion of outer boundary)' % (ct_i)
            print 'ct_sim[0] = %4.8e cm' % (ts[0])
            print 'ct_sim[-1] = %4.8e cm' % (ts[-1])
            raise SystemExit

        if args.show_ghosts:
            i = -3
        else:
            i = -1

    except:
        pass

    M_os = [m_bs[i] for m_bs in m_bss]
    R_os = [r_bs[i] for r_bs in r_bss]
    m_revss = [[M_o-m for m in ms] for M_o, ms in zip(M_os, mss)]
    r_revss = [[R_o-r for r in rs] for R_o, rs in zip(R_os, rss)]
    gammass = [[1./sqrt(1.-v*v) for v in vs] for vs in vss]
    betagammass = [[v*gamma for v, gamma in zip(vs, gammas)] for vs, gammas in zip(vss, gammass)]

    # Print information
    if not args.quiet:
        print 'File contains %i snapshots' % (len(ts))

    # Check if radiation variables should be loaded
    load_radiation_vars = False
    args_that_want_to_load_radiation_vars = ['z_pm', 'z_pm_m1', 'tau_exp', 'dtau', 'theta', 'theta_e', 'theta_p', 'theta_c', 'dtheta_c', 'p_gamma', 'r_p_gamma', 'rr_p_gamma',
                                             'rrr_p_gamma', 'tau', 'tau_rev', 'tau_inv', 't_sc', 'ct_sc', 'xi', 'adiabat_e', 'adiabat_p', 'theta_c_over_theta_p', 'w_gamma',
                                             'cooling_ratio', 'eps_avg', 'eps_avg_lab', 'eta_gamma', 'dbetadtau']
    for var in args.VAR:
        if var.lower() in args_that_want_to_load_radiation_vars:
            load_radiation_vars = True
    if args.x_coordinate.lower() in args_that_want_to_load_radiation_vars:
        load_radiation_vars = True

    # Load radiation variables if requested
    if load_radiation_vars:
        # f_heat = 1.
        p_gammass, f_heat = read_pre_file(args.INPUT_MHD_FILE[:-3]+'pre', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
        Z_pmss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'zpm', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
        xiss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'xis', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
        thetass = [[get_theta_e(rho, p, Z_pm, xi, f_heat) for rho, p, Z_pm, xi in zip(rhos, ps, Z_pms, xis)] for rhos, ps, Z_pms, xis in zip(rhoss, pss, Z_pmss, xiss)]
        theta_pss = [[get_theta_p(rho, p, Z_pm, xi, f_heat) for rho, p, Z_pm, xi in zip(rhos, ps, Z_pms, xis)] for rhos, ps, Z_pms, xis in zip(rhoss, pss, Z_pmss, xiss)]
        theta_Css = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'cte', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
        if not args.quiet:
            print 'Simulation used f_heat = %4.4e' % (f_heat)
        # thetass = [[get_theta(rho, p, Z_pm, f_heat) for rho, p, Z_pm in zip(rhos, ps, Z_pms)] for rhos, ps, Z_pms in zip(rhoss, pss, Z_pmss)]

    # Get the x-coordinate
    xss = []
    xlabel = None
    if args.x_coordinate == 'm':
        xss = mss
        if alpha == 2:
            xlabel = r'$m \, \mathrm{[g]}$'
        elif alpha == 1:
            xlabel = r'$m \, \mathrm{[g/cm]}$'
        elif alpha == 0:
            xlabel = r'$m \, \mathrm{[g/cm^2]}$'
    elif args.x_coordinate == 'r':
        xss = rss
        xlabel = r'$r \, \mathrm{[cm]}$'
    elif args.x_coordinate == 'dr':
        xss = [[r-r_bs[0] for r in rs] for rs, r_bs in zip(rss, r_bss)]
        xlabel = r'$r - r_L \, \mathrm{[cm]}$'
    elif args.x_coordinate == 'tau':
        xss = get_tauss(m_bss, rss, alpha, Z_pmss=Z_pmss, show_ghosts=args.show_ghosts)
        xlabel = r'$\tau$'
    elif args.x_coordinate == 'tau_inv':
        xss = get_tauss(m_bss, rss, alpha, Z_pmss=Z_pmss, show_ghosts=args.show_ghosts)
        xss = [[1./x for x in xs] for xs in xss]
        xlabel = r'$R_\star/r$'
    elif args.x_coordinate == 'tau_p':
        xss = get_tauss(m_bss, rss, alpha, Z_pmss=None, show_ghosts=args.show_ghosts)
        xlabel = r'$\tau_p$'
    elif args.x_coordinate == 'tau_p_inv':
        xss = get_tauss(m_bss, rss, alpha, Z_pmss=None, show_ghosts=args.show_ghosts)
        xss = [[1./x for x in xs] for xs in xss]
        xlabel = r'$R_\star/r$'

    elif args.x_coordinate == 'm_rev':
        xss = m_revss
        if alpha == 2:
            xlabel = r'$m \, \mathrm{[g]}$'
        elif alpha == 1:
            xlabel = r'$m \, \mathrm{[g/cm]}$'
        if alpha == 0:
            xlabel = r'$m \, \mathrm{[g/cm^2]}$'
    elif args.x_coordinate == 'r_rev':
        xss = r_revss
        xlabel = r'$r \, \mathrm{[cm]}$'
    elif args.x_coordinate == 'tau_rev':
        xss = get_tauss(m_bss, rss, alpha, Z_pmss=Z_pmss, show_ghosts=args.show_ghosts, reverse=True)
        xlabel = r'$\tau$'
    elif args.x_coordinate == 'tau_p_rev':
        xss = get_tauss(m_bss, rss, alpha, Z_pmss=None, show_ghosts=args.show_ghosts, reverse=True)
        xlabel = r'$\tau_p$'

    elif args.x_coordinate == 'j':
        xss = [[j for j in xrange(len(ms))] for ms in mss]
        xlabel = r'$j$'

    # Get the y-coordinates
    ysss = []
    ylabels = []
    for var in args.VAR:

        if var.lower() in ['v', 'beta']:
            yss = vss
            ylabel = r'$v/c$'

        elif var == 'rho':
            yss = rhoss
            ylabel = r'$\rho \, \mathrm{[g/cm^3]}$'

        elif var == 'r_rho':
            yss = [[r*rho for r, rho in zip(rs, rhos)] for rs, rhos in zip(rss, rhoss)]
            ylabel = r'$r\rho \, \mathrm{[g/cm^2]}$'

        elif var == 'rr_rho':
            yss = [[r*r*rho for r, rho in zip(rs, rhos)] for rs, rhos in zip(rss, rhoss)]
            ylabel = r'$r^2\rho \, \mathrm{[g/cm]}$'

        elif var == 'rrr_rho':
            yss = [[r*r*r*rho for r, rho in zip(rs, rhos)] for rs, rhos in zip(rss, rhoss)]
            ylabel = r'$r^3\rho \, \mathrm{[g]}$'

        elif var == 'n_p':
            m_p = 1.6726231e-24
            yss = [[rho/m_p for rho in rhos] for rhos in rhoss]
            ylabel = r'$n_p$'

        elif var == 'r_n_p':
            m_p = 1.6726231e-24
            yss = [[r*rho/m_p for r, rho in zip(rs, rhos)] for rs, rhos in zip(rss, rhoss)]
            ylabel = r'$r n_p$'

        elif var == 'rr_n_p':
            m_p = 1.6726231e-24
            yss = [[r*r*rho/m_p for r, rho in zip(rs, rhos)] for rs, rhos in zip(rss, rhoss)]
            ylabel = r'$r^2 n_p$'

        elif var == 'rrr_n_p':
            m_p = 1.6726231e-24
            yss = [[r*r*r*rho/m_p for r, rho in zip(rs, rhos)] for rs, rhos in zip(rss, rhoss)]
            ylabel = r'$r^3 n_p$'

        elif var in ['p', 'p_g']:
            if not args.cgs:
                yss = pss
                ylabel = r'$P_g/c^2 \, \mathrm{[g/cm^3]}$'
            else:
                yss = [[p*c*c for p in ps] for ps in pss]
                ylabel = r'$P_g \, \mathrm{[erg/cm^3]}$'

        elif var == 'r_p':
            if not args.cgs:
                yss = [[r*p for r, p in zip(rs, ps)] for rs, ps in zip(rss, pss)]
                ylabel = r'$r p_g/c^2 \, \mathrm{[g/cm^2]}$'
            else:
                yss = [[r*p*c*c for r, p in zip(rs, ps)] for rs, ps in zip(rss, pss)]
                ylabel = r'$r p_g \, \mathrm{[erg/cm^2]}$'

        elif var == 'rr_p':
            if not args.cgs:
                yss = [[r*r*p for r, p in zip(rs, ps)] for rs, ps in zip(rss, pss)]
                ylabel = r'$r^2 p_g/c^2 \, \mathrm{[g/cm]}$'
            else:
                yss = [[r*r*p*c*c for r, p in zip(rs, ps)] for rs, ps in zip(rss, pss)]
                ylabel = r'$r^2 p_g \, \mathrm{[erg/cm]}$'

        elif var == 'rrr_p':
            if not args.cgs:
                yss = [[r*r*r*p for r, p in zip(rs, ps)] for rs, ps in zip(rss, pss)]
                ylabel = r'$r^3 p_g/c^2 \, \mathrm{[g]}$'
            else:
                yss = [[r*r*r*p*c*c for r, p in zip(rs, ps)] for rs, ps in zip(rss, pss)]
                ylabel = r'$r^3 p_g \, \mathrm{[erg]}$'

        elif var == 'b':
            yss = bss
            ylabel = r'$b$'

        elif var == 'p_b':
            if not args.cgs:
                yss = [[.5*b*b for b in bs] for bs in bss]
                ylabel = r'$p_b/c^2 \, \mathrm{[g/cm^3]}$'
            else:
                yss = [[.5*b*b*c*c for b in bs] for bs in bss]
                ylabel = r'$p_b \, \mathrm{[erg/cm^3]}$'

        elif var == 'r_p_b':
            if not args.cgs:
                yss = [[.5*b*b for b in bs] for bs in bss]
                yss = [[r*p_b for r, p_b in zip(rs, p_bs)] for rs, p_bs in zip(rss, yss)]
                ylabel = r'$r p_b/c^2 \, \mathrm{[g/cm^2]}$'
            else:
                yss = [[.5*b*b for b in bs] for bs in bss]
                yss = [[r*p_b*c*c for r, p_b in zip(rs, p_bs)] for rs, p_bs in zip(rss, yss)]
                ylabel = r'$r p_b \, \mathrm{[erg/cm^2]}$'

        elif var == 'rr_p_b':
            if not args.cgs:
                yss = [[.5*b*b for b in bs] for bs in bss]
                yss = [[r*r*p_b for r, p_b in zip(rs, p_bs)] for rs, p_bs in zip(rss, yss)]
                ylabel = r'$r^2 p_b/c^2 \, \mathrm{[g/cm]}$'
            else:
                yss = [[.5*b*b for b in bs] for bs in bss]
                yss = [[r*r*p_b*c*c for r, p_b in zip(rs, p_bs)] for rs, p_bs in zip(rss, yss)]
                ylabel = r'$r^2 p_b \, \mathrm{[erg/cm]}$'

        elif var == 'rrr_p_b':
            if not args.cgs:
                yss = [[.5*b*b for b in bs] for bs in bss]
                yss = [[r*r*r*p_b for r, p_b in zip(rs, p_bs)] for rs, p_bs in zip(rss, yss)]
                ylabel = r'$r^3 p_b/c^2 \, \mathrm{[g]}$'
            else:
                yss = [[.5*b*b for b in bs] for bs in bss]
                yss = [[r*r*r*p_b*c*c for r, p_b in zip(rs, p_bs)] for rs, p_bs in zip(rss, yss)]
                ylabel = r'$r^3 p_b \, \mathrm{[erg]}$'

        elif var.lower() == 'gamma':
            yss = gammass
            if var == 'Gamma':
                ylabel = r'$\Gamma$'
            else:
                ylabel = r'$\gamma$'

        elif var.lower() in ['betagamma', 'gammabeta']:
            yss = betagammass
            if var == 'betagamma':
                ylabel = r'$\beta\gamma$'
            elif var == 'gammabeta':
                ylabel = r'$\gamma\beta$'
            elif var == 'betaGamma':
                ylabel = r'$\beta\Gamma$'
            else:
                ylabel = r'$\Gamma\beta$'

        elif var.lower() in ['u']:
            yss = betagammass
            ylabel = r'$u = \gamma\beta$'

        elif var.lower() in ['dbetadtau']:
            tauss = get_tauss(m_bss, rss, alpha, Z_pmss=Z_pmss, show_ghosts=args.show_ghosts)
            yss = [get_derivatives(vs, taus) for vs, taus in zip(vss, tauss)]
            ylabel = r'$d\beta/d\tau$'

        elif var.lower() in ['v_rho']:
            yss = [[v*rho for v, rho in zip(vs, rhos)] for vs, rhos in zip(vss, rhoss)]
            ylabel = r'$\beta\rho$'

        elif var.lower() in ['w']:
            yss = [[gamma_ad/(gamma_ad-1.)*p/rho for p, rho in zip(ps, rhos)] for ps, rhos in zip(pss, rhoss)]
            ylabel = r'$w$'

        elif var.lower() in ['w_gamma']:
            yss = [[4.*p_gamma/rho for p_gamma, rho in zip(p_gammas, rhos)] for p_gammas, rhos in zip(p_gammass, rhoss)]
            ylabel = r'$w_\gamma$'

        elif var.lower() in ['theta', 'theta_e']:
            yss = thetass
            ylabel = r'$\theta_e$'

        elif var.lower() == 'theta_p':
            yss = theta_pss
            ylabel = r'$\theta_p$'

        elif var.lower() == 'cooling_ratio':
            w_gammass = [[4.*p_gamma/rho for p_gamma, rho in zip(p_gammas, rhos)] for p_gammas, rhos in zip(p_gammass, rhoss)]
            m_p_over_m_e = 1836.
            lnLambda = 15.
            yss = [[sqrt(2.*3.14)/lnLambda*m_p_over_m_e*m_p_over_m_e*w_gamma*(theta_e**1.5)/Z_pm for w_gamma, theta_e, Z_pm in zip(w_gammas, theta_es, Z_pms)] for w_gammas, theta_es, Z_pms in zip(w_gammass, thetass, Z_pmss)]
            ylabel = r'$t_{ep}/t_{e\gamma}$'

        elif var.lower() == 't_comp':
            yss = get_compression_times(vss, rss)
            ylabel = r'$t_{comp} \equiv |\partial v / \partial r|^{-1} \, \mathrm{[s]}$'

        elif var.lower() == 'ct_comp':
            yss = get_compression_times(vss, rss)
            yss = [[c*y for y in ys] for ys in yss]
            ylabel = r'$ct_{comp} \equiv |\partial \beta / \partial r|^{-1} \, \mathrm{[cm]}$'

        elif var.lower() == 't_exp':
            c = 2.99e10
            yss = [[r/c/v for r, v in zip(rs, vs)] for rs, vs in zip(rss, vss)]
            ylabel = r'$t_{exp} = r/v \mathrm{[s]}$'

        elif var.lower() == 'ct_exp':
            c = 2.99e10
            yss = [[r/v for r, v in zip(rs, vs)] for rs, vs in zip(rss, vss)]
            ylabel = r'$ct_{exp} = r/\beta \mathrm{[s]}$'

        elif var.lower() == 'adiabat_e':
            yss = [[theta/(rho**(gamma_ad-1.)) for theta, rho in zip(thetas, rhos)] for thetas, rhos in zip(thetass, rhoss)]
            ylabel = r'$\theta_e/\rho^{(\gamma_{ad}-1)}$'

        elif var.lower() == 'adiabat_p':
            yss = [[theta/(rho**(gamma_ad-1.)) for theta, rho in zip(thetas, rhos)] for thetas, rhos in zip(theta_pss, rhoss)]
            ylabel = r'$\theta_p/\rho^{(\gamma_{ad}-1)}$'

        elif var.lower() == 'xi':
            yss = xiss
            ylabel = r'$\xi \equiv \theta_e/\theta_p$'

        elif var.lower() == 'theta_c_over_theta_p':
            yss = [[theta_c/theta_p for theta_c, theta_p in zip(theta_cs, theta_ps)] for theta_cs, theta_ps in zip(theta_Css, theta_pss)]
            ylabel = r'$\theta_C/\theta_p$'

        elif var.lower() == 'theta_c':
            yss = theta_Css
            ylabel = r'$\theta_C$'
            if args.cgs:
                m_e = 9.11e-28
                erg_to_keV = 6.24e+8
                yss = [[y*m_e*c*c*erg_to_keV for y in ys] for ys in yss]
                ylabel = r'$kT_C \, \mathrm{[keV]}$'

        elif var.lower() == 'dtheta_c':
            yss = [[(theta_C-theta_e)/theta_C for theta_C, theta_e in zip(theta_Cs, theta_es)] for theta_Cs, theta_es in zip(theta_Css, thetass)]
            ylabel = r'$(\theta_C - \theta_e)/\theta_C$'

        elif var.lower() == 'eps_avg':
            m_e = 9.11e-28
            m_p = 1.6726231e-24
            zetass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'zet', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            U_gammass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            eps_avgss = [[(m_p/m_e)*U_gamma/rho/zeta for U_gamma, rho, zeta in zip(U_gammas, rhos, zetas)] for U_gammas, rhos, zetas in zip(U_gammass, rhoss, zetass)]
            yss = eps_avgss
            ylabel = r'$\epsilon_{avg}$'

        elif var.lower() in ['therm', 'thermalized']:
            # thermalized = 3*theta_C/eps_avg; equals 1 if Wien spectrum, and > 1 if "non-thermal"
            m_e = 9.11e-28
            m_p = 1.6726231e-24
            zetass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'zet', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            U_gammass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            eps_avgss = [[(m_p/m_e)*U_gamma/rho/zeta for U_gamma, rho, zeta in zip(U_gammas, rhos, zetas)] for U_gammas, rhos, zetas in zip(U_gammass, rhoss, zetass)]
            theta_Css = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'cte', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)

            yss = [[3.*theta_C/eps_avg for theta_C, eps_avg in zip(theta_Cs, eps_avgs)] for theta_Cs, eps_avgs in zip(theta_Css, eps_avgss)]
            ylabel = r'$3\theta_C/\epsilon_{avg}$'

        elif var == 'p_gamma':
            if not args.cgs:
                yss = p_gammass
                ylabel = r'$P_\gamma/c^2 \, \mathrm{[g/cm^3]}$'
            else:
                yss = [[p*c*c for p in ps] for ps in p_gammass]
                ylabel = r'$P_\gamma \, \mathrm{[erg/cm^3]}$'

        elif var == 'r_p_gamma':
            if not args.cgs:
                yss = [[r*p_gamma for r, p_gamma in zip(rs, p_gammas)] for rs, p_gammas in zip(rss, p_gammass)]
                ylabel = r'$r p_\gamma/c^2 \, \mathrm{[g/cm^2]}$'
            else:
                yss = [[r*p_gamma*c*c for r, p_gamma in zip(rs, p_gammas)] for rs, p_gammas in zip(rss, p_gammass)]
                ylabel = r'$r p_\gamma \, \mathrm{[erg/cm^2]}$'

        elif var == 'rr_p_gamma':
            if not args.cgs:
                yss = [[r*r*p_gamma for r, p_gamma in zip(rs, p_gammas)] for rs, p_gammas in zip(rss, p_gammass)]
                ylabel = r'$r^2 p_\gamma/c^2 \, \mathrm{[g/cm]}$'
            else:
                yss = [[r*r*p_gamma*c*c for r, p_gamma in zip(rs, p_gammas)] for rs, p_gammas in zip(rss, p_gammass)]
                ylabel = r'$r^2 p_\gamma \, \mathrm{[erg/cm]}$'

        elif var == 'rrr_p_gamma':
            if not args.cgs:
                yss = [[r*r*r*p_gamma for r, p_gamma in zip(rs, p_gammas)] for rs, p_gammas in zip(rss, p_gammass)]
                ylabel = r'$r^3 p_\gamma/c^2 \, \mathrm{[g]}$'
            else:
                yss = [[r*r*r*p_gamma*c*c for r, p_gamma in zip(rs, p_gammas)] for rs, p_gammas in zip(rss, p_gammass)]
                ylabel = r'$r^3 p_\gamma \, \mathrm{[erg]}$'

        elif var.lower() == 'z_pm':
            yss = Z_pmss
            ylabel = r'$Z_\pm$'

        elif var.lower() == 'z_pm_m1':
            yss = [[Z_pm-1. for Z_pm in Z_pms] for Z_pms in Z_pmss]
            ylabel = r'$Z_\pm - 1$'

        elif var == 'dm':
            yss = [[m_bs[j+1] - m_bs[j] for j in xrange(len(m_bs)-1)] for m_bs in m_bss]
            ylabel = r'$\delta m$'

        elif var == 'dtau':
            yss = get_dtauss(m_bss, rss, alpha, Z_pmss)
            ylabel = r'$\delta \tau \, \mathrm{(column)}$'

        elif var == 'dtau_p':
            yss = get_dtauss(m_bss, rss, alpha)
            ylabel = r'$\delta \tau \, \mathrm{(column)}$'

        elif var == 'r':
            yss = [[.5*(r_bs[j+1] + r_bs[j]) for j in xrange(len(r_bs)-1)] for r_bs in r_bss]
            r_min = yss[0][0]
            r_max = yss[0][-1]
            dr = .5*(r_max - r_min)
            c = 2.99e10
            print 'dr = %4.4e, dt = %4.4f' % (dr, dr/c)
            print 'r_max - r_min = %4.4e' % (r_max-r_min)
            print '(r_max - r_min)/2/c = %4.4f' % ((r_max-r_min)/2./c)
            print '(r_max - r_min)/3/c = %4.4f' % ((r_max-r_min)/3./c)

            r_geo = sqrt(r_max*r_min)
            dr_geo = r_geo-r_min
            print 'dr_geo = %4.4e, dt_geo = %4.4f' % (dr_geo, dr_geo/c)
            ylabel = r'$r \, \mathrm{[cm]}$'

        elif var == 'tau_exp':
            kappa_T = get_kappa_T()
            rss = [[.5*(r_bs[j+1] + r_bs[j]) for j in xrange(len(r_bs)-1)] for r_bs in r_bss]
            gammass = [[1./sqrt(1.-v*v) for v in vs] for vs in vss]
            tau_expss = [[Z_pm*rho*kappa_T*r/gamma for Z_pm, rho, r, gamma in zip(Z_pms, rhos, rs, gammas)] for Z_pms, rhos, rs, gammas in zip(Z_pmss, rhoss, rss, gammass)]
            yss = tau_expss
            ylabel = r'$\tau_{exp} \equiv t_{exp}/t_{sc}$'

        elif var == 'tau_p_exp':
            kappa_T = get_kappa_T()
            rss = [[.5*(r_bs[j+1] + r_bs[j]) for j in xrange(len(r_bs)-1)] for r_bs in r_bss]
            gammass = [[1./sqrt(1.-v*v) for v in vs] for vs in vss]
            tau_p_expss = [[rho*kappa_T*r/gamma for rho, r, gamma in zip(rhos, rs, gammas)] for rhos, rs, gammas in zip(rhoss, rss, gammass)]
            yss = tau_p_expss
            ylabel = r'$\tau_{exp} \equiv t_{exp}/t_{sc}$'

        elif var == 'eta_gamma':
            w_gammass = [[4.*p_gamma/rho for p_gamma, rho in zip(p_gammas, rhos)] for p_gammas, rhos in zip(p_gammass, rhoss)]
            gammass = [[1./sqrt(1.-v*v) for v in vs] for vs in vss]
            yss = [[gamma*(1. + w_gamma) for gamma, w_gamma in zip(gammas, w_gammas)] for gammas, w_gammas in zip(gammass, w_gammass)]
            ylabel = r'$\eta_\gamma = \Gamma(1+w_\gamma)$'

        elif var.lower() in ['u_gamma_l', 'u_gamma_lab']:
            try:
                u_gamma_lss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'ugl', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            except:
                print "Error: could not find '%s', exiting..." % (args.INPUT_MHD_FILE[:-3]+'ugl')
                raise SystemExit
            if not args.cgs:
                yss = u_gamma_lss
                ylabel = r'$u_{\gamma, \mathrm{lab}} \, \mathrm{[g/cm^3]}$'
            else:
                yss = [[u_gamma_l*c*c for u_gamma_l in u_gamma_ls] for u_gamma_ls in u_gamma_lss]
                ylabel = r'$u_{\gamma, \mathrm{lab}} \, \mathrm{[erg/cm^3]}$'

        elif var.lower() in ['r_u_gamma_l', 'r_u_gamma_lab']:
            try:
                u_gamma_lss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'ugl', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            except:
                print "Error: could not find '%s', exiting..." % (args.INPUT_MHD_FILE[:-3]+'ugl')
                raise SystemExit
            yss = [[r*u_gamma_l for u_gamma_l, r in zip(u_gamma_ls, rs)] for u_gamma_ls, rs in zip(u_gamma_lss, rss)]
            if not args.cgs:
                ylabel = r'$r u_{\gamma, \mathrm{lab}} \, \mathrm{[g/cm^2]}$'
            else:
                yss = [[y*c*c for y in ys] for ys in yss]
                ylabel = r'$r u_{\gamma, \mathrm{lab}} \, \mathrm{[erg/cm^2]}$'

        elif var.lower() in ['rr_u_gamma_l', 'rr_u_gamma_lab']:
            try:
                u_gamma_lss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'ugl', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            except:
                print "Error: could not find '%s', exiting..." % (args.INPUT_MHD_FILE[:-3]+'ugl')
                raise SystemExit
            yss = [[r*r*u_gamma_l for u_gamma_l, r in zip(u_gamma_ls, rs)] for u_gamma_ls, rs in zip(u_gamma_lss, rss)]
            if not args.cgs:
                ylabel = r'$r^2 u_{\gamma, \mathrm{lab}} \, \mathrm{[g/cm]}$'
            else:
                yss = [[y*c*c for y in ys] for ys in yss]
                ylabel = r'$r^2 u_{\gamma, \mathrm{lab}} \, \mathrm{[erg/cm]}$'

        elif var.lower() in ['rrr_u_gamma_l', 'rrr_u_gamma_lab']:
            try:
                u_gamma_lss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'ugl', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            except:
                print "Error: could not find '%s', exiting..." % (args.INPUT_MHD_FILE[:-3]+'ugl')
                raise SystemExit
            yss = [[r*r*r*u_gamma_l for u_gamma_l, r in zip(u_gamma_ls, rs)] for u_gamma_ls, rs in zip(u_gamma_lss, rss)]
            if not args.cgs:
                ylabel = r'$r^3 u_{\gamma, \mathrm{lab}} \, \mathrm{[g]}$'
            else:
                yss = [[y*c*c for y in ys] for ys in yss]
                ylabel = r'$r^3 u_{\gamma, \mathrm{lab}} \, \mathrm{[erg]}$'

        elif var.lower() == 'l_gamma':
            try:
                u_gamma_lss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'ugl', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            except:
                print "Error: could not find '%s', exiting..." % (args.INPUT_MHD_FILE[:-3]+'ugl')
                raise SystemExit
            L_gammass = [[4.*pi*r*r*v*u_gamma_l*c*c*c for r, v, u_gamma_l in zip(rs, vs, u_gamma_ls)] for rs, vs, u_gamma_ls in zip(rss, vss, u_gamma_lss)]
            yss = L_gammass
            ylabel = r'$L_\gamma \, \mathrm{[erg/s]}$'

        elif var.lower() == 'l_k':
            gammass = [[1./sqrt(1.-v*v) for v in vs] for vs in vss]
            L_kss = [[4.*pi*r*r*v*gamma*gamma*rho*c*c*c for r, v, gamma, rho in zip(rs, vs, gammas, rhos)] for rs, vs, gammas, rhos in zip(rss, vss, gammass, rhoss)]
            yss = L_kss
            ylabel = r'$L_k \, \mathrm{[erg/s]}$'

        elif var.lower() == 'g0':
            yss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'g0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            ylabel = r'$G^0$'

        elif var.lower() == 'r_g0':
            g0ss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'g0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*g0 for r, g0 in zip(rs, g0s)] for rs, g0s in zip(rss, g0ss)]
            ylabel = r'$rG^0$'

        elif var.lower() == 'rr_g0':
            g0ss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'g0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*r*g0 for r, g0 in zip(rs, g0s)] for rs, g0s in zip(rss, g0ss)]
            ylabel = r'$r^2 G^0$'

        elif var.lower() == 'rrr_g0':
            g0ss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'g0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*r*r*g0 for r, g0 in zip(rs, g0s)] for rs, g0s in zip(rss, g0ss)]
            ylabel = r'$r^3 G^0$'

        elif var.lower() == 'g1':
            yss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'g1s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            ylabel = r'$G^1$'

        elif var.lower() == 'r_g1':
            g1ss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'g1s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*g1 for r, g1 in zip(rs, g1s)] for rs, g1s in zip(rss, g1ss)]
            ylabel = r'$rG^1$'

        elif var.lower() == 'rr_g1':
            g1ss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'g1s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*r*g1 for r, g1 in zip(rs, g1s)] for rs, g1s in zip(rss, g1ss)]
            ylabel = r'$r^2 G^1$'

        elif var.lower() == 'rrr_g1':
            g1ss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'g1s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*r*r*g1 for r, g1 in zip(rs, g1s)] for rs, g1s in zip(rss, g1ss)]
            ylabel = r'$r^3 G^1$'

        elif var.lower() == 'i0':
            yss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            ylabel = r'$I_0$'

        elif var.lower() == 'i1':
            yss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i1s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            ylabel = r'$I_1$'

        elif var == 'u_gamma':
            if not args.cgs:
                yss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
                ylabel = r'$U_\gamma/c^2 \, \mathrm{[g/cm^3]}$'
            else:
                yss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
                yss = [[y*c*c for y in ys] for ys in yss]
                ylabel = r'$U_\gamma \, \mathrm{[erg/cm^3]}$'

        elif var == 'r_u_gamma':
            if not args.cgs:
                U_gammass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
                yss = [[r*u_gamma for r, u_gamma in zip(rs, u_gammas)] for rs, u_gammas in zip(rss, U_gammass)]
                ylabel = r'$r U_\gamma/c^2 \, \mathrm{[g/cm^2]}$'
            else:
                U_gammass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
                yss = [[r*u_gamma*c*c for r, u_gamma in zip(rs, u_gammas)] for rs, u_gammas in zip(rss, U_gammass)]
                ylabel = r'$r U_\gamma \, \mathrm{[erg/cm^2]}$'

        elif var == 'rr_u_gamma':
            if not args.cgs:
                U_gammass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
                yss = [[r*r*u_gamma for r, u_gamma in zip(rs, u_gammas)] for rs, u_gammas in zip(rss, U_gammass)]
                ylabel = r'$r^2 U_\gamma/c^2 \, \mathrm{[g/cm]}$'
            else:
                U_gammass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
                yss = [[r*r*u_gamma*c*c for r, u_gamma in zip(rs, u_gammas)] for rs, u_gammas in zip(rss, U_gammass)]
                ylabel = r'$r^2 U_\gamma \, \mathrm{[erg/cm]}$'

        elif var == 'rrr_u_gamma':
            if not args.cgs:
                U_gammass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
                yss = [[r*r*r*u_gamma for r, u_gamma in zip(rs, u_gammas)] for rs, u_gammas in zip(rss, U_gammass)]
                ylabel = r'$r^3 U_\gamma/c^2 \, \mathrm{[g]}$'
            else:
                U_gammass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
                yss = [[r*r*r*u_gamma*c*c for r, u_gamma in zip(rs, u_gammas)] for rs, u_gammas in zip(rss, U_gammass)]
                ylabel = r'$r^3 U_\gamma \, \mathrm{[erg]}$'

        elif var == 'mu_avg':
            i1ss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i1s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            i0ss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'i0s', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[i1/i0 for i1, i0 in zip(i1s, i0s)] for i1s, i0s in zip(i1ss, i0ss)]
            ylabel = r'$\bar{\mu} \equiv I_1/I_0$'

        elif var == 'zeta':
            yss = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'zet', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            ylabel = r'$\zeta$'

        elif var == 'n_gamma':
            m_p = 1.6726231e-24
            zetass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'zet', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[zeta*rho/m_p for zeta, rho in zip(zetas, rhos)] for zetas, rhos in zip(zetass, rhoss)]
            ylabel = r'$n_\gamma$'

        elif var == 'r_n_gamma':
            m_p = 1.6726231e-24
            zetass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'zet', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*zeta*rho/m_p for r, zeta, rho in zip(rs, zetas, rhos)] for rs, zetas, rhos in zip(rss, zetass, rhoss)]
            ylabel = r'$r n_\gamma$'

        elif var == 'rr_n_gamma':
            m_p = 1.6726231e-24
            zetass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'zet', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*r*zeta*rho/m_p for r, zeta, rho in zip(rs, zetas, rhos)] for rs, zetas, rhos in zip(rss, zetass, rhoss)]
            ylabel = r'$r^2 n_\gamma$'

        elif var == 'rrr_n_gamma':
            m_p = 1.6726231e-24
            zetass = read_zpm_file(args.INPUT_MHD_FILE[:-3]+'zet', args.n_max, args.show_ghosts, keep_nth=args.keep_nth)
            yss = [[r*r*r*zeta*rho/m_p for r, zeta, rho in zip(rs, zetas, rhos)] for rs, zetas, rhos in zip(rss, zetass, rhoss)]
            ylabel = r'$r^3 n_\gamma$'

        elif var == 't_sc':
            c = 2.99e10
            yss = get_t_scss(vss, rhoss, Z_pmss)
            yss = [[y/c for y in ys] for ys in yss]
            ylabel = r'$t_{sc} \, \mathrm{[s],\, (lab \, frame)}$'

        elif var == 'ct_sc':
            yss = get_t_scss(vss, rhoss, Z_pmss)
            ylabel = r'$ct_{sc} \, \mathrm{[cm],\, (lab \, frame)}$'

        elif var == 't_hom':
            c = 2.99e10
            yss = [[r/v/c for r, v in zip(rs, vs)] for rs, vs in zip(rss, vss)]
            ylabel = r'$r/v \, \mathrm{[s]}$'

        elif var == 'ct_hom':
            c = 2.99e10
            yss = [[r/v for r, v in zip(rs, vs)] for rs, vs in zip(rss, vss)]
            ylabel = r'$r(c/v) \, \mathrm{[cm]}$'

        # elif var == 't_i':
        #     c = 2.99e10
        #     yss = [[ts[0]/c for r, v in zip(rs, vs)] for rs, vs in zip(rss, vss)]
        #     ylabel = r'$t_i \, \mathrm{[s]}$'

        elif var == 'ct_i':
            yss = [[ts[0] for r, v in zip(rs, vs)] for rs, vs in zip(rss, vss)]
            ylabel = r'$ct_i \, \mathrm{[cm]}$'

        elif var == 't_cr':
            c = 2.99e10
            yss = get_t_crss(r_bss, vss)
            yss = [[y/c for y in ys] for ys in yss]
            ylabel = r'$t_{cr} \, \mathrm{[s],\, (lab \, frame)}$'

        elif var == 'ct_cr':
            yss = get_t_crss(r_bss, vss)
            ylabel = r'$ct_{cr} \, \mathrm{[cm],\, (lab \, frame)}$'

        else:
            print 'VAR = %s is not defined,' % (var)
            print 'Exiting...'
            raise SystemExit

        ysss.append(yss)
        ylabels.append(ylabel)

    # Preliminary?
    if args.preliminary:
        watermark = r'$\mathrm{PRELIMINARY}$'
    else:
        watermark = None
    if args.marker:
        marker = '.'
    else:
        marker = None
    if args.abs:
        ysss = [[[abs(y) for y in ys] for ys in yss] for yss in ysss]
        ylabels = [r'$|$' + ylabel + r'$|$' for ylabel in ylabels]

    return xss, ysss, xlabel, ylabels, ts, m_bss, r_bss, rss, rhoss, gammass, marker, watermark

def get_ns(args, n_max):
    # Determining snapshot numbers
    make_sample_range = 0
    if args.all:
        ns = range(n_max)
    elif args.sample:
        make_sample_range = 1
    elif args.plot_range != None:
        ns = range(args.plot_range[0], args.plot_range[1]+1)
    elif args.plot_before != None:
        if args.plot_after != None:
            ns = range(args.plot_after+1, args.plot_before)
        else:
            ns = range(args.plot_before)
    elif args.plot_after != None:
        ns = range(args.plot_after+1, n_max)
    elif args.n:
        ns = args.n
    else:
        make_sample_range = 1

    # If movie, assume --all
    if args.output_file:
        if args.output_file[-3:] == 'mp4':
            if not args.sample and make_sample_range == 1:
                ns = range(n_max)
                make_sample_range = 0

    # Make a sample range?
    if make_sample_range:
        step = max(1, n_max/10) # show ~10 snapshots, plus final snapshot
        ns = range(0, n_max-1, step) + [n_max-1]

    # Are all snapshots available?
    ns_cut = [n for n in ns if n < n_max]
    if len(ns_cut) < len(ns):
        print '  WARNING: Cut away final value(s) from -n (max for this file is %i)' % (n_max-1)

    return ns_cut

def get_periodic_ysss(ysss):
    ysss_new = []
    for yss in ysss:
        yss_new = []
        for ys in yss:
            c = len(ys)/2
            ys_new = ys[c:] + ys[:c]

            yss_new.append(ys_new)
        ysss_new.append(yss_new)
    return ysss_new

def connect_periods(xss, ysss, n_periods):
    xss_new = []
    # For each snapshot
    for n in xrange(len(xss)):
        xs_new = []
        dx = .5*(xss[n][-1]-xss[n][-2])
        x_max = xss[n][-1] + dx
        # For each period
        for i in xrange(n_periods):
            # For each bin
            for j in xrange(len(xss[n])):
                x_new = xss[n][j] + x_max*i
                xs_new.append(x_new)
        xss_new.append(xs_new)

    # For each VAR
    ysss_new = []
    for yss in ysss:
        yss_new = []
        for n in xrange(len(yss)):
            ys_new = []
            for i in xrange(n_periods):
                ys_new += yss[n]
            yss_new.append(ys_new)
        ysss_new.append(yss_new)

    return xss_new, ysss_new

#

def main():
    parser = argparse.ArgumentParser(description='a script that produces plots (and movies) from a snapshots file (.mhd)')
    parser.add_argument('INPUT_MHD_FILE', help='the input MHD snapshots file (.mhd)')
    parser.add_argument('VAR', nargs='+', help='the variable(s) to plot (at most 3 variables)')
    parser.add_argument('-o', '--output-file', help='the output pdf (for plot) or mp4 file (for movie)')
    parser.add_argument('-x', '--x-coordinate', default='m', help='the horizontal coordinate (m, m_rev, r, r_rev, j) [default=m]')
    parser.add_argument('-n', type=int, nargs='+', help='the snapshot(s) to plot')
    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    parser.add_argument('--keep-nth', type=int, help="keep only every n'th snapshot (always keeps the first and last snapshots)")
    parser.add_argument('--sample', action='store_true', help='plots a nice sample of snapshots (roughly 15, evenly spaced, including n = 0 and -1)')
    parser.add_argument('--all', action='store_true', help='ignores --sample and plots all snapshots')
    parser.add_argument('--plot-range', nargs=2, type=int, help='plots all snapshots in this range')
    parser.add_argument('--plot-after', type=int, help='plots all snapshots after this snapshot')
    parser.add_argument('--plot-before', type=int, help='plots all snapshots before this snapshot')
    parser.add_argument('--separate', action='store_true', help='shifts each next line upwards by a factor 2, so that lines overlap less (for increased visibility)')
    parser.add_argument('--track', type=int, help='plots a circle at the given spatial bin')
    parser.add_argument('--zero-line', action='store_true', help='draws a line at y = 0')
    parser.add_argument('--colored-final-line', action='store_true', help='makes the final snapshot line colored (as opposed to black)')
    parser.add_argument('--cmap', help='the color map to use for all plots')
    parser.add_argument('--marker', action='store_true', help='plots markers')
    parser.add_argument('--preliminary', action='store_true', help='adds a "preliminary" watermark to the plot')
    parser.add_argument('--z-rev', action='store_true', help='reverses the z-order of the lines (i.e. which lines cover each other)')
    parser.add_argument('--show-ghosts', action='store_true', help='also show ghost cells in the plot')
    parser.add_argument('--check-periodicity', nargs='?', type=int, default=0, const=-1, help='splits the grid at the center, and connects the ends, to find noticable features at the (supposedly periodic) boundaries. adds a red where the grid is reconnected. the optional argument (W) sets the x-axis limits as centered at +-W/2 grid points about the red line.')
    parser.add_argument('--connect-periods', type=int, default=1, help='assume that the grid is periodic, connecting several grids.')
    parser.add_argument('--v-shift', type=float, default=0, help='shift the r-coordinate by this speed (r -> r + v*(t-t_i))')
    parser.add_argument('-tx', '--toggle-xlog', action='store_true', help='turn ON log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_false', help='turn OFF log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--pad', action='store_true', help='adds some extra space outside the x-grid limits (has no effect if --x-lims are given)')
    parser.add_argument('--abs', action='store_true', help='plots the absolute values')
    parser.add_argument('--legend-loc', type=int, help='legend location [default=1]')
    parser.add_argument('--find-shocks', action='store_true', help='attempts to find discontinuities and plots markers in the immediate up- and downstreams')
    parser.add_argument('--legend-times', action='store_true', help='show snapshot time (t) in the legend (only for single VAR)')
    parser.add_argument('--legend-ctimes', action='store_true', help='show snapshot time (ct) in the legend (only for single VAR)')
    parser.add_argument('--legend-n', action='store_true', help='show snapshot number (n) in the legend (only for single VAR)')
    parser.add_argument('--legend-short', action='store_true', help='show only first and last legend items (only for single VAR)')
    parser.add_argument('--legend-font-size', type=int, default=24, help='legend font size [default=24]')
    parser.add_argument('--legend-labels-in-plot', nargs='+', type=float, help='labels are put next to the plot lines instead of in the legend; provide an x-coordinate per label')
    parser.add_argument('--legend-labels-in-plot-no-rotation', action='store_true', help='labels in the plot will not be rotated')
    parser.add_argument('--no-tight-layout', action='store_true', help="don't use tight_layout()")
    parser.add_argument('--multiple-panels', action='store_true', help="plots each VAR in a separate panel")
    parser.add_argument('--multiple-y-lims', nargs='+', type=float, help="limits on the y-axis for each panel")
    parser.add_argument('--multiple-cmaps', nargs='+', type=str, help="color maps for each panel")
    parser.add_argument('--cgs', action='store_true', help='use cgs units (for the pressure: p -> p*c^2)')
    parser.add_argument('--fps', type=int, default=30, help='movie fps [default=30]')
    parser.add_argument('--no-plot', action='store_true', help='does NOT show the plot')
    parser.add_argument('--print-times', action='store_true', help='instead, print timing information')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    # Check number of variables
    if len(args.VAR) > 3 and args.multiple_panels == False:
        print '  ERROR: Too many variables passed, can handle at most three (3).'
        print '         Exiting...'
        raise SystemExit

    # Get variables to plot
    xss, ysss, xlabel, ylabels, ts, m_bss, r_bss, rss, rhoss, gammass, marker, watermark = get_xss_ysss_and_labels(args)
    ns_cut = get_ns(args, n_max=len(ts))


    # Check if grid really is periodic?
    vlines = None
    if args.check_periodicity:
        ysss = get_periodic_ysss(ysss)
        c = len(xss[0])/2
        x_line = .5*(xss[0][c-1] + xss[0][c])
        vlines = [x_line]
        if args.x_lims == None and args.check_periodicity > 0:
            w = args.check_periodicity
            c = len(xss[0])/2
            x_min = xss[0][c-w/2]
            x_max = xss[0][c+w/2-1]
            args.x_lims = x_min, x_max

    # Connect periods?
    if args.connect_periods > 1:
        xss, ysss = connect_periods(xss, ysss, args.connect_periods)

    # Check if output file name should be automatically generated
    if args.output_file:
        if args.output_file[0] == '.':
            string = ''
            for var in args.VAR:
                string += var + '_'
            string = string[:-1]
            args.output_file = string + args.output_file

    if args.legend_times:
        c = 2.99e10
        ylabels_for_same_line = [r'$t = %2.f \, \mathrm{s}$' % (ts[ns_cut[0]]/c)]
        for n in ns_cut[1:]:
            label = r'$%2.f \, \mathrm{s}$' % (ts[n]/c)
            if args.output_file:
                if args.output_file[-3:] == 'mp4':
                    label = r'$t = %2.f \, \mathrm{s}$' % (ts[n]/c)
            ylabels_for_same_line.append(label)
        if args.legend_short:
            for i in xrange(1, len(ns_cut)-1):
                ylabels_for_same_line[i] = None
    elif args.legend_ctimes:
        ylabels_for_same_line = [r'$ct = \, $' + sci_notation(ts[ns_cut[0]]) + r'$ \, \mathrm{cm}$']
        for n in ns_cut[1:]:
            label = sci_notation(ts[n]) + r'$ \, \mathrm{cm}$'
            if args.output_file:
                if args.output_file[-3:] == 'mp4':
                    label = r'$ct = \, $' + sci_notation(ts[n]) + r'$ \, \mathrm{cm}$'
            ylabels_for_same_line.append(label)
        if args.legend_short:
            for i in xrange(1, len(ns_cut)-1):
                ylabels_for_same_line[i] = None
    elif args.legend_n:
        ylabels_for_same_line = [r'$n = %i$' % (ns_cut[0])]
        for n in ns_cut[1:]:
            label = r'$%i$' % (n)
            if args.output_file:
                if args.output_file[-3:] == 'mp4':
                    label = r'$n = %i$' % (n)
            ylabels_for_same_line.append(label)
        if args.legend_short:
            for i in xrange(1, len(ns_cut)-1):
                ylabels_for_same_line[i] = None
    else:
        ylabels_for_same_line = None

    if args.pad:
        if args.x_lims == None:
            x_min = min([min(xs) for xs in xss])
            x_max = max([max(xs) for xs in xss])
            dx = x_max-x_min
            args.x_lims = [x_min-.05*dx, x_max+.05*dx]

    #

    # Save movie, or do plot?
    made_movie = False
    if args.output_file:
        if args.output_file[-3:] == 'mp4':
            if not args.quiet:
                print 'Making movie from snapshots', ns_cut
            made_movie = True
            # Make the movie here!
            make_movie(xss, ysss, ns_cut, xlabel, ylabels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, watermark=watermark,
                       marker=marker, legend_loc=args.legend_loc, zero_line=args.zero_line, track=args.track, save_name=args.output_file, FPS=args.fps, find_all_shocks=args.find_shocks,
                       texts=ylabels_for_same_line, vlines=vlines)

    if not made_movie:
        if not args.quiet:
            print 'Plotting snapshots', ns_cut
        if args.multiple_cmaps:
            cmaps = args.multiple_cmaps
        else:
            cmaps = [args.cmap for yss in ysss]
        plot_data(xss, ysss, ns_cut, xlabel, ylabels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, watermark=watermark,
                  no_show=args.no_plot, marker=marker, legend_loc=args.legend_loc, legend_font_size=args.legend_font_size, zero_line=args.zero_line, separate=args.separate, track=args.track,
                  final_line_is_black=not args.colored_final_line, ylabels_for_same_line=ylabels_for_same_line, zorder_reverse=args.z_rev, cmaps=cmaps,
                  no_tight_layout=args.no_tight_layout, save_name=args.output_file, find_all_shocks=args.find_shocks, multiple_panels=args.multiple_panels, multiple_ylims=args.multiple_y_lims,
                  legend_labels_in_plot=args.legend_labels_in_plot, no_rotation_of_text=args.legend_labels_in_plot_no_rotation, vlines=vlines)

if __name__ == '__main__':
    main()
