 #!/usr/bin/env python

import os
import sys
import argparse

from math import pi, sqrt
from plot_snapshots import plot_data, make_movie
from plot_spectra import sci_notation, find_alpha, read_spc_file, convert_to_luminosity

#

def bisect(x, xs):
    # Finds bin where x fits inside xs (assuming monotonic rise of xs)
    l = 0
    h = len(xs)-1
    while h-l > 1:
        m = (h+l)/2
        if x > xs[m]:
            l = m
        else:
            h = m
    return l

def get_spec_at_obs_time(t, rs, dLdlnepsss, R_GW=None):
    # Selects the spectrum at a given observer time, where
    # the observer time zero point is given by the arrival of
    # a photon that propagates from the outermost r coordinate

    # If R_GW is given, zero point is instead given by arrival of GWs

    c = 2.99e10
    if R_GW:
        r = R_GW - c*t
        # print (R_GW-rs[-1])/c
    else:
        r = rs[-1] - c*t

    if r < rs[0] or r > rs[-1]:
        return [0. for dLdlneps in dLdlnepsss[0]]
    j = bisect(r, rs)
    dLdlnepss = dLdlnepsss[j]
    return dLdlnepss

def get_avg_energy_curve(epss, eps_bs, dLdlnepsss):
    Ls = get_light_curve(epss, eps_bs, dLdlnepsss, counts=False)
    Ns = get_light_curve(epss, eps_bs, dLdlnepsss, counts=True)
    E_avgs = [L/N for L, N in zip(Ls, Ns)]
    m_e = 9.1094e-28
    c = 2.998e+10
    E_avgs = [E/m_e/c/c for E in E_avgs]
    return E_avgs

def get_light_curve(epss, eps_bs, dLdlnepsss, eps_min=None, eps_max=None, counts=False):
    m_e = 9.1094e-28
    c = 2.998e+10

    if eps_min == None:
        eps_min = 0.
    if eps_max == None:
        eps_max = 1e60
    Ls = []
    Ns = []
    for dLdlnepss in dLdlnepsss:
        L = 0.
        N = 0.
        for i in xrange(len(epss)):
            eps = epss[i]
            if eps_min <= eps <= eps_max:
                deps = eps_bs[i+1]-eps_bs[i]
                L += dLdlnepss[i]*deps/eps

                E = eps*m_e*c*c
                N += dLdlnepss[i]*deps/eps/E
        Ls.append(L)
        Ns.append(N)

    if not counts:
        return Ls
    else:
        return Ns

def fix_energy_normalization(x_coordinate_dim):
    if x_coordinate_dim == '1':
        k = 1.
        label = r'$E/m_e c^2$'
        unit = ''
    elif x_coordinate_dim.lower() == 'hz':
        m_e = 9.1094e-28
        c = 2.998e+10
        h = 6.626e-27
        k = m_e*c*c/h
        label = r'$\nu \, \mathrm{[Hz]}$'
        unit = r'$\mathrm{Hz}$'
    elif x_coordinate_dim.lower() == 'erg':
        m_e = 9.1094e-28
        c = 2.998e+10
        k = m_e*c*c
        label = r'$E \, \mathrm{[erg]}$'
        unit = r'$\mathrm{erg}$'
    elif x_coordinate_dim.lower() == 'kev':
        k = 511.
        label = r'$E \, \mathrm{[keV]}$'
        unit = r'$\mathrm{keV}$'
    elif x_coordinate_dim.lower() == 'mev':
        k = .511
        label = r'$E \, \mathrm{[MeV]}$'
        unit = r'$\mathrm{MeV}$'
    return k, label, unit

def main():
    parser = argparse.ArgumentParser(description='a script that produces observed plots (and movies) from a spectrum snapshot file (.spc)')
    parser.add_argument('INPUT_SPC_FILE', help='the input spectrum snapshots file (.spc)')
    parser.add_argument('-o', '--output-file', help='the output pdf (for plot) or mp4 file (for movie)')
    parser.add_argument('-x', '--x-coordinate-dim', default='1', help='the dimensions on the x-axis (1, Hz, erg, keV, MeV) [default=1]')
    parser.add_argument('-i', '--integrate', action='store_true', help='plots the observed spectrum integrated over time (this is the default option, used if -t or --t-range are not specified)')
    parser.add_argument('-t', type=float, nargs='+', help='the observer time(s) to plot')
    parser.add_argument('--t-range', nargs=3, type=float, help='the observer time range to plot (order: t_min t_max n_times)')
    parser.add_argument('--t-offset', default=0, type=float, help='offset the observer time by subtracting this value')

    parser.add_argument('--r-gw', default=0, type=float, help='the radius of the gravitational waves at the time of the spc file')

    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    # parser.add_argument('--sample', action='store_true', help='plots a nice sample of snapshots (roughly 15, evenly spaced, including n = 0 and -1)')
    parser.add_argument('--separate', action='store_true', help='shifts each next line upwards by a factor 2, so that lines overlap less (for increased visibility)')
    parser.add_argument('--marker', action='store_true', help='plots markers')
    parser.add_argument('--preliminary', action='store_true', help='adds a "preliminary" watermark to the plot')
    parser.add_argument('--cmap', type=str, default='magma', help='the color map to use for the spectrum plot [default=magma]')
    parser.add_argument('--z-rev', action='store_true', help='reverses the z-order of the lines (i.e. which lines cover each other)')

    parser.add_argument('-tx', '--toggle-xlog', action='store_false', help='turn OFF log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_false', help='turn OFF log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--legend-loc', type=int, default=0, help='legend location [default=0, which turns off the legend]')
    parser.add_argument('--legend-font-size', type=int, default=24, help='legend font size [default=24]')
    parser.add_argument('--no-tight-layout', action='store_true', help="don't use tight_layout()")

    parser.add_argument('--light-curve', action='store_true', help='instead, plots the light curve of the spectrum')
    parser.add_argument('--clean-before', type=float, help='remove emission before this time')
    parser.add_argument('--lc-energy-range', nargs=2, type=float, help='limit the light curve energy integration to this energy range (same units as -x)')
    # parser.add_argument('--lc-multi-cut', type=float, help='plot three light curves: integrated below the cut (same units as -x), above the cut and total')
    parser.add_argument('--lc-multi-cut', type=float, help='plot two light curves: integrated below the cut (same units as -x) and above the cut')
    parser.add_argument('--lc-counts', action='store_true', help='plots counts per second instead of energy per second')

    parser.add_argument('--avg-energy-curve', action='store_true', help='instead, plots the average observed energy versus time')

    parser.add_argument('--sci-not', action='store_true', help='use scientific notation for the legend')
    parser.add_argument('--fps', type=int, default=0, help='movie fps [default makes movie time equal to observer time]')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    if args.cmap:
        cmaps = [args.cmap]

    # # Load the file
    # alpha = find_alpha(args.INPUT_SPC_FILE[:-3]+'mhd')
    # if alpha != 2:
    #     print 'ERROR: in order to compute the observed spectrum,'
    #     print '       the simulation geometry must be spherical'
    #     raise SystemExit

    # Loading the data
    m_bss, r_bss, epsss, dudlnepssss_all, n_max, eps_bs = read_spc_file(args.INPUT_SPC_FILE, args.n_max, return_eps_bs=True)
    rss = [[.5*(r_bs[i+1]+r_bs[i]) for i in xrange(len(r_bs)-1)] for r_bs in r_bss]
    dLdlnepssss = convert_to_luminosity(dudlnepssss_all, rss)
    # ylabel = r'$d L_\gamma / d\ln E \, \mathrm{[erg/s]}$'
    ylabel = r'$d L / d\ln E \, \mathrm{[erg/s]}$'
    # ylabel = r'$E (d L / d E) \, \mathrm{[erg/s]}$'

    # Picking the latest snapshot
    if not args.quiet:
        print 'File contains %i snapshots' % (len(m_bss))
    rs = rss[-1]
    dLdlnepsss = dLdlnepssss[-1]

    # c = 2.9979e10
    # # R_GW = args.r_gw
    # ct_i = 4.56837494e+11 - 4e11
    # ct_i = 4.52618312e+11 - 4e11
    # ct_sim = 2.44310728e+12
    # ct_beam = 2.47327265e+14
    # #ct_beam = 0.
    # R_GW = ct_i + ct_sim + ct_beam
    # #R_GW = ct_sim + ct_beam
    # print 't_i = %4.2f s' % (ct_i/c)
    # print 't_sim = %4.2f s' % (ct_sim/c)
    # print 't_beam = %4.2f s' % (ct_beam/c)
    # print 'rs[-1]/c = %4.2f s' % (rs[-1]/c)
    # # v = vss[-1][-1]
    # # Gamma = 1./sqrt(1. - v*v)
    # Gamma = 23.42
    # v = sqrt(1. - 1./Gamma/Gamma)
    # r = rs[-1]
    # t = r/v/c
    # print 't = %4.2f' % (t)
    
    # print 'Gamma = %4.2f' % (Gamma)
    # print 'r/(2 c Gamma^2) = %4.2f s' % (rs[-1]/2./c/Gamma/Gamma)
    # print '---'
    # print 'rs[-1] = %4.8e cm' % (rs[-1])
    # print 'R_GW = %4.8e cm' % (R_GW)
    # print 'dt = %4.2f s' % ((R_GW - rs[-1])/c)
    # raise SystemExit

    # Plot light curve?
    if args.light_curve or args.avg_energy_curve:
        if args.output_file:
            if args.output_file[-3:] == 'mp4':
                print "ERROR: can't make a movie of the light curve"
                raise SystemExit
        c = 2.9979e10

        #

        if args.toggle_xlog:
            t_min = (rs[1]-rs[0])/c
        else:
            t_min = 0.

        # t_obs(r) = (R_GW - r)/c

        t_max = (rs[-1]-rs[0])/c
        n_ts = 1000
        dt = (t_max-t_min)/(n_ts-1)
        ts = [t_min + i*dt for i in xrange(n_ts)]

        # dLdlnepsss_ob = [get_spec_at_obs_time(t, rs, dLdlnepsss) for t in ts]
        dLdlnepsss_ob = [get_spec_at_obs_time(t+args.t_offset, rs, dLdlnepsss, R_GW=args.r_gw) for t in ts]
        epsss = [epsss[0] for dLdlnepss_ob in dLdlnepsss_ob]

        #

        xlabel = r'$t_{obs} \, \mathrm{[s]}$'
        labels = [None]
        ns = [0]

        if args.lc_energy_range:
            eps_min = args.lc_energy_range[0]/fix_energy_normalization(args.x_coordinate_dim)[0]
            eps_max = args.lc_energy_range[1]/fix_energy_normalization(args.x_coordinate_dim)[0]
            Lss = [get_light_curve(epsss[0], eps_bs, dLdlnepsss_ob, eps_min=eps_min, eps_max=eps_max, counts=args.lc_counts)]
        elif args.lc_multi_cut:
            k, label, unit = fix_energy_normalization(args.x_coordinate_dim)
            eps_cut = args.lc_multi_cut/k

            # Lss = [get_light_curve(epsss[0], eps_bs, dLdlnepsss_ob, counts=args.lc_counts)]
            # Lss.append(get_light_curve(epsss[0], eps_bs, dLdlnepsss_ob, eps_min=eps_cut, eps_max=1e60, counts=args.lc_counts))
            # Lss.append(get_light_curve(epsss[0], eps_bs, dLdlnepsss_ob, eps_min=0., eps_max=eps_cut, counts=args.lc_counts))
            # labels = [r'$\mathrm{Full \, light \, curve}$',
            #           r'$E >%i \,$' % (args.lc_multi_cut)+unit,
            #           r'$E <%i \,$' % (args.lc_multi_cut)+unit]

            Lss = [get_light_curve(epsss[0], eps_bs, dLdlnepsss_ob, counts=args.lc_counts)]
            Lss = [get_light_curve(epsss[0], eps_bs, dLdlnepsss_ob, eps_min=eps_cut, eps_max=1e60, counts=args.lc_counts)]
            Lss.append(get_light_curve(epsss[0], eps_bs, dLdlnepsss_ob, eps_min=0., eps_max=eps_cut, counts=args.lc_counts))
            labels = [r'$E >%i \,$' % (args.lc_multi_cut)+unit,
                      r'$E <%i \,$' % (args.lc_multi_cut)+unit]

        else:
            Lss = [get_light_curve(epsss[0], eps_bs, dLdlnepsss_ob, counts=args.lc_counts)]
        tss = [ts for Ls in Lss]

        if args.clean_before:
            t_clean = args.clean_before
            for ts, Ls in zip(tss, Lss):
                for i in xrange(len(ts)):
                    if ts[i] < t_clean:
                        Ls[i] = 0.

        ns = [i for i in xrange(len(Lss))]
        if not args.quiet:
            print 'Plotting light curve'

        if not args.lc_counts:
            ylabel = r'$L \, \mathrm{[erg/s]}$'
        else:
            ylabel = r'$\dot{N}_\gamma \, \mathrm{[photons/s]}$'

        if args.avg_energy_curve:
            k, label, unit = fix_energy_normalization(args.x_coordinate_dim)
            E_avgs = get_avg_energy_curve(epsss[0], eps_bs, dLdlnepsss_ob)
            Lss = [[E_avg*k for E_avg in E_avgs]]
            ylabel = label

        plot_data(tss, [Lss], ns, xlabel, labels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True, watermark=None,
                  marker=None, separate=None, save_name=args.output_file, legend_loc=args.legend_loc, legend_font_size=args.legend_font_size, single_ylabel=ylabel,
                  final_line_is_black=False, ylabels_for_same_line=labels, no_tight_layout=args.no_tight_layout, zorder_reverse=args.z_rev)

        raise SystemExit

    # Determining the observer times
    will_integrate = 0
    if args.integrate:
        if args.output_file:
            if args.output_file[-3:] == 'mp4':
                print "ERROR: can't make a movie of the time-integrated spectrum"
                raise SystemExit
        will_integrate = 1
    elif args.t_range:
        t_min = args.t_range[0]
        t_max = args.t_range[1]
        n_ts = int(args.t_range[2])
        dt = (t_max-t_min)/(n_ts-1)
        ts = [t_min + i*dt for i in xrange(n_ts)]
        print ts
    elif args.t:
        ts = args.t
    else:
        will_integrate = 1

    if will_integrate:
        ylabel = r'$d E_\gamma / d\ln E \, \mathrm{[erg]}$'
        # ylabel = r'$E (d E_\gamma / d E) \, \mathrm{[erg]}$'
        c = 3e10
        t_min = 0
        t_max = (rs[-1]-rs[0])/c
        n_ts = 400
        dt = (t_max-t_min)/(n_ts-1)
        ts = [t_min + i*dt for i in xrange(n_ts)]

    if args.sci_not:
        labels = [r'$t = \, $' + sci_notation(t) + r'$\, \mathrm{s}$' for t in ts]
    else:
        # labels = [r'$t = %2.1f \mathrm{s}$' % (t) for t in ts]
        # labels = [r'$%2.1f \, \mathrm{s}$' % (t) for t in ts]
        labels = [r'$%2.2f \, \mathrm{s}$' % (t) for t in ts]

    # Make the observer spectra
    dLdlnepsss_ob = [get_spec_at_obs_time(t+args.t_offset, rs, dLdlnepsss) for t in ts]
    epsss = [epsss[0] for dLdlnepss_ob in dLdlnepsss_ob]

    if will_integrate:
        dt = ts[1]-ts[0]
        dEdlnepss_ob = [0. for eps in epsss[0]]
        for i, dLdlnepss_ob in enumerate(dLdlnepsss_ob):
            for j in xrange(len(dEdlnepss_ob)):
                dEdlnepss_ob[j] += dLdlnepss_ob[j]*dt
        dLdlnepsss_ob = [dEdlnepss_ob]
        ts = [0]

    # Watermark
    if args.preliminary:
        watermark = r'$\mathrm{PRELIMINARY}$'
    else:
        watermark = None

    # Marker
    if args.marker:
        marker = '.'
    else:
        marker = None

    # Convert energies?
    if args.x_coordinate_dim == '1':
        xlabel = r'$E/m_e c^2$'
    elif args.x_coordinate_dim.lower() == 'hz':
        m_e = 9.1094e-28
        c = 2.998e+10
        h = 6.626e-27
        k = m_e*c*c/h
        epsss = [[eps*k for eps in epss] for epss in epsss]
        xlabel = r'$\nu \, \mathrm{[Hz]}$'
    elif args.x_coordinate_dim.lower() == 'erg':
        m_e = 9.1094e-28
        c = 2.998e+10
        k = m_e*c*c
        epsss = [[eps*k for eps in epss] for epss in epsss]
        xlabel = r'$E \, \mathrm{[erg]}$'
    elif args.x_coordinate_dim.lower() == 'kev':
        k = 511.
        epsss = [[eps*k for eps in epss] for epss in epsss]
        xlabel = r'$E \, \mathrm{[keV]}$'
    elif args.x_coordinate_dim.lower() == 'mev':
        k = .511
        epsss = [[eps*k for eps in epss] for epss in epsss]
        xlabel = r'$E \, \mathrm{[MeV]}$'

    # Fix FPS
    if args.fps:
        fps = args.fps
    else:
        if len(ts) > 1:
            dt = ts[1]-ts[0]
        else:
            dt = 1.
        fps = 1./dt

    # Save movie, or do plot?
    made_movie = False
    if args.output_file:
        if args.output_file[-3:] == 'mp4':
            if not args.quiet:
                print 'Making movie of observed spectra'
            made_movie = True
            # Make the movie here!
            if args.legend_loc != 0:
                legend_loc = args.legend_loc
                ylabels_for_same_line = labels
            else:
                legend_loc = 1
                ylabels_for_same_line = None
            labels = [None for label in labels]
            ns = [i for i in xrange(len(dLdlnepsss_ob))]
            make_movie(epsss, [dLdlnepsss_ob], ns, xlabel, [None for n in ns], xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True,
                       watermark=watermark, marker=marker, save_name=args.output_file, legend_loc=args.legend_loc, FPS=fps, single_ylabel=ylabel, texts=ylabels_for_same_line, text_loc=legend_loc)

    # Plot spectra
    if not made_movie:
        if not args.quiet:
            print 'Plotting observed spectra'

        ns = [i for i in xrange(len(dLdlnepsss_ob))]
        if args.legend_loc != 0:
            legend_loc = args.legend_loc
            ylabels_for_same_line = labels
        else:
            legend_loc = 1
            ylabels_for_same_line = None
            labels = [None for label in labels]
        plot_data(epsss, [dLdlnepsss_ob], ns, xlabel, labels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True, watermark=watermark,
                  marker=marker, separate=args.separate, save_name=args.output_file, legend_loc=legend_loc, legend_font_size=args.legend_font_size, single_ylabel=ylabel, final_line_is_black=False,
                  ylabels_for_same_line=ylabels_for_same_line, cmaps=cmaps, zorder_reverse=args.z_rev)

if __name__ == '__main__':
    main()
