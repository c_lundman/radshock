#!/usr/bin/env python

import argparse
from numpy import sqrt, log10, linspace, logspace
from generate_state_files import bisect

def load_rsf_file(filename):
    ws = []
    rs = []
    mus = []
    epss = []
    t = 0.

    # Load rsf
    with open(filename, 'r') as f:
        for line in f.readlines():
            s_line = line.split()
            if len(s_line) == 2:
                if s_line[0] == 'eps_min':
                    eps_min = float(s_line[1])
                if s_line[0] == 'eps_max':
                    eps_max = float(s_line[1])
                if s_line[0] == 'n_eps_bs':
                    n_eps_bs = int(s_line[1])
                if s_line[0] == 't':
                    t = float(s_line[1])
                if s_line[0] == 'alpha':
                    alpha = int(s_line[1])
            if len(s_line) == 4:
                rs.append(float(s_line[0]))
                mus.append(float(s_line[1]))
                epss.append(float(s_line[2]))
                ws.append(float(s_line[3]))

    return ws, rs, mus, epss, t, n_eps_bs, eps_min, eps_max, alpha

def make_spc_data(ws, rs, mus, epss, t, n_eps_bs, eps_min, eps_max, n_bs, Dr):
    eps_bs = logspace(log10(eps_min), log10(eps_max), n_eps_bs)

    # m_bs = [0. for i in xrange(n_bs)]
    # r_max = max(rs)
    # r_min = r_max - Dr
    # r_bs = linspace(r_min, r_max, n_bs)

    # Add ghosts
    r_max = max(rs)
    r_min = r_max - Dr
    r_bs = list(linspace(r_min, r_max, n_bs))
    dr = r_bs[1]-r_bs[0]
    r_bs.insert(0, r_bs[0]-dr)
    # r_bs.insert(0, r_bs[0]-dr)
    # r_bs.append(r_bs[-1]+dr)
    r_bs.append(r_bs[-1]+dr)
    # n_bs += 4
    n_bs += 2
    m_bs = [0. for i in xrange(n_bs)]

    m_e = 9.1093897e-28

    # Filling the spectrum
    dudlnepsss = [0. for i in xrange((n_bs-1)*(n_eps_bs-1))]
    for w, r, mu, eps in zip(ws, rs, mus, epss):
        # Find spatial bin j, spectral bin k
        j = bisect(r_bs, r)
        k = bisect(eps_bs, eps)
        l = j*(n_eps_bs-1) + k

        # Radial stuff
        Dr = r_bs[j+1]-r_bs[j]
        r_avg = .5*(r_bs[j+1]+r_bs[j])
        A = r_avg*r_avg

        # Energy stuff
        deps = eps_bs[k+1]-eps_bs[k]
        eps_avg = .5*(eps_bs[k+1]+eps_bs[k])
        dlneps = deps/eps_avg

        # Add to spectrum
        dudlnepsss[l] += eps*w*m_e/dlneps/A/Dr

    return eps_bs, m_bs, r_bs, dudlnepsss

def save_spc_file(eps_bs, m_bs, r_bs, dudlnepsss, filename):
    # Make strings of the information
    quantities = [eps_bs, m_bs, r_bs, dudlnepsss]
    strings = []
    for quantity in quantities:
        string = ''
        for q in quantity:
            string += ' %18.12e' % (q)
        strings.append(string)

    # Write strings to file
    with open(filename, 'w') as f:
        for string in strings:
            f.write(string+'\n')

def beam(rs, mus, t, f=100):
    t_1 = f*max(rs)
    dt = t_1-t
    r_1s = []
    mu_1s = []
    for r, mu in zip(rs, mus):
        r_1 = sqrt((dt+r*mu)*(dt+r*mu) + r*r*(1.-mu*mu))
        mu_1 = (dt+r*mu)/r_1
        r_1s.append(r_1)
        mu_1s.append(mu_1)

    return r_1s, mu_1s, t_1

def main():
    parser = argparse.ArgumentParser(description='a script that takes a radiation state file (.rsf) and makes a spectrum file (.spc) with a single snapshot')
    parser.add_argument('INPUT_RSF_FILE', help='the input radiation state file (.rsf)')
    parser.add_argument('OUTPUT_SPC_FILE', help='the output spectrum file (.spc)')
    
    parser.add_argument('--r-bins', type=int, default=100, help='the number of radial bins [default=100]')
    parser.add_argument('--eps-bins', type=int, help='the number of energy bins [default uses the number from the rsf file]')
    parser.add_argument('--eps-min', type=float, help='the minimum spectrum energy [default uses the number from the rsf file]')
    parser.add_argument('--eps-max', type=float, help='the maximum spectrum energy [default uses the number from the rsf file]')

    parser.add_argument('--print-r-gw', action='store_true', help='print the final gravitational wave radius')
    parser.add_argument('--beam', action='store_true', help='propagate photons to a radius that is 100 times larger than largest r of any photon, so that they all become radially beamed (for computing observed properties later)')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    # Load data from the rsf file
    ws, rs, mus, epss, t, n_eps_bs, eps_min, eps_max, alpha = load_rsf_file(args.INPUT_RSF_FILE)
    Dr = max(rs)-min(rs)

    # New values?
    if args.eps_bins:
        n_eps_bs = args.eps_bins
    if args.eps_min:
        eps_min = args.eps_min
    if args.eps_max:
        eps_max = args.eps_max

    # Beam photons?
    if args.beam:
        rs, mus, t_1 = beam(rs, mus, t, f=100.)
        t_beam = t_1 - t
        if args.print_r_gw and not args.quiet:
            # print 'ct_beam = %4.8e cm' % (t_beam)
            # print 'ct_final = %4.8e cm' % (t_1)
            print 'r_gw = %4.8e cm' % (t_1)
        t = t_1
        Dr *= 3.

    # Make data for the spc file
    eps_bs, m_bs, r_bs, dudlnepsss = make_spc_data(ws, rs, mus, epss, t, n_eps_bs, eps_min, eps_max, args.r_bins, Dr)

    # Save the spc file
    save_spc_file(eps_bs, m_bs, r_bs, dudlnepsss, args.OUTPUT_SPC_FILE)

if __name__ == '__main__':
    main()
