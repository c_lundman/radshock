 #!/usr/bin/env python

import os
import sys
import argparse

from math import pi, sqrt
from plot_snapshots import get_colors, import_mplstyle, plot_data, make_movie, sci_notation, read_mhd_file

# def sci_notation(num, decimal_digits=1, precision=None, exponent=None, remove_leading_ones=False):

#     # Returns a string representation of the scientific
#     # notation of the given number formatted for use with
#     # LaTeX or Mathtext, with specified number of significant
#     # decimal digits and precision (number of decimal digits
#     # to show). The exponent to be used can also be specified
#     # explicitly.

#     from numpy import round, abs, floor, log10

#     if num == 0.:
#         exponent = 0
#     if exponent == None:
#         exponent = int(floor(log10(abs(num))))
#     coeff = round(num / float(10**exponent), decimal_digits)
#     if not precision:
#         precision = decimal_digits
#     if remove_leading_ones and coeff == 1.:
#         return r"$10^{{{1:d}}}$".format(coeff, exponent, precision)
#     else:
#         return r"${0:.{2}f}\times\,10^{{{1:d}}}$".format(coeff, exponent, precision)

def print_shape(A):
    iterable_types = [type([]), type(())]
    shape = "shape:"
    while type(A) in iterable_types:
        shape += " %i," % (len(A))
        A = A[0]
    print shape[:-1]

def read_spc_file(input_file, n_max=None, return_eps_bs=False):
    if input_file[-3:] != 'spc':
        print '  error: wrong file format!'
        raise SystemExit
    m_bss = []
    r_bss = []
    dudlnepssss = []
    with open(input_file, 'r') as f:
        line = f.readline()
        eps_bs = [float(x) for x in line.split()]
        n_eps_bs = len(eps_bs)

        n = 0
        counter = 0
        for line in f.readlines():
            if counter == 0:
                m_bss.append([float(x) for x in line.split()])
            elif counter == 1:
                r_bss.append([float(x) for x in line.split()])
            elif counter == 2:
                N = len(line.split()) / (n_eps_bs-1)

                dudlnepsss = []
                for j in xrange(N):

                    dudlnepsss.append([float(x) for x in line.split()[j*(n_eps_bs-1) : (j+1)*(n_eps_bs-1)]])
                dudlnepssss.append(dudlnepsss)

            counter += 1
            if counter > 2:
                counter = 0
                n += 1

            if n_max:
                if n == n_max:
                    break

    epss = [sqrt(eps_bs[i+1]*eps_bs[i]) for i in xrange(len(eps_bs)-1)]
    epsss = [epss for dudlnepsss in dudlnepssss]
    n_max = len(dudlnepssss)
    if not return_eps_bs:
        return m_bss, r_bss, epsss, dudlnepssss, n_max
    else:
        return m_bss, r_bss, epsss, dudlnepssss, n_max, eps_bs

def read_ispc_file(input_file, n_max=None):
    if input_file[-4:] != 'ispc':
        print '  error: wrong file format!'
        raise SystemExit
    dudlnepsss = []
    with open(input_file, 'r') as f:
        line = f.readline()
        eps_bs = [float(x) for x in line.split()]

        n = 0
        for line in f.readlines():
            dudlnepsss.append([float(x) for x in line.split()])
            n += 1
            if n_max:
                if n == n_max:
                    break
    epss = [sqrt(eps_bs[i+1]*eps_bs[i]) for i in xrange(len(eps_bs)-1)]
    epsss = [epss for dudlnepss in dudlnepsss]
    n_max = len(dudlnepsss)
    return epsss, dudlnepsss, n_max

def find_alpha(input_file):
    with open(input_file, 'r') as f:
        line = f.readline()
        line = f.readline()
        line = f.readline()
        line = f.readline()
        line = f.readline()
        alpha = int(line.split()[2])
    return alpha

def get_spectra_ylims(ysss):
    # Find max value
    y_max = 0.
    for yss in ysss:
        for ys in yss:
            y_max_p = max(ys)
            if y_max_p > y_max:
                y_max = y_max_p

    y_top = 3.*y_max
    y_lims = [1e-4*y_top, y_top]
    return y_lims

def get_spectra_xlims(xss, ysss):
    # Find largest bin number with data
    x_max = 0
    for yss in ysss:
        for xs, ys in zip(xss, yss):
            for x, y in zip(xs, ys):
                if y > 0:
                    if x > x_max:
                        x_max = x

    x_top = 10*x_max
    x_lims = [1e-5*x_top, x_top]
    return x_lims

def convert_to_luminosity(dudlnepssss, rss):
    c = 3e10
    dLdlnepssss = []
    for rs, dudlnepsss in zip(rss, dudlnepssss):
        dLdlnepsss = [] # This is dL/dlneps [erg/s]
        for r, dudlnepss in zip(rs, dudlnepsss):
            dLdlnepss = []
            for dudlneps in dudlnepss:
                dLdlneps = 4.*pi*r*r*dudlneps*c*c*c
                dLdlnepss.append(dLdlneps)
            dLdlnepsss.append(dLdlnepss)
        dLdlnepssss.append(dLdlnepsss)
    return dLdlnepssss

# def find_spec_within_range(x_min,  x_max, x_bss, ysss):
#     yss_range = []
#     bins_tot = 0
#     binss = []
#     for x_bs, yss in zip(x_bss, ysss):
#         ys_range = [0. for y in yss[0]]
#         Dx = 0.
#         bins = 0
#         for j in xrange(len(x_bs)-1):
#             x_b_l = x_bs[j]
#             x_b_r = x_bs[j+1]

#             if x_b_l <= x_max and x_b_r > x_min:
#                 # Pick bin
#                 bins += 1
#                 dx = x_b_r-x_b_l
#                 Dx += dx
#                 ys = yss[j]
#                 for i in xrange(len(ys)):
#                     ys_range[i] += ys[i]*dx
#                 # print j, x_b_l, x_b_r

#         # print
#         if Dx:
#             ys_range = [y/Dx for y in ys_range]
#         bins_tot += bins
#         binss.append(bins)
#         yss_range.append(ys_range)

#     if not bins_tot:
#         print '  ERROR: No spectra found within the given range.'
#         print '         Exiting...'
#         raise SystemExit
#     return [yss_range], binss

def find_spec_within_range(x_min,  x_max, x_bss, x_bss_weight, ysss):
    # The spectra are weighted by x_bss_weight (= r_bss typically, since the spectra are "densities")
    yss_range = []
    bins_tot = 0
    binss = []
    js = []
    for x_bs, x_bs_weight, yss in zip(x_bss, x_bss_weight, ysss):
        ys_range = [0. for y in yss[0]]
        Dx = 0.
        bins = 0
        for j in xrange(len(x_bs)-1):
            x_b_l = x_bs[j]
            x_b_r = x_bs[j+1]

            pick_bin = 0
            if j == len(x_bs)-2:
                if x_b_l <= x_max and x_b_r >= x_min:
                    pick_bin = 1
            else:
                if x_b_l <= x_max and x_b_r > x_min:
                    pick_bin = 1
            if pick_bin:
                # Pick bin
                bins += 1
                if j not in js:
                    js.append(j)
                # dx = x_b_r-x_b_l
                dx = x_bs_weight[j+1]-x_bs_weight[j]
                Dx += dx
                ys = yss[j]
                for i in xrange(len(ys)):
                    ys_range[i] += ys[i]*dx

        # print
        if Dx:
            ys_range = [y/Dx for y in ys_range]
        bins_tot += bins
        binss.append(bins)
        yss_range.append(ys_range)

    # if not bins_tot:
    #     print '  ERROR: No spectra found within the given range.'
    #     print '         Exiting...'
    #     raise SystemExit
    return [yss_range], binss

#

def main():
    parser = argparse.ArgumentParser(description='a script that produces plots (and movies) from a spectrum snapshot file (.spc)')
    parser.add_argument('INPUT_SPC_FILE', help='the input spectrum snapshots file (.spc)')
    parser.add_argument('-o', '--output-file', help='the output pdf (for plot) or mp4 file (for movie)')
    parser.add_argument('-x', '--x-coordinate-dim', default='1', help='the dimensions on the x-axis (1, Hz, erg, keV, MeV) [default=1]')
    parser.add_argument('-i', '--integrate', action='store_true', help='plots the spectrum integrated over the spatial coordinate')
    parser.add_argument('-n', type=int, nargs='+', help='the temporal snapshot(s) to plot [default=-1]')

    parser.add_argument('-j', type=int, nargs='+', help='the spatial index(s) to plot [default=0]')
    parser.add_argument('-r', type=float, nargs='+', help='the radial coordinate(s) to plot')
    parser.add_argument('-m', type=float, nargs='+', help='the mass coordinate(s) to plot')
    parser.add_argument('--r-between', nargs=2, type=float, help='plots time evolution of the spectrum within this integrated radius range')
    parser.add_argument('--m-between', nargs=2, type=float, help='plots time evolution of the spectrum within this integrated mass range')
    parser.add_argument('--r-range', nargs=3, type=float, help='give r_min, r_max, N to make N locations within r_min, r_max, and plot the time evolution of the spectrum at these locations')
    parser.add_argument('--m-range', nargs=3, type=float, help='give m_min, m_max, N to make N locations within m_min, m_max, and plot the time evolution of the spectrum at these locations')
    parser.add_argument('--reverse', action='store_true', help='reverses the mass bins, measuring them from the opposite end')
    parser.add_argument('--print-locations', action='store_true', help='print the spatial mass and radius coordinates for each j and n')
    parser.add_argument('--print-spectra', action='store_true', help='print the x and y-coordinates for the spectra (to e.g. copy into text file and send to others)')
    parser.add_argument('--v-shift', type=float, default=0, help='shift the r-coordinate by this speed (r -> r + v*(t-t_i))')

    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    parser.add_argument('--sample', action='store_true', help='plots a nice sample of snapshots (roughly 15, evenly spaced, including n = 0 and -1)')
    parser.add_argument('--all', action='store_true', help='ignores --sample and plots all snapshots')
    parser.add_argument('--plot-range', nargs=2, type=int, help='plots all snapshots in this range')
    parser.add_argument('--plot-after', type=int, help='plots all snapshots after this snapshot')
    parser.add_argument('--plot-before', type=int, help='plots all snapshots before this snapshot')

    parser.add_argument('--separate', action='store_true', help='shifts each next line upwards by a factor 2, so that lines overlap less (for increased visibility)')
    parser.add_argument('--luminosity', action='store_true', help='converts spectrum to a spherical, spectral luminosity (assumes v ~ c)')
    parser.add_argument('--marker', action='store_true', help='plots markers')
    parser.add_argument('--preliminary', action='store_true', help='adds a "preliminary" watermark to the plot')

    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--legend-loc', type=int, help='legend location [default=1]')
    parser.add_argument('--legend-times', action='store_true', help='show snapshot time (t) in the legend (only for single VAR)')
    parser.add_argument('--legend-ctimes', action='store_true', help='show snapshot time (ct) in the legend (only for single VAR)')
    parser.add_argument('--legend-n', action='store_true', help='show snapshot number (n) in the legend (only for single VAR)')
    parser.add_argument('--legend-short', action='store_true', help='only show first and last labels in the legend')
    parser.add_argument('--legend-off', action='store_true', help='no legend')
    parser.add_argument('--fps', type=int, default=30, help='movie fps [default=30]')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    # # Check number of spatial locations
    # if args.j:
    #     if len(args.j) > 3:
    #         print '  ERROR: Too many spatial locations passed, can handle at most three (3).'
    #         print '         Exiting...'
    #         raise SystemExit

    # Load the file
    try:
        alpha = find_alpha(args.INPUT_SPC_FILE[:-3]+'mhd')
    except:
        alpha = 2
    if not args.integrate:
        m_bss, r_bss, epsss, dudlnepssss_all, n_max = read_spc_file(args.INPUT_SPC_FILE, args.n_max)

        if args.v_shift:
            _, ts, _, _, _, _, _, _, _, _ = read_mhd_file(args.INPUT_SPC_FILE[:-4]+'.mhd', args.n_max, False, 1)
            v = args.v_shift
            t_i = ts[0]
            r_bss = [[r_b - v*(t - t_i) for r_b in r_bs] for r_bs, t in zip(r_bss, ts)]

            # if len(ts) != len(m_bss):
            #     print 'WARNING: different number of snapshots in the .mhd and .spc files! (%i vs %i)' % (len(ts), len(m_bss))
            #     print '         (use --n-max to limit number of loaded snapshots)'

            if len(ts) < len(m_bss):
                n_max = len(ts)
                m_bss = m_bss[:n_max]
                r_bss = r_bss[:n_max]
                epsss = epsss[:n_max]
                dudlnepssss_all = dudlnepssss_all[:n_max]

        mss = [[.5*(m_bs[i+1]+m_bs[i]) for i in xrange(len(m_bs)-1)] for m_bs in m_bss]
        rss = [[.5*(r_bs[i+1]+r_bs[i]) for i in xrange(len(r_bs)-1)] for r_bs in r_bss]
        if alpha == 2:
            m_bss = [[4.*pi*m for m in m_bs] for m_bs in m_bss]
            mss = [[4.*pi*m for m in ms] for ms in mss]

        if args.luminosity:
            dudlnepssss_all = convert_to_luminosity(dudlnepssss_all, rss)
            ylabel = r'$d L_\gamma / d\ln E \, \mathrm{[erg/s]}$'
        else:
            ylabel = r'$d u_\gamma / d\ln E \, \mathrm{[erg/cm^3]}$'

        if args.r_between:
            js = None
            labels = [None]
            dudlnepssss, binss = find_spec_within_range(args.r_between[0], args.r_between[1], r_bss, r_bss, dudlnepssss_all)

        elif args.m_between:

            js = None
            labels = [None]
            m_between = args.m_between
            if args.reverse:
                M_o = m_bss[0][-1]
                m_between = M_o-m_between[1], M_o-m_between[0]
            dudlnepssss, binss = find_spec_within_range(m_between[0], m_between[1], m_bss, r_bss, dudlnepssss_all)

        elif args.r:
            js = None
            # labels = [r'$r = %2.1f \, \mathrm{[cm]}$' % (r) for r in args.r]
            labels = [r'$r = \, $' + sci_notation(r) + r'$\, \mathrm{cm}$' for r in args.r]
            bins = [1 for r_bs in r_bss]
            dudlnepssss = [find_spec_within_range(r, r, r_bss, r_bss, dudlnepssss_all)[0][0] for r in args.r]

        elif args.m or args.m_range:
            if args.m_range:
                m_min, m_max, N_m = args.m_range
                N_m = int(N_m)
                dm = (m_max-m_min)/float(N_m)
                ms = [m_min + (i+.5)*dm for i in xrange(N_m)]
                m_ranges = [(m_min + i*dm, m_min + (i+1)*dm) for i in xrange(N_m)]
            else:
                ms = args.m
                m_ranges = [(m, m) for m in ms]

            js = None
            if alpha == 0:
                labels = [r'$m = \, $' + sci_notation(m) + r'$\, \mathrm{g/cm^2}$' for m in ms]
            elif alpha == 2:
                labels = [r'$m = \, $' + sci_notation(m) + r'$\, \mathrm{g}$' for m in ms]
            bins = [1 for m_bs in m_bss]
            if args.reverse:
                M_o = m_bss[0][-1]
                ms = [M_o-m for m in ms]
                m_ranges = [(M_o-m_range[0], M_o-m_range[1]) for m_range in m_ranges]
            # dudlnepssss = [find_spec_within_range(m, m, m_bss, r_bss, dudlnepssss_all)[0][0] for m in ms]
            dudlnepssss = [find_spec_within_range(m_range[0], m_range[1], m_bss, r_bss, dudlnepssss_all)[0][0] for m_range in m_ranges]

        elif args.r or args.r_range:
            if args.r_range:
                r_min, r_max, N_r = args.r_range
                N_r = int(N_r)
                dr = (r_max-r_min)/float(N_r)
                rs = [r_min + (i+.5)*dr for i in xrange(N_r)]
                r_ranges = [(r_min + i*dr, r_min + (i+1)*dr) for i in xrange(N_r)]
            else:
                rs = args.r
                r_ranges = [(r, r) for r in rs]

            js = None
            labels = [r'$r = \, $' + sci_notation(r) + r'$\, \mathrm{cm}$' for r in rs]
            bins = [1 for r_bs in r_bss]
            if args.reverse:
                R_o = r_bss[0][-1]
                rs = [R_o-r for r in rs]
                r_ranges = [(R_o-r_range[0], R_o-r_range[1]) for r_range in r_ranges]
            # dudlnepssss = [find_spec_within_range(m, m, m_bss, r_bss, dudlnepssss_all)[0][0] for m in ms]
            dudlnepssss = [find_spec_within_range(r_range[0], r_range[1], r_bss, r_bss, dudlnepssss_all)[0][0] for r_range in r_ranges]

        else:
            # Pick locations
            if args.j == None:
                js = [0]
            else:
                js = args.j

            # Picking the spectra to plot
            dudlnepssss = [[dudlnepsss[j] for dudlnepsss in dudlnepssss_all] for j in js]
            labels = [r'$j=%i$' % (j) for j in js]

    else:
        epsss, dudlnepsss, n_max = read_ispc_file(args.INPUT_SPC_FILE[:-3]+'ispc', args.n_max)
        dudlnepssss = [dudlnepsss]
        labels = [None]
        js = None
        ylabel = r'$d u_\gamma / d\ln E \, \mathrm{[erg/cm^3]} \, \mathrm{(integrated)}$'
        mss = [[None]]
        rss = [[None]]

        if args.luminosity:
            m_bss, r_bss, epsss, dudlnepssss_all, n_max = read_spc_file(args.INPUT_SPC_FILE, args.n_max)
            rss = [[.5*(r_bs[i+1]+r_bs[i]) for i in xrange(len(r_bs)-1)] for r_bs in r_bss]
            rs_new = [.5*(rs[1]+rs[0]) for rs in rss]
            dudlnepssss = convert_to_luminosity(dudlnepssss, [rs_new])
            ylabel = r'$d L_\gamma / d\ln E \, \mathrm{[erg/s]}$'
        # else:
        #     ylabel = r'$d u_\gamma / d\ln E \, \mathrm{[erg/cm^3]}$'

    # Determining snapshot numbers
    if args.n == None:
        ns = [-1]
    else:
        ns = args.n

    if not args.all:
        if args.plot_range != None:
            ns = range(args.plot_range[0], args.plot_range[1]+1)
        elif args.plot_before != None:
            if args.plot_after != None:
                ns = range(args.plot_after+1, args.plot_before)
            else:
                ns = range(args.plot_before)
        elif args.plot_after != None:
            ns = range(args.plot_after+1, n_max)
        elif args.sample and args.n == None:
            step = max(1, n_max/10) # show ~10 snapshots, plus final snapshot
            ns = range(0, n_max-1, step) + [n_max-1]

        ns_cut = [n for n in ns if n < n_max]
        if len(ns_cut) < len(ns):
            print '  WARNING: Cut away final value(s) from -n (max for this file is %i)' % (len(n_max)-1)
    else:
        ns_cut = range(n_max)

    # Print the locations?
    if args.print_locations:

        print 'mass coordinates:'
        for j in js:
            string_m = 'r[j=%4i] = ' % (j)
            for n in ns_cut:
                string_m += '%3.3e ' % (mss[n][j])
            print string_m

        print 'radial coordinates:'
        for j in js:
            string_r = 'r[j=%4i] = ' % (j)
            for n in ns_cut:
                string_r += '%3.3e ' % (rss[n][j])
            print string_r

    # Watermark
    if args.preliminary:
        watermark = r'$\mathrm{PRELIMINARY}$'
    else:
        watermark = None

    # Marker
    if args.marker:
        marker = '.'
    else:
        marker = None

    # Convert energies?
    if args.x_coordinate_dim == '1':
        xlabel = r'$E/m_e c^2$'
    elif args.x_coordinate_dim.lower() == 'hz':
        m_e = 9.1094e-28
        c = 2.998e+10
        h = 6.626e-27
        k = m_e*c*c/h
        epsss = [[eps*k for eps in epss] for epss in epsss]
        xlabel = r'$\nu \, \mathrm{[Hz]}$'
    elif args.x_coordinate_dim.lower() == 'erg':
        m_e = 9.1094e-28
        c = 2.998e+10
        k = m_e*c*c
        epsss = [[eps*k for eps in epss] for epss in epsss]
        xlabel = r'$E \, \mathrm{[erg]}$'
    elif args.x_coordinate_dim.lower() == 'kev':
        k = 511.
        epsss = [[eps*k for eps in epss] for epss in epsss]
        xlabel = r'$E \, \mathrm{[keV]}$'
    elif args.x_coordinate_dim.lower() == 'mev':
        k = .511
        epsss = [[eps*k for eps in epss] for epss in epsss]
        xlabel = r'$E \, \mathrm{[MeV]}$'

    ylabels_for_same_line = [None]
    if args.legend_times:
        c = 2.99e10
        ylabels_for_same_line = [r'$t = %2.f \, \mathrm{s}$' % (ts[ns_cut[0]]/c)]
        for n in ns_cut[1:]:
            label = r'$%2.f \, \mathrm{s}$' % (ts[n]/c)
            if args.output_file:
                if args.output_file[-3:] == 'mp4':
                    label = r'$t = %2.f \, \mathrm{s}$' % (ts[n]/c)
            ylabels_for_same_line.append(label)
        if args.legend_short:
            for i in xrange(1, len(ns_cut)-1):
                ylabels_for_same_line[i] = None
        # labels = ylabels_for_same_line
    elif args.legend_ctimes:
        ylabels_for_same_line = [r'$ct = \, $' + sci_notation(ts[ns_cut[0]]) + r'$ \, \mathrm{cm}$']
        for n in ns_cut[1:]:
            label = sci_notation(ts[n]) + r'$ \, \mathrm{cm}$'
            if args.output_file:
                if args.output_file[-3:] == 'mp4':
                    label = r'$ct = \, $' + sci_notation(ts[n]) + r'$ \, \mathrm{cm}$'
            ylabels_for_same_line.append(label)
        if args.legend_short:
            for i in xrange(1, len(ns_cut)-1):
                ylabels_for_same_line[i] = None
        # labels = ylabels_for_same_line
    elif args.legend_n:
        ylabels_for_same_line = [r'$n = %i$' % (ns_cut[0])]
        for n in ns_cut[1:]:
            label = r'$%i$' % (n)
            if args.output_file:
                if args.output_file[-3:] == 'mp4':
                    label = r'$n = %i$' % (n)
            ylabels_for_same_line.append(label)
        if args.legend_short:
            for i in xrange(1, len(ns_cut)-1):
                ylabels_for_same_line[i] = None
        # labels = ylabels_for_same_line
    # else:
    #     ylabels_for_same_line = None

    if args.legend_short:
        for i in xrange(1, len(labels)-1):
            labels[i] = None
    if args.legend_off:
        labels = [None for label in labels]

    if not args.y_lims:
        args.y_lims = get_spectra_ylims(dudlnepssss)
    if not args.x_lims:
        args.x_lims = get_spectra_xlims(epsss, dudlnepssss)

    # Save movie, or do plot?
    made_movie = False

    # if args.output_file and args.print_spectra:
    if args.output_file:
        if args.output_file[-3:] == 'txt':
            with open(args.output_file, 'w') as f:
                for dudlnepsss in dudlnepssss:
                    for n in ns_cut:
                        # print '        eps      nuFnu'
                        for x, y in zip(epsss[n], dudlnepsss[n]):
                            f.write(' %6.4e %6.4e\n' % (x, y))
                            # print ' %6.4e %6.4e' % (x, y)
                            # print ''
            raise SystemExit

    if args.output_file and not args.print_spectra:
        if args.output_file[-3:] == 'mp4':
            if not args.quiet:
                if args.r_between and not args.integrate:
                    print 'Making movie snapshots', ns_cut, '| r-between, r-bins per snapshot:', binss
                elif args.m_between and not args.integrate:
                    print 'Making movie snapshots', ns_cut, '| m-between, m-bins per snapshot:', binss
                elif not js:
                    print 'Making movie from snapshots', ns_cut
                else:
                    print 'Making movie from snapshots', ns_cut, ', spatial bins', js
            made_movie = True

            # # Make the movie here!
            # make_movie(epsss, dudlnepssss, ns_cut, xlabel, labels, xlog=True, ylog=True, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True, watermark=watermark,
            #            marker=marker, save_name=args.output_file, legend_loc=args.legend_loc, FPS=args.fps, single_ylabel=ylabel, texts=ylabels_for_same_line)
            make_movie(epsss, dudlnepssss, ns_cut, xlabel, labels, xlog=True, ylog=True, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True, watermark=watermark,
                       marker=marker, save_name=args.output_file, legend_loc=args.legend_loc, FPS=args.fps, single_ylabel=ylabel)

    # Plot spectra
    if not made_movie and not args.print_spectra:
        if not args.quiet:
            if args.r_between and not args.integrate:
                print 'Plotting snapshots', ns_cut, '| r-between, r-bins per snapshot:', binss
            elif args.m_between and not args.integrate:
                print 'Plotting snapshots', ns_cut, '| m-between, m-bins per snapshot:', [binss[n] for n in ns_cut]
            elif not js:
                print 'Plotting snapshots', ns_cut
            else:
                print 'Plotting snapshots', ns_cut, ', spatial bins', js

        plot_data(epsss, dudlnepssss, ns_cut, xlabel, labels, xlog=True, ylog=True, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True, watermark=watermark,
                  marker=marker, separate=args.separate, save_name=args.output_file, legend_loc=args.legend_loc, single_ylabel=ylabel)

    # Print spectra?
    if args.print_spectra:
        for dudlnepsss in dudlnepssss:
            for n in ns_cut:
                # print '        eps      nuFnu'
                for x, y in zip(epsss[n], dudlnepsss[n]):
                    print ' %6.4e %6.4e' % (x, y)
                print ''

if __name__ == '__main__':
    main()
