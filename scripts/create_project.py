#/usr/bin/env python

import os
import shutil
import argparse

# def make_standard_param_file(param_folder, proj_name, no_radiation=False):
#     # Copying the default parameter file, with some modifications
#     with open('project_init_files/params.par', 'r') as f_default:
#         with open(param_folder+'/params.par', 'w') as f:
#             for line in f_default.readlines():
#                 if line[:8] == 'sim_name':
#                     f.write('sim_name = '+proj_name+'\n')
#                 elif line[:14] == 'mhd_state_file':
#                     f.write('mhd_state_file = state_files/'+proj_name+'_init.msf\n')
#                 elif line[:14] == 'rad_state_file':
#                     if no_radiation:
#                         f.write('rad_state_file = None\n')
#                     else:
#                         f.write('rad_state_file = state_files/'+proj_name+'_init.rsf\n')
#                 elif line[:61] == "# The parameters below are only needed when radiation is used":
#                     if no_radiation:
#                         break
#                     else:
#                         f.write(line)
#                 else:
#                     f.write(line)

#     # If not using radiation, the above code will generate one too many newlines; remove that newline here
#     if no_radiation:
#         with open(param_folder+'/params.par', 'r') as f:
#             lines = f.readlines()
#         with open(param_folder+'/params.par', 'w') as f:
#             for line in lines[:-1]:
#                 f.write(line)

def main():
    parser = argparse.ArgumentParser(description='a script that generates the folder structure and some standard files for a new project. the new project is by default located inside the projects/ folder.')
    parser.add_argument('PROJ_NAME', help='the project name')
    parser.add_argument('--no-radiation', action='store_true', help='generates no radiation files for the project')
    parser.add_argument('--is-test', action='store_true', help='puts the project in the tests/ folder instead')
    args = parser.parse_args()

    # Find folder
    if args.is_test:
        proj_folder = '../tests/'
    else:
        proj_folder = '../projects/'
    proj_path = proj_folder+args.PROJ_NAME

    try:
        # Generating project
        os.makedirs(proj_path)
        print "Generating '"+proj_path+"'"
    except OSError:
        print "  Error: A project with the name '"+args.PROJ_NAME+"' already exists inside the "+proj_folder+" folder."
        print "         The project was not created."
        print "         Exiting..."

    # If project was generated, make folder structure
    folders = ['/sim_init_files',
               '/output_movies',
               '/output_plots',
               '/param_files',
               '/snapshot_files',
               '/state_files',
               '/custom_scripts']
    for folder in folders:
        os.makedirs(proj_path+folder)
        print "Generating '"+proj_path+folder+"'"

    # Make .gitignore files inside each folder (for Git to commit also empty folders), telling git to ignore output files
    for folder in folders:
        with open(proj_path+folder+'/.gitignore', 'w') as f:
            if 'output' in folder:
                # Ignore all output files
                f.write('*\n')
                f.write('!.gitignore')
                print "Generating '"+proj_path+folder+'/.gitignore'+"' (ignores *)"
            else:
                print "Generating '"+proj_path+folder+'/.gitignore'+"' (empty)"

    # Make the README.md file
    shutil.copyfile("project_init_files/README.md", proj_path+"/README.md")
    print "Generating '"+proj_path+'/README.md'+"'"

    # Make a "standard" sim_init.py file
    if args.no_radiation:
        shutil.copyfile("project_init_files/sim_init_no_rad.py", proj_path+folders[0]+"/sim_init.py")
    else:
        shutil.copyfile("project_init_files/sim_init.py", proj_path+folders[0]+"/sim_init.py")
    print "Generating '"+proj_path+folders[0]+'/sim_init.py'+"'"

    # Make a "standard" parameter file (.par)
    # make_standard_param_file(proj_path+folders[3], args.PROJ_NAME, no_radiation=args.no_radiation)
    if args.no_radiation:
        shutil.copyfile("project_init_files/params_no_rad.par", proj_path+folders[3]+"/params.par")
    else:
        shutil.copyfile("project_init_files/params.par", proj_path+folders[3]+"/params.par")
    print "Generating '"+proj_path+folders[3]+'/params.par'+"'"

if __name__ == '__main__':
    main()
