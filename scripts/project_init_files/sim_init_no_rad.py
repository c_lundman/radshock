#!/usr/bin/env python

# The INNER PARTS of these functions should be modified by the user, but not their names, input parameters or return statements
# This file is used as a command line argument for the "generate_msf.py" script
# The number of cells N (excluding ghost cells) is given as an argument to "generate_msf.py"
# For now, assumes that the primitives (v, rho, p, b) are given as functions of the mass coordinate, NOT of radius

# The MHD part

def get_mhd_params():

    # MHD parameters

    msf_filename = 'state_files/sim.msf'
    N = 128 # The number of spatial bins (excluding ghost cells)

    M_o = 1. # Total simulation mass (in spherical geometry: mass per steradian)
    R_i = 1. # The inner radial boundary
    alpha = 0 # The simulation geometry
    gamma_ad = 4./3. # The adiabatic index
    t = 0. # The initial simulation time
    iteration = 0 # The current MHD iteration

    basic_params = [msf_filename, N, M_o, R_i, alpha, gamma_ad, t, iteration]

    # Problem specific parameters (what you put here will be passed on
    # to 'get_m_bs' and 'get_prims')

    problem_params = []

    return basic_params, problem_params

def get_m_bs(basic_params, problem_params):

    # The mass coordinate grid
    # Change however you want, but m_bs[2] = 0 is required (i.e. the grid starts
    # at m_b = 0, excluding the ghost cells)

    N = basic_params[1]
    M_o = basic_params[2]
    dm = M_o/float(N)
    m_bs = [i*dm for i in xrange(N+5)]
    m_norm = m_bs[2]
    m_bs = [m_b-m_norm for m_b in m_bs]

    return m_bs

def get_prims(m, basic_params, problem_params):

    # The primitives, as functions of the mass coordinate
    # If using radiation, the pressure here will be the radiation pressure,
    # the gas pressure will be lower by a factor Z_gamma/2

    v = 0
    rho = 1
    p = 1e-2
    b = 0

    return v, rho, p, b
