#!/usr/bin/env python

# The INNER PARTS of these functions should be modified by the user, but not their names, input parameters or return statements
# This file is used as a command line argument for the "generate_msf.py" script
# The number of cells N (excluding ghost cells) is given as an argument to "generate_msf.py"
# For now, assumes that the primitives (v, rho, p, b) are given as functions of the mass coordinate, NOT of radius

def get_params():

    # Basic parameters

    M_o = 1.
    R_i = 1.
    alpha = 0
    gamma_ad = 4./3.
    t = 0.
    iteration = 0
    basic_params = [M_o, R_i, alpha, gamma_ad, t, iteration]

    # Problem specific parameters
    problem_params = []

    return basic_params, problem_params

def get_m_bs(N, basic_params, problem_params):

    # The mass coordinate grid (m_bs[2] = 0 is required!)

    M_o = basic_params[0]
    dm = M_o/float(N)
    m_bs = [i*dm for i in xrange(N+5)]
    m_norm = m_bs[2]
    m_bs = [m_b-m_norm for m_b in m_bs]

    return m_bs

def get_prims(m, basic_params, problem_params):

    # The primitives, as functions of the mass coordinate

    v = 0
    rho = 1
    p = 1e-2
    b = 0

    return v, rho, p, b
