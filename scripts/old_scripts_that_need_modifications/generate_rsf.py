#!/usr/bin/env python

import argparse
from random import random
from math import exp, sqrt
from flatten_msf_density import read_mhd_state_file, save_mhd_state_file, get_consts

# Constants
m_p = 1.6726e-24
m_e = 9.1094e-28

def Wien_norm(x):
    return .5*x*x*exp(-x)

def bisect(xs, x, no_ghosts=False):

    # Returns l, such that xs[l] < x < xs[l+1]
    # Assumes that xs is ordered, such that xs[i] < xs[i+1] for all i

    l = 0
    h = len(xs)-1
    if no_ghosts:
        l += 2
        h -= 2

    while h-l != 1:
        m = (l+h)/2
        if xs[m] > x:
            h = m
        else:
            l = m
    return l

class NumDistribution():
    def __init__(self, n):
        self.n = n
        self.Ps = [0. for i in xrange(n)]
        self.xs = [0. for i in xrange(n)]

    def load_Wien_norm(self):
        x_max = 100.
        dx = x_max/float(self.n)

        # "Integrating"
        self.xs[0] = 0
        self.Ps[0] = 0
        for i in xrange(1, self.n):
            self.xs[i] = i*dx
            x = .5*(self.xs[i]+self.xs[i-1])
            self.Ps[i] = self.Ps[i-1] + Wien_norm(x)*dx

        # Normalizing
        P_max = self.Ps[-1]
        self.Ps = [P/P_max for P in self.Ps]

    def load_r_dist(self, r_bs, vs, rhos, Z_gamma, alpha):
        # Z_gamma is the photon-to-proton ratio

        n_bs = len(r_bs)
        r_min = r_bs[2]
        r_max = r_bs[n_bs-3]
        A = 1
        dr = (r_max-r_min)/float(len(self.Ps)-1)

        # Filling cumulative distribution
        self.xs[0] = r_min
        self.Ps[0] = 0.

        # for(unsigned int i=0; i<Ps.size()-1; i++) {
        for i in xrange(len(self.Ps)-1):
            # Next location
            self.xs[i+1] = self.xs[i] + dr

            # Next probability
            r_avg = .5*(self.xs[i+1] + self.xs[i])
            if alpha == 2:
                A = r_avg*r_avg
            elif alpha == 1:
                A = r_avg
            elif alpha == 0:
                A = 1.

            # Find corresponding bin
            l = bisect(r_bs, r_avg)

            # Find bin properties
            v = vs[l];
            rho = rhos[l];
            gamma = 1./sqrt(1.-v*v);

            # Integrating the cumulative distribution
            f = gamma*rho*A*Z_gamma/m_p # Really N_gamma*f, where f is prob. dens. of r
            self.Ps[i+1] = self.Ps[i]+f*dr;

        # Normalizing the distribution
        N_gamma = self.Ps[-1]
        self.Ps = [P/N_gamma for P in self.Ps]

        # This specific loading function returns the total number of physical photons
        return N_gamma

    def sample(self, P):
        l = bisect(self.Ps, P)
        x_l = self.xs[l]
        x_r = self.xs[l+1]
        P_l = self.Ps[l]
        P_r = self.Ps[l+1]
        x = x_l + (P-P_l)/(P_r-P_l)*(x_r-x_l)
        return x

def get_theta(rho, p, Z_pm, f_heat):
    # Returns the electron temperature
    theta = m_p/m_e*p/rho/(1.+Z_pm)/f_heat
    return theta

class RadHeader():
    def __init__(self):
        self.n_phs = 0
        self.n_bs = 0
        self.eps_min = 0
        self.eps_max = 0
        self.n_bs_spec = 0
        self.n_eps_bs = 0
        self.n_el_spec_bins = 0
        self.alpha = 0
        self.f_heat = 0
        self.gamma_ad = 0
        self.t = 0
        self.iteration = 0
        self.photons_per_split = 0
        self.w_i = 0
        self.n_eps_splits_bins = 0

    def set_n_phs(self, n_phs): self.n_phs = n_phs
    def set_n_bs(self, n_bs): self.n_bs = n_bs
    def set_eps_min(self, eps_min): self.eps_min = eps_min
    def set_eps_max(self, eps_max): self.eps_max = eps_max
    def set_n_bs_spec(self, n_bs_spec): self.n_bs_spec = n_bs_spec
    def set_n_eps_bs(self, n_eps_bs): self.n_eps_bs = n_eps_bs
    def set_n_el_spec_bins(self, n_el_spec_bins): self.n_el_spec_bins = n_el_spec_bins
    def set_alpha(self, alpha): self.alpha = alpha
    def set_f_heat(self, f_heat): self.f_heat = f_heat
    def set_gamma_ad(self, gamma_ad): self.gamma_ad = gamma_ad
    def set_t(self, t): self.t = t
    def set_iteration(self, iteration): self.iteration = iteration
    def set_photons_per_split(self, photons_per_split): self.photons_per_split = photons_per_split
    def set_w_i(self, w_i): self.w_i = w_i
    def set_n_eps_splits_bins(self, n_eps_splits_bins): self.n_eps_splits_bins = n_eps_splits_bins

    def get_n_phs(self): return self.n_phs
    def get_n_bs(self): return self.n_bs
    def get_eps_min(self): return self.eps_min
    def get_eps_max(self): return self.eps_max
    def get_n_bs_spec(self): return self.n_bs_spec
    def get_n_eps_bs(self): return self.n_eps_bs
    def get_n_el_spec_bins(self): return self.n_el_spec_bins
    def get_alpha(self): return self.alpha
    def get_f_heat(self): return self.f_heat
    def get_gamma_ad(self): return self.gamma_ad
    def get_t(self): return self.t
    def get_iteration(self): return self.iteration
    def get_photons_per_split(self): return self.photons_per_split
    def get_w_i(self): return self.w_i
    def get_n_eps_splits_bins(self): return self.n_eps_splits_bins

    def print_header(self, header_name):
        print header_name
        print '  n_phs = %i' % (self.n_phs)
        print '  n_bs = %i' % (self.n_bs)
        print '  eps_min = %4.4e' % (self.eps_min)
        print '  eps_max = %4.4e' % (self.eps_max)
        print '  n_bs_spec = %i' % (self.n_bs_spec)
        print '  n_eps_bs = %i' % (self.n_eps_bs)
        print '  n_el_spec_bins = %i' % (self.n_el_spec_bins)
        print '  alpha = %i' % (self.alpha)
        print '  f_heat = %4.4e' % (self.f_heat)
        print '  gamma_ad = %4.4f' % (self.gamma_ad)
        print '  t = %4.4e' % (self.t)
        print '  iteration = %i' % (self.iteration)
        print '  photons_per_split = %i' % (self.photons_per_split)
        print '  w_i = %4.4e' % (self.w_i)
        print '  n_eps_splits_bins = %i' % (self.n_eps_splits_bins)

def save_rsf_file(rad_header, mhd_grid, Z_gamma, msf_pressure_is_gas_pressure, output_file_name):

    # Z_pm assumed unity
    Z_pm = 1.

    # Distributions
    Wien_spec_norm_dist = NumDistribution(300)
    Wien_spec_norm_dist.load_Wien_norm()
    r_dist = NumDistribution(rad_header.get_n_bs())
    N_gamma = r_dist.load_r_dist(mhd_grid.get_r_bs(), mhd_grid.get_vs(), mhd_grid.get_rhos(), Z_gamma, rad_header.get_alpha())
    w_i = N_gamma/rad_header.get_n_phs()
    rad_header.set_w_i(w_i)

    with open(output_file_name, 'w') as f:

        # Write header
        strings = ['n_phs          %i' % (rad_header.get_n_phs()),
                   'n_bs           %i' % (rad_header.get_n_bs()),
                   'eps_min        %18.12e' % (rad_header.get_eps_min()),
                   'eps_max        %18.12e' % (rad_header.get_eps_max()),
                   'n_bs_spec      %i' % (rad_header.get_n_bs_spec()),
                   'n_eps_bs       %i' % (rad_header.get_n_eps_bs()),
                   'n_el_spec_bins %i' % (rad_header.get_n_el_spec_bins()),
                   'alpha          %i' % (rad_header.get_alpha()),
                   'f_heat         %18.12e' % (rad_header.get_f_heat()),
                   'gamma_ad       %18.12e' % (rad_header.get_gamma_ad()),
                   't              %18.12e' % (rad_header.get_t()),
                   'iter           %i' % (rad_header.get_iteration()),
                   'photons_per_split %i' % (rad_header.get_photons_per_split()),
                   'w_i            %18.12e' % (rad_header.get_w_i()),
                   'n_eps_splits_bins %i' % (rad_header.get_n_eps_splits_bins()),
                   '',
                   '---',
                   '']
        for string in strings:
            f.write(string+'\n')

        # Eps splits [NOT YET IMPLEMENTED!]
        string = ''
        for i in xrange(rad_header.get_n_eps_splits_bins()):
            string += ' %18.12e' % (1.)
        f.write(string+'\n')

        # Z_pm (assumed unity)
        string = ''
        for i in xrange(rad_header.get_n_bs()-1):
            string += ' %18.12e' % (Z_pm)
        f.write(string+'\n')

        # Photon generation
        for i in xrange(rad_header.get_n_phs()):
            # draw r
            r = r_dist.sample(random())
            l = bisect(mhd_grid.get_r_bs(), r, no_ghosts=True)
            v = mhd_grid.get_vs()[l]
            rho = mhd_grid.get_rhos()[l]
            p = mhd_grid.get_ps()[l]
            gamma = 1./sqrt(1.-v*v)

            if not msf_pressure_is_gas_pressure:
                p *= 2./Z_gamma

            # draw mu
            if v == 0:
                mu = 2.*random()-1.
            else:
                u = sqrt(1./gamma/gamma/gamma/gamma/((1.-v)*(1.-v) + 4.*v*random()))
                mu = (1.-u)/v

            # draw eps
            # theta_old = get_theta(rho, p, Z_pm, rad_header.get_f_heat())
            theta = get_theta(rho, p, Z_pm, 1.)
            eps_prime = theta*Wien_spec_norm_dist.sample(random())
            eps = eps_prime/gamma/(1.-mu*v)

            string = '%18.12e ' % (r)
            string += '%18.12e ' % (mu)
            string += '%18.12e ' % (eps)
            string += '%18.12e' % (rad_header.get_w_i())
            f.write(string+'\n')

def main():
    parser = argparse.ArgumentParser(description='generates a RAD state file (.rsf) from a MHD state file (.msf). the RAD state files contain the Z_pm grid; however, for now, Z_pm = 1 is assumed everywhere for the files generated by this script.')
    parser.add_argument('INPUT_MSF_FILE', help='the MHD state file (.msf)')
    parser.add_argument('N_MC', type=int, help='the number of generated Monte Carlo photon packets')
    parser.add_argument('OUTPUT_RSF_FILE', help='the output RAD state file (.rsf)')

    parser.add_argument('--Z-gamma', type=float, default=1e4, help='the photon-to-proton ratio [default=1e4]')
    parser.add_argument('--f-heat', type=float, default=1, help='"fake heat" which can be used (f_heat > 1) to make the simulation more stable, but makes temperatures incorrect behind subshocks [default=1]')
    parser.add_argument('--also-mod-input-msf', action='store_true', help='modifies the pressure of the input msf file using information from --f-heat (and, if --msf-pressure-is-not-rad-pressure is not given, also --Z-gamma)')
    # parser.add_argument('--msf-pressure-is-gas-pressure', action='store_true', help='if given, assumes that the pressure in the msf file is gas pressure, and radiation pressure is a factor --Z-gamma larger (else, radiation pressure equals the pressure in the msf file)')
    parser.add_argument('--msf-pressure-is-not-rad-pressure', action='store_true', help='if given, assumes that the pressure in the msf file is gas pressure, and radiation pressure is a factor --Z-gamma larger (else, radiation pressure equals the pressure in the msf file)')
    parser.add_argument('--eps-min', type=float, default=1e-5, help='lower (lab frame) energy in the output spectrum [default=1e-5]')
    parser.add_argument('--eps-max', type=float, default=1e+2, help='upper (lab frame) energy in the output spectrum [default=1e+2]')
    parser.add_argument('--n-bs-spec', type=int, default=101, help='number of spectrum bin boundaries [default=101]')
    parser.add_argument('--n-eps-bs', type=int, default=101, help='number of spectrum bin boundaries (?) [default=101]')
    parser.add_argument('--n-el-spec-bins', type=int, default=30, help='number of electron spectrum energy bins [default=30]')
    parser.add_argument('--iteration', type=int, default=0, help='the current radiation iteration [default=0]')
    parser.add_argument('--photons-per-split', type=int, default=1, help='when photon passes a "split energy", how many photons should it be split into? [default=1]')
    parser.add_argument('--n-eps-splits-bins', type=int, default=20, help='number of bins in the "split energy" vector [default=20]')
    args = parser.parse_args()

    # Get the MHD grid and make the RAD header
    mhd_header, mhd_grid = read_mhd_state_file(args.INPUT_MSF_FILE)
    rad_header = RadHeader()
    rad_header.set_n_phs(args.N_MC)
    rad_header.set_n_bs(mhd_header.get_n_bs())
    rad_header.set_eps_min(args.eps_min)
    rad_header.set_eps_max(args.eps_max)
    rad_header.set_n_bs_spec(args.n_bs_spec)
    rad_header.set_n_eps_bs(args.n_eps_bs)
    rad_header.set_n_el_spec_bins(args.n_el_spec_bins)
    rad_header.set_alpha(mhd_header.get_alpha())
    rad_header.set_f_heat(args.f_heat)
    rad_header.set_gamma_ad(mhd_header.get_gamma_ad())
    rad_header.set_t(mhd_header.get_t())
    rad_header.set_iteration(args.iteration)
    rad_header.set_photons_per_split(args.photons_per_split)
    rad_header.set_n_eps_splits_bins(args.n_eps_splits_bins)

    # Compute w_i, save the RAD header, generate and save the photons
    print "Saving '"+args.OUTPUT_RSF_FILE+"'"
    save_rsf_file(rad_header, mhd_grid, args.Z_gamma, args.msf_pressure_is_not_rad_pressure, args.OUTPUT_RSF_FILE)

    # Modify msf file?
    if args.also_mod_input_msf:
        ps = mhd_grid.get_ps()
        if args.msf_pressure_is_not_rad_pressure:
            ps = [p*args.f_heat for p in ps]
        else:
            ps = [p*args.f_heat/args.Z_gamma*2 for p in ps]
        mhd_grid.set_ps(ps)
        Vs, Ss, Es, Ms = get_consts(mhd_grid.get_rs(), mhd_grid.get_vs(), mhd_grid.get_rhos(), mhd_grid.get_ps(), mhd_grid.get_bs(), mhd_header.get_alpha(), mhd_header.get_gamma_ad())
        mhd_grid.set_Vs(Vs)
        mhd_grid.set_Ss(Ss)
        mhd_grid.set_Es(Es)
        mhd_grid.set_Ms(Ms)
        print "Saving '"+args.INPUT_MSF_FILE+"' (modifying pressure)"
        save_mhd_state_file(mhd_header, mhd_grid, args.INPUT_MSF_FILE)

if __name__ == '__main__':
    main()
