#!/usr/bin/env python

from matplotlib import pyplot as plt
from numpy import log10

from matplotlib.font_manager import FontProperties
monofont = FontProperties()
monofont.set_family("monospace")

import sys
sys.path.append('/home/chr/Work/PythonScripts')
plt.style.use('/home/chr/Work/PythonScripts/chris.mplstyle')

def load_photons(filename):
    photons = []
    header = {}
    # header_names = ['n_phs', 'n_bs', 'boundary_L', 'boundary_R', 'alpha', 'f_heat', 'iter', 't', 'NUM_PROC']
    header_names = ['n_phs', 'n_bs', 'alpha', 'f_heat', 'iter', 't', 'NUM_PROC']
    nr_lines = 0

    try:
        f = open(filename, 'r')
    except:
        print
        print '    WARNING: Could not open file '+filename
        print
        return

    for line in f.readlines():
        nr_lines += 1
        elms = line.split()
        if len(elms) == 2:

            # Reading header
            for header_name in header_names:
                if elms[0] == header_name:
                    header[header_name] = elms[1]

        elif len(elms) > 4:
            Z_pms = elms

        elif len(elms) == 4:
            r = float(elms[0])
            mu = float(elms[1])
            eps = float(elms[2])
            w = float(elms[3])
            photons.append((r, mu, eps, w))

    # Print header
    print
    print 'Loading file:', filename
    print 'Printing header;'
    for header_name in header_names:
        if header_name in header:
            print ' '+header_name+':', header[header_name]
        else:
            print
            print '    WARNING: '+header_name+' missing from the file header.'
            print '             Old file formatting?'
            print

    print 'Number of photons loaded:', len(photons)
    print 'Total number of lines in the file:', nr_lines
    if int(header['n_phs']) != len(photons):
        print
        print '    WARNING: The file does not contain the correct number of photons, only ~%2.0f%% of intended photons present [n_phs = %i, len(photons) = %i]' % (100.*float(len(photons))/float(header['n_phs']), int(header['n_phs']), len(photons))
        print '             radshock is expected to _CRASH_ after loading this file.'
    else:
        print
        print ' => All expected photons were found!'
    print
    return photons

def show_dist(photons, var, filename):
    var_indexs = {'r':0,
                  'mu':1,
                  'eps':2,
                  'w':3}
    var_labels = {'r':r'$r \, \mathrm{[cm]}$',
                  'mu':r'$\mu$',
                  'eps':r'$\log_{10} \epsilon$',
                  'w':r'$w$'}
    var_colors = {'r':'green',
                  'mu':'blue',
                  'eps':'red',
                  'w':'k'}
    index = var_indexs[var]
    label = var_labels[var]
    color = var_colors[var]

    samples = [photon[index] for photon in photons]
    out_of_bounds = 0

    if var == 'mu':
        xlims = -1, +1
        for sample in samples:
            if sample < xlims[0] or sample > xlims[1]:
                out_of_bounds += 1
                print
                print '    WARNING: mu sample out of plot bounds! [mu = %4.4e, out_of_bounds = %i]' % (sample, out_of_bounds)
                print
        plt.xlim(xlims)

    i = 0
    if var == 'eps':
        samples = [log10(sample) for sample in samples]
        xlims = -7, 1
        for sample in samples:
            if sample < xlims[0] or sample > xlims[1]:
                out_of_bounds += 1
                print
                print '    WARNING: eps sample out of plot bounds! [eps = %4.4e, out_of_bounds = %i' % (10.**sample, out_of_bounds), ', photon =', photons[i], ']'
                print
            i += 1
        plt.xlim(xlims)

    plt.hist(samples, 100, alpha=.5, normed=0, edgecolor=color, histtype='step', linewidth=2)
    plt.ylabel(r'$\mathrm{MC \, photons \, per \, bin}$')
    plt.xlabel(label)
    plt.title(filename.replace('_', '\_'), fontproperties=monofont, fontsize=30)
    plt.tight_layout()
    plt.show()

def main():
    if len(sys.argv) < 2:
        print '    ERROR: No file name provided.'
        print '           Exiting.'
        raise SystemExit

    filenames = []
    arguments = []
    for argv in sys.argv[1:]:
        if argv[0] != '-':
            filenames.append(argv)
        else:
            arguments.append(argv)

    for filename in filenames:
        photons = load_photons(filename)

        for var in arguments:
            print '---'
            print
            if var[1:] in ['r', 'mu', 'eps', 'w']:
                print 'Plotting histogram of variable '+var[1:]+'.'
                print
                show_dist(photons, var[1:], filename)
            else:
                print '    ERROR: Bad argument provided, argument =', var
                print '           Must be one of -r, -mu, -eps, -w.'
                print '           Exiting.'
                raise SystemExit

        if filename != filenames[-1]:
            print '------------------------------------'

if __name__ == '__main__':
    main()
