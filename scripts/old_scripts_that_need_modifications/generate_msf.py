#!/usr/bin/env python

import os
import sys
import argparse
from math import sqrt
from flatten_msf_density import Header, MHD_Grid, save_mhd_state_file
from plot_snapshots import plot_data

# To do:
# - fix make_r_bs()

def get_prim_vectors(m_bs, func, basic_params, problem_params):
    vs = []
    rhos = []
    ps = []
    bs = []
    for i in xrange(len(m_bs)-1):
        m = .5*(m_bs[i+1]+m_bs[i])

        v, rho, p, b = func(m, basic_params, problem_params)

        vs.append(v)
        rhos.append(rho)
        ps.append(p)
        bs.append(b)

    return vs, rhos, ps, bs

def make_r_bs(m_bs, vs, rhos, R_i, alpha):
    r_bs = [0. for m_b in m_bs]
    r_bs[2] = R_i

    for i in xrange(3, len(m_bs)):
        dm = m_bs[i]-m_bs[i-1]
        r_1 = r_bs[i-1]
        gamma = 1./sqrt(1.-vs[i-1]*vs[i-1])
        rho = rhos[i-1]
        r_bs[i] = ( r_1**(alpha+1.) + (alpha+1.)*dm/gamma/rho )**(1./(alpha+1.))

    for i in range(0, 2)[::-1]:
        dm = m_bs[i+1]-m_bs[i]
        r_2 = r_bs[i+1]
        gamma = 1./sqrt(1.-vs[i]*vs[i])
        rho = rhos[i]
        r_bs[i] = ( r_2**(alpha+1.) - (alpha+1.)*dm/gamma/rho )**(1./(alpha+1))

    return r_bs

def make_m_bs(r_bs, vs, rhos, alpha):
    m_bs = [0. for r_b in r_bs]

    for i in xrange(3, len(m_bs)):
        r_1 = r_bs[i-1]
        r_2 = r_bs[i]
        gamma = 1./sqrt(1.-vs[i-1]*vs[i-1])
        rho = rhos[i-1]
        m_bs[i] = m_bs[i-1] + gamma*rho/(alpha+1.)*(r_2**(alpha+1.) - r_1**(alpha+1.))

    for i in range(0, 2)[::-1]:
        r_1 = r_bs[i]
        r_2 = r_bs[i+1]
        gamma = 1./sqrt(1.-vs[i]*vs[i])
        rho = rhos[i]
        m_bs[i] = m_bs[i+1] - gamma*rho/(alpha+1.)*(r_2**(alpha+1.) - r_1**(alpha+1.))

    return m_bs

def get_consts(vs, rhos, ps, bs, r_bs, gamma_ad, alpha):
    Vs = []
    Ss = []
    Es = []
    Ms = []

    for i in xrange(len(vs)):
        v = vs[i]
        rho = rhos[i]
        p = ps[i]
        b = bs[i]
        r = .5*(r_bs[i+1]+r_bs[i])

        gamma = 1./sqrt(1.-v*v)
        w = gamma_ad/(gamma_ad-1.)*p/rho
        sigma = b*b/rho
        p_b = .5*b*b
        h_star = 1. + w + sigma
        p_star = p + p_b

        V = 1./gamma/rho
        S = V*gamma*gamma*rho*h_star*v
        E = V*(gamma*gamma*rho*h_star - p_star)

        if alpha == 2:
            M = b/r/rho
        elif alpha == 1:
            M = b/sqrt(r)/rho # Not yet implemented in the C++ code
        elif alpha == 0:
            M = b/rho

        Vs.append(V)
        Ss.append(S)
        Es.append(E)
        Ms.append(M)

    return Vs, Ss, Es, Ms

def main():
    parser = argparse.ArgumentParser(description='a file that generates MHD state files (.msf) from provided initial conditions (m, r, v, rho, p, b) functions.')
    parser.add_argument('INIT_COND_FILE', help='the Python file that contains the initial condition functions')
    parser.add_argument('N', type=int, help='the number of spatial bins (excluding the ghost cells)')
    parser.add_argument('-o', '--output-msf-file', help='the output (.msf) file: if not provided, instead plots the initial conditions')
    parser.add_argument('--print-piston-speeds', action='store_true', help='prints the value of betagamma at the boundaries (useful if piston boundaries are used)')
    parser.add_argument('--plot-only-v', action='store_true', help='if not output file provided, plots only v')
    parser.add_argument('--plot-only-rho', action='store_true', help='if not output file provided, plots only rho')
    parser.add_argument('--plot-only-p', action='store_true', help='if not output file provided, plots only p')
    parser.add_argument('--plot-only-b', action='store_true', help='if not output file provided, plots only b')
    parser.add_argument('--toggle-xlog', action='store_true', help='if showing plot, turn ON log scale on the x-axis')
    parser.add_argument('--toggle-ylog', action='store_false', help='if showing plot, turn OFF log scale on the y-axis')
    args = parser.parse_args()

    # Import the module
    full_path_to_file = os.getcwd() + '/' + args.INIT_COND_FILE
    path_name = full_path_to_file[::-1].split('/', 1)[1][::-1]
    file_name = full_path_to_file[::-1].split('/', 1)[0][::-1][:-3]
    sys.path.append(path_name)
    init_cond_funcs = __import__(file_name)

    # Get the grid, prims, consts
    basic_params, problem_params = init_cond_funcs.get_params()
    m_bs = init_cond_funcs.get_m_bs(args.N, basic_params, problem_params)
    vs, rhos, ps, bs = get_prim_vectors(m_bs, init_cond_funcs.get_prims, basic_params, problem_params)
    M_o, R_i, alpha, gamma_ad, t, iteration = basic_params
    r_bs = make_r_bs(m_bs, vs, rhos, R_i, alpha)
    Vs, Ss, Es, Ms = get_consts(vs, rhos, ps, bs, r_bs, gamma_ad, alpha)

    # Make the Header
    header = Header()
    header.set_n_bs(args.N+5)
    header.set_alpha(alpha)
    header.set_gamma_ad(gamma_ad)
    header.set_t(t)
    header.set_iteration(iteration)

    # Make the MHD_Grid
    mhd_grid = MHD_Grid()
    mhd_grid.set_m_bs(m_bs)
    mhd_grid.set_r_bs(r_bs)
    mhd_grid.set_vs(vs)
    mhd_grid.set_rhos(rhos)
    mhd_grid.set_ps(ps)
    mhd_grid.set_bs(bs)
    mhd_grid.set_Vs(Vs)
    mhd_grid.set_Ss(Ss)
    mhd_grid.set_Es(Es)
    mhd_grid.set_Ms(Ms)

    #
    if args.print_piston_speeds:
        v_L = vs[2]
        v_R = vs[-2]
        betagamma_L = v_L/sqrt(1.-v_L*v_L)
        betagamma_R = v_R/sqrt(1.-v_R*v_R)
        print 'betagamma_L = %4.4e' % (betagamma_L)
        print 'betagamma_R = %4.4e' % (betagamma_R)

    # Save the state file
    if args.output_msf_file:
        print "Saving '"+args.output_msf_file+"'"
        save_mhd_state_file(header, mhd_grid, args.output_msf_file)
    else:
        xlog=args.toggle_xlog
        ylog=args.toggle_ylog
        # xlog = 0
        # ylog = 0
        if args.N < 100:
            marker = '.'
        else:
            marker = None
        ms = [.5*(m_bs[i]+m_bs[i+1]) for i in xrange(len(vs))]
        watermark = r'$\mathrm{INIT. \, CONDITIONS}$'

        if args.plot_only_v:
            print 'Plotting v...'
            plot_data([ms], [[vs]], [0], r'$m$', [r'$v/c$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark)
        elif args.plot_only_rho:
            print 'Plotting rho...'
            plot_data([ms], [[rhos]], [0], r'$m$', [r'$\rho \, \mathrm{[g/cm^3]}$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark)
        elif args.plot_only_p:
            print 'Plotting p...'
            plot_data([ms], [[ps]], [0], r'$m$', [r'$p/c^2 \, \mathrm{[g/cm^3]}$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark)
        elif args.plot_only_b:
            print 'Plotting b...'
            plot_data([ms], [[bs]], [0], r'$m$', [r'$b$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark)

        else:
            print 'Plotting all prims...'
            plot_data([ms], [[vs]], [0], r'$m$', [r'$v/c$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark)
            plot_data([ms], [[rhos]], [0], r'$m$', [r'$\rho \, \mathrm{[g/cm^3]}$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark)
            plot_data([ms], [[ps]], [0], r'$m$', [r'$p/c^2 \, \mathrm{[g/cm^3]}$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark)
            if max(bs) != 0:
                plot_data([ms], [[bs]], [0], r'$m$', [r'$b$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark)

if __name__ == '__main__':
    main()
