#!/usr/bin/env python

# import os
# import sys
import argparse

from math import pi, sqrt
from numpy import nan, isnan
from plot_snapshots import read_mhd_file, plot_data
from plot_spectra import read_spc_file
from shock_finder import find_shocks

# def get_light_curve(epss, eps_bs, dLdlnepsss, eps_min=None, eps_max=None, counts=False):
#     m_e = 9.1094e-28
#     c = 2.998e+10

#     if eps_min == None:
#         eps_min = 0.
#     if eps_max == None:
#         eps_max = 1e60
#     Ls = []
#     Ns = []
#     for dLdlnepss in dLdlnepsss:
#         L = 0.
#         N = 0.
#         for i in xrange(len(epss)):
#             eps = epss[i]
#             if eps_min <= eps <= eps_max:
#                 deps = eps_bs[i+1]-eps_bs[i]
#                 L += dLdlnepss[i]*deps/eps

#                 E = eps*m_e*c*c
#                 N += dLdlnepss[i]*deps/eps/E
#         Ls.append(L)
#         Ns.append(N)
#     if not counts:
#         return Ls
#     else:
#         return Ns

def get_u_gammass(dudlnepssss, eps_bs):
    u_gammass = []
    for dudlnepsss in dudlnepssss:
        u_gammas = []
        for dudlnepss in dudlnepsss:
            u_gamma = 0.
            for i in xrange(len(eps_bs)-1):
                eps = .5*(eps_bs[i+1]+eps_bs[i])
                deps = eps_bs[i+1]-eps_bs[i]
                u_gamma += dudlnepss[i]*deps/eps
            u_gammas.append(u_gamma)
        u_gammass.append(u_gammas)
    return u_gammass

def get_E_gammas(u_gammass, r_bss):
    E_gammas = []
    for u_gammas, r_bs in zip(u_gammass, r_bss):
        E_gamma = 0.
        for i in xrange(len(r_bs)-1):
            dr = r_bs[i+1]-r_bs[i]
            r = .5*(r_bs[i+1]+r_bs[i])
            E_gamma += u_gammas[i]*4.*pi*r*r*dr
        E_gammas.append(E_gamma)
    return E_gammas

def get_subshock_locations(yss):
    # Calibrated for betagamma
    dAA_lim = .001
    W = 15
    Nss = []
    for ys in yss:
        Ns = find_shocks(ys, dAA_min=dAA_lim, dAA_max=dAA_lim, W=W)
        # if len(Ns) > 2:
        #     Ns = [Ns[0], Ns[-1]]
        if len(Ns) == 1:
            Ns.append(nan)
        if len(Ns) == 0:
            Ns = [nan, nan]
        Nss.append(Ns)
    return Nss

def find_shock_power(vss, rhoss, m_bss, r_bss, Ns):
    W = 10
    c = 2.99e10

    Ps = [0.]
    E_sh = 0.
    for i in xrange(len(vss)-1):
    # for vs, rhos, r_bs, N in zip(vss, rhoss, r_bss, Ns):
        vs_1 = vss[i]
        vs_2 = vss[i+1]
        rhos_1 = rhoss[i]
        rhos_2 = rhoss[i+1]
        m_bs_1 = m_bss[i]
        m_bs_2 = m_bss[i+1]
        N_1 = Ns[i]
        N_2 = Ns[i+1]
        if (not isnan(N_1)) and (not isnan(N_2)):
            dm_sh = 4.*pi*(m_bs_2[int(N_2)]-m_bs_1[int(N_1)])

            N_l = int(N_1)-W
            N_r = int(N_1)+W
            if rhos_1[N_l] < rhos_1[N_r]:
                v_u_1 = vs_1[N_l]
                v_d_1 = vs_1[N_r]
            else:
                v_u_1 = vs_1[N_r]
                v_d_1 = vs_1[N_l]

            N_l = int(N_2)-W
            N_r = int(N_2)+W
            if rhos_2[N_l] < rhos_2[N_r]:
                v_u_2 = vs_2[N_l]
                v_d_2 = vs_2[N_r]
            else:
                v_u_2 = vs_2[N_r]
                v_d_2 = vs_2[N_l]

            # Compute erg/cm^2/s dissipated
            v_rel_1 = (v_u_1-v_d_1)/(1.-v_u_1*v_d_1)
            gamma_rel_1 = 1./sqrt(1.-v_rel_1*v_rel_1)

            v_rel_2 = (v_u_2-v_d_2)/(1.-v_u_2*v_d_2)
            gamma_rel_2 = 1./sqrt(1.-v_rel_2*v_rel_2)

            Gamma_d_1 = 1./sqrt(1.-v_d_1*v_d_1)
            Gamma_d_2 = 1./sqrt(1.-v_d_2*v_d_2)
            Gamma_d = .5*(Gamma_d_2 + Gamma_d_1)

            # Compute mass flow rate through the shock?
            P_1 = (gamma_rel_1 - 1.)
            P_2 = (gamma_rel_2 - 1.)

            P = .5*(P_2 + P_1)
            dm_sh = sqrt(dm_sh*dm_sh)
            dE_sh = dm_sh*c*c*P*Gamma_d
            E_sh += dE_sh
        else:
            dE_sh = nan

        Ps.append(E_sh)

        # if isnan(N):
        #     Ps.append(nan)
        # else:
        #     N_l = int(N)-W
        #     N_r = int(N)+W
        #     r = r_bs[int(N)]
        #     if rhos[N_l] < rhos[N_r]:
        #         v_u = vs[N_l]
        #         v_d = vs[N_r]
        #         rho_u = rhos[N_l]
        #         rho_d = rhos[N_r]
        #     else:
        #         v_u = vs[N_r]
        #         v_d = vs[N_l]
        #         rho_u = vs[N_r]
        #         rho_d = vs[N_l]

        #     # Compute erg/cm^2/s dissipated
        #     v_rel = (v_u-v_d)/(1.-v_u*v_d)
        #     gamma_rel = 1./sqrt(1.-v_rel*v_rel)

        #     # Compute mass flow rate through the shock?
        #     P = gamma_rel - 1.


        #     Ps.append(P)

    return Ps

def main():
    parser = argparse.ArgumentParser(description='a script that plots the total radiation energy versus time from a spectrum snapshot file (.spc)')
    parser.add_argument('INPUT_SPC_FILE', help='the input spectrum snapshots file (.spc)')
    parser.add_argument('-o', '--output-file', help='the output pdf (for plot) or mp4 file (for movie)')

    parser.add_argument('--n-start', type=int, default=0, help='computes subshocks only from snapshot --n-start')
    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    parser.add_argument('--z-rev', action='store_true', help='reverses the z-order of the lines (i.e. which lines cover each other)')
    parser.add_argument('--keep-nth', type=int, help="keep only every n'th snapshot (always keeps the first and last snapshots)")

    parser.add_argument('-tx', '--toggle-xlog', action='store_false', help='turn OFF log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_false', help='turn OFF log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--legend-loc', type=int, default=0, help='legend location [default=0, which turns off the legend]')
    parser.add_argument('--legend-font-size', type=int, default=24, help='legend font size [default=24]')
    parser.add_argument('--no-tight-layout', action='store_true', help="don't use tight_layout()")
    parser.add_argument('--plot-max-temps', action='store_true', help="a very strange option... plots maximum electron and proton temperatures instead")
    parser.add_argument('--plot-chi', action='store_true', help="a very strange option... finds RMS and plots quantities related to it")

    # parser.add_argument('--sci-not', action='store_true', help='use scientific notation for the legend')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    # Loading the data
    iterations, ts, m_bss, r_bss, vss, rhoss, pss, bss, alpha, gamma_ad = read_mhd_file(args.INPUT_SPC_FILE[:-3]+'mhd', args.n_max, False, keep_nth=args.keep_nth)

    if args.plot_max_temps:
        from generate_state_files import get_theta_e, get_theta_p
        from plot_snapshots import read_zpm_file
        Z_pmss = read_zpm_file(args.INPUT_SPC_FILE[:-3]+'zpm', None, False, keep_nth=args.keep_nth)
        xiss = read_zpm_file(args.INPUT_SPC_FILE[:-3]+'xis', None, False, keep_nth=args.keep_nth)

        theta_ess = [[get_theta_e(rho, p, Z_pm, xi, 1.) for rho, p, Z_pm, xi in zip(rhos, ps, Z_pms, xis)] for rhos, ps, Z_pms, xis in zip(rhoss, pss, Z_pmss, xiss)]
        theta_e_maxs = [max(theta_es) for theta_es in theta_ess]

        theta_pss = [[get_theta_p(rho, p, Z_pm, xi, 1.) for rho, p, Z_pm, xi in zip(rhos, ps, Z_pms, xis)] for rhos, ps, Z_pms, xis in zip(rhoss, pss, Z_pmss, xiss)]
        theta_p_maxs = [max(theta_ps) for theta_ps in theta_pss]

        # theta_css = read_zpm_file(args.INPUT_SPC_FILE[:-3]+'cte', None, False, keep_nth=args.keep_nth)
        # theta_c_maxs = [max(theta_cs) for theta_cs in theta_css]

        theta_maxss = [theta_p_maxs, theta_e_maxs]

        tss = [ts for theta_e_maxs in theta_maxss]
        ns = [i for i in xrange(len(theta_maxss))]
        labels = [r'$\mathrm{max} \, \theta_p$', r'$\mathrm{max} \, \theta_e$']
        xlabel = r'$ct \, \mathrm{[cm]}$'
        ylabel = None
        plot_data(tss, [theta_maxss], ns, xlabel, labels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True, watermark=None,
                  marker=None, separate=None, save_name=args.output_file, legend_loc=args.legend_loc, legend_font_size=args.legend_font_size, single_ylabel=ylabel,
                  final_line_is_black=False, ylabels_for_same_line=labels, no_tight_layout=args.no_tight_layout, zorder_reverse=args.z_rev)
        raise SystemExit

    if args.plot_chi:
        gammass = [[1./sqrt(1.-v*v) for v in vs] for vs in vss]
        m_cut = 1.5e25
        M_o = m_bss[0][-1]
        i_cut = 0
        for m in m_bss[0]:
            if M_o - m > m_cut:
                i_cut += 1

        Gammas = [max(gammas[:i_cut]) for gammas in gammass]
        gammas = [min(gammas[:i_cut]) for gammas in gammass]
        chis = [Gamma/gamma for Gamma, gamma in zip(Gammas, gammas)]

        yss = [Gammas, gammas, chis]

        tss = [ts for ys in yss]
        ns = [i for i in xrange(len(yss))]
        labels = [r'$\Gamma$', r'$\gamma$', r'$\chi$']
        xlabel = r'$ct \, \mathrm{[cm]}$'
        ylabel = None
        plot_data(tss, [yss], ns, xlabel, labels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True, watermark=None,
                  marker=None, separate=None, save_name=args.output_file, legend_loc=args.legend_loc, legend_font_size=args.legend_font_size, single_ylabel=ylabel,
                  final_line_is_black=False, ylabels_for_same_line=labels, no_tight_layout=args.no_tight_layout, zorder_reverse=args.z_rev)
        raise SystemExit

    m_bss_spec, r_bss_spec, epsss, dudlnepssss, n_max, eps_bs = read_spc_file(args.INPUT_SPC_FILE, args.n_max, return_eps_bs=True)
    u_gammass = get_u_gammass(dudlnepssss, eps_bs)
    E_gammas = get_E_gammas(u_gammass, r_bss_spec)

    #

    # Fix units
    c = 2.998e+10
    E_gammas = [E_gamma*c*c for E_gamma in E_gammas]

    # betagamma, for finding shocks
    betagammass = [[v/sqrt(1.-v*v) for v in vs] for vs in vss]
    Nss = get_subshock_locations(betagammass)

    for n, Ns in enumerate(Nss):
        if n < args.n_start:
            Nss[n] = [nan, nan]
        # print '%i: %4.4e %4.4e' % (len(Ns), Ns[0], Ns[1])
    Nss = [Ns[:2] for Ns in Nss]

    labels = [None]
    ns = [0]
    xlabel = r'$ct \, \mathrm{[cm]}$'
    ylabel = r'$E \, \mathrm{[erg]}$'

    N_fs = [Ns[1] for Ns in Nss]
    N_rs = [Ns[0] for Ns in Nss]
    E_sh_r = find_shock_power(vss, rhoss, m_bss, r_bss, N_rs)
    E_sh_f = find_shock_power(vss, rhoss, m_bss, r_bss, N_fs)

    Ess = [E_gammas, E_sh_f, E_sh_r]

    tss = [ts for Es in Ess]
    ns = [i for i in xrange(len(Ess))]
    labels = [r'$\mathrm{Radiation}$', r'$\mathrm{Forw. \, shock}$', r'$\mathrm{Rev. \, shock}$']
    plot_data(tss, [Ess], ns, xlabel, labels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, suppress_warning=True, watermark=None,
              marker=None, separate=None, save_name=args.output_file, legend_loc=args.legend_loc, legend_font_size=args.legend_font_size, single_ylabel=ylabel,
              final_line_is_black=False, ylabels_for_same_line=labels, no_tight_layout=args.no_tight_layout, zorder_reverse=args.z_rev)

if __name__ == '__main__':
    main()
