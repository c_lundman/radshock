#!/usr/bin/env python

import os
import sys
import argparse
from matplotlib import pyplot as plt

from plot_snapshots import get_colors, import_mplstyle

def read_tim_file(input_file, n_max=None):
    ks = []
    wts = []
    wt_mhds = []
    wt_rads = []
    cts = []
    iter_mhds = []
    iter_rads = []
    wt_mhd_mpis = []
    wt_rad_mpis = []
    wt_savs = []
    wt_rad_props = []
    wt_rad_pairs = []
    wt_rad_smooths = []
    wt_rad_moms = []

    NUM_PROC = None

    with open(input_file, 'r') as f:
        n = 0
        for line in f.readlines():
            split_line = line.split()

            if split_line[0] == 'NUM_PROC':
                NUM_PROC = int(split_line[2])

            else:
                ks.append(int(split_line[0]))
                wts.append(float(split_line[1]))
                wt_mhds.append(float(split_line[2]))
                wt_rads.append(float(split_line[3]))
                cts.append(float(split_line[4]))
                iter_mhds.append(int(split_line[5]))
                iter_rads.append(int(split_line[6]))

                if len(split_line) > 7:
                    wt_mhd_mpis.append(float(split_line[7]))
                    wt_rad_mpis.append(float(split_line[8]))
                else:
                    wt_mhd_mpis.append(0.)
                    wt_rad_mpis.append(0.)

                if len(split_line) > 9:
                    wt_savs.append(float(split_line[9]))
                else:
                    wt_savs.append(0.)

                if len(split_line) > 13:
                    wt_rad_props.append(float(split_line[10]))
                    wt_rad_pairs.append(float(split_line[11]))
                    wt_rad_smooths.append(float(split_line[12]))
                    wt_rad_moms.append(float(split_line[13]))
                else:
                    wt_rad_props.append(0.)
                    wt_rad_pairs.append(0.)
                    wt_rad_smooths.append(0.)
                    wt_rad_moms.append(0.)
                n += 1
                if n_max:
                    if n == n_max:
                        break

    return NUM_PROC, ks, wts, wt_mhds, wt_rads, cts, iter_mhds, iter_rads, wt_mhd_mpis, wt_rad_mpis, wt_savs, wt_rad_props, wt_rad_pairs, wt_rad_smooths, wt_rad_moms

def plot_data(xs, yss, xlabel, ylabels, xlog=False, ylog=True, xlims=None, ylims=None, watermark=False, legend_loc=1, no_show=False, extrapolate=None, marker=None, print_to_screen=None, save_name=None):
    import_mplstyle('radshock.mplstyle')

    if watermark:
        fig, ax = plt.subplots()
        plt.text(.05, .025, watermark, ha='left', va='bottom', fontsize=40, color='k', alpha=.1, rotation=0, transform=ax.transAxes)

    for ys, ylabel in zip(yss, ylabels):
        if len(ylabels) > 1:
            label = ylabel
        else:
            label=None
        if extrapolate:
            N = 200
            k = (ys[-1]-ys[-2])/(xs[-1]-xs[-2])
            x_ext = extrapolate
            dx = (x_ext-xs[-1])/float(N-1)
            xs_ext = [xs[-1] + dx*i for i in xrange(N)]
            ys_ext = [ys[-1] + k*(x - xs[-1]) for x in xs_ext]
            remaining = ys_ext[-1] - ys[-1]

            plt.plot(xs_ext, ys_ext, ls=':', linewidth=1.75, alpha=.5)
        if print_to_screen:
            print 'n = %i' % (len(xs))
            print 'x:', ['%4.4e' % (x) for x in xs]
            print 'y:', ['%4.4e' % (y) for y in ys]
            raise SystemExit
        plt.plot(xs, ys, marker=marker, markersize=10, label=label)

    if len(ylabels) == 1 and extrapolate:
        ylabels[0] += r'$\, (\mathrm{difference} \, \approx %3.1f)$' % (remaining)

    if not extrapolate:
        plt.xlim(min(xs), max(xs))
    else:
        plt.xlim(min(xs), extrapolate)
    plt.xlabel(xlabel)
    if len(ylabels) == 1:
        plt.ylabel(ylabels[0])
    else:
        plt.legend(loc=legend_loc)

    if ylog:
        plt.yscale('log')
        ymin = 1e60
        ymin_trial = min([min(ys) for ys in yss])
        if ymin_trial < ymin:
            ymin = ymin_trial
        ymax = 1e-60
        ymax_trial = max([max(ys) for ys in yss])
        if ymax_trial > ymax:
            ymax = ymax_trial
        if ymin > 0:
            R = ymax/ymin
            if len(ylabels) > 1:
                plt.ylim(ymin/(R**(1./10.)), ymax*(R**(1./2.)))
            else:
                plt.ylim(ymin/(R**(1./10.)), ymax*(R**(1./10.)))

    if xlog:
        plt.xscale('log')

    if xlims:
        plt.xlim(xlims[0], xlims[1])
    if ylims:
        plt.ylim(ylims[0], ylims[1])
    # else:
    #     if not ylog:
    #         plt.ylim(bottom=0)

    plt.tight_layout()
    if save_name:
        plt.savefig(save_name)
    elif not no_show:
        plt.show()
    plt.close()

def main():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('INPUT_TIM_FILE', help='the input TIM file (.tim)')
    parser.add_argument('VAR', nargs='+', help='the variable(s) to plot')
    parser.add_argument('-p', '--show-all-vars', action='store_true', help='prints a list of all variables and exit')
    parser.add_argument('-o', '--output-pdf-file', help='the output pdf file')
    parser.add_argument('-x', '--x-coordinate', help='the horizontal coordinate, which can be any of the variables [default simply shows snapshot number]')
    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    parser.add_argument('--days', action='store_true', help='changes wall time units to days')
    parser.add_argument('--hours', action='store_true', help='changes wall time units to hours [default]')
    parser.add_argument('--minutes', action='store_true', help='changes wall time units to minutes')
    parser.add_argument('--seconds', action='store_true', help='changes wall time units to seconds')
    parser.add_argument('--extrapolate', type=float, help='extrapolates the y-values linearly to this x-value')
    parser.add_argument('--marker', action='store_true', help='plots markers')
    parser.add_argument('--preliminary', action='store_true', help='adds a "preliminary" watermark to the plot')
    parser.add_argument('-tx', '--toggle-xlog', action='store_true', help='turn ON log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_true', help='turn ON log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--legend-loc', type=int, help='legend location [default=1]')
    parser.add_argument('--no-plot', action='store_true', help='does NOT show the plot')
    parser.add_argument('--print-values', action='store_true', help='instead, prints the x and y values to screen and exits')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    # Load the file
    data = read_tim_file(args.INPUT_TIM_FILE, args.n_max)
    NUM_PROC, ks, wts, wt_mhds, wt_rads, cts, iter_mhds, iter_rads, wt_mhd_mpis, wt_rad_mpis, wt_savs, wt_rad_props, wt_rad_pairs, wt_rad_smooths, wt_rad_moms = data

    # Print information
    if not args.quiet and not args.show_all_vars:
        if NUM_PROC != None:
            print 'File contains %i snapshots (ran on %i threads)' % (len(ks), NUM_PROC)
        else:
            print 'File contains %i snapshots' % (len(ks))

    # Make new data products
    Dcts = [ct-cts[0] for ct in cts]
    Diter_mhds = [i-iter_mhds[0] for i in iter_mhds]
    Diter_rads = [i-iter_rads[0] for i in iter_rads]

    # All data products, labels, names, infos
    wts = [wt/60./60./24. for wt in wts]
    wt_mhds = [wt/60./60./24. for wt in wt_mhds]
    wt_rads = [wt/60./60./24. for wt in wt_rads]
    wt_mhd_mpis = [wt/60./60./24. for wt in wt_mhd_mpis]
    wt_rad_mpis = [wt/60./60./24. for wt in wt_rad_mpis]
    wt_savs = [wt/60./60./24. for wt in wt_savs]

    labels = [r'$n$',
              r'$ct \, \mathrm{[cm]}$',
              r'$t_{tot} \, \mathrm{[days]}$',
              r'$t_{mhd} \, \mathrm{[days]}$',
              r'$t_{rad} \, \mathrm{[days]}$',
              r'$t_{mhd}^\mathrm{MPI} \, \mathrm{[days]}$',
              r'$t_{rad}^\mathrm{MPI} \, \mathrm{[days]}$',
              r'$t_{sav} \, \mathrm{[days]}$',
              r'$\mathrm{iterations \, (MHD)}$',
              r'$\mathrm{iterations \, (RAD)}$',
              r'$\Delta \, ct \, \mathrm{[cm]}$',
              r'$\mathrm{\Delta \, iterations \, (MHD)}$',
              r'$\mathrm{\Delta \, iterations \, (RAD)}$',
              r'$\delta t_{tot} \, \mathrm{[days]}$',
              r'$\delta t_{mhd} \, \mathrm{[days]}$',
              r'$\delta t_{rad} \, \mathrm{[days]}$',
              r'$\delta t_{mhd}^\mathrm{MPI} \, \mathrm{[days]}$',
              r'$\delta t_{rad}^\mathrm{MPI} \, \mathrm{[days]}$']

    # Did the user specify what time units to use?
    if not any([args.days, args.hours, args.minutes, args.seconds]):
        if args.VAR[0] == 'dwt':
            args.minutes = True
            if not args.y_lims and not args.toggle_ylog:
                args.y_lims = [0, None]
        else:
            args.hours = True

    # Change time format?
    if args.seconds:
        wts = [wt*24.*60.*60. for wt in wts]
        wt_mhds = [wt*24.*60.*60. for wt in wt_mhds]
        wt_rads = [wt*24.*60.*60. for wt in wt_rads]
        wt_mhd_mpis = [wt*24.*60.*60. for wt in wt_mhd_mpis]
        wt_rad_mpis = [wt*24.*60.*60. for wt in wt_rad_mpis]
        wt_savs = [wt*24.*60.*60. for wt in wt_savs]
        labels[2] = r'$t_{tot} \, \mathrm{[s]}$'
        labels[3] = r'$t_{mhd} \, \mathrm{[s]}$'
        labels[4] = r'$t_{rad} \, \mathrm{[s]}$'
        labels[5] = r'$t_{mhd}^\mathrm{MPI} \, \mathrm{[s]}$'
        labels[6] = r'$t_{rad}^\mathrm{MPI} \, \mathrm{[s]}$'
        labels[7] = r'$t_{sav} \, \mathrm{[s]}$'
        labels[13] = r'$\delta t_{tot} \, \mathrm{[s]}$'
        labels[14] = r'$\delta t_{mhd} \, \mathrm{[s]}$'
        labels[15] = r'$\delta t_{rad} \, \mathrm{[s]}$'
        labels[16] = r'$\delta t_{mhd}^\mathrm{MPI} \, \mathrm{[s]}$'
        labels[17] = r'$\delta t_{rad}^\mathrm{MPI} \, \mathrm{[s]}$'

    elif args.minutes:
        wts = [wt*24.*60. for wt in wts]
        wt_mhds = [wt*24.*60. for wt in wt_mhds]
        wt_rads = [wt*24.*60. for wt in wt_rads]
        wt_mhd_mpis = [wt*24.*60. for wt in wt_mhd_mpis]
        wt_rad_mpis = [wt*24.*60. for wt in wt_rad_mpis]
        wt_savs = [wt*24.*60. for wt in wt_savs]
        labels[2] = r'$t_{tot} \, \mathrm{[min]}$'
        labels[3] = r'$t_{mhd} \, \mathrm{[min]}$'
        labels[4] = r'$t_{rad} \, \mathrm{[min]}$'
        labels[5] = r'$t_{mhd}^\mathrm{MPI} \, \mathrm{[min]}$'
        labels[6] = r'$t_{rad}^\mathrm{MPI} \, \mathrm{[min]}$'
        labels[7] = r'$t_{sav} \, \mathrm{[min]}$'
        labels[13] = r'$\delta t_{tot} \, \mathrm{[min]}$'
        labels[14] = r'$\delta t_{mhd} \, \mathrm{[min]}$'
        labels[15] = r'$\delta t_{rad} \, \mathrm{[min]}$'
        labels[16] = r'$\delta t_{mhd}^\mathrm{MPI} \, \mathrm{[min]}$'
        labels[17] = r'$\delta t_{rad}^\mathrm{MPI} \, \mathrm{[min]}$'

    elif args.hours:
        wts = [wt*24. for wt in wts]
        wt_mhds = [wt*24. for wt in wt_mhds]
        wt_rads = [wt*24. for wt in wt_rads]
        wt_mhd_mpis = [wt*24. for wt in wt_mhd_mpis]
        wt_rad_mpis = [wt*24. for wt in wt_rad_mpis]
        wt_savs = [wt*24. for wt in wt_savs]
        labels[2] = r'$t_{tot} \, \mathrm{[h]}$'
        labels[3] = r'$t_{mhd} \, \mathrm{[h]}$'
        labels[4] = r'$t_{rad} \, \mathrm{[h]}$'
        labels[5] = r'$t_{mhd}^\mathrm{MPI} \, \mathrm{[h]}$'
        labels[6] = r'$t_{rad}^\mathrm{MPI} \, \mathrm{[h]}$'
        labels[7] = r'$t_{sav} \, \mathrm{[h]}$'
        labels[13] = r'$\delta t_{tot} \, \mathrm{[h]}$'
        labels[14] = r'$\delta t_{mhd} \, \mathrm{[h]}$'
        labels[15] = r'$\delta t_{rad} \, \mathrm{[h]}$'
        labels[16] = r'$\delta t_{mhd}^\mathrm{MPI} \, \mathrm{[h]}$'
        labels[17] = r'$\delta t_{rad}^\mathrm{MPI} \, \mathrm{[h]}$'

    # Making some additional data products
    dwts = [wts[i+1]-wts[i] for i in xrange(len(wts)-1)]
    dwt_mhds = [wt_mhds[i+1]-wt_mhds[i] for i in xrange(len(wt_mhds)-1)]
    dwt_rads = [wt_rads[i+1]-wt_rads[i] for i in xrange(len(wt_rads)-1)]
    dwt_mhd_mpis = [wt_mhd_mpis[i+1]-wt_mhd_mpis[i] for i in xrange(len(wt_mhd_mpis)-1)]
    dwt_rad_mpis = [wt_rad_mpis[i+1]-wt_rad_mpis[i] for i in xrange(len(wt_rad_mpis)-1)]
    dwts.insert(0, None)
    dwt_mhds.insert(0, None)
    dwt_rads.insert(0, None)
    dwt_mhd_mpis.insert(0, None)
    dwt_rad_mpis.insert(0, None)

    # Collecting data products
    names = ['n', 'ct', 'wt', 'wt_mhd', 'wt_rad', 'wt_mhd_mpi', 'wt_rad_mpi', 'wt_sav', 'i_mhd', 'i_rad',
             'Dct', 'Di_mhd', 'Di_rad', 'dwt', 'dwt_mhd', 'dwt_rad', 'dwt_mhd_mpi', 'dwt_rad_mpi']
    infos = ['n            snapshot number',
             'ct           lab frame simulation time',
             'wt           wall time duration of the simulation',
             'wt_mhd       wall time duration of the MHD part of the simulation',
             'wt_rad       wall time duration of the RAD part of the simulation',
             'wt_mhd_mpi   wall time duration of the MHD MPI part of the simulation',
             'wt_rad_mpi   wall time duration of the RAD MPI part of the simulation',
             'wt_sav       wall time duration of saving data',
             'i_mhd        number of MHD iterations',
             'i_rad        number of RAD iterations',
             'Dct          lab frame simulation time minus the first element (D for Delta)',
             'Di_mhd       number of MHD iterations minus the first element',
             'Di_rad       number of RAD iterations minus the first element',
             'dwt          wall time duration since last snapshot (d for delta)',
             'dwt_mhd      wall time duration of the MHD part of the simulation since last snapshot',
             'dwt_rad      wall time duration of the RAD part of the simulation since last snapshot',
             'dwt_mhd_mpi  wall time duration of the MHD MPI part of the simulation since last snapshot',
             'dwt_rad_mpi  wall time duration of the RAD MPI part of the simulation since last snapshot']
    data = ks, cts, wts, wt_mhds, wt_rads, wt_mhd_mpis, wt_rad_mpis, wt_savs, iter_mhds, iter_rads, Dcts, Diter_mhds, Diter_rads, dwts, dwt_mhds, dwt_rads, dwt_mhd_mpis, dwt_rad_mpis

    # Printing variables?
    if args.show_all_vars:
        print 'pick variables (and the x-coordinate) from:'
        for info in infos:
            print '  '+info
        raise SystemExit

    # Picking the x-coordinate
    if args.x_coordinate in names:
        i = names.index(args.x_coordinate)
        xs = data[i]
        xlabel = labels[i]
    elif args.x_coordinate == None:
        xs = ks
        xlabel = r'$\mathrm{Snapshot \, number}$'
    else:
        print 'error: the x-coordinate is invalid'
        raise SystemExit

    # Picking y-coordiate(s)
    yss = []
    ylabels = []
    for var in args.VAR:
        if var in names:
            i = names.index(var)
            yss.append(data[i])
            ylabels.append(labels[i])
        else:
            print "error: invalid variable choice (%s)" % (var)
            raise SystemExit

    # Watermark and markers
    if args.preliminary:
        watermark = r'$\mathrm{PRELIMINARY}$'
    else:
        watermark = None
    if args.marker:
        marker = '.'
    else:
        marker = None

    # Plotting
    plot_data(xs, yss, xlabel, ylabels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, watermark=watermark, no_show=args.no_plot, extrapolate=args.extrapolate, marker=marker, legend_loc=args.legend_loc, print_to_screen=args.print_values, save_name=args.output_pdf_file)

if __name__ == '__main__':
    main()
