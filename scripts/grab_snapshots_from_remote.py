#!/usr/bin/env python

# import subprocess
import os
import argparse
from getpass import getpass
from datetime import date, datetime
def main():

    # Defaults
    default_user = 'chlu3235'
    # default_remote = 'rama21.astro.su.se'
    default_remote = 'krishna03.astro.su.se'

    # Parser
    parser = argparse.ArgumentParser(description='a script for copying simulation output from a remote location to the local computer (or in the opposite direction). will ask the user for the password. the simulation output is automatically put into the correct folder.')
    parser.add_argument('PROJ_NAME', help='the name of the project folder')
    parser.add_argument('SNAP_NAME', help='the name of the saved snapshots (e.g. SNAP_NAME.mhd)')
    parser.add_argument('--min', action='store_true', help='grabs only [tim, mhd]')
    parser.add_argument('--min-rad', action='store_true', help='grabs only [tim, mhd, pre]')
    parser.add_argument('--all', action='store_true', help='grabs all output files, instead of just [mhd, pre, ugl, cte, zpm, tim]')
    parser.add_argument('--state', action='store_true', help='grabs state files called SNAP_NAME.msf, SNAP_NAME.rsf instead')
    parser.add_argument('--spc', action='store_true', help='grabs the spectrum files called SNAP_NAME.spc, SNAP_NAME.ispc instead')
    parser.add_argument('--mhd', action='store_true', help='grabs the file SNAP_NAME.mhd instead')
    parser.add_argument('--pre', action='store_true', help='grabs the file SNAP_NAME.pre instead')
    parser.add_argument('--tim', action='store_true', help='grabs the file SNAP_NAME.tim instead')
    parser.add_argument('-f', '--force', action='store_true', help='always overwrite simulation output with same name')
    parser.add_argument('--cheat', action='store_true', help='?')
    parser.add_argument('--is-test', action='store_true', help='looks for the project folder in the test/ folder instead')
    parser.add_argument('--push', action='store_true', help='instead copies from the local to the remote')
    parser.add_argument('--user', default=default_user, help='the user [default='+default_user+']')
    parser.add_argument('--remote', default=default_remote, help='the remote [default='+default_remote+']')
    args = parser.parse_args()

    # Find project folder
    if args.is_test:
        p_folder = 'tests'
    else:
        p_folder = 'projects'

    # Is the script ran within the radshock folder structure?
    full_path = os.getcwd()[1:]
    if not 'radshock' in full_path.split('/'):
        print "This script has to be run from somewhere (anywhere) inside the 'radshock' folder structure."
        print "Exiting..."
        raise SystemExit

    # Get the password
    if not args.cheat:
        password = getpass()
    else:
        password = 'ifz'+'thog'+'ZiR83'

    # Find output location
    for i, folder in enumerate(full_path.split('/')):
        if folder == 'radshock':
            break
    output_location = ''
    for folder in full_path.split('/')[:i+1]:
        output_location += '/'+folder

    # Copy files
    file_formats = ['mhd', 'pre', 'ugl', 'cte', 'zpm', 'zet', 'xis', 'tim']
    snapshot_location = 'snapshot_files/'
    if args.min:
        file_formats = ['tim', 'mhd']
    elif args.min_rad:
        file_formats = ['tim', 'mhd', 'pre']
    elif args.pre:
        file_formats = ['pre']
    elif args.all:
        file_formats += ['g0s', 'g1s', 'i0s', 'i1s', 'ispc', 'spc']
    elif args.state:
        file_formats = ['msf', 'rsf']
        snapshot_location = 'state_files/'
    elif args.spc:
        file_formats = ['spc', 'ispc']
    elif args.tim:
        file_formats = ['tim']
    elif args.mhd:
        file_formats = ['mhd']

    output_location += '/'+p_folder+'/'+args.PROJ_NAME+'/'+snapshot_location
    input_location = args.user+'@'+args.remote+':/home/'+args.user+'/radshock/'+p_folder+'/'+args.PROJ_NAME+'/'+snapshot_location

    # Print current time
    now = datetime.now()
    today = date.today()
    current_time = now.strftime("%H:%M:%S")
    print 'Grabbing data on '+str(today)+' ['+current_time+']'

    file_names = [args.SNAP_NAME+'.'+file_format for file_format in file_formats]

    for file_name in file_names:
        if not args.push:

            if file_name == file_names[0] and os.path.isfile(output_location+file_name) and not args.force:
                print "  Simulation output with the same name already exists inside '"+output_location+"'"
                answer = raw_input("  Overwrite? [y/n]: ")
                if answer.lower() not in ["y", "yes"]:
                    print "Exiting..."
                    raise SystemExit

            print "Copying '"+input_location+file_name+"' to local..."
            os.system("sshpass -p "+password+" scp -o StrictHostKeyChecking=no "+input_location+file_name+" "+output_location)

        else:

            if file_name == file_names[0]:
                print "  Copying to remote! This will overwrite any remote files with the same name."
                answer = raw_input("  Proceed? [y/n]: ")
                if answer.lower() not in ["y", "yes"]:
                    print "Exiting..."
                    raise SystemExit

            print "Copying '"+output_location+file_name+"' to remote..."
            os.system("sshpass -p "+password+" scp -o StrictHostKeyChecking=no "+output_location+file_name+" "+input_location)

if __name__ == '__main__':
    main()
