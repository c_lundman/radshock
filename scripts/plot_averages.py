#!/usr/bin/env python

import argparse
from numpy import sqrt, nan, log10, exp
from plot_snapshots import get_xss_ysss_and_labels, get_ns, plot_data
from shock_finder import find_shocks

#

m_p = 1.6726231e-24
sigma_T = 6.6524e-25
kappa_T = sigma_T/m_p

def get_power_law(x_p, y_p, index, xs):
    # Return a power law, with the index "index" and a normalization y_p at the location x_p. Used to quickly see power law indices in plots.

    W = .25*log10(xs[-1]/xs[0])
    xmin = x_p*10**(-W/2)
    xmax = x_p*10**(W/2)

    A = y_p/x_p**index
    ys = []
    for x in xs:
        if xmin <= x <= xmax:
            ys.append(A*x**index)
        else:
            ys.append(nan)
    return ys

def get_average(x_bs, fs):
    # Computes the average <f> = int f(x) dx / int dx, where x is typically m (-> mass average) or r (-> volumetric average, if planar geometry or if r >> dr)
    x_tot = x_bs[-1]-x_bs[0]
    integral = 0.
    for i in xrange(len(fs)):
        dx = x_bs[i+1]-x_bs[i]
        integral += fs[i]*dx
    f_avg = integral/x_tot
    return f_avg

def get_averages(x_bss, fss):
    return [get_average(x_bs, fs) for x_bs, fs in zip(x_bss, fss)]

def get_averaged_values(args, ts, xi_bss, rss, rhoss, gammass, ysss, ylabels):
    # Find x-coordinate
    if args.x_coordinate == 't':
        c = 2.99e10
        xs = [t/c for t in ts]
        xlabel = r'$t \, \mathrm{[s]}$'
    elif args.x_coordinate == 'ct':
        xs = ts
        xlabel = r'$ct \, \mathrm{[cm]}$'
    elif args.x_coordinate == 'r':
        xs = get_averages(xi_bss, rss)
        xlabel = r'$<r> \, \mathrm{[cm]}$'
    elif args.x_coordinate == 'tau':
        print 'WARNING: this definition of <tau> probably does not make sense...'
        tauss = [[kappa_T*rho*r/gamma for rho, r, gamma in zip(rhos, rs, gammas)] for rhos, rs, gammas in zip(rhoss, rss, gammass)]
        xs = get_averages(xi_bss, tauss)
        xlabel = r'$<\tau>$'
    elif args.x_coordinate == 'tau_inv':
        print 'WARNING: this definition of <tau> probably does not make sense...'
        tauss = [[kappa_T*rho*r/gamma for rho, r, gamma in zip(rhos, rs, gammas)] for rhos, rs, gammas in zip(rhoss, rss, gammass)]
        tauss_inv = [[1./tau for tau in taus] for taus in tauss]
        xs = get_averages(xi_bss, tauss_inv)
        xlabel = r'$<R_\star/r>$'
    elif args.x_coordinate == 'n':
        xs = range(len(ts))
        xlabel = r'$n$'

    # Find mass-averaged y-values
    if not args.min and not args.max:
        yss_avg = []
        for yss in ysss:
            ys_avg = get_averages(xi_bss, yss)
            yss_avg.append(ys_avg)

        # Add mass-average symbols to y-labels
        ylabels_new = []
        for ylabel in ylabels:
            if ylabel.find('\,') > 0:
                i = ylabel.find(r'\,') - 1
                ylabel_new = r'$<$' + ylabel[:i] + r'>' + ylabel[i:]
            else:
                ylabel_new = r'$<$' + ylabel + r'$>$'
            ylabels_new.append(ylabel_new)

    else:
        yss_avg = []
        for yss in ysss:
            if args.max:
                ys_avg = [max(ys) for ys in yss]
            else:
                ys_avg = [min(ys) for ys in yss]
            yss_avg.append(ys_avg)

        ylabels_new = []
        for ylabel in ylabels:
            if args.max:
                pre_string = r'$\mathrm{max}($'
            else:
                pre_string = r'$\mathrm{min}($'
            if ylabel.find('\,') > 0:
                i = ylabel.find(r'\,') - 1
                ylabel_new = pre_string + ylabel[:i] + r')' + ylabel[i:]
            else:
                ylabel_new = pre_string + ylabel + r'$)$'
            ylabels_new.append(ylabel_new)

    return xs, yss_avg, xlabel, ylabels_new

#

def get_r_sh(Ns, rss):
    r_shs = [rs[int(N)] for N, rs in zip(Ns, rss)]
    return [r_shs], [r'$r_{sh}$']

def get_v_sh(Ns, rss, ts):
    [r_shs], _ = get_r_sh(Ns, rss)
    v_shs = [0.]
    for i in xrange(len(ts)-1):
        dr = r_shs[i+1]-r_shs[i]
        dt = ts[i+1]-ts[i]
        v_sh = dr/dt
        v_shs.append(v_sh)
    return [v_shs], [r'$v_{sh}$']

def get_dv_u(Ns, rss, uss, ts, W):
    [v_shs], _ = get_v_sh(Ns, rss, ts)
    dv_us = []
    dv_ds = []
    dv_uds = []

    for v_sh, us, N in zip(v_shs, uss, Ns):
        u_u = us[int(N+W/2)]
        v_u = u_u/sqrt(1. + u_u*u_u)
        dv_u = (v_u - v_sh)/(1. - v_u*v_sh)
        dv_us.append(dv_u)

        u_d = us[int(N-W/2)]
        v_d = u_d/sqrt(1. + u_d*u_d)
        dv_d = (v_d - v_sh)/(1. - v_d*v_sh)
        dv_ds.append(dv_d)

        dv_ud = sqrt((v_d - v_u)*(v_d - v_u))/(1. - v_u*v_d)
        dv_uds.append(dv_ud)
    return [dv_us, dv_ds, dv_uds], [r'$\delta v_u$', r'$\delta v_d$', r'$\delta v_{ud}$']

#

def main():
    parser = argparse.ArgumentParser(description='a script that plots mass-averaged quantities versus t, r or tau')
    parser.add_argument('INPUT_MHD_FILE', help='the input MHD snapshots file (.mhd)')
    parser.add_argument('VAR', nargs='+', help='the variable(s) to plot (at most 3 variables)')
    parser.add_argument('-o', '--output-file', help='the output pdf')
    parser.add_argument('-x', '--x-coordinate', default='r', help='the horizontal coordinate (t, ct, r, tau) [default=r]')
    parser.add_argument('--min', action='store_true', help='instead of average, plot the minimum value on the grid')
    parser.add_argument('--max', action='store_true', help='instead of average, plot the maximum value on the grid')
    parser.add_argument('--rms', action='store_true', help='instead of average, find RMS and plot ... (?)')
    parser.add_argument('--xi-best-fit', action='store_true', help='plot xi_best_fit (for Filips project)')
    # parser.add_argument('-v', '--volumetric', action='store_true', help='plots volumetric average instead of mass-average (for instance, real L_gamma_tot is volumetric average of L_gamma)')
    parser.add_argument('-m', '--mass-average', action='store_true', help='plots mass-average instead of volumetric average')
    parser.add_argument('-pl', '--power-law', type=float, nargs='+', help='also plots a power law. give either INDEX, or INDEX, X_P, or INDEX, X_P, Y_P.')
    parser.add_argument('-n', type=int, nargs='+', help='the snapshot(s) to plot')
    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    parser.add_argument('--keep-nth', type=int, help="keep only every n'th snapshot (always keeps the first and last snapshots)")
    parser.add_argument('--sample', action='store_true', help='plots a nice sample of snapshots (roughly 15, evenly spaced, including n = 0 and -1)')
    parser.add_argument('--all', action='store_true', help='ignores --sample and plots all snapshots')
    parser.add_argument('--plot-range', nargs=2, type=int, help='plots all snapshots in this range')
    parser.add_argument('--plot-after', type=int, help='plots all snapshots after this snapshot')
    parser.add_argument('--plot-before', type=int, help='plots all snapshots before this snapshot')
    parser.add_argument('-fl', '--flat-line', action='store_true', help='extrapolate a flat line from the first data point (to check how much the variable varies)')
    # parser.add_argument('--separate', action='store_true', help='shifts each next line upwards by a factor 2, so that lines overlap less (for increased visibility)')
    # parser.add_argument('--track', type=int, help='plots a circle at the given spatial bin')
    # parser.add_argument('--zero-line', action='store_true', help='draws a line at y = 0')
    # parser.add_argument('--colored-final-line', action='store_true', help='makes the final snapshot line colored (as opposed to black)')
    # parser.add_argument('--cmap', help='the color map to use for all plots')
    parser.add_argument('--v-shift', type=float, default=0, help='shift the r-coordinate by this speed (r -> r + v*(t-t_i))')
    parser.add_argument('--marker', action='store_true', help='plots markers')
    parser.add_argument('--preliminary', action='store_true', help='adds a "preliminary" watermark to the plot')
    parser.add_argument('--z-rev', action='store_true', help='reverses the z-order of the lines (i.e. which lines cover each other)')
    parser.add_argument('--show-ghosts', action='store_true', help='also show ghost cells in the plot')
    parser.add_argument('-tx', '--toggle-xlog', action='store_false', help='turn OFF log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_false', help='turn OFF log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--abs', action='store_true', help='plots the absolute values')
    parser.add_argument('--legend-loc', type=int, help='legend location [default=1]')
    # parser.add_argument('--find-shocks', action='store_true', help='attempts to find discontinuities and plots markers in the immediate up- and downstreams')
    # parser.add_argument('--legend-times', action='store_true', help='show snapshot time (ct) in the legend (only for single VAR)')
    # parser.add_argument('--legend-short', action='store_true', help='show only first and last legend items (only for single VAR)')
    parser.add_argument('--legend-font-size', type=int, default=24, help='legend font size [default=24]')
    parser.add_argument('--legend-labels-in-plot', nargs='+', type=float, help='labels are put next to the plot lines instead of in the legend; provide an x-coordinate per label')
    parser.add_argument('--legend-labels-in-plot-no-rotation', action='store_true', help='labels in the plot will not be rotated')
    parser.add_argument('--no-tight-layout', action='store_true', help="don't use tight_layout()")
    # parser.add_argument('--multiple-panels', action='store_true', help="plots each VAR in a separate panel")
    # parser.add_argument('--multiple-y-lims', nargs='+', type=float, help="limits on the y-axis for each panel")
    # parser.add_argument('--multiple-cmaps', nargs='+', type=str, help="color maps for each panel")
    parser.add_argument('--cgs', action='store_true', help='use cgs units (for the pressure: p -> p*c^2)')
    # parser.add_argument('--fps', type=int, default=30, help='movie fps [default=30]')
    # parser.add_argument('--no-plot', action='store_true', help='does NOT show the plot')
    # parser.add_argument('--print-times', action='store_true', help='instead, print timing information')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    # Check number of variables
    if len(args.VAR) > 3:
        print '  ERROR: Too many variables passed, can handle at most three (3).'
        print '         Exiting...'
        raise SystemExit

    if args.rms:
        args.VAR = ['u']

    # Get variables to plot
    xss, ysss, xlabel, ylabels, ts, m_bss, r_bss, rss, rhoss, gammass, marker, watermark = get_xss_ysss_and_labels(args)

    # Convert to mass-average plot
    if args.mass_average:
        xs, yss, xlabel, ylabels = get_averaged_values(args, ts, m_bss, rss, rhoss, gammass, ysss, ylabels)
    else:
        xs, yss, xlabel, ylabels = get_averaged_values(args, ts, r_bss, rss, rhoss, gammass, ysss, ylabels)

    # Plot RMS properties?
    if args.rms:
        # Calibrated for betagamma
        dAA_lim = .015
        W = 300

        Ns = []
        uss = ysss[0]
        for us in uss:
            Ns_sh = find_shocks(us, dAA_min=dAA_lim, dAA_max=dAA_lim, W=W)
            if len(Ns_sh):
                Ns.append(Ns_sh[0])
            else:
                Ns.append(0)

        # yss, ylabels = get_r_sh(Ns, rss)
        # yss, ylabels = get_v_sh(Ns, rss, ts)
        yss, ylabels = get_dv_u(Ns, rss, uss, ts, W)

    # Is x-axis logscale, while x has zero or negative values?
    if args.toggle_xlog:
        if min(xs) <= 0:
            print "  ERROR: Can't plot logarithmic x-axis with negative (or zero) x-values"
            print "         Exiting..."
            raise SystemExit

    # Add flat lines?
    if args.flat_line:
        for i in xrange(len(yss)):
            ys = yss[i]
            ys_flat = [ys[0] for y in ys]
            yss.append(ys_flat)
            ylabels.append(ylabels[i] + r'$\, \mathrm{(flat)}$')

    # Add a power law?
    if args.power_law:
        if len(args.power_law) not in [1, 2, 3]:
            print 'Error: need to provide one, two or three arguments to --power-law'
            raise SystemExit
        if len(args.power_law) == 3:
            index, x_p, y_p = args.power_law
        elif len(args.power_law) == 2:
            index, x_p = args.power_law
            y_p = sqrt(min(yss[0])*max(yss[0]))
        elif len(args.power_law) == 1:
            index = args.power_law[0]
            x_p = sqrt(xs[0]*xs[-1])
            y_p = sqrt(min(yss[0])*max(yss[0]))
        ys = get_power_law(x_p, y_p, index, xs)
        x_coord_str = xlabel[1:xlabel.find('\,')]
        ylabel = r'$\mathrm{PL} \, \propto \, %s^{%2.2f}$' % (x_coord_str, index)
        yss.append(ys)
        ylabels.append(ylabel)

    # Add flat lines?
    if args.xi_best_fit:
        taus = [1./x for x in xs]
        xis = [1.34e-4*(tau**(2./3.) + .8) for tau in taus]
        # xis = [3e-4*(tau**(2./3.) + .2) for tau in taus]

        tau_br = 3e0
        alpha = 0.
        beta = 2./3.
        beta = .6
        A = 4e-5
        xis = []
        for tau in taus:
            # if tau < tau_br:
            #     xi = A*(tau**alpha)*exp(-(alpha-beta)*tau/tau_br)
            # else:
            #     xi = A*(tau_br**alpha)*exp(-(alpha-beta))*((tau/tau_br)**beta)
            xi = A*exp(beta*tau/tau_br) if tau < tau_br else A*exp(beta)*(tau/tau_br)**beta
            xis.append(xi)
        yss.append(xis)
        ylabels.append(r'$\xi$')

    #

    ns = range(0, len(yss))
    ylabels_for_same_line = ylabels if len(ns) > 1 else None

    xss = [xs for ys in yss]
    ysss = [yss]
    plot_data(xss, ysss, ns, xlabel, ylabels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims, watermark=watermark,
              marker=marker, legend_loc=args.legend_loc, legend_font_size=args.legend_font_size, zero_line=False,
              final_line_is_black=False, ylabels_for_same_line=ylabels_for_same_line, zorder_reverse=args.z_rev, cmaps=None,
              no_tight_layout=args.no_tight_layout, save_name=args.output_file, find_all_shocks=False, multiple_panels=False, multiple_ylims=False,
              legend_labels_in_plot=args.legend_labels_in_plot, no_rotation_of_text=args.legend_labels_in_plot_no_rotation)

if __name__ == '__main__':
    main()
