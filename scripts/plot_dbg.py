#!/usr/bin/env python

import argparse
# from plot_snapshots import get_xss_ysss_and_labels, get_ns, plot_data
from plot_snapshots import get_ns, plot_data, get_periodic_ysss
from numpy import nan, sqrt

#

# m_p = 1.6726231e-24
# sigma_T = 6.6524e-25
# kappa_T = sigma_T/m_p

def load_dbg_file(filename):
    yss = []
    with open(filename, 'r') as f:
        for line in f.readlines():
            ys = [float(l) for l in line.split()]
            yss.append(ys)
    return yss

def get_xss_from_ysss(ysss):
    xs = []
    for yss in ysss:
        for y in yss[0]:
            if not xs:
                xs.append(0)
            else:
                xs.append(xs[-1]+1)

    ysss_new = []
    # for each input file (i gives file index)
    for i, yss in enumerate(ysss):

        yss_new = []
        # for each time snapshot
        for ys in yss:

            ys_new = []
            for j in xrange(len(ysss)):
                if i == j:
                    ys_new += ys
                else:
                    ys_new += [nan for y in ys]
            yss_new.append(ys_new)
        ysss_new.append(yss_new)

    xss = [xs for ys in ysss[0]]
    return xss, ysss_new

#

def main():
    parser = argparse.ArgumentParser(description='a script that plots mass-averaged quantities versus t, r or tau')
    parser.add_argument('INPUT_DBG_FILE', nargs='+', help='the input debugging snapshots file (.dbg)')
    parser.add_argument('-u', '--make-gammabeta', action='store_true', help='assumes that the data is beta = v/c, and plots gamma*beta')
    parser.add_argument('--pad', action='store_true', help='adds some space around the grid')
    parser.add_argument('--overlap', action='store_true', help='plots all files on top of each other')
    parser.add_argument('--check-periodicity', nargs='?', type=int, default=0, const=-1, help='splits the grid at the center, and connects the ends, to find noticable features at the (supposedly periodic) boundaries. adds a red where the grid is reconnected. the optional argument (W) sets the x-axis limits as centered at +-W/2 grid points about the red line.')
    parser.add_argument('-o', '--output-file', help='the output pdf')
    parser.add_argument('-x', '--x-coordinate', default='r', help='the horizontal coordinate (t, ct, r, tau) [default=r]')
    parser.add_argument('--min', action='store_true', help='instead of average, plot the minimum value on the grid')
    parser.add_argument('--max', action='store_true', help='instead of average, plot the maximum value on the grid')
    parser.add_argument('-n', type=int, nargs='+', help='the snapshot(s) to plot')
    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    parser.add_argument('--keep-nth', type=int, help="keep only every n'th snapshot (always keeps the first and last snapshots)")
    parser.add_argument('--sample', action='store_true', help='plots a nice sample of snapshots (roughly 15, evenly spaced, including n = 0 and -1)')
    parser.add_argument('--all', action='store_true', help='ignores --sample and plots all snapshots')
    parser.add_argument('--plot-range', nargs=2, type=int, help='plots all snapshots in this range')
    parser.add_argument('--plot-after', type=int, help='plots all snapshots after this snapshot')
    parser.add_argument('--plot-before', type=int, help='plots all snapshots before this snapshot')
    parser.add_argument('--marker', action='store_true', help='plots markers')
    parser.add_argument('--preliminary', action='store_true', help='adds a "preliminary" watermark to the plot')
    parser.add_argument('--z-rev', action='store_true', help='reverses the z-order of the lines (i.e. which lines cover each other)')
    parser.add_argument('--hide-ghosts', action='store_true', help='hide ghost cells in the plot')
    parser.add_argument('-tx', '--toggle-xlog', action='store_true', help='turn ON log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_true', help='turn ON log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--abs', action='store_true', help='plots the absolute values')
    parser.add_argument('--legend-loc', type=int, help='legend location [default=1]')
    parser.add_argument('--legend-font-size', type=int, default=24, help='legend font size [default=24]')
    parser.add_argument('--legend-labels-in-plot', nargs='+', type=float, help='labels are put next to the plot lines instead of in the legend; provide an x-coordinate per label')
    parser.add_argument('--legend-labels-in-plot-no-rotation', action='store_true', help='labels in the plot will not be rotated')
    parser.add_argument('--no-tight-layout', action='store_true', help="don't use tight_layout()")
    parser.add_argument('--cgs', action='store_true', help='use cgs units (for the pressure: p -> p*c^2)')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    # Get variables to plot
    ylabels = []
    ysss = []
    for f in args.INPUT_DBG_FILE:
        yss = load_dbg_file(f)
        if args.hide_ghosts:
            yss = [ys[2:-2] for ys in yss]

        if args.make_gammabeta:
            yss = [[y/sqrt(1.-y*y) for y in ys] for ys in yss]

        if not args.n_max:
            args.n_max = len(yss)
        ns = get_ns(args, args.n_max)

        ylabel = f.split('/')[-1][1:].split('.')[0]
        ylabel = ylabel.replace('_', '\_')
        ylabel = r'$\mathrm{%s}$' % (ylabel)

        ysss.append(yss)
        ylabels.append(ylabel)

    # Shift the data
    if not args.overlap:
        xss, ysss = get_xss_from_ysss(ysss)
    else:
        n = max([len(yss[0]) for yss in ysss])
        xss = [[i for i in xrange(n)] for ys in ysss[0]]
        for yss in ysss:
            for ys in yss:
                while len(ys) < n:
                    ys.append(nan)

    vlines = None
    if args.check_periodicity:
        ysss = get_periodic_ysss(ysss)
        c = len(xss[0])/2
        if len(xss[0]) % 2:
            c += 1
        x_line = .5*(xss[0][c-1] + xss[0][c])
        vlines = [x_line]
        if args.x_lims == None and args.check_periodicity > 0:
            w = args.check_periodicity
            x_min = xss[0][c-w/2]
            x_max = xss[0][c+w/2-1]
            args.x_lims = x_min, x_max

    # Fix stuff
    marker = '.' if args.marker == True else None
    if args.pad and not args.x_lims:
        x_min = min([min(xs) for xs in xss])
        x_max = max([max(xs) for xs in xss])
        dx = x_max - x_min
        args.x_lims = [x_min-.05*dx, x_max+.05*dx]
    ylabels_for_same_line = None
    plot_data(xss, ysss, ns, r'$j$', ylabels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=args.y_lims,
              marker=marker, legend_loc=args.legend_loc, legend_font_size=args.legend_font_size, zero_line=False,
              final_line_is_black=False, ylabels_for_same_line=ylabels_for_same_line, zorder_reverse=args.z_rev, cmaps=None,
              no_tight_layout=args.no_tight_layout, save_name=args.output_file, find_all_shocks=False, multiple_panels=False, multiple_ylims=False,
              legend_labels_in_plot=args.legend_labels_in_plot, no_rotation_of_text=args.legend_labels_in_plot_no_rotation, vlines=vlines)

if __name__ == '__main__':
    main()
