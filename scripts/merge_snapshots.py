#!/usr/bin/env python

import os
import argparse
import glob

def save_output_file(fname1, fname2, fname_out, i, di, skip_first_n_snapshots=1, n_max=1e30):
    with open(fname_out, 'w') as fout:

        with open(fname1, 'r') as f1:
            # for line in f1.readlines():
            #     fout.write(line)

            j = 0
            for line in f1.readlines():
                if j < i + di*n_max:
                    fout.write(line)
                j += 1

        with open(fname2, 'r') as f2:
            j = 0
            for line in f2.readlines():

                if j >= i + di*skip_first_n_snapshots:
                    fout.write(line)

                # if j >= i + di*skip_first_n_snapshots and j < i + di*n_max:

                #     # print fname_out[-3:], 'mhd', fname_out[-3:] == 'mhd'

                #     # if (j < 4) and (fname_out[-3:] == 'mhd'):
                #     # if fname_out[-3:] == 'mhd':
                #     #     if j < i + di*skip_first_n_snapshots + 4*di:
                #     #         urgh = 200
                #     #         # print '--- line '+str(j)+' ---'
                #     #         if len(line) > urgh:
                #     #             print line[:urgh]
                #     #         else:
                #     #             print line[:-1]

                #     fout.write(line)

                j += 1


def merge_files(ext, snap_name_1, snap_name_2, snap_name_out, n_max):
    # Need to know:
    # - i: Line where data begins
    # - di: How many lines in a single snapshot
    # - Merged snapshot file = first snapshot file + second snapshot file from line i_l+n_l (removing first snapshot)

    # Find i_l, n_l for the current file type
    if ext in ['mhd']:
        i = 16
        di = 9
    elif ext in ['zpm', 'zet', 'cte', 'xis', 'g0s', 'g1s', 'i0s', 'i1s', 'ugl']:
        i = 0
        di = 1
    elif ext in ['pre', 'tim', 'ispc']:
        i = 1
        di = 1
    elif ext in ['spc']:
        i = 1
        di = 3

    print "Saving '"+snap_name_out+"."+ext+"'"
    save_output_file(snap_name_1+'.'+ext, snap_name_2+'.'+ext, snap_name_out+'.'+ext, i, di, n_max=n_max)

def get_extensions_to_merge(filenames_1, filenames_2):
    exts_1 = [filename.split('.')[-1] for filename in filenames_1]
    exts_2 = [filename.split('.')[-1] for filename in filenames_2]
    exts_merge = [ext for ext in exts_1 if ext in exts_2]
    exts_ignore = []
    for ext in exts_1:
        if ext not in exts_merge:
            exts_ignore.append(ext)
    for ext in exts_2:
        if ext not in exts_merge:
            exts_ignore.append(ext)
    return exts_merge, exts_ignore

def main():
    parser = argparse.ArgumentParser(description='a script that merges snapshot files from two different runs. by default, removes first snapshot from second run. keeps the header from the first run')
    parser.add_argument('INPUT_MHD_FILE_1', help='the first input MHD snapshots file (.mhd)')
    parser.add_argument('INPUT_MHD_FILE_2', help='the second input MHD snapshots file (.mhd)')
    parser.add_argument('OUTPUT_MHD_FILE', help='the new snapshots file name')
    parser.add_argument('--n-max', type=int, help='cuts data after --n-max for the FIRST input MHD snapshots file')
    args = parser.parse_args()

    # Snapshot names
    snap_name_1 = args.INPUT_MHD_FILE_1[:-4]
    snap_name_2 = args.INPUT_MHD_FILE_2[:-4]
    snap_name_out = args.OUTPUT_MHD_FILE[:-4]

    # Find the files
    filenames_1 = glob.glob(snap_name_1+'.*')
    if not filenames_1:
        print "Could not find '"+args.INPUT_MHD_FILE_1+"', exiting..."
        raise SystemExit

    filenames_2 = glob.glob(snap_name_2+'.*')
    if not filenames_2:
        print "Could not find '"+args.INPUT_MHD_FILE_2+"', exiting..."
        raise SystemExit

    # # Check if output files already exists
    # filenames_out = glob.glob(snap_name_out+'.*')
    # if filenames_out:
    #     print "Output files already exists, exiting..."
    #     raise SystemExit

    # Determine which files can be merged
    exts_merge, exts_ignore = get_extensions_to_merge(filenames_1, filenames_2)
    merge_string = '*.'+exts_merge[0]
    for ext in exts_merge[1:]:
        merge_string += ', *.'+ext
    print 'Files to merge:'
    print '  '+merge_string
    if len(exts_ignore):
        ignore_string = '*.'+exts_ignore[0]
        for ext in exts_ignore[1:]:
            ignore_string += ', *.'+ext
        print 'Files to ignore (missing in one snapshot):'
        print '  '+ignore_string

    # Merge
    if not args.n_max:
        n_max = 1e30
    else:
        n_max = args.n_max
    for ext in exts_merge:
        merge_files(ext, snap_name_1, snap_name_2, snap_name_out, n_max)

if __name__ == '__main__':
    main()

    # # Rename the files
    # print "Renaming '"+snap_name_in+".*'"
    # print "      -> '"+snap_name_out+".*' ..."
    # for filename_in in filenames_in:
    #     extension = filename_in.split('.')[-1]
    #     filename_out = snap_name_out+'.'+extension
    #     os.rename(filename_in, filename_out)
