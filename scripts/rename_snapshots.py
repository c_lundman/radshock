#!/usr/bin/env python

import os
import argparse
import glob

def main():
    parser = argparse.ArgumentParser(description='a script that produces plots (and movies) from a snapshots file (.mhd)')
    parser.add_argument('INPUT_MHD_FILE', help='the input MHD snapshots file (.mhd)')
    parser.add_argument('OUTPUT_MHD_FILE', help='the new snapshots file name')
    args = parser.parse_args()

    # Snapshot names
    snap_name_in = args.INPUT_MHD_FILE[:-4]
    snap_name_out = args.OUTPUT_MHD_FILE[:-4]

    # Find the files
    filenames_in = glob.glob(snap_name_in+'.*')
    if not filenames_in:
        print "Could not find '"+args.INPUT_MHD_FILE+"', exiting..."
        raise SystemExit

    # Check if output files already exists
    filenames_out = glob.glob(snap_name_out+'.*')
    if filenames_out:
        print "Output files already exists, exiting..."
        raise SystemExit

    # Rename the files
    print "Renaming '"+snap_name_in+".*'"
    print "      -> '"+snap_name_out+".*' ..."
    for filename_in in filenames_in:
        extension = filename_in.split('.')[-1]
        filename_out = snap_name_out+'.'+extension
        os.rename(filename_in, filename_out)

if __name__ == '__main__':
    main()
