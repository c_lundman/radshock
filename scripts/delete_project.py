#/usr/bin/env python

import os
import shutil
import argparse
from random import choice

def make_random_string(n=6):
    cap_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    sma_letters = 'abcdefghijklmnopqrstuvwxyz'
    numbers = '0123456789'
    all_letters = cap_letters+sma_letters+numbers
    string = ''
    for i in xrange(n):
        string += choice(all_letters)
    return string

def main():
    parser = argparse.ArgumentParser(description='a script that IRREVERSABLE DELETES a project folder. use with caution!')
    parser.add_argument('PROJ_NAME', help='the project name')
    parser.add_argument('--is-test', action='store_true', help='searches for the project in the tests/ folder instead')
    args = parser.parse_args()

    make_random_string()
    
    # Find folder
    if args.is_test:
        proj_folder = '../tests/'
    else:
        proj_folder = '../projects/'
    proj_path = proj_folder+args.PROJ_NAME

    if os.path.isdir(proj_path):
        print "Found the project '"+proj_path+"'"
        answer = raw_input("  Question: Is this the correct project? [y/n]: ")
        if answer.lower() in ["y", "yes"]:
            s = make_random_string(6)
            answer = raw_input('  In order to delete the project, type "'+s+'" (without the quotation marks): ')
            if answer == s:
                print "Deleting project '"+proj_path+"'"
                shutil.rmtree(proj_path)
                print "Project deleted."
                raise SystemExit
            else:
                print "Wrong answer (%s != %s)" % (answer, s)
    else:
        print "Could not find project '"+proj_path+"'"
    print "Exiting WITHOUT deleting project."

if __name__ == '__main__':
    main()
