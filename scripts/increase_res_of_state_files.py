#!/usr/bin/env python

import argparse
from copy import deepcopy
from numpy import sqrt
from flatten_state_files import read_mhd_state_file, save_mhd_state_file, MHD_Grid, get_consts, save_mhd_state_file, plot_grids

# Finish the computation of ghosts

def read_and_save_rad_state_file(infilename, outfilename, n_bs, n_bs_old, Z_pms):
    reading_photons = False
    turn_on_reading_photons = False
    with open(infilename, 'r') as f_in:
        with open(outfilename, 'w') as f_out:
            i = 0
            for line in f_in.readlines():

                if i == 1:
                    # n_bs_old = int(line.split()[1])
                    string = 'n_bs           %i' % (n_bs)
                    string += '\n'
                elif len(line.split()) > 4:
                    if float(line.split()[0]) == 1 and float(line.split()[1]) == 1 and float(line.split()[2]) == 1:
                        string = ''
                        for Z_pm in Z_pms:
                            string += ' %18.12e' % (Z_pm)
                            turn_on_reading_photons = True
                        string += '\n'
                    else:
                        string = line
                else:
                    string = line

                if not (reading_photons and len(string.split()) != 4):
                    photon_string = string
                    f_out.write(string)
                else:
                    if reading_photons:
                        f_out.write(photon_string)

                if turn_on_reading_photons:
                    turn_on_reading_photons = False
                    reading_photons = True

                i += 1

def theEEquation(w, V, E, S, M, r, alpha, gamma_ad):

    # This function is copied (and translated to Python) directly from 'lagrangian_mhd_functions.cpp'

    Gamma_r = gamma_ad/(gamma_ad-1.)

    if alpha == 2:
        v = S*V/(w*V + r*r*M*M)
    elif alpha == 1:
        v = S*V/(w*V + r*M*M) # NOT IMPLEMENTED YET!
    elif alpha == 0:
        v = S*V/(w*V + M*M)
    else:
        v = 0.

    gamma = 1./sqrt(1.-v*v)
    p = (w-gamma)/Gamma_r/gamma/gamma/V

    if alpha == 2:
        f = w - V*p + (1.-1./2./gamma/gamma)*r*r*M*M/V - E
        dfdw = 1. - 1./Gamma_r/gamma/gamma - v*v*v/S*(r*r*M*M/V - (gamma-2.*w)/Gamma_r)
    elif alpha == 1:
        # NOT IMPLEMENTED YET
        f = w - V*p + (1.-1./2./gamma/gamma)*r*M*M/V - E
        dfdw = 1. - 1./Gamma_r/gamma/gamma - v*v*v/S*(r*M*M/V - (gamma-2.*w)/Gamma_r)
    elif alpha == 0:
        f = w - V*p + (1.-1./2./gamma/gamma)*M*M/V - E
        dfdw = 1. - 1./Gamma_r/gamma/gamma - v*v*v/S*(M*M/V - (gamma-2.*w)/Gamma_r)
    else:
        f = 0.
        dfdw = 0.

    return f, dfdw

def rtnewt(w_guess, w_acc, V, E, S, M, r, alpha, gamma_ad):

    # This function is copied (and translated to Python) directly from 'lagrangian_mhd_functions.cpp'

    JMAX = 20

    # Initial guess for the root
    rtn = w_guess

    for j in xrange(1, JMAX):

        f, dfdw = theEEquation(rtn, V, E, S, M, r, alpha, gamma_ad)
        dw = f/dfdw
        rtn -= dw

        # Checking convergence to desired accuracy
        if dw*dw < w_acc*w_acc:
            return rtn

    return 0. # Never get here!

def get_prims(Vs, Ss, Es, Ms, alpha, gamma_ad, rs):

    # This function is copied (and translated to Python) directly from 'lagrangian_mhd_functions.cpp'

    Gamma_r = gamma_ad/(gamma_ad-1.)
    v_max = 0.999999944444 # Equals gamma_max = 3e3

    vs = []
    rhos = []
    ps = []
    bs = []
    for V, S, E, M, r in zip(Vs, Ss, Es, Ms, rs):

        if S != 0.:
            if alpha == 2:
                w_i = sqrt(S*S)/v_max - r*r*M*M/V
            elif alpha == 1:
                # NOT IMPLEMENTED YET!
                w_i = sqrt(S*S)/v_max - r*M*M/V
            elif alpha == 0:
                w_i = E - M*M/V
            else:
                w_i = 1.

            # Finding the root
            rel_acc = 1e-14
            w = rtnewt(w_i, rel_acc*w_i, V, E, S, M, r, alpha, gamma_ad)
        else:
            if alpha == 2:
                w = (E - r*r*M*M/V - 1./Gamma_r)/(1.-1./Gamma_r)
            elif alpha == 1:
                # NOT IMPLEMENTED YET!
                w = (E - r*M*M/V - 1./Gamma_r)/(1.-1./Gamma_r)
            else:
                w = (E - M*M/V - 1./Gamma_r)/(1.-1./Gamma_r)

        # Reconstructing the variables
        if alpha == 2:
            v = S*V/(w*V + r*r*M*M)
        elif alpha == 1:
            # NOT IMPLEMENTED YET!
            v = S*V/(w*V + r*M*M)
        elif alpha == 0:
            v = S*V/(w*V + M*M)

        if v > v_max:
            v = v_max
        elif v < -v_max:
            v = -v_max

        gamma = 1./sqrt(1.-v*v)
        rho = 1./gamma/V
        p = (w-gamma)/Gamma_r/gamma/gamma/V

        if alpha == 2:
            b = r*rho*M
        elif alpha == 1:
            # NOT IMPLEMENTED YET!
            # b = pow(r, .5)*(*rho)*M_m;
            b = (r**.5)*rho*M
        elif alpha == 0:
            b = rho*M

        vs.append(v)
        rhos.append(rho)
        ps.append(p)
        bs.append(b)

    return vs, rhos, ps, bs

def make_r_bs_from_consts(m_bs, Vs, R_i, alpha):
    r_bs = [0. for m_b in m_bs]
    r_bs[0] = R_i

    # for i in xrange(3, len(m_bs)):
    for i in xrange(1, len(m_bs)):
        dm = m_bs[i]-m_bs[i-1]
        r_1 = r_bs[i-1]
        V = Vs[i-1]
        r_bs[i] = ( r_1**(alpha+1.) + (alpha+1.)*dm*V )**(1./(alpha+1.))

    # for i in range(0, 2)[::-1]:
    #     dm = m_bs[i+1]-m_bs[i]
    #     r_2 = r_bs[i+1]
    #     V = Vs[i]
    #     r_bs[i] = ( r_2**(alpha+1.) - (alpha+1.)*dm*V )**(1./(alpha+1))

    return r_bs

def split_subgrid_once(m_bs, r_bs, ps, Vs, Ss, Es, Ms, alpha, gamma_ad, linear_interpolation, smooth_pressure):

    # Assumes that the subgrid contains no ghost cells
    
    # Make new consts
    m_bs_new = [m_bs[0]]
    Vs_new = []
    Ss_new = []
    Es_new = []
    Ms_new = []

    # Only used if smoothing the pressure
    ps_new_smoothed = []

    for i in xrange(len(Vs)):
        # New mass bins
        m_L = m_bs[i]
        m_R = m_bs[i+1]
        m_c = .5*(m_R+m_L)
        m_bs_new.append(m_c)
        m_bs_new.append(m_R)

        # Estimating local derivative
        if i == 0:
            m_1 = .5*(m_bs[0]+m_bs[1])
            m_2 = .5*(m_bs[1]+m_bs[2])
            V_1 = Vs[0]
            V_2 = Vs[1]
            S_1 = Ss[0]
            S_2 = Ss[1]
            E_1 = Es[0]
            E_2 = Es[1]
            M_1 = Ms[0]
            M_2 = Ms[1]
            p_1 = ps[0]
            p_2 = ps[1]

        elif i == len(Vs)-1:
            m_1 = .5*(m_bs[-3]+m_bs[-2])
            m_2 = .5*(m_bs[-2]+m_bs[-1])
            V_1 = Vs[-2]
            V_2 = Vs[-1]
            S_1 = Ss[-2]
            S_2 = Ss[-1]
            E_1 = Es[-2]
            E_2 = Es[-1]
            M_1 = Ms[-2]
            M_2 = Ms[-1]
            p_1 = ps[-2]
            p_2 = ps[-1]

        else:
            m_1 = .5*(m_bs[i-1]+m_bs[i])
            m_2 = .5*(m_bs[i+1]+m_bs[i+2])
            V_1 = Vs[i-1]
            V_2 = Vs[i+1]
            S_1 = Ss[i-1]
            S_2 = Ss[i+1]
            E_1 = Es[i-1]
            E_2 = Es[i+1]
            M_1 = Ms[i-1]
            M_2 = Ms[i+1]
            p_1 = ps[i-1]
            p_2 = ps[i+1]

        dVdm = (V_2-V_1)/(m_2-m_1)
        dSdm = (S_2-S_1)/(m_2-m_1)
        dEdm = (E_2-E_1)/(m_2-m_1)
        dMdm = (M_2-M_1)/(m_2-m_1)

        # Finding "left and right values"
        if linear_interpolation:
            dm = .25*(m_R-m_L)
        else:
            dm = 0.
        V = Vs[i]
        S = Ss[i]
        E = Es[i]
        M = Ms[i]
        V_L = V - dVdm*dm
        V_R = V + dVdm*dm
        S_L = S - dSdm*dm
        S_R = S + dSdm*dm
        E_L = E - dEdm*dm
        E_R = E + dEdm*dm
        M_L = M - dMdm*dm
        M_R = M + dMdm*dm

        # Adding the const values to the grid
        Vs_new.append(V_L)
        Vs_new.append(V_R)
        Ss_new.append(S_L)
        Ss_new.append(S_R)
        Es_new.append(E_L)
        Es_new.append(E_R)
        Ms_new.append(M_L)
        Ms_new.append(M_R)

        # Smoothed pressure
        dpdm = (p_2-p_1)/(m_2-m_1)
        p = ps[i]
        p_L = p - dpdm*dm
        p_R = p + dpdm*dm
        ps_new_smoothed.append(p_L)
        ps_new_smoothed.append(p_R)

    # Make r_bs_new
    r_bs_new = make_r_bs_from_consts(m_bs_new, Vs_new, r_bs[0], alpha)

    # Make new prims from new consts
    rs_new = [.5*(r_bs_new[i+1]+r_bs_new[i]) for i in xrange(len(m_bs_new)-1)]
    vs_new, rhos_new, ps_new, bs_new = get_prims(Vs_new, Ss_new, Es_new, Ms_new, alpha, gamma_ad, rs_new)

    # Smooth pressure?
    if smooth_pressure:
        Vs_new, Ss_new, Es_new, Ms_new = get_consts(rs_new, vs_new, rhos_new, ps_new_smoothed, bs_new, alpha, gamma_ad)

    return m_bs_new, r_bs_new, vs_new, rhos_new, ps_new, bs_new, Vs_new, Ss_new, Es_new, Ms_new

def add_ghosts(m_bs, r_bs, vs, rhos, ps, bs, Vs, Ss, Es, Ms, alpha, gamma_ad):

    # Mass bins
    dm = m_bs[1]-m_bs[0]
    m_bs.insert(0, m_bs[0]-dm)
    m_bs.insert(0, m_bs[0]-dm)
    dm = m_bs[-1]-m_bs[-2]
    m_bs.append(m_bs[-1]+dm)
    m_bs.append(m_bs[-1]+dm)

    # Prims
    v = vs[0]
    vs.insert(0, v)
    vs.insert(0, v)
    v = vs[-1]
    vs.append(v)
    vs.append(v)

    rho = rhos[0]
    rhos.insert(0, rho)
    rhos.insert(0, rho)
    rho = rhos[-1]
    rhos.append(rho)
    rhos.append(rho)

    p = ps[0]
    ps.insert(0, p)
    ps.insert(0, p)
    p = ps[-1]
    ps.append(p)
    ps.append(p)

    b = bs[0]
    bs.insert(0, b)
    bs.insert(0, b)
    b = bs[-1]
    bs.append(b)
    bs.append(b)

    # Fix radial boundaries
    r_bs.insert(0, 0)
    r_bs.insert(0, 0)
    for i in range(0, 2)[::-1]:
        dm = m_bs[i+1]-m_bs[i]
        r_2 = r_bs[i+1]
        V = sqrt(1.-vs[i]*vs[i])/rhos[i]
        r_bs[i] = ( r_2**(alpha+1.) - (alpha+1.)*dm*V )**(1./(alpha+1))

    r_bs.append(0)
    r_bs.append(0)
    for i in xrange(1, len(m_bs)):
        dm = m_bs[i]-m_bs[i-1]
        r_1 = r_bs[i-1]
        V = sqrt(1.-vs[i-1]*vs[i-1])/rhos[i-1]
        r_bs[i] = ( r_1**(alpha+1.) + (alpha+1.)*dm*V )**(1./(alpha+1.))

    # Fix also the consts
    rs = [.5*(r_bs[i+1]+r_bs[i]) for i in xrange(len(m_bs)-1)]
    Vs, Ss, Es, Ms = get_consts(rs, vs, rhos, ps, bs, alpha, gamma_ad)

    return m_bs, r_bs, vs, rhos, ps, bs, Vs, Ss, Es, Ms

def do_all_splits(m_bs, r_bs, vs, rhos, ps, bs, Vs, Ss, Es, Ms, alpha, gamma_ad, linear_interpolation, smooth_pressure, number_of_splits, points):

    # Make "sections"
    if not points:
        sections = [(0, len(vs)-1, number_of_splits)]
    else:
        dp = 20 # Number of "original" grid points to decrease one split level
        n_s = number_of_splits
        section_c = (points[0], points[1], number_of_splits)
        sections = [section_c]

        # Going to the left
        done = False
        n_s = number_of_splits-1
        p_r = points[0]
        p_l = p_r - dp
        while not done:
            if p_l <= 0:
                done = True
                p_l = 0
            section = (p_l, p_r, n_s)
            sections.insert(0, section)

            p_r = p_l
            p_l = p_r - dp
            if n_s > 0:
                n_s -= 1

        # Going to the right
        done = False
        n_s = number_of_splits-1
        p_l = points[1]
        p_r = p_l + dp
        while not done:
            # if p_r >= len(vs)-1:
            #     done = True
            #     p_r = len(vs)-1
            if p_r >= len(vs):
                done = True
                p_r = len(vs)
            section = (p_l, p_r, n_s)
            sections.append(section)

            p_l = p_r
            p_r = p_l + dp
            if n_s > 0:
                n_s -= 1

    # New grids
    m_bs_new = []
    r_bs_new = []
    vs_new = []
    rhos_new = []
    ps_new = []
    bs_new = []
    Vs_new = []
    Ss_new = []
    Es_new = []
    Ms_new = []

    # Make subgrid
    for i_l, i_r, n_splits in sections:
        m_bs_s = m_bs[i_l:i_r+1]
        r_bs_s = r_bs[i_l:i_r+1]
        vs_s = vs[i_l:i_r]
        rhos_s = rhos[i_l:i_r]
        ps_s = ps[i_l:i_r]
        bs_s = bs[i_l:i_r]
        Vs_s = Vs[i_l:i_r]
        Ss_s = Ss[i_l:i_r]
        Es_s = Es[i_l:i_r]
        Ms_s = Ms[i_l:i_r]

        # Split subgrid
        for n in xrange(n_splits):
            output = split_subgrid_once(m_bs_s, r_bs_s, ps_s, Vs_s, Ss_s, Es_s, Ms_s, alpha, gamma_ad, linear_interpolation, smooth_pressure)
            m_bs_s, r_bs_s, vs_s, rhos_s, ps_s, bs_s, Vs_s, Ss_s, Es_s, Ms_s = output

        # Add to new, total grid
        m_bs_new = m_bs_new[:-1] + m_bs_s
        r_bs_new = r_bs_new[:-1] + r_bs_s
        vs_new += vs_s
        rhos_new += rhos_s
        ps_new += ps_s
        bs_new += bs_s
        Vs_new += Vs_s
        Ss_new += Ss_s
        Es_new += Es_s
        Ms_new += Ms_s

    # Add ghosts
    output = add_ghosts(m_bs_new, r_bs_new, vs_new, rhos_new, ps_new, bs_new, Vs_new, Ss_new, Es_new, Ms_new, alpha, gamma_ad)
    m_bs_new, r_bs_new, vs_new, rhos_new, ps_new, bs_new, Vs_new, Ss_new, Es_new, Ms_new = output

    return m_bs_new, r_bs_new, vs_new, rhos_new, ps_new, bs_new, Vs_new, Ss_new, Es_new, Ms_new

# Main

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='a script for modifying MHD (and also RAD) state files, increasing their resolution. currently, the script can NOT handle non-unity Z_pms; it will set Z_pm = 1 everywhere.')
    parser.add_argument('INPUT_MSF_FILE', help='the input MHD save state file (*.msf)')
    parser.add_argument('-n', '--number-of-splits', type=int, default=1, help='the number of times to split each spatial bin [default=1]')
    parser.add_argument('-p', '--points', nargs=2, type=int, help='two points (actually x-indices), within which the resolution is increased. outside of these points, the increase is smoothened outside of these points. if not given, increases the resolution of the whole grid')
    parser.add_argument('-o', '--output-msf-file', help='the output MHD save state file')
    parser.add_argument('--no-rsf-mod', action='store_true', help='normally, finds the corresponding .rsf, modifies n_bs, f_heat and Z_pms, and saves it as the corresponding output file [for now, simply fills with ones], but if this parameter is true, skips this step')
    parser.add_argument('--linear-interpolation', action='store_true', help='interpolate linearly between mass bins (instead of constant)')
    parser.add_argument('--smooth-pressure', action='store_true', help='smoothes the pressure (which can sometimes become "noisy" when p << rho)')
    parser.add_argument('-v', '--variable', default='dm', help='the variable to plot [default=dm]')
    parser.add_argument('--reverse', action='store_true', help='reverse the mass coordinate')
    parser.add_argument('--show-ghosts', action='store_true', help='also show ghost cells in the plot')
    parser.add_argument('-tx', '--toggle-xlog', action='store_true', help='turn ON log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_false', help='turn OFF log scale on the y-axis')
    parser.add_argument('--no-plot', action='store_true', help='does NOT show the plot')
    parser.add_argument('--verbose', action='store_true', help='print detailed information to the terminal')
    args = parser.parse_args()

    mhd_header, mhd_grid = read_mhd_state_file(args.INPUT_MSF_FILE)
    alpha = mhd_header.get_alpha()
    gamma_ad = mhd_header.get_gamma_ad()

    if args.verbose:
        mhd_header.print_header('input MHD file header:')

    mhd_grids = [mhd_grid]
    if args.number_of_splits > 0:
        # Make new grid here

        # Cut the ghosts
        m_bs = mhd_grid.get_m_bs()[2:-2]
        r_bs = mhd_grid.get_r_bs()[2:-2]
        vs = mhd_grid.get_vs()[2:-2]
        rhos = mhd_grid.get_rhos()[2:-2]
        ps = mhd_grid.get_ps()[2:-2]
        bs = mhd_grid.get_bs()[2:-2]
        Vs = mhd_grid.get_Vs()[2:-2]
        Ss = mhd_grid.get_Ss()[2:-2]
        Es = mhd_grid.get_Es()[2:-2]
        Ms = mhd_grid.get_Ms()[2:-2]

        # Do all requested splits
        output = do_all_splits(m_bs, r_bs, vs, rhos, ps, bs, Vs, Ss, Es, Ms, alpha, gamma_ad, args.linear_interpolation, args.smooth_pressure, args.number_of_splits, args.points)
        m_bs_new, r_bs_new, vs_new, rhos_new, ps_new, bs_new, Vs_new, Ss_new, Es_new, Ms_new = output

        # print mhd_grid.get_m_bs()[:3], mhd_grid.get_m_bs()[-4:]
        # print m_bs_new[:3], m_bs_new[-4:]

        # Making the new grid
        mhd_grid_new = MHD_Grid()
        mhd_grid_new.set_m_bs(m_bs_new)
        mhd_grid_new.set_r_bs(r_bs_new)
        mhd_grid_new.set_vs(vs_new)
        mhd_grid_new.set_rhos(rhos_new)
        mhd_grid_new.set_ps(ps_new)
        mhd_grid_new.set_bs(bs_new)
        mhd_grid_new.set_Vs(Vs_new)
        mhd_grid_new.set_Ss(Ss_new)
        mhd_grid_new.set_Es(Es_new)
        mhd_grid_new.set_Ms(Ms_new)
        mhd_grids.append(mhd_grid_new)

        if args.verbose:
            print 'Old grid: n_bs = %i' % (len(m_bs))
            print 'New grid: n_bs = %i' % (len(m_bs_new))

    if not args.no_plot and not args.output_msf_file:
        marker = '.'
        ls = ''
        marker = ''
        ls = '-'
        if args.variable != 'all':
            plot_grids(mhd_grids, args.variable, None, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, marker=marker, ls=ls, rev=args.reverse, alpha=alpha)
        else:
            if args.verbose:
                print 'plotting all primitives...'
            plot_grids(mhd_grids, 'v', None, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, marker=marker, ls=ls, rev=args.reverse, alpha=alpha)
            plot_grids(mhd_grids, 'rho', None, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, marker=marker, ls=ls, rev=args.reverse, alpha=alpha)
            plot_grids(mhd_grids, 'p', None, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, marker=marker, ls=ls, rev=args.reverse, alpha=alpha)

            isnull = True
            for b in mhd_grids[0].get_bs():
                if b > 0:
                    isnull = False
            if not isnull:
                plot_grids(mhd_grids, 'b', args.points, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, marker=marker, ls=ls, rev=args.reverse, alpha=alpha)

    if args.output_msf_file and len(mhd_grids) > 1:
        mhd_header_new = deepcopy(mhd_header)
        mhd_header_new.set_n_bs(mhd_grids[-1].get_n_bs())
        if args.verbose:
            mhd_header_new.print_header('output MHD file header:')
        save_mhd_state_file(mhd_header_new, mhd_grids[-1], args.output_msf_file)
        if args.verbose:
            print 'file saved: %s (--smooth-pressure = %s)' % (args.output_msf_file, args.smooth_pressure)

        if not args.no_rsf_mod:
            infilename = args.INPUT_MSF_FILE[:-3]+'rsf'
            outfilename = args.output_msf_file[:-3]+'rsf'
            Z_pms_new = [1. for i in xrange(mhd_header_new.get_n_bs()-1)]
            read_and_save_rad_state_file(infilename, outfilename, mhd_header_new.get_n_bs(), mhd_header.get_n_bs(), Z_pms_new)
            if args.verbose:
                print 'file saved: %s' % (outfilename)
