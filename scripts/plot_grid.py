#!/usr/bin/env python

import argparse
from numpy import pi
# from plot_snapshots import get_xss_ysss_and_labels, get_ns, plot_data
from plot_snapshots import read_mhd_file, get_ns, plot_data
from plot_spectra import read_spc_file

#

def main():
    parser = argparse.ArgumentParser(description='a script that plots the spectrum (or mhd) mass (or radius) grid')
    parser.add_argument('INPUT_SPC_FILE', help='the input SPC snapshots file (.spc)')
    parser.add_argument('-o', '--output-file', help='the output pdf')
    parser.add_argument('--mhd', action='store_true', help='plot the MHD grid instead')
    parser.add_argument('-r', action='store_true', help='compare radius grids (instead of mass grids)')
    # parser.add_argument('-x', '--x-coordinate', default='r', help='the horizontal coordinate (t, ct, r, tau) [default=r]')
    # parser.add_argument('--min', action='store_true', help='instead of average, plot the minimum value on the grid')
    # parser.add_argument('--max', action='store_true', help='instead of average, plot the maximum value on the grid')
    parser.add_argument('-n', type=int, nargs='+', help='the snapshot(s) to plot')
    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    parser.add_argument('--keep-nth', type=int, help="keep only every n'th snapshot (always keeps the first and last snapshots)")
    parser.add_argument('--sample', action='store_true', help='plots a nice sample of snapshots (roughly 15, evenly spaced, including n = 0 and -1)')
    parser.add_argument('--all', action='store_true', help='ignores --sample and plots all snapshots')
    parser.add_argument('--plot-range', nargs=2, type=int, help='plots all snapshots in this range')
    parser.add_argument('--plot-after', type=int, help='plots all snapshots after this snapshot')
    parser.add_argument('--plot-before', type=int, help='plots all snapshots before this snapshot')
    parser.add_argument('--separate', action='store_true', help='shifts each next line upwards by a factor 2, so that lines overlap less (for increased visibility)')
    parser.add_argument('--reverse', action='store_true', help='reverses the mass grids')
    # parser.add_argument('--track', type=int, help='plots a circle at the given spatial bin')
    # parser.add_argument('--zero-line', action='store_true', help='draws a line at y = 0')
    # parser.add_argument('--colored-final-line', action='store_true', help='makes the final snapshot line colored (as opposed to black)')
    # parser.add_argument('--cmap', help='the color map to use for all plots')
    # parser.add_argument('--marker', action='store_true', help='plots markers')
    # parser.add_argument('--preliminary', action='store_true', help='adds a "preliminary" watermark to the plot')
    parser.add_argument('--z-rev', action='store_true', help='reverses the z-order of the lines (i.e. which lines cover each other)')
    # parser.add_argument('--show-ghosts', action='store_true', help='also show ghost cells in the plot')
    parser.add_argument('-tx', '--toggle-xlog', action='store_true', help='turn ON log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_true', help='turn ON log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    # parser.add_argument('--abs', action='store_true', help='plots the absolute values')
    parser.add_argument('--legend-loc', type=int, help='legend location [default=1]')
    # parser.add_argument('--find-shocks', action='store_true', help='attempts to find discontinuities and plots markers in the immediate up- and downstreams')
    # parser.add_argument('--legend-times', action='store_true', help='show snapshot time (ct) in the legend (only for single VAR)')
    # parser.add_argument('--legend-short', action='store_true', help='show only first and last legend items (only for single VAR)')
    parser.add_argument('--legend-font-size', type=int, default=24, help='legend font size [default=24]')
    # parser.add_argument('--legend-labels-in-plot', nargs='+', type=float, help='labels are put next to the plot lines instead of in the legend; provide an x-coordinate per label')
    # parser.add_argument('--legend-labels-in-plot-no-rotation', action='store_true', help='labels in the plot will not be rotated')
    # parser.add_argument('--no-tight-layout', action='store_true', help="don't use tight_layout()")
    # parser.add_argument('--multiple-panels', action='store_true', help="plots each VAR in a separate panel")
    # parser.add_argument('--multiple-y-lims', nargs='+', type=float, help="limits on the y-axis for each panel")
    # parser.add_argument('--multiple-cmaps', nargs='+', type=str, help="color maps for each panel")
    # parser.add_argument('--cgs', action='store_true', help='use cgs units (for the pressure: p -> p*c^2)')
    # parser.add_argument('--fps', type=int, default=30, help='movie fps [default=30]')
    # parser.add_argument('--no-plot', action='store_true', help='does NOT show the plot')
    # parser.add_argument('--print-times', action='store_true', help='instead, print timing information')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    #

    iterations, ts, m_bss, r_bss, vss, rhoss, pss, bss, alpha, gamma_ad = read_mhd_file(args.INPUT_SPC_FILE[:-3]+'mhd', args.n_max, False, keep_nth=args.keep_nth)
    if not args.mhd:
        m_bss, r_bss, epsss, dudlnepssss_all, n_max = read_spc_file(args.INPUT_SPC_FILE, args.n_max)
        if alpha == 2:
            m_bss = [[4.*pi*m_b for m_b in m_bs] for m_bs in m_bss]
    n_max = len(ts)
    ns = get_ns(args, n_max)

    if args.reverse:
        M_os = [m_bs[-1] for m_bs in m_bss]
        m_bss = [[M_o-m_b for m_b in m_bs] for m_bs, M_o in zip(m_bss, M_os)]

    if not args.r:
        if not args.reverse:
            xlabel = r'$m_b \, \mathrm{[g]}$'
        else:
            xlabel = r'$m_b \, \mathrm{[g] \, (reverse)}$'
        xss = m_bss
    else:
        xlabel = r'$r_b \, \mathrm{[cm]}$'
        xss = r_bss

    if args.toggle_xlog and not args.r:
        xss = [xs[:-1] for xs in xss]

    yss = []
    y = 0
    for i, xs in enumerate(xss):
        ys = [y for x in xs]
        yss.append(ys)
        if i in ns:
            y += 1

    if args.mhd:
        ylabels = [r'$\mathrm{MHD \, grid}$']
    else:
        ylabels = [r'$\mathrm{Spectrum \, grid}$']
    ysss = [yss]

    # ylabels_for_same_line = ylabels if len(ns) > 1 else None
    ylabels_for_same_line = None
    ylims = [-.5, yss[ns[-1]][0]+.5]
    plot_data(xss, ysss, ns, xlabel, ylabels, xlog=args.toggle_xlog, ylog=args.toggle_ylog, xlims=args.x_lims, ylims=ylims, watermark=None,
              marker='|', legend_loc=args.legend_loc, legend_font_size=args.legend_font_size, zero_line=False,
              final_line_is_black=False, ylabels_for_same_line=ylabels_for_same_line, zorder_reverse=args.z_rev, cmaps=None,
              no_tight_layout=False, save_name=args.output_file)

if __name__ == '__main__':
    main()
