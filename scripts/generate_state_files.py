#!/usr/bin/env python

import os
import sys
import argparse
from random import random
from math import exp, sqrt, pi
from flatten_state_files import Header, MHD_Grid, save_mhd_state_file, get_consts

# To do:
# - fix make_r_bs()

# Constants
m_p = 1.6726e-24
m_e = 9.1094e-28

# MHD part

def get_prim_vectors(m_bs, func, basic_params, problem_params):
    vs = []
    rhos = []
    ps = []
    bs = []
    for i in xrange(len(m_bs)-1):
        m = .5*(m_bs[i+1]+m_bs[i])

        v, rho, p, b = func(m, basic_params, problem_params)

        vs.append(v)
        rhos.append(rho)
        ps.append(p)
        bs.append(b)

    return vs, rhos, ps, bs

def make_r_bs(m_bs, vs, rhos, R_i, alpha):
    # Integrates forward from an inner radius
    r_bs = [0. for m_b in m_bs]
    r_bs[2] = R_i

    for i in xrange(3, len(m_bs)):
        dm = m_bs[i]-m_bs[i-1]
        r_1 = r_bs[i-1]
        gamma = 1./sqrt(1.-vs[i-1]*vs[i-1])
        rho = rhos[i-1]
        r_bs[i] = ( r_1**(alpha+1.) + (alpha+1.)*dm/gamma/rho )**(1./(alpha+1.))

        # if alpha == 0:
        #     r_bs[i] = r_1 + dm/gamma/rho
        # elif alpha == 2:
        #     x = r_1*r_1*r_1 + 3.*dm/gamma/rho
        #     r_bs[i] = x**(1./3.)

        #     dr = r_bs[i] - r_bs[i-1]
        #     if dr <= 0.:
        #         r_bs[i] = r_1 + dm/r_1/r_1/gamma/rho

    for i in range(0, 2)[::-1]:
        dm = m_bs[i+1]-m_bs[i]
        r_2 = r_bs[i+1]
        gamma = 1./sqrt(1.-vs[i]*vs[i])
        rho = rhos[i]
        r_bs[i] = ( r_2**(alpha+1.) - (alpha+1.)*dm/gamma/rho )**(1./(alpha+1))

        # if alpha == 0:
        #     r_bs[i] = r_1 - dm/gamma/rho
        # elif alpha == 2:
        #     x = r_1*r_1*r_1 - 3.*dm/gamma/rho
        #     r_bs[i] = x**(1./3.)

        #     dr = r_bs[i+1] - r_bs[i]
        #     if dr <= 0.:
        #         r_bs[i] = r_1 - dm/r_1/r_1/gamma/rho

    return r_bs

def make_r_bs_backwards(m_bs, vs, rhos, R_f, alpha):
    # Integrates backward from an outer radius
    r_bs = [0. for m_b in m_bs]
    r_bs[-3] = R_f

    # for i in range(0, 2)[::-1]:
    for i in range(0, len(m_bs)-3)[::-1]:
        dm = m_bs[i+1]-m_bs[i]
        r_2 = r_bs[i+1]
        gamma = 1./sqrt(1.-vs[i]*vs[i])
        rho = rhos[i]
        r_bs[i] = ( r_2**(alpha+1.) - (alpha+1.)*dm/gamma/rho )**(1./(alpha+1))

    # for i in xrange(3, len(m_bs)):
    for i in xrange(len(m_bs)-2, len(m_bs)):
        dm = m_bs[i]-m_bs[i-1]
        r_1 = r_bs[i-1]
        gamma = 1./sqrt(1.-vs[i-1]*vs[i-1])
        rho = rhos[i-1]
        r_bs[i] = ( r_1**(alpha+1.) + (alpha+1.)*dm/gamma/rho )**(1./(alpha+1.))

    return r_bs

def make_m_bs(r_bs, vs, rhos, alpha):
    m_bs = [0. for r_b in r_bs]

    for i in xrange(3, len(m_bs)):
        r_1 = r_bs[i-1]
        r_2 = r_bs[i]
        gamma = 1./sqrt(1.-vs[i-1]*vs[i-1])
        rho = rhos[i-1]
        m_bs[i] = m_bs[i-1] + gamma*rho/(alpha+1.)*(r_2**(alpha+1.) - r_1**(alpha+1.))

    for i in range(0, 2)[::-1]:
        r_1 = r_bs[i]
        r_2 = r_bs[i+1]
        gamma = 1./sqrt(1.-vs[i]*vs[i])
        rho = rhos[i]
        m_bs[i] = m_bs[i+1] - gamma*rho/(alpha+1.)*(r_2**(alpha+1.) - r_1**(alpha+1.))

    return m_bs

def get_consts(vs, rhos, ps, bs, r_bs, gamma_ad, alpha):
    Vs = []
    Ss = []
    Es = []
    Ms = []

    for i in xrange(len(vs)):
        v = vs[i]
        rho = rhos[i]
        p = ps[i]
        b = bs[i]
        r = .5*(r_bs[i+1]+r_bs[i])

        gamma = 1./sqrt(1.-v*v)
        # print i, gamma_ad, p, rho
        w = gamma_ad/(gamma_ad-1.)*p/rho
        sigma = b*b/rho
        p_b = .5*b*b
        h_star = 1. + w + sigma
        p_star = p + p_b

        V = 1./gamma/rho
        S = V*gamma*gamma*rho*h_star*v
        E = V*(gamma*gamma*rho*h_star - p_star)

        if alpha == 2:
            M = b/r/rho
        elif alpha == 1:
            M = b/sqrt(r)/rho # Not yet implemented in the C++ code
        elif alpha == 0:
            M = b/rho

        Vs.append(V)
        Ss.append(S)
        Es.append(E)
        Ms.append(M)

    return Vs, Ss, Es, Ms

# RAD part

def Wien_norm(x):
    return .5*x*x*exp(-x)

def Planck_norm(x):
    return x*x/(exp(x) - 1.)

def bisect(xs, x, no_ghosts=False):

    # Returns l, such that xs[l] < x < xs[l+1]
    # Assumes that xs is ordered, such that xs[i] < xs[i+1] for all i

    l = 0
    h = len(xs)-1
    if no_ghosts:
        l += 2
        h -= 2

    while h-l != 1:
        m = (l+h)/2
        if xs[m] > x:
            h = m
        else:
            l = m
    return l

def bisect_rev(xs, x, no_ghosts=False):

    # Returns l, such that xs[l] > x > xs[l+1]
    # Assumes that xs is ordered in the reverse way, such that xs[i] > xs[i+1] for all i
    if x > xs[0]:
        return 0
    elif x < xs[-1]:
        return len(xs)-1

    xs_rev = xs[::-1]
    l_rev = bisect(xs_rev, x, no_ghosts=no_ghosts)
    l = len(xs_rev) - l_rev - 2
    return l

class NumDistribution():
    def __init__(self, n):
        self.n = n
        self.Ps = [0. for i in xrange(n)]
        self.xs = [0. for i in xrange(n)]

    def load_Wien_norm(self, bias_by_one_power_of_x=False):
        # If bias_by_one_power_of_x == True, then multiplies distribution by one power of x
        # This results in more photons drawn at large x
        # These photons then get a lower weight (w \propto 1/x)

        x_max = 100.
        dx = x_max/float(self.n)

        # "Integrating"
        self.xs[0] = 0
        self.Ps[0] = 0
        for i in xrange(1, self.n):
            self.xs[i] = i*dx
            x = .5*(self.xs[i]+self.xs[i-1])
            if bias_by_one_power_of_x == True:
                self.Ps[i] = self.Ps[i-1] + x*Wien_norm(x)*dx
            else:
                self.Ps[i] = self.Ps[i-1] + Wien_norm(x)*dx

        # Normalizing
        P_max = self.Ps[-1]
        self.Ps = [P/P_max for P in self.Ps]

    def load_Planck_norm(self):
        x_max = 100.
        dx = x_max/float(self.n)

        # "Integrating"
        self.xs[0] = 0
        self.Ps[0] = 0
        for i in xrange(1, self.n):
            self.xs[i] = i*dx
            x = .5*(self.xs[i]+self.xs[i-1])
            self.Ps[i] = self.Ps[i-1] + Planck_norm(x)*dx

        # Normalizing
        P_max = self.Ps[-1]
        self.Ps = [P/P_max for P in self.Ps]

    def load_r_dist(self, r_bs, vs, rhos, Z_gamma, alpha):
        # Z_gamma is the photon-to-proton ratio

        n_bs = len(r_bs)
        r_min = r_bs[2]
        r_max = r_bs[n_bs-3]
        A = 1
        dr = (r_max-r_min)/float(len(self.Ps)-1)

        # Filling cumulative distribution
        self.xs[0] = r_min
        self.Ps[0] = 0.

        # for(unsigned int i=0; i<Ps.size()-1; i++) {
        for i in xrange(len(self.Ps)-1):
            # Next location
            self.xs[i+1] = self.xs[i] + dr

            # Next probability
            r_avg = .5*(self.xs[i+1] + self.xs[i])
            if alpha == 2:
                A = r_avg*r_avg
            elif alpha == 1:
                A = r_avg
            elif alpha == 0:
                A = 1.

            # Find corresponding bin
            l = bisect(r_bs, r_avg)

            # Find bin properties
            v = vs[l];
            rho = rhos[l];
            gamma = 1./sqrt(1.-v*v);

            # Integrating the cumulative distribution
            f = gamma*rho*A*Z_gamma/m_p # Really N_gamma*f, where f is prob. dens. of r
            self.Ps[i+1] = self.Ps[i]+f*dr;

        # Normalizing the distribution
        N_gamma = self.Ps[-1]
        self.Ps = [P/N_gamma for P in self.Ps]

        # This specific loading function returns the total number of physical photons
        return N_gamma

    def sample(self, P):
        l = bisect(self.Ps, P)
        x_l = self.xs[l]
        x_r = self.xs[l+1]
        P_l = self.Ps[l]
        P_r = self.Ps[l+1]
        x = x_l + (P-P_l)/(P_r-P_l)*(x_r-x_l)
        return x

def get_theta(rho, p, Z_pm, f_heat):
    # Returns the electron temperature
    theta = m_p/m_e*p/rho/(1.+Z_pm)/f_heat
    return theta

def get_theta_e(rho, p, Z_pm, xi, f_heat):
    # Returns the electron temperature
    theta_e = (m_p/m_e)*(p/rho)*xi/(1.+Z_pm*xi)/f_heat
    return theta_e

def get_theta_p(rho, p, Z_pm, xi, f_heat):
    # Returns the proton temperature
    theta_p = (m_p/m_e)*(p/rho)/(1.+Z_pm*xi)/f_heat
    return theta_p

class RadHeader():
    def __init__(self):
        self.n_phs = 0
        self.n_bs = 0
        self.eps_min = 0
        self.eps_max = 0
        self.n_bs_spec = 0
        self.n_eps_bs = 0
        self.n_el_spec_bins = 0
        self.alpha = 0
        self.f_heat = 0
        self.gamma_ad = 0
        self.t = 0
        self.iteration = 0
        self.photons_per_split = 0
        self.w_i = 0
        self.n_eps_splits_bins = 0

    def set_n_phs(self, n_phs): self.n_phs = n_phs
    def set_n_bs(self, n_bs): self.n_bs = n_bs
    def set_eps_min(self, eps_min): self.eps_min = eps_min
    def set_eps_max(self, eps_max): self.eps_max = eps_max
    def set_n_bs_spec(self, n_bs_spec): self.n_bs_spec = n_bs_spec
    def set_n_eps_bs(self, n_eps_bs): self.n_eps_bs = n_eps_bs
    def set_n_el_spec_bins(self, n_el_spec_bins): self.n_el_spec_bins = n_el_spec_bins
    def set_alpha(self, alpha): self.alpha = alpha
    def set_f_heat(self, f_heat): self.f_heat = f_heat
    def set_gamma_ad(self, gamma_ad): self.gamma_ad = gamma_ad
    def set_t(self, t): self.t = t
    def set_iteration(self, iteration): self.iteration = iteration
    def set_photons_per_split(self, photons_per_split): self.photons_per_split = photons_per_split
    def set_w_i(self, w_i): self.w_i = w_i
    def set_n_eps_splits_bins(self, n_eps_splits_bins): self.n_eps_splits_bins = n_eps_splits_bins

    def get_n_phs(self): return self.n_phs
    def get_n_bs(self): return self.n_bs
    def get_eps_min(self): return self.eps_min
    def get_eps_max(self): return self.eps_max
    def get_n_bs_spec(self): return self.n_bs_spec
    def get_n_eps_bs(self): return self.n_eps_bs
    def get_n_el_spec_bins(self): return self.n_el_spec_bins
    def get_alpha(self): return self.alpha
    def get_f_heat(self): return self.f_heat
    def get_gamma_ad(self): return self.gamma_ad
    def get_t(self): return self.t
    def get_iteration(self): return self.iteration
    def get_photons_per_split(self): return self.photons_per_split
    def get_w_i(self): return self.w_i
    def get_n_eps_splits_bins(self): return self.n_eps_splits_bins

    def print_header(self, header_name):
        print header_name
        print '  n_phs = %i' % (self.n_phs)
        print '  n_bs = %i' % (self.n_bs)
        print '  eps_min = %4.4e' % (self.eps_min)
        print '  eps_max = %4.4e' % (self.eps_max)
        print '  n_bs_spec = %i' % (self.n_bs_spec)
        print '  n_eps_bs = %i' % (self.n_eps_bs)
        print '  n_el_spec_bins = %i' % (self.n_el_spec_bins)
        print '  alpha = %i' % (self.alpha)
        print '  f_heat = %4.4e' % (self.f_heat)
        print '  gamma_ad = %4.4f' % (self.gamma_ad)
        print '  t = %4.4e' % (self.t)
        print '  iteration = %i' % (self.iteration)
        print '  photons_per_split = %i' % (self.photons_per_split)
        print '  w_i = %4.4e' % (self.w_i)
        print '  n_eps_splits_bins = %i' % (self.n_eps_splits_bins)

def save_rsf_file(rad_header, mhd_grid, Z_gamma, output_file_name):

    # Z_pm assumed unity
    Z_pm = 1.

    # Radial distribution
    r_dist = NumDistribution(rad_header.get_n_bs())
    N_gamma = r_dist.load_r_dist(mhd_grid.get_r_bs(), mhd_grid.get_vs(), mhd_grid.get_rhos(), Z_gamma, rad_header.get_alpha())

    # Compute weight
    w = N_gamma/rad_header.get_n_phs()
    rad_header.set_w_i(w)

    # Bias the spectrum towards high energies?
    bias_spectrum = True

    # Spectrum distribution
    spec_norm_dist = NumDistribution(300)
    if bias_spectrum:
        spec_norm_dist.load_Wien_norm(bias_by_one_power_of_x=bias_spectrum)
        x_avg = 3. # Wien spectrum, x = eps/theta
    else:
        spec_norm_dist.load_Wien_norm(bias_by_one_power_of_x=bias_spectrum)
    # spec_norm_dist.load_Planck_norm()

    with open(output_file_name, 'w') as f:

        # Write header
        strings = ['n_phs          %i' % (rad_header.get_n_phs()),
                   'n_bs           %i' % (rad_header.get_n_bs()),
                   'eps_min        %18.12e' % (rad_header.get_eps_min()),
                   'eps_max        %18.12e' % (rad_header.get_eps_max()),
                   'n_bs_spec      %i' % (rad_header.get_n_bs_spec()),
                   'n_eps_bs       %i' % (rad_header.get_n_eps_bs()),
                   'n_el_spec_bins %i' % (rad_header.get_n_el_spec_bins()),
                   'alpha          %i' % (rad_header.get_alpha()),
                   'f_heat         %18.12e' % (rad_header.get_f_heat()),
                   'gamma_ad       %18.12e' % (rad_header.get_gamma_ad()),
                   't              %18.12e' % (rad_header.get_t()),
                   'iter           %i' % (rad_header.get_iteration()),
                   'photons_per_split %i' % (rad_header.get_photons_per_split()),
                   'w_i            %18.12e' % (rad_header.get_w_i()),
                   'n_eps_splits_bins %i' % (rad_header.get_n_eps_splits_bins()),
                   '',
                   '---',
                   '']
        for string in strings:
            f.write(string+'\n')

        # Eps splits [NOT YET IMPLEMENTED!]
        string = ''
        for i in xrange(rad_header.get_n_eps_splits_bins()):
            string += ' %18.12e' % (1.)
        f.write(string+'\n')

        # Z_pm (assumed unity)
        string = ''
        for i in xrange(rad_header.get_n_bs()-1):
            string += ' %18.12e' % (Z_pm)
        f.write(string+'\n')

        # Photon generation
        for i in xrange(rad_header.get_n_phs()):
            # draw r
            r = r_dist.sample(random())
            l = bisect(mhd_grid.get_r_bs(), r, no_ghosts=True)
            v = mhd_grid.get_vs()[l]
            rho = mhd_grid.get_rhos()[l]
            p = mhd_grid.get_ps()[l]/rad_header.get_f_heat()
            gamma = 1./sqrt(1.-v*v)

            # draw mu
            if v == 0:
                mu = 2.*random()-1.
            else:
                u = sqrt(1./gamma/gamma/gamma/gamma/((1.-v)*(1.-v) + 4.*v*random()))
                mu = (1.-u)/v

            # draw eps
            theta = get_theta(rho, p, Z_pm, 1.)
            eps_prime = theta*spec_norm_dist.sample(random())
            eps = eps_prime/gamma/(1.-mu*v)

            string = '%18.12e ' % (r)
            string += '%18.12e ' % (mu)
            string += '%18.12e ' % (eps)
            if bias_spectrum == True:
                eps_avg = x_avg*theta
                string += '%18.12e' % (w*(eps_avg/eps_prime))
                # string += '%18.12e' % (w*(gamma*eps_avg/eps))
            else:
                string += '%18.12e' % (w)
            f.write(string+'\n')

# Main

def main():
    parser = argparse.ArgumentParser(description='a file that generates MHD (and RAD) state files (.msf, .rsf) from provided initial conditions (m, r, v, rho, p, b) functions.')
    parser.add_argument('INIT_COND_FILE', help='the Python file that contains the initial condition functions')
    parser.add_argument('-v', '--plot-only-v', action='store_true', help='if no output file provided, plots only v')
    parser.add_argument('-rho', '--plot-only-rho', action='store_true', help='if no output file provided, plots only rho')
    parser.add_argument('-p', '--plot-only-p', action='store_true', help='if no output file provided, plots only p')
    parser.add_argument('-b', '--plot-only-b', action='store_true', help='if no output file provided, plots only b')
    parser.add_argument('-u', '--plot-only-betagamma', action='store_true', help='if no output file provided, plots only betagamma')
    parser.add_argument('-g', '--plot-only-gamma', action='store_true', help='if no output file provided, plots only gamma')
    parser.add_argument('-dm', '--plot-only-dm', action='store_true', help='if no output file provided, plots only mass bin sizes')
    parser.add_argument('-dtau', '--plot-only-dtau', action='store_true', help='if no output file provided, plots only column optical depth for each bin')
    parser.add_argument('--reverse', action='store_true', help='if showing plot, use m_rev on the x-axis')
    parser.add_argument('--marker', action='store_true', help='if showing plot, use markers')
    parser.add_argument('--times-4-pi', action='store_true', help='if showing plot, multiply mass coordinate by 4 pi')
    parser.add_argument('-tx', '--toggle-xlog', action='store_true', help='if showing plot, turn ON log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_false', help='if showing plot, turn OFF log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--is-homologous', action='store_true', help='plots t = r/v; if homologous, t should be constant over the grid')
    parser.add_argument('--plot-time-delays', action='store_true', help='plots (r_o - r)/c and exit')
    parser.add_argument('--print-time-delays', action='store_true', help='prints time delays and exit')
    parser.add_argument('--print-piston-speeds', action='store_true', help='prints the value of betagamma at the boundaries (useful if piston boundaries are used) and exit')
    parser.add_argument('--show-ghosts', action='store_true', help='if showing plot, also show the ghost cells')
    parser.add_argument('--save', action='store_true', help='instead of plotting, save the state file(s)')
    args = parser.parse_args()

    # Import the module
    full_path_to_file = os.getcwd() + '/' + args.INIT_COND_FILE
    path_name = full_path_to_file[::-1].split('/', 1)[1][::-1]
    file_name = full_path_to_file[::-1].split('/', 1)[0][::-1][:-3]
    sys.path.append(path_name)
    init_cond_funcs = __import__(file_name)

    # MHD part

    # Get the grid, prims, consts
    mhd_basic_params, mhd_problem_params = init_cond_funcs.get_mhd_params()
    m_bs = init_cond_funcs.get_m_bs(mhd_basic_params, mhd_problem_params)
    vs, rhos, ps, bs = get_prim_vectors(m_bs, init_cond_funcs.get_prims, mhd_basic_params, mhd_problem_params)

    if len(mhd_basic_params) == 8:
        msf_filename, N, M_o, R_i, alpha, gamma_ad, t, mhd_iteration = mhd_basic_params
        integrate_forward = True
    else:
        msf_filename, N, M_o, R_i, alpha, gamma_ad, t, mhd_iteration, integrate_forward = mhd_basic_params

    if integrate_forward:
        r_bs = make_r_bs(m_bs, vs, rhos, R_i, alpha)
    else:
        r_bs = make_r_bs_backwards(m_bs, vs, rhos, R_i, alpha)

    Vs, Ss, Es, Ms = get_consts(vs, rhos, ps, bs, r_bs, gamma_ad, alpha)

    if args.print_time_delays:
        c = 2.99e10
        dr_1 = r_bs[-1] - r_bs[0]
        print '(r_outer - r_inner)/c = %4.2e s' % (dr_1/c)

        dr_2 = r_bs[0]*(1.-vs[0])
        # gamma = 3.
        # v = sqrt(1.-1./gamma/gamma)
        # dr_2 = r_bs[0]*(1.-v)
        print 'r_inner*(1-v_inner) = %4.2e s' % (dr_2/c)

        print 'To be consistent, %4.2f s should be subtracted from the observer time' % ((dr_1-dr_2)/c)
        raise SystemExit

    # Make the Header
    mhd_header = Header()
    mhd_header.set_n_bs(N+5)
    mhd_header.set_alpha(alpha)
    mhd_header.set_gamma_ad(gamma_ad)
    mhd_header.set_t(t)
    mhd_header.set_iteration(mhd_iteration)

    # Make the MHD_Grid
    mhd_grid = MHD_Grid()
    mhd_grid.set_m_bs(m_bs)
    mhd_grid.set_r_bs(r_bs)
    mhd_grid.set_vs(vs)
    mhd_grid.set_rhos(rhos)
    mhd_grid.set_ps(ps)
    mhd_grid.set_bs(bs)
    mhd_grid.set_Vs(Vs)
    mhd_grid.set_Ss(Ss)
    mhd_grid.set_Es(Es)
    mhd_grid.set_Ms(Ms)

    # Piston speeds?
    if args.print_piston_speeds:
        v_L = vs[2]
        v_R = vs[-2]
        betagamma_L = v_L/sqrt(1.-v_L*v_L)
        betagamma_R = v_R/sqrt(1.-v_R*v_R)
        print 'betagamma_L = %4.4e' % (betagamma_L)
        print 'betagamma_R = %4.4e' % (betagamma_R)
        raise SystemExit

    # RAD part

    # Is there information about the .rsf file?
    rad_params = init_cond_funcs.get_rad_params()
    rsf_filename, N_MC, Z_gamma, f_heat, eps_min, eps_max, n_bs_spec, n_eps_bs, n_el_spec_bins, rad_iteration, n_eps_splits_bins, photons_per_split = rad_params
    N_MC = int(N_MC)
    if rsf_filename == 'None':
        rsf_filename = None

    # Save the state file(s)
    if args.save:
        if rsf_filename:
            rad_header = RadHeader()
            rad_header.set_n_phs(N_MC)
            rad_header.set_n_bs(mhd_header.get_n_bs())
            rad_header.set_eps_min(eps_min)
            rad_header.set_eps_max(eps_max)
            rad_header.set_n_bs_spec(n_bs_spec)
            rad_header.set_n_eps_bs(n_eps_bs)
            rad_header.set_n_el_spec_bins(n_el_spec_bins)
            rad_header.set_alpha(mhd_header.get_alpha())
            rad_header.set_f_heat(f_heat)
            rad_header.set_gamma_ad(mhd_header.get_gamma_ad())
            rad_header.set_t(mhd_header.get_t())
            rad_header.set_iteration(rad_iteration)
            rad_header.set_photons_per_split(photons_per_split)
            rad_header.set_n_eps_splits_bins(n_eps_splits_bins)

            # Modify the MHD grid
            # print '-- p = %6.2e' % (ps[10])
            ps = [(2./Z_gamma)*f_heat*p for p in mhd_grid.get_ps()]
            # print '-- p = %6.2e' % (ps[10])
            mhd_grid.set_ps(ps)
            Vs, Ss, Es, Ms = get_consts(mhd_grid.get_vs(), mhd_grid.get_rhos(), mhd_grid.get_ps(), mhd_grid.get_bs(), mhd_grid.get_r_bs(), mhd_header.get_gamma_ad(), mhd_header.get_alpha())
            mhd_grid.set_Vs(Vs)
            mhd_grid.set_Ss(Ss)
            mhd_grid.set_Es(Es)
            mhd_grid.set_Ms(Ms)

            print "Saving '"+msf_filename+"'"
            save_mhd_state_file(mhd_header, mhd_grid, msf_filename)

            # Compute w_i, save the RAD header, generate and save the photons
            print "Saving '"+rsf_filename+"'"
            save_rsf_file(rad_header, mhd_grid, Z_gamma, rsf_filename)

        else:

            print "Saving '"+msf_filename+"'"
            save_mhd_state_file(mhd_header, mhd_grid, msf_filename)

    else:
        xlog=args.toggle_xlog
        ylog=args.toggle_ylog
        if N < 100 or args.marker:
            marker = '.'
        else:
            marker = None
        m_label = r'$m \, \mathrm{[g/ster]}$'
        if args.times_4_pi:
            m_bs = [m_b*4.*pi for m_b in m_bs]
            m_label = r'$m \, \mathrm{[g]}$'
        ms = [.5*(m_bs[i]+m_bs[i+1]) for i in xrange(len(vs))]
        if args.reverse:
            m_tot = m_bs[-3]
            ms = [m_tot - m for m in ms]
        watermark = r'$\mathrm{INIT. \, CONDITIONS}$'

        dms = [m_bs[j+1]-m_bs[j] for j in xrange(len(m_bs)-1)]

        R_o = r_bs[-3]
        if not args.show_ghosts:
            ms = ms[2:-2]
            vs = vs[2:-2]
            rhos = rhos[2:-2]
            ps = ps[2:-2]
            bs = bs[2:-2]
            dms = dms[2:-2]

        from plot_snapshots import plot_data
        if args.is_homologous:
            print 'Plotting t_hom...'
            rs = [.5*(r_bs[i]+r_bs[i+1]) for i in xrange(len(vs))]
            t_homs = [r/v for r, v in zip(rs, vs)]
            plot_data([ms], [[t_homs]], [0], m_label, [r'$r/v$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_time_delays:
            print 'Plotting time delays...'
            c = 2.99e10
            rs = [.5*(r_bs[i]+r_bs[i+1]) for i in xrange(len(vs))]
            Delta_ts = [(R_o - r)/c for r in rs]
            plot_data([ms], [[Delta_ts]], [0], m_label, [r'$(R_o - r)/c$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_only_v:
            if ylog and min(vs) <= 0:
                plot_data([ms], [[[abs(v) for v in vs]]], [0], r'$m$', [r'$|v/c|$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
            else:
                plot_data([ms], [[vs]], [0], m_label, [r'$v/c$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_only_betagamma:
            print 'Plotting betagamma...'
            betagammas = [v/sqrt(1.-v*v) for v in vs]
            plot_data([ms], [[betagammas]], [0], m_label, [r'$\gamma\beta$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_only_gamma:
            print 'Plotting gamma...'
            gammas = [1./sqrt(1.-v*v) for v in vs]
            plot_data([ms], [[gammas]], [0], m_label, [r'$\gamma$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_only_rho:
            print 'Plotting rho...'
            plot_data([ms], [[rhos]], [0], m_label, [r'$\rho \, \mathrm{[g/cm^3]}$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_only_p:
            print 'Plotting p...'
            plot_data([ms], [[ps]], [0], m_label, [r'$p/c^2 \, \mathrm{[g/cm^3]}$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_only_b:
            print 'Plotting b...'
            plot_data([ms], [[bs]], [0], m_label, [r'$b$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_only_dm:
            print 'Plotting dm...'
            plot_data([ms], [[dms]], [0], m_label, [r'$\delta m$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
        elif args.plot_only_dtau:
            print 'Plotting dtau...'
            if not args.times_4_pi and alpha == 2:
                dms = [4.*pi*dm for dm in dms]
            m_p = 1.6726231e-24
            sigma_T = 6.6524e-25
            kappa_T = sigma_T/m_p
            if alpha == 0:
                dtaus = [kappa_T*dm for dm in dms]
            elif alpha == 2:
                rs = [.5*(r_bs[i]+r_bs[i+1]) for i in xrange(len(vs))]
                dtaus = [kappa_T*dm/4./pi/r/r for dm, r in zip(dms, rs)]
            plot_data([ms], [[dtaus]], [0], m_label, [r'$\delta \tau$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)

        else:
            print 'Plotting all prims...'
            if ylog and min(vs) <= 0:
                plot_data([ms], [[[abs(v) for v in vs]]], [0], m_label, [r'$|v/c|$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
            else:
                plot_data([ms], [[vs]], [0], m_label, [r'$v/c$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
            plot_data([ms], [[rhos]], [0], m_label, [r'$\rho \, \mathrm{[g/cm^3]}$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
            plot_data([ms], [[ps]], [0], m_label, [r'$p/c^2 \, \mathrm{[g/cm^3]}$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)
            if max(bs) != 0:
                plot_data([ms], [[bs]], [0], m_label, [r'$b$'], xlog=xlog, ylog=ylog, marker=marker, watermark=watermark, xlims=args.x_lims, ylims=args.y_lims)

if __name__ == '__main__':
    main()
