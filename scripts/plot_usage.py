#!/usr/bin/env python

import os
import sys
import argparse
from matplotlib import pyplot as plt

from plot_snapshots import get_colors, import_mplstyle
from plot_tim import read_tim_file

def show_time_chart(valuess, NUM_PROCs, xlog=None, xlims=None, days=None, minutes=None, seconds=None, norm=None, show_sum=None, save_name=None):
    import_mplstyle('radshock.mplstyle')

    fig, ax = plt.subplots(figsize=(13, 8))
    # ax.tick_params(axis=u'both', which=u'both',length=0)
    # ax.tick_params(axis='x', which='x', length=0)
    ax.tick_params(left=False)

    if show_sum:
        labels = [r'$\mathrm{Saving \, all \, data \, (0)}$',
                  '',
                  r'$\mathrm{All \, hydrodynamics \, (1)}$',
                  r'$\mathrm{MPI \, (1.1)}$',
                  '',
                  r'$\mathrm{All \, radiation \, (2)}$',
                  r'$\mathrm{MPI \, (2.1)}$',
                  r'$\mathrm{Propagation \, (2.2)}$',
                  r'$\mathrm{Pairs \, (2.3)}$',
                  r'$\mathrm{Smoothing \, (2.4)}$',
                  r'$\mathrm{Moments \, (2.5)}$']

    else:
        labels = [r'$\mathrm{Saving \, all \, data}$',
                  '',
                  r'$\mathrm{All \, hydrodynamics}$',
                  r'$\mathrm{MPI}$',
                  '',
                  r'$\mathrm{All \, radiation}$',
                  r'$\mathrm{MPI}$',
                  r'$\mathrm{Propagation}$',
                  r'$\mathrm{Pairs}$',
                  r'$\mathrm{Smoothing}$',
                  r'$\mathrm{Moments}$']

    colors = ['dodgerblue',
              'k',
              (.2, .2, .2),
              (.5, .5, .5),
              'k',
              'gold',
              'khaki',
              '#FCE205',
              '#D2B55B',
              '#FFC30B',
              '#F8DE7E']

    if show_sum:
        valuess_new = []
        for values in valuess:
            value_rad = values[6] + values[7] + values[8] + values[9] + values[10]
            value_tot = values[0] + values[2] + values[5]
            values_new = values[:6] + [value_rad] + values[6:] + [value_tot]
            valuess_new.append(values_new)
        valuess = valuess_new

        # labels.insert(6, r'$\mathrm{Sum \, radiation}$')
        labels.insert(6, r'$\mathrm{Sum\,of\,(2.x)}$')
        colors.insert(6, 'pink')
        # labels.insert(6, r'$\mathrm{(sum\,of\,next\,5)}$')
        labels.append(r'$\mathrm{Sum\,of\,(x)}$')
        colors.append('pink')

    # Plot hours instead of seconds
    if not norm:
        valuess = [[value/60./60. for value in values] for values in valuess]
        if seconds:
            valuess = [[value*60.*60. for value in values] for values in valuess]
        elif minutes:
            valuess = [[value*60. for value in values] for values in valuess]
        elif days:
            valuess = [[value/24. for value in values] for values in valuess]

    if xlog:
        xmin = min([min(filter(lambda x: x > 1e-8, values)) for values in valuess])
        xmax = max([max(filter(lambda x: x > 1e-8, values)) for values in valuess])
        ax.set_xlim(xmin/2., xmax*2.)
        ax.set_xscale('log')
        left = 1e-2*xmin
    else:
        left = 0

    if norm and not xlog:
        ax.set_xlim(0, 1)
    if xlims:
        ax.set_xlim(xlims[0], xlims[1])

    # Plot the horizontal bars
    w_bar = .67
    w_bar = .8
    N = float(len(valuess))
    w_i = w_bar/N
    ys = range(len(labels))
    dw = .5*w_bar/N
    dys = [i*dw-.5*w_bar for i in xrange(int(2*N)) if i%2]
    alphas = [(float(i)+1.)/len(valuess) for i in xrange(len(valuess))][::-1]
    dalpha = .8/float(len(valuess))
    alphas = [1. - i*dalpha for i in xrange(len(valuess))]

    for values, dy, alpha in zip(valuess, dys, alphas):
        ax.barh([y + dy for y in ys], values, [w_i for value in values], align='center', color=colors, left=left, zorder=2, alpha=alpha)

    # Fix labels
    ax.set_yticks(range(len(labels)))
    ax.set_yticklabels(labels)
    ax.invert_yaxis()

    # Making the x-label
    if NUM_PROCs[0] is not None:
        proc_string = r'$%i$' % (NUM_PROCs[0])
    else:
        proc_string = r'$?$'
    for i in xrange(len(NUM_PROCs)-1):
        if NUM_PROCs[i+1] is not None:
            proc_string += r'$, %i$' % (NUM_PROCs[i+1])
        else:
            proc_string += r'$, ?$'
    if norm:
        ax.set_xlabel(r'$\mathrm{Normalized \, wall \, time \, on \, }$'+proc_string+r'$\mathrm{ \, thread(s)}$')
    elif seconds:
        ax.set_xlabel(r'$\mathrm{Seconds \, of \, wall \, time \, on \, }$'+proc_string+r'$\mathrm{ \, thread(s)}$')
    elif minutes:
        ax.set_xlabel(r'$\mathrm{Minutes \, of \, wall \, time \, on \, }$'+proc_string+r'$\mathrm{ \, thread(s)}$')
    elif days:
        ax.set_xlabel(r'$\mathrm{Days \, of \, wall \, time \, on \, }$'+proc_string+r'$\mathrm{ \, thread(s)}$')
    else:
        ax.set_xlabel(r'$\mathrm{Hours \, of \, wall \, time \, on \, }$'+proc_string+r'$\mathrm{ \, thread(s)}$')

    ax.xaxis.grid(linestyle='-', linewidth=2, alpha=.15)
    plt.tight_layout()
    if save_name:
        plt.savefig(save_name)
    else:
        plt.show()
    plt.close()

def main():
    parser = argparse.ArgumentParser(description='this script shows a bar chart with timing information')
    parser.add_argument('INPUT_TIM_FILE', nargs='+', help='the input TIM file(s) (.tim)')
    parser.add_argument('-o', '--output-pdf-file', help='the output pdf file')
    parser.add_argument('--n-max', type=int, help='loads data only up to snapshot --n-max')
    parser.add_argument('--norm', action='store_true', help='normalizes all times by the total wall time')
    parser.add_argument('--days', action='store_true', help='convert time to days instead of hours')
    parser.add_argument('--minutes', action='store_true', help='convert time to minutes instead of hours')
    parser.add_argument('--seconds', action='store_true', help='convert time to seconds instead of hours')
    parser.add_argument('-tx', '--toggle-xlog', action='store_true', help='turn ON log scale on the x-axis')
    parser.add_argument('--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('--show-sum', action='store_true', help='plots also the sum of the radiation times (which should approximately equal the total radiation time)')
    parser.add_argument('--print-mpi', action='store_true', help='prints the fraction of the wall time taken by MPI')
    parser.add_argument('--quiet', action='store_true', help='no terminal output (except errors)')
    args = parser.parse_args()

    # Load the file
    valuess = []
    NUM_PROCs = []
    for filename in args.INPUT_TIM_FILE:
        data = read_tim_file(filename, args.n_max)
        NUM_PROC, ks, wts, wt_mhds, wt_rads, cts, iter_mhds, iter_rads, wt_mhd_mpis, wt_rad_mpis, wt_savs, wt_rad_props, wt_rad_pairs, wt_rad_smooths, wt_rad_moms = data
        values = [wt_savs[-1], 0, wt_mhds[-1], wt_mhd_mpis[-1], 0, wt_rads[-1], wt_rad_mpis[-1], wt_rad_props[-1], wt_rad_pairs[-1], wt_rad_smooths[-1], wt_rad_moms[-1]]
        if args.norm:
            values = [value/wts[-1] for value in values]
        valuess.append(values)
        NUM_PROCs.append(NUM_PROC)

        # Print information
        if not args.quiet:
            print "File '%s' contains %i snapshots" % (filename, len(ks))

            if args.print_mpi:
                print "MPI used %2.1f%% of the wall time" % ((wt_mhd_mpis[-1] + wt_rad_mpis[-1]) / wts[-1] * 100.)

    # Show the overview plot and exit
    show_time_chart(valuess, NUM_PROCs, xlog=args.toggle_xlog, xlims=args.x_lims, days=args.days, minutes=args.minutes, seconds=args.seconds, norm=args.norm, show_sum=args.show_sum,
                    save_name=args.output_pdf_file)

if __name__ == '__main__':
    main()
