#!/usr/bin/env python

import argparse
from numpy import pi, sqrt

class Header():
    def __init__(self):
        self.n_bs = 0
        self.alpha = 0
        self.gamma_ad = 0
        self.t = 0
        self.iteration = 0
        self.two_temp = 0

    def set_n_bs(self, n_bs): self.n_bs = n_bs
    def set_alpha(self, alpha): self.alpha = alpha
    def set_gamma_ad(self, gamma_ad): self.gamma_ad = gamma_ad
    def set_t(self, t): self.t = t
    def set_iteration(self, iteration): self.iteration = iteration
    def set_two_temp(self, two_temp): self.two_temp = two_temp

    def get_n_bs(self): return self.n_bs
    def get_alpha(self): return self.alpha
    def get_gamma_ad(self): return self.gamma_ad
    def get_t(self): return self.t
    def get_iteration(self): return self.iteration

    def print_header(self, header_name):
        print header_name
        print '  n_bs = %i' % (self.n_bs)
        print '  alpha = %i' % (self.alpha)
        print '  gamma_ad = %4.4f' % (self.gamma_ad)
        print '  t = %4.4e' % (self.t)
        print '  iteration = %i' % (self.iteration)
        print

class MHD_Grid():
    def __init__(self):

        self.m_bs = []
        self.r_bs = []

        self.ms = []
        self.rs = []

        self.vs = []
        self.rhos = []
        self.ps = []
        self.bs = []

        self.Vs = []
        self.Ss = []
        self.Es = []
        self.Ms = []

    def set_m_bs(self, m_bs):
        self.m_bs = m_bs
        self.ms = [.5*(m_bs[i+1]+m_bs[i]) for i in xrange(len(m_bs)-1)]

    def set_r_bs(self, r_bs):
        self.r_bs = r_bs
        self.rs = [.5*(r_bs[i+1]+r_bs[i]) for i in xrange(len(r_bs)-1)]

    def set_vs(self, vs): self.vs = vs
    def set_rhos(self, rhos): self.rhos = rhos
    def set_ps(self, ps): self.ps = ps
    def set_bs(self, bs): self.bs = bs

    def set_Vs(self, Vs): self.Vs = Vs
    def set_Ss(self, Ss): self.Ss = Ss
    def set_Es(self, Es): self.Es = Es
    def set_Ms(self, Ms): self.Ms = Ms

    def get_n_bs(self): return len(self.m_bs)

    def get_m_bs(self): return self.m_bs
    def get_r_bs(self): return self.r_bs

    def get_ms(self): return self.ms
    def get_rs(self): return self.rs

    def get_vs(self): return self.vs
    def get_rhos(self): return self.rhos
    def get_ps(self): return self.ps
    def get_bs(self): return self.bs

    def get_Vs(self): return self.Vs
    def get_Ss(self): return self.Ss
    def get_Es(self): return self.Es
    def get_Ms(self): return self.Ms

def read_mhd_state_file(filename):
    OLD_SYNTAX = 0
    header = Header()
    mhd_grid = MHD_Grid()
    with open(filename, 'r') as f:

        header.set_n_bs(int(f.readline().split()[-1]))
        header.set_alpha(int(f.readline().split()[-1]))
        header.set_gamma_ad(float(f.readline().split()[-1]))
        # header.set_boundary_L(int(f.readline().split()[-1]))
        # header.set_boundary_R(int(f.readline().split()[-1]))
        # header.set_M_grav(float(f.readline().split()[-1]))
        header.set_t(float(f.readline().split()[-1]))
        header.set_iteration(int(f.readline().split()[-1]))
        header.set_two_temp(int(f.readline().split()[-1]))

        f.readline()
        f.readline()
        f.readline()

        mhd_grid.set_m_bs([float(number) for number in f.readline().split()])
        mhd_grid.set_r_bs([float(number) for number in f.readline().split()])

        mhd_grid.set_vs([float(number) for number in f.readline().split()])
        mhd_grid.set_rhos([float(number) for number in f.readline().split()])
        mhd_grid.set_ps([float(number) for number in f.readline().split()])
        mhd_grid.set_bs([float(number) for number in f.readline().split()])

        f.readline()

        if OLD_SYNTAX:

            mhd_grid.set_Vs([float(number) for number in f.readline().split()])

            f.readline()

            mhd_grid.set_Ss([float(number) for number in f.readline().split()])

            f.readline()

            mhd_grid.set_Es([float(number) for number in f.readline().split()])

            f.readline()

            mhd_grid.set_Ms([float(number) for number in f.readline().split()])

        else:

            mhd_grid.set_Vs([float(number) for number in f.readline().split()])
            mhd_grid.set_Ss([float(number) for number in f.readline().split()])
            mhd_grid.set_Es([float(number) for number in f.readline().split()])
            mhd_grid.set_Ms([float(number) for number in f.readline().split()])

    return header, mhd_grid

def save_mhd_state_file(header, mhd_grid, filename):
    OLD_SYNTAX = 0
    strings = []
    strings.append('n_bs       %i' % (header.get_n_bs()))
    strings.append('alpha      %i' % (header.get_alpha()))
    strings.append('gamma_ad   %20.16e' % (header.get_gamma_ad()))
    # strings.append('boundary_L %i' % (header.get_boundary_L()))
    # strings.append('boundary_R %i' % (header.get_boundary_R()))
    # strings.append('M_grav     %20.16e' % (header.get_M_grav()))
    strings.append('t          %20.16e' % (header.get_t()))
    strings.append('iter       %i' % (header.get_iteration()))
    strings.append('two_temp_version %i' % (1))
    strings.append('')
    strings.append('---')
    strings.append('')

    variables = [mhd_grid.get_m_bs(), mhd_grid.get_r_bs(), mhd_grid.get_vs(), mhd_grid.get_rhos(), mhd_grid.get_ps(), mhd_grid.get_bs()]
    for variable in variables:
        string = ''
        for x in variable:
            string += ' %20.16e' % (x)
        strings.append(string)
    strings.append('')

    if OLD_SYNTAX:
        variables = [mhd_grid.get_Vs(), mhd_grid.get_Ss(), mhd_grid.get_Es(), mhd_grid.get_Ms()]
        for variable in variables:
            string = ''
            for x in variable:
                string += ' %20.16e' % (x)
            strings.append(string)
            strings.append('')
    else:
        # variables = [mhd_grid.get_Vs(), mhd_grid.get_Ss(), mhd_grid.get_Es(), mhd_grid.get_Ms()]
        xis = [1. for V in mhd_grid.get_Vs()] # xi = theta_e/theta_p
        variables = [mhd_grid.get_Vs(), mhd_grid.get_Ss(), mhd_grid.get_Es(), mhd_grid.get_Ms(), xis]
        for variable in variables:
            string = ''
            for x in variable:
                string += ' %20.16e' % (x)
            strings.append(string)
        strings.append('')

    # for string in strings:
    #     if len(string) > 100:
    #         print string[:100]
    #     else:
    #         print string

    with open(filename, 'w') as f:
        for string in strings:
            f.write(string+'\n')

def read_and_save_rad_state_file(infilename, outfilename, n_bs, decrease_pressure_factor, reset_time, multiply_photons):
    with open(infilename, 'r') as f_in:
        with open(outfilename, 'w') as f_out:
            i = 0
            for line in f_in.readlines():

                # if i < 30:
                #     if len(line) > 100:
                #         print i, line[:100]
                #     else:
                #         print i, line[:-1]

                if i == 0 and multiply_photons > 1:
                    n_phs_old = int(line.split()[1])
                    string = 'n_phs          %i' % (multiply_photons*n_phs_old)
                    f_out.write(string+'\n')
                elif i == 1:
                    n_bs_old = int(line.split()[1])
                    string = 'n_bs           %i' % (n_bs)
                    f_out.write(string+'\n')
                elif i == 8:
                    f_heat = float(line.split()[1])
                    f_heat /= decrease_pressure_factor
                    string = 'f_heat         %18.12e' % (f_heat)
                    f_out.write(string+'\n')
                elif i == 19:
                    Z_pms = [float(number) for number in line.split()]
                    string = ''
                    for Z_pm in Z_pms[n_bs_old-n_bs:]:
                        string += ' %18.12e' % (Z_pm)
                    f_out.write(string+'\n')
                elif i == 12 and reset_time:
                    string = 't              %18.12e' % (0.)
                    f_out.write(string+'\n')
                elif i == 13 and reset_time:
                    string = 'iter           %i' % (0)
                    f_out.write(string+'\n')
                else:
                    if len(line.split()) == 4:
                        r, mu, eps, w = line.split()
                        w = float(w)/float(multiply_photons)
                        for j in xrange(multiply_photons):
                            f_out.write(r+' '+mu+' '+eps+' %18.12e\n' % (w))
                    else:
                        f_out.write(line)

                i += 1

def print_input_file(filename):
    with open(filename, 'r') as f:
        for line in f.readlines():
            if len(line) > 100:
                print line[:100]
            else:
                print line[:-1]

def get_consts(rs, vs, rhos, ps, bs, alpha, gamma_ad):
    Vs = []
    Ss = []
    Es = []
    Ms = []

    for r, v, rho, p, b in zip(rs, vs, rhos, ps, bs):

        gamma = 1./sqrt(1.-v*v)
        w = gamma_ad/(gamma_ad-1.)*p/rho
        sigma = b*b/rho
        p_b = .5*b*b
        h_star = 1. + w + sigma
        p_star = p + p_b

        V = 1./gamma/rho
        S = V*gamma*gamma*rho*h_star*v
        E = V*(gamma*gamma*rho*h_star - p_star)
        if alpha == 2:
            M = b/r/rho
        elif alpha == 0:
            M = b/rho

        Vs.append(V)
        Ss.append(S)
        Es.append(E)
        Ms.append(M)
    return Vs, Ss, Es, Ms

def get_modified_profile(m_bs, r_bs, vs, rhos, ps, bs, p1, p2, alpha):
    x1 = m_bs[p1]
    x2 = m_bs[p2]
    y1 = rhos[p1]
    y2 = rhos[p2]
    k_rho = (y2-y1)/(x2-x1)
    a_rho = y1-k_rho*x1

    y1 = ps[p1]
    y2 = ps[p2]
    k_p = (y2-y1)/(x2-x1)
    a_p = y1-k_p*x1

    y1 = bs[p1]
    y2 = bs[p2]
    k_b = (y2-y1)/(x2-x1)
    a_b = y1-k_b*x1

    # dm = m_bs[1]-m_bs[0]
    m_bs_rev = m_bs[:-2][::-1]
    r_bs_rev = r_bs[:-2][::-1]
    vs_rev = vs[:-2][::-1]
    rhos_rev = rhos[:-2][::-1]
    ps_rev = ps[:-2][::-1]
    bs_rev = bs[:-2][::-1]

    m_bs_new_rev = [m_bs_rev[0]]
    r_bs_new_rev = [r_bs_rev[0]]
    rhos_new_rev = []
    ps_new_rev = []
    bs_new_rev = []
    make_ghost_cells = -1
    p = min(p1, p2)

    for i in xrange(len(rhos_rev)):
        if i < len(r_bs)-p:

            dm = m_bs_rev[i]-m_bs_rev[i+1]
            rhos_new_rev.append(rhos_rev[i])
            ps_new_rev.append(ps_rev[i])
            bs_new_rev.append(bs_rev[i])
            r_bs_new_rev.append(r_bs_rev[i+1])
            m_bs_new_rev.append(m_bs_new_rev[-1]-dm)

        else:

            # dm = m_bs_rev[i+1]-m_bs_rev[i]

            v = vs_rev[-1]
            gamma = 1./sqrt(1.-v*v)
            rho_new = a_rho + k_rho*(m_bs_new_rev[-1]+.5*dm)
            p_new = a_p + k_p*(m_bs_new_rev[-1]+.5*dm)
            b_new = a_b + k_b*(m_bs_new_rev[-1]+.5*dm)
            r2 = r_bs_new_rev[-1]

            if alpha == 2:
                r1 = pow(r2*r2*r2-3.*dm/gamma/rho_new, 1./3.)
            elif alpha == 0:
                r1 = r2-dm/gamma/rho_new

            if make_ghost_cells > 0:
                make_ghost_cells -= 1
                rhos_new_rev.append(rho_new)
                ps_new_rev.append(p_new)
                bs_new_rev.append(b_new)
                r_bs_new_rev.append(r1)
                m_bs_new_rev.append(m_bs_new_rev[-1]-dm)
            elif make_ghost_cells == 0:
                break

            if make_ghost_cells == -1:
                if r1 > r_bs[2]:
                    rhos_new_rev.append(rho_new)
                    ps_new_rev.append(p_new)
                    bs_new_rev.append(b_new)
                    r_bs_new_rev.append(r1)
                    m_bs_new_rev.append(m_bs_new_rev[-1]-dm)
                else:
                    # modify previous bins!
                    r_bs_new_rev[-1] = r_bs[2]
                    r2 = r_bs_new_rev[-2]
                    r1 = r_bs_new_rev[-1]
                    if alpha == 2:
                        rho_mod = 3.*dm/gamma/(r2*r2*r2 - r1*r1*r1)
                    elif alpha == 0:
                        rho_mod = dm/gamma/(r2-r1)

                    # modify pressure to keep the temperature constant
                    ps_new_rev[-1] = ps_new_rev[-1]*rho_mod/rhos_new_rev[-1]

                    # modify density
                    rhos_new_rev[-1] = rho_mod

                    make_ghost_cells = 2

    rhos_new = rhos_new_rev[::-1]
    ps_new = ps_new_rev[::-1]
    bs_new = bs_new_rev[::-1]
    r_bs_new = r_bs_new_rev[::-1]
    m_bs_new = m_bs_new_rev[::-1]

    for i in xrange(2):
        rho_new = rhos[-3]
        p_new = ps[-3]
        b_new = bs[-3]
        v = vs[-3]
        gamma = 1./sqrt(1.-v*v)
        r1 = r_bs_new[-1]
        if alpha == 2:
            r2 = pow(r1*r1*r1+3.*dm/gamma/rho_new, 1./3.)
        elif alpha == 0:
            r2 = r1+dm/gamma/rho_new
        rhos_new.append(rho_new)
        ps_new.append(p_new)
        bs_new.append(b_new)
        r_bs_new.append(r2)
        m_bs_new.append(m_bs_new[-1]+dm)

    # re-normalize the mass coordinate
    m_norm = m_bs_new[2]
    m_bs_new = [m_b-m_norm for m_b in m_bs_new]
    return m_bs_new, r_bs_new, rhos_new, ps_new, bs_new

def get_new_grid(m_bs, r_bs, vs, rhos, ps, bs, p1, p2, alpha, gamma_ad, decrease_pressure=1., multiply_mass_bins=1):
    mhd_grid = MHD_Grid()
    if p1 and p2:
        m_bs_new, r_bs_new, rhos_new, ps_new, bs_new = get_modified_profile(m_bs, r_bs, vs, rhos, ps, bs, p1+2, p2+2, alpha)
    else:
        m_bs_new, r_bs_new, rhos_new, ps_new, bs_new = m_bs, r_bs, rhos, ps, bs

    ps_new = [p/decrease_pressure for p in ps_new]
    rs_new = [.5*(r_bs_new[i+1]+r_bs_new[i]) for i in xrange(len(r_bs_new)-1)]
    n_start = len(m_bs)-len(m_bs_new)
    vs_new = vs[n_start:]

    Vs_new, Ss_new, Es_new, Ms_new = get_consts(rs_new, vs_new, rhos_new, ps_new, bs_new, alpha, gamma_ad)
    mhd_grid.set_m_bs(m_bs_new)
    mhd_grid.set_r_bs(r_bs_new)
    mhd_grid.set_vs(vs_new)
    mhd_grid.set_rhos(rhos_new)
    mhd_grid.set_ps(ps_new)
    mhd_grid.set_bs(bs_new)
    mhd_grid.set_Vs(Vs_new)
    mhd_grid.set_Ss(Ss_new)
    mhd_grid.set_Es(Es_new)
    mhd_grid.set_Ms(Ms_new)
    return mhd_grid

def get_new_header(header, mhd_grid_new, reset_time):
    header_new = Header()
    header_new.set_n_bs(mhd_grid_new.get_n_bs())
    header_new.set_alpha(header.get_alpha())
    header_new.set_gamma_ad(header.get_gamma_ad())
    # header_new.set_boundary_L(header.get_boundary_L())
    # header_new.set_boundary_R(header.get_boundary_R())
    # header_new.set_M_grav(header.get_M_grav())
    if reset_time:
        header_new.set_t(0.)
        header_new.set_iteration(0)
    else:
        header_new.set_t(header.get_t())
        header_new.set_iteration(header.get_iteration())
    return header_new

def plot_grids(mhd_grids, var, points, show_ghosts, xlog, ylog, xlims=None, ylims=None, marker=None, ls='-', rev=False, alpha=0, hide_points=False, hide_original=False):

    import sys
    sys.path.append('/home/chr/Work/PythonScripts')
    from matplotlib import pyplot as plt
    plt.style.use('/home/chr/Work/PythonScripts/chris.mplstyle')

    for i in xrange(len(mhd_grids)):
        mhd_grid = mhd_grids[i]
        xs = mhd_grid.get_ms()
        if rev:
            x_tot = xs[-3]
            xs = [x_tot-x for x in xs]
            if alpha == 2:
                xs = [4.*pi*x for x in xs]

        if var == 'v':
            ys = mhd_grid.get_vs()
            ylabel = r'$v$'
        elif var == 'rho':
            ys = mhd_grid.get_rhos()
            ylabel = r'$\rho$'
        elif var == 'p':
            ys = mhd_grid.get_ps()
            ylabel = r'$p$'
        elif var == 'b':
            ys = mhd_grid.get_bs()
            ylabel = r'$b$'
        elif var == 'r':
            ys = mhd_grid.get_rs()
            ylabel = r'$r$'
        elif var == 'V':
            ys = mhd_grid.get_Vs()
            ylabel = r'$V$'
        elif var == 'S':
            ys = mhd_grid.get_Ss()
            ylabel = r'$S$'
        elif var == 'E':
            ys = mhd_grid.get_Es()
            ylabel = r'$E$'
        elif var == 'M':
            ys = mhd_grid.get_Ms()
            ylabel = r'$M$'
        elif var in ['u', 'betagamma']:
            vs = mhd_grid.get_vs()
            gammas = [1./sqrt(1.-v*v) for v in vs]
            ys = [v*gamma for v, gamma in zip(vs, gammas)]
            ylabel = r'$\beta\gamma$'
        elif var == 'dm':
            m_bs = mhd_grid.get_m_bs()
            ys = [m_bs[j+1]-m_bs[j] for j in xrange(len(m_bs)-1)]
            ylabel = r'$\delta m$'

        if not show_ghosts:
            xs = xs[2:-2]
            ys = ys[2:-2]

        plt.plot(xs, ys, marker=marker, markersize=10, ls=ls)

        if show_ghosts:
            plt.plot(xs[:2], ys[:2], marker='.', markersize=10, color='r')
            plt.plot(xs[-2:], ys[-2:], marker='.', markersize=10, color='r')

        if i == 0:
            xs_0 = xs
            ys_0 = ys

    if points and not hide_points:
        p1 = points[0]
        p2 = points[1]

        plt.plot([xs_0[p1]], [ys_0[p1]], marker='o', markerfacecolor='w', markeredgewidth=2, markersize=6, color='k')
        plt.plot([xs_0[p2]], [ys_0[p2]], marker='o', markerfacecolor='w', markeredgewidth=2, markersize=6, color='k')

    if not rev:
        plt.xlabel(r'$m$')
    else:
        plt.xlabel(r'$m \, \mathrm{(reverse)}$')
    # plt.xlabel(r'$m/4\pi$')
    plt.ylabel(ylabel)
    if xlims:
        plt.xlim(xlims[0], xlims[1])
    if ylims:
        plt.ylim(ylims[0], ylims[1])

    if ylog:
        plt.yscale('log')
    if xlog:
        plt.xscale('log')
    else:
        if not xlims:
            plt.xlim(min(xs_0), max(xs_0))

    plt.legend(loc='best')
    plt.tight_layout()
    plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='a script for modifying MHD (and also RAD) state files, getting rid of ugly (and slow) high density regions caused by initial shock launching. by default, the script loads and plots a desired VARIABLE vs mass coordinate (as measured from the outside) from the MHD state file. if given two POINTS, the script will linearly extrapolate the density, pressure and "b" from the innermost point and inwards, until the innermost radial coordinate of the original file. the new hydrodynamic profiles can then be saved to OUTPUT_MSF_FILE. this procedure gets rid of over-dense inner regions, that was caused by initial shock launching. it also decreases the number of spatial bins, and the total grid mass, while keeping the simulation volume intact (i.e. the radial range). artificially added "fake heat" that was useful during initial shock launching can also be removed, and the corresponding radiation state file can be modified to account for the changes.')
    parser.add_argument('INPUT_MSF_FILE', help='the input MHD save state file (*.msf)')
    parser.add_argument('-o', '--output-msf-file', help='the output MHD save state file')
    parser.add_argument('-p', '--points', nargs=2, type=int, help='two points (actually x-indices), from which a line can be extrapolated (counts from the boundary, NOT including ghosts)')
    parser.add_argument('--decrease-pressure', type=float, default=1, help='divide new grid pressure by this factor (in order to remove "fake heat" from the grid)')
    parser.add_argument('--no-rsf-mod', action='store_true', help='normally, finds the corresponding .rsf, modifies n_bs, f_heat and Z_pms, and saves it as the corresponding output file [for now, simply removes the innermost Z_pms], but if this flag is used, then skips this step')
    # parser.add_argument('--multiply-photons', type=int, default=1, help='copies all photons from the RAD state file the given number of times')
    # parser.add_argument('--reset-time', action='store_true', help='sets t=0, iter=0, k=0 in the output file(s)')
    parser.add_argument('-v', '--variable', default='rho', help='the variable to plot [default=rho]')
    parser.add_argument('--show-ghosts', action='store_true', help='also show ghost cells in the plot')
    parser.add_argument('-tx', '--toggle-xlog', action='store_true', help='turn ON log scale on the x-axis')
    parser.add_argument('-ty', '--toggle-ylog', action='store_false', help='turn OFF log scale on the y-axis')
    parser.add_argument('-xl', '--x-lims', nargs=2, type=float, help='limits on the x-axis')
    parser.add_argument('-yl', '--y-lims', nargs=2, type=float, help='limits on the y-axis')
    parser.add_argument('--hide-points', action='store_true', help='does NOT show the points')
    parser.add_argument('--hide-original', action='store_true', help='does NOT show the original grid')
    parser.add_argument('--no-plot', action='store_true', help='does NOT show the plot')
    parser.add_argument('--verbose', action='store_true', help='print detailed information to the terminal')
    args = parser.parse_args()

    header, mhd_grid = read_mhd_state_file(args.INPUT_MSF_FILE)

    if args.verbose:
        header.print_header('input MHD file header:')

    mhd_grids = []
    if not args.hide_original:
        mhd_grids.append(mhd_grid)
    if args.points or args.decrease_pressure != 1:

        if args.points:
            p1 = args.points[0]
            p2 = args.points[1]
        else:
            p1 = None
            p2 = None

        mhd_grid_new = get_new_grid(mhd_grid.get_m_bs(), mhd_grid.get_r_bs(), mhd_grid.get_vs(), mhd_grid.get_rhos(), mhd_grid.get_ps(), mhd_grid.get_bs(), p1, p2, header.get_alpha(),
                                    header.get_gamma_ad(), args.decrease_pressure)
        mhd_grids.append(mhd_grid_new)

    else:
        mhd_grid_new = None

    # if not args.no_plot and mhd_grid_new:
    if not args.no_plot and not args.output_msf_file:
        if args.variable != 'all':
            plot_grids(mhd_grids, args.variable, args.points, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, args.x_lims, args.y_lims, rev=True, alpha=header.get_alpha(),
                       hide_points=args.hide_points)
        else:
            if args.verbose:
                print 'plotting all primitives...'
            plot_grids(mhd_grids, 'v', args.points, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, args.x_lims, args.y_lims, rev=True, alpha=header.get_alpha(),
                       hide_points=args.hide_points)
            plot_grids(mhd_grids, 'rho', args.points, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, args.x_lims, args.y_lims, rev=True, alpha=header.get_alpha(),
                       hide_points=args.hide_points)
            plot_grids(mhd_grids, 'p', args.points, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, args.x_lims, args.y_lims, rev=True, alpha=header.get_alpha(),
                       hide_points=args.hide_points)

            isnull = True
            for b in mhd_grids[0].get_bs():
                if b > 0:
                    isnull = False
            if not isnull:
                plot_grids(mhd_grids, 'b', args.points, args.show_ghosts, args.toggle_xlog, args.toggle_ylog, args.x_lims, args.y_lims, rev=True, alpha=header.get_alpha(),
                           hide_points=args.hide_points)

    if args.output_msf_file and not mhd_grid_new:
        print '  error: no POINTS or DECREASE-PRESSURE provided -> new grid was not generated, and could not be saved'
        raise SystemExit

    #     # Simply saves the unmodified grid; allows for changing f_heat without modifying the snapshot
    #     header_new = header

    #     mhd_grid_new = get_new_grid(mhd_grid.get_m_bs(), mhd_grid.get_r_bs(), mhd_grid.get_vs(), mhd_grid.get_rhos(), mhd_grid.get_ps(), mhd_grid.get_bs(), p1, p2, header.get_alpha(),
    #                                 header.get_gamma_ad(), args.decrease_pressure, args.multiply_mass_bins)

    # if not args.no_rsf_mod and not args.output_msf_file:
    #     print '  error: will not modify rsf file if no mhd_state output file name is provided'
    #     raise SystemExit

    if args.output_msf_file:
        header_new = get_new_header(header, mhd_grid_new, 0)
        if args.verbose:
            header_new.print_header('output MHD file header:')
        save_mhd_state_file(header_new, mhd_grid_new, args.output_msf_file)
        if args.verbose:
            print 'file saved: %s (--decrease-pressure = %4.4e)' % (args.output_msf_file, args.decrease_pressure)

        if not args.no_rsf_mod:
            infilename = args.INPUT_MSF_FILE[:-3]+'rsf'
            outfilename = args.output_msf_file[:-3]+'rsf'
            read_and_save_rad_state_file(infilename, outfilename, header_new.get_n_bs(), args.decrease_pressure, 0, 1)
            if args.verbose:
                print 'file saved: %s (--decrease-pressure = %4.4e)' % (outfilename, args.decrease_pressure)
