#!/usr/bin/env python

from math import sqrt

def compute_derivatives(As):
    DAs = [sqrt((As[i+1]-As[i])*(As[i+1]-As[i])) for i in xrange(len(As)-1)]
    return DAs

def compute_averages(As):
    MAs = [.5*(As[i+1]+As[i]) for i in xrange(len(As)-1)]
    return MAs

def compute_expectation_value(gs):
    S1 = 0.
    S2 = 0.
    for i in xrange(len(gs)):
        g = gs[i]
        S1 += i*g
        S2 += g
    i_mean = S1/S2
    return i_mean

def compute_block_means(Ns):
    if not Ns:
        return []
    Ns_merged = []
    S = Ns[0]
    nr = 1.
    if not len(Ns)-1:
        Ns_merged.append(S)
    for i in xrange(len(Ns)-1):
        dN = Ns[i+1]-Ns[i]
        if dN < 1.:
            S += Ns[i+1]
            nr += 1.
            if i+1 == len(Ns)-1:
                N_m = S/nr
                Ns_merged.append(N_m)
                S = Ns[i+1]
                nr = 1.
        else:
            N_m = S/nr
            Ns_merged.append(N_m)
            S = Ns[i+1]
            nr = 1.
    return Ns_merged

def find_shocks(As, dAA_min=.1, dAA_max=.2, W=10):
    # Find dA/A in between cell values
    DAs = compute_derivatives(As)
    MAs = compute_averages(As)
    dAAs = [DA/MA for DA, MA in zip(DAs, MAs)]
    N_Ws = len(dAAs)-W
    N_shocks = []
    for m in xrange(len(dAAs)-W):
        dAAs_W = dAAs[m:m+W]
        dAA_l = dAAs_W[0]
        dAA_r = dAAs_W[-1]
        dAA_c = dAAs_W[W/2]
        if dAA_c > dAA_max and dAA_l < dAA_min and dAA_r < dAA_min:
            i_mean = compute_expectation_value(dAAs_W)
            N_mean = i_mean + m - .5
            N_shocks.append(N_mean)

    # Average N_shocks that are within one bin from each other
    Ns = compute_block_means(N_shocks)

    # if not Ns:
    #     Ns = [nan]

    return Ns
