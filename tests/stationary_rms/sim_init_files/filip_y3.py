#!/usr/bin/env python

# The INNER PARTS of these functions should be modified by the user, but not their names, input parameters or return statements
# This file is used as a command line argument for the "generate_msf.py" script
# The number of cells N (excluding ghost cells) is given as an argument to "generate_msf.py"
# For now, assumes that the primitives (v, rho, p, b) are given as functions of the mass coordinate, NOT of radius
# If using radiation, the pressure in the 'get_prims' function is the radiation pressure! The gas pressure will be lower by a factor of Z_gamma/2

from math import sqrt, tanh

# The RAD part

# Parameters (y = 3)

# beta_ud = 2.4495e-01
# theta_u = 2.1667e-06
# Z_gamma = 1.4195e+05

beta_ud = 2.1213e-01
theta_u = 2.1667e-06
Z_gamma = 1.0646e+05

tau_tot = 350

def get_rad_params():

    # RAD parameters (simply set rsf_filename = None, and then ignore this function if radiation is not used)
    rsf_filename = 'state_files/filip_y3.rsf' # If 'rsf_filename = None', no .rsf file will be generated
    N_MC = 1e7 # The number of generated Monte Carlo photon packets
    f_heat = max(1e-2*Z_gamma, 1) # "Fake heat", which can be used to make the simulation more stable,
                 # but produces incorrect temperatures behind subshocks

    eps_min = 1e-8 # Lower (lab frame) energy in the output spectrum
    eps_max = 1e-0 # Upper (lab frame) energy in the output spectrum
    n_bs_spec = 101 # Number of spectrum bin boundaries
    n_eps_bs = 101 # Number of spectrum bin boundaries (?)
    n_el_spec_bins = 3000 # Number of electron spectrum energy bins
    iteration = 0 # The current radiation iteration
    n_eps_splits_bins = 20 # Number of bins in the "split energy" vector
    photons_per_split = 1 # When photon passes a "split energy", how many photons should it
                          # split into?

    params = [rsf_filename, N_MC, Z_gamma, f_heat, eps_min, eps_max, n_bs_spec, n_eps_bs, n_el_spec_bins, iteration, n_eps_splits_bins, photons_per_split]

    return params

# The MHD part

def get_mhd_params():

    # MHD parameters
    msf_filename = 'state_files/filip_y3.msf'
    N = 2048 # The number of spatial bins (excluding ghost cells)

    # Constants
    m_p = 1.6726231e-24
    sigma_T = 6.6524e-25
    kappa_T = sigma_T/m_p

    # Grid size
    M_o = tau_tot/kappa_T
    R_i = 1.
    alpha = 0
    gamma_ad = 5./3.
    t = 0.
    iteration = 0

    basic_params = [msf_filename, N, M_o, R_i, alpha, gamma_ad, t, iteration]

    # Problem specific parameters (what you put here will be passed on
    # to 'get_m_bs' and 'get_prims')

    # Frame in which the shock is stationary
    betagamma_shift = 0.

    # Upstream shock parameters
    # m_sh = .12*M_o
    m_sh = 35./kappa_T
    rho_u = 1e0

    m_e = 9.1094e-28
    m_p = 1.6726231e-24
    p_u = (m_e/m_p)*theta_u*rho_u
    A = -(7./6.)*beta_ud
    B = -(4./3.)*Z_gamma*p_u/rho_u
    v_u = -A/2. + sqrt((A/2.)*(A/2.) - B) # choosing the positive root
    p_gamma_bar_u = Z_gamma*p_u/rho_u/v_u/v_u

    betagamma_u = v_u/sqrt(1. - v_u*v_u)
    problem_params = [tau_tot, betagamma_shift, betagamma_u, rho_u, p_gamma_bar_u, m_sh, kappa_T]

    # # Printing beta_ud
    # A = 4./7.*(1. + p_gamma_bar_u)
    # B = .5*(3. - 4.*p_gamma_bar_u)
    # dv_u = v_u*(A - 2./7.*B)
    # dv_d = v_u*(A + 2./7.*B)
    # # beta_ud_computed = (dv_d - dv_u)/(1. - dv_u*dv_d)
    # beta_ud_computed = dv_d - dv_u
    # print("beta_ud_computed = %4.4e" % (beta_ud_computed))
    # raise SystemExit

    return basic_params, problem_params

def get_m_bs(basic_params, problem_params):

    # The mass coordinate grid
    # Change however you want, but m_bs[2] = 0 is required (i.e. the grid starts
    # at m_b = 0, excluding the ghost cells)

    N = basic_params[1]
    M_o = basic_params[2]
    dm = M_o/float(N)
    m_bs = [i*dm for i in xrange(N+5)]
    m_norm = m_bs[2]
    m_bs = [m_b-m_norm for m_b in m_bs]

    return m_bs

def get_prims(m, basic_params, problem_params):

    # The primitives, as functions of the mass coordinate
    # If using radiation, the pressure here will be the radiation pressure,
    # the gas pressure will be lower by a factor Z_gamma/2

    M_o = basic_params[0]
    tau_tot, betagamma_shift, betagamma_u, rho_u, p_gamma_bar_u, m_sh, kappa_T = problem_params

    A = 4./7.*(1. + p_gamma_bar_u)
    B = .5*(3. - 4.*p_gamma_bar_u)

    # Finding tau_bar
    v_u = betagamma_u/sqrt(1. + betagamma_u*betagamma_u)
    tau_bar = v_u*kappa_T*(m-m_sh)
    # tau_bar = v_u*v_u/betagamma_u*kappa_T*(m-m_sh)

    # Reflecting
    tau_bar *= -1

    # Finding the dimensionless primitives
    v_bar = A - 2./7.*B*tanh(B*tau_bar)
    p_gamma_bar = (3./4.)*A + 2./7.*B*tanh(B*tau_bar)

    # Real, physical primitives
    # v = v_u*v_bar
    rho = rho_u/v_bar
    p = rho_u*v_u*v_u*p_gamma_bar
    # p = rho_u*betagamma_u*betagamma_u*p_gamma_bar

    # Reflecting
    v_bar *= -1

    # Shifting
    v = v_u*v_bar
    v_shift = betagamma_shift/sqrt(1. + betagamma_shift*betagamma_shift)
    v = (v + v_shift)/(1. + v*v_shift)

    # No mag. fields
    b = 0

    return v, rho, p, b
