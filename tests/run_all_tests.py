#/usr/bin/env python

import os
import subprocess

def main():

    # Assumes that all tests have parameter files called 'params.par'

    NUM_PROC = 2

    # Find all test folders
    folders = [folder for folder in os.listdir('.') if os.path.isdir(folder)]

    if not folders:
        print 'Found no tests!'
        print 'Exiting...'
        raise SystemExit
    
    print 'Running all tests:'
    print

    # Run simulation
    for i, folder in enumerate(folders):
        print "Test %i/%i: Entering '/%s'" % (i+1, len(folders), folder)
        os.chdir(folder)

        # Run simulation
        call = "mpirun -np %i ../../bin/radshock param_files/params.par" % (NUM_PROC)
        print '$ '+call
        return_code = subprocess.call(call, shell=True)
        print

        # Find output name, then make plots
        for filename in os.listdir('./snapshot_files'):
            if filename.endswith('.mhd'):
                break
        call = "python ../../scripts/plot_snapshots.py snapshot_files/%s --sample --no-plot -o output_plots/rho.pdf rho p_gamma p" % (filename)
        print '$ '+call
        return_code = subprocess.call(call, shell=True)
        print

        os.chdir("..")

if __name__ == '__main__':
    main()
