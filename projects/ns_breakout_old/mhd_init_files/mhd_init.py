#!/usr/bin/env python

# The INNER PARTS of these functions should be modified by the user, but not their names, input parameters or return statements
# This file is used as a command line argument for the "generate_msf.py" script
# The number of cells N (excluding ghost cells) is given as an argument to "generate_msf.py"
# For now, assumes that the primitives (v, rho, p, b) are given as functions of the mass coordinate, NOT of radius

from math import pi, sqrt, tanh

def get_params():

    # Constants

    m_p = 1.6726231e-24
    sigma_T = 6.6524e-25
    kappa_T = sigma_T/m_p

    # Profile parameters

    sim_fraction = 5e-3
    M_sun = 2e33
    M_ej = 1e-4*M_sun
    R_i = 4e11
    m_1 = 3*1e27/4./pi
    psi = .25
    w = 3e-3

    # Shock parameters

    tau_bar_sh = 4.
    betagamma_u_prime = .33
    v_u_prime = betagamma_u_prime/sqrt(1. + betagamma_u_prime*betagamma_u_prime)

    # Basic parameters

    M_o = sim_fraction*M_ej/4./pi
    R_i = R_i
    alpha = 2
    gamma_ad = 4./3.
    t = 4e11
    iteration = 0
    basic_params = [M_o, R_i, alpha, gamma_ad, t, iteration]

    # Problem specific parameters
    problem_params = [kappa_T, M_o, R_i, tau_bar_sh, v_u_prime, m_1, psi, w]

    return basic_params, problem_params

def get_m_bs(N, basic_params, problem_params):

    # The mass coordinate grid (m_bs[2] = 0 is required!)

    M_o = basic_params[0]
    dm = M_o/float(N)
    m_bs = [i*dm for i in xrange(N+5)]
    m_norm = m_bs[2]
    m_bs = [m_b-m_norm for m_b in m_bs]

    return m_bs

def get_prims(m, basic_params, problem_params):

    # The primitives, as functions of the mass coordinate

    kappa_T, M_o, R_i, tau_bar_sh, v_u_prime, m_1, psi, w = problem_params

    dm = 1e-10*M_o

    # Mass coordinate
    m_sh = R_i*R_i*tau_bar_sh/v_u_prime/kappa_T

    # Determining upstream primitives
    m_over_m_1 = (M_o-2*m_sh)/m_1
    betagamma_u = m_over_m_1**(-psi)
    v_u = betagamma_u/sqrt(1. + betagamma_u*betagamma_u)
    rho_u = m_1/v_u/v_u/v_u/v_u/R_i/R_i/R_i/psi*m_over_m_1**(1.-psi)
    p_u = .25*w*rho_u

    # Useful "constants"
    v_b = (v_u + v_u_prime)/(1. + v_u*v_u_prime)
    p_gamma_bar_u = p_u/rho_u/v_u_prime/v_u_prime
    A = 4./7.*(1. + p_gamma_bar_u)
    B = (3.-4.*p_gamma_bar_u)/2.

    # Compute the profiles
    if m < 2*m_sh:

        # Non-relativistic tau_bar
        tau_bar = -v_u_prime*kappa_T/R_i/R_i*(m-m_sh)

        # "Barred" primitives
        v_bar = A - 2./7.*B*tanh(B*tau_bar)
        p_bar = 3./4.*A + 2./7.*B*tanh(B*tau_bar)

        # Real primitives
        v_prime = -v_u_prime*v_bar
        rho = rho_u/v_bar

        # Non-relativistic p
        p = rho_u*v_u_prime*v_u_prime*p_bar

        # Lab frame speed
        v = (v_prime + v_b)/(1. + v_prime*v_b)

    else:

        # Homologous profile
        dm = 1e-5*M_o
        if m > M_o-dm:
            m = M_o-dm
        m_over_m_1 = (M_o-m)/m_1
        betagamma = m_over_m_1**(-psi)
        v = betagamma/sqrt(1. + betagamma*betagamma)
        rho = m_1/v/v/v/v/R_i/R_i/R_i/psi*m_over_m_1**(1.-psi)
        p = .25*w*rho

    # Stabilizing magnetic pressure
    p_b = 0.
    b = sqrt(2.*p_b)

    return v, rho, p, b
