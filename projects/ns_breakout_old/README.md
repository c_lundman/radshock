# **Breakout of a shock through the relativistic envelope that surrounds NS mergers**

## Strategy

### Make "clean" initial conditions

* ~~Produce an initial profile in `mhd_init.py`, with an almost mildly relativistic shock at the inner end~~
* Make `ns_breakout_accel_piston_f10_x1_s0.par`:
    * The first of several simulation steps (therefore s0, or step 0, in the file name)
    * Also produce correspondingly named .msf and .rsf files: spatial bins = 2000
    * Accelerating piston that ends at `betagamma = 4` (therefore accel_piston in the filename)
    * `f_heat = 10` (therefore f10 in the file name)
    * `xi_rad = 20`
    * `load_photon_multiplier = 1` (therefore x1 in the filename)
    * `save_photon_divider = 1` (therefore x1 in the filename)
    * Output named `ns_breakout_f10_x1_s1_rough` (rough, because density will later be flattened; removed accel_piston, because the new piston will not be accelerating)
* [laptop] Run `ns_breakout_accel_piston_f10_x1_s0.par` until speed has saturated

### Flatten the density profile and do the breakout

* "Flatten" `ns_breakout_f10_x1_s1_rough.msf` and `ns_breakout_f10_x1_s1_rough.rsf` into `ns_breakout_f10_x1_s1.msf` and `ns_breakout_f10_x1_s1.rsf`
* Remove heat (`f_heat = 10` -> `f_heat = 1`) from `ns_breakout_f10_x1_s1.msf` and `ns_breakout_f10_x1_s1.rsf` into `ns_breakout_f1_x1_s1.msf` and `ns_breakout_f1_x1_s1.rsf`
* Make `ns_breakout_f10_x1_s1.par` and `ns_breakout_f1_x1_s1.par`:
    * Uses corresponding input files
    * `xi_rad = 1`
    * Fixed `betagamma = 4` piston speed
    * Output named correspondingly, but with `s2`
* Make `ns_breakout_f10_x30_s1.par` and `ns_breakout_f1_x30_s1.par`:
    * Same as above, but:
    * load_photon_multiplier = 30
    * save_photon_divider = 30

* [laptop] Run `breakout_f10_piston_s1.par` until breakout is over
* [laptop] Run `breakout_f1_piston_s1.par` until breakout is over
* [rama]   Run `breakout_f10_piston_s1_x30.par` until breakout is over
* [rama]   Run `breakout_f1_piston_s1_x30.par` until breakout is over

### If no crashes

* Check the cooling length of the subshocks
* If needed, make script to increase number of spatial bins, redo the "Do the breakout" step 
