#/usr/bin/env python

import subprocess

def run_calls(calls):
    print "starting 'run_calls':"
    print
    n = len(calls)
    for i, call in enumerate(calls):
        print 'call %i/%i:' % (i+1, n)
        print '$ '+call
        return_code = subprocess.call(call, shell=True)
        if return_code:
            print 'return_code =', return_code
        print

def main():
    sim_name = "breakout_init_v1_313_267_x2"

    toggle_xlog = ''
    toggle_xlog = ' --toggle-xlog'

    calls = ["python ../../../scripts/plot_snapshots.py ../snapshot_files/%s.mhd -x m_rev --x-lims 1e24 8e26 --y-lims 1e22 1e29 --sample --preliminary --no-plot --legend-loc 2 -o ../output_plots/automatic_rho_p.pdf rrr_rho rrr_p_gamma rrr_p" % (sim_name) + toggle_xlog,
             "python ../../../scripts/plot_snapshots.py ../snapshot_files/%s.mhd -x m_rev --x-lims 1e24 8e26 --y-lims 1e0 1e1 --sample --preliminary --no-plot -o ../output_plots/automatic_betagamma.pdf betagamma" % (sim_name) + toggle_xlog,
             "python ../../../scripts/plot_snapshots.py ../snapshot_files/%s.mhd -x m_rev --x-lims 1e24 8e26 --y-lims 1e-5 1e1 --sample --preliminary --no-plot -o ../output_plots/automatic_theta.pdf theta_c theta" % (sim_name) + toggle_xlog]

    run_calls(calls)

if __name__ == '__main__':
    main()
