#!/usr/bin/env python

# The INNER PARTS of these functions should be modified by the user, but not their names, input parameters or return statements
# This file is used as a command line argument for the "generate_msf.py" script
# The number of cells N (excluding ghost cells) is given as an argument to "generate_msf.py"
# For now, assumes that the primitives (v, rho, p, b) are given as functions of the mass coordinate, NOT of radius
# If using radiation, the pressure in the 'get_prims' function is the radiation pressure! The gas pressure will be lower by a factor of Z_gamma/2

# The RAD part

from math import pi, sqrt, tanh

def get_rad_params():

    # RAD parameters (simply set rsf_filename = None, and then ignore this function if radiation is not used)

    rsf_filename = 'state_files/r0_f100_FULL_RMS.rsf' # If 'rsf_filename = None', no .rsf file will be generated
    # N_MC = 1e7 # The number of generated Monte Carlo photon packets
    N_MC = 3e6 # The number of generated Monte Carlo photon packets
    # N_MC = 3e5 # The number of generated Monte Carlo photon packets
    Z_gamma = 1e4 # The photon-to-proton ratio
    f_heat = 1e2 # "Fake heat", which can be used to make the simulation more stable,
                 # but produces incorrect temperatures behind subshocks

    eps_min = 1e-5 # Lower (lab frame) energy in the output spectrum
    eps_max = 1e+2 # Upper (lab frame) energy in the output spectrum
    n_bs_spec = 101 # Number of spectrum bin boundaries
    n_eps_bs = 101 # Number of spectrum bin boundaries (?)
    n_el_spec_bins = 3000 # Number of electron spectrum energy bins
    iteration = 0 # The current radiation iteration
    n_eps_splits_bins = 20 # Number of bins in the "split energy" vector
    photons_per_split = 1 # When photon passes a "split energy", how many photons should it
                          # split into?

    params = [rsf_filename, N_MC, Z_gamma, f_heat, eps_min, eps_max, n_bs_spec, n_eps_bs, n_el_spec_bins, iteration, n_eps_splits_bins, photons_per_split]

    return params

# The MHD part

def get_mhd_params():

    # File name and number of bins

    msf_filename = 'state_files/r0_f100_FULL_RMS.msf'
    # N = 24000 # The number of spatial bins (excluding ghost cells)
    # integrate_forward = True # Assumes that R_i is the leftmost radial coordinate, and integrates to the right to find the grid radii
    integrate_forward = False # Assumes that R_i is the leftmost radial coordinate, and integrates to the right to find the grid radii
    # N = 10000 # The number of spatial bins (excluding ghost cells)
    N = 8192 # The number of spatial bins (excluding ghost cells)

    # Constants

    m_p = 1.6726231e-24
    sigma_T = 6.6524e-25
    kappa_T = sigma_T/m_p
    c = 3e10
    #c = 2.99e10

    # Mass bin parameters

    # dm_ratio = 2e2
    # N_c = N/30.
    # DN_c = N/30.
    dm_ratio = 1e-1
    N_c = N/30.
    DN_c = N/30.

    # Profile parameters

    # t_i = 4e11 # = (13.33)*c
    # sim_fraction = 5e-3
    t_i = 4*c
    # sim_fraction = 3.*5e-3
    sim_fraction = 2e-3
    M_sun = 2e33
    M_ej = 1e-4*M_sun
    m_1 = 3*1e27/4./pi
    psi = .25
    w = 3e-3

    # Shock parameters

    tau_bar_sh = 4.
    # betagamma_u_prime = .33
    #betagamma_u_prime = .8622
    betagamma_u_prime = .95
    v_u_prime = betagamma_u_prime/sqrt(1. + betagamma_u_prime*betagamma_u_prime)

    # Basic parameters

    M_o = sim_fraction*M_ej/4./pi
    R_i = t_i
    alpha = 2
    gamma_ad = 5./3.
    t = t_i
    iteration = 0
    basic_params = [msf_filename, N, M_o, R_i, alpha, gamma_ad, t, iteration, integrate_forward]

    # Problem specific parameters
    problem_params = [kappa_T, M_o, R_i, tau_bar_sh, v_u_prime, m_1, psi, w, dm_ratio, N_c, DN_c, t_i]

    return basic_params, problem_params

def get_m_bs(basic_params, problem_params):

    # The mass coordinate grid
    # Change however you want, but m_bs[2] = 0 is required (i.e. the grid starts
    # at m_b = 0, excluding the ghost cells)

    N = basic_params[1]
    M_o = basic_params[2]

    dm_ratio = problem_params[8]
    N_c = problem_params[9]
    DN_c = problem_params[10]

    dm_max = dm_ratio
    dm_min = 1e0

    dms = []
    m_bs = [0.]
    for i in xrange(N):
        # x = (N_c-i)/DN_c
        # dm = dm_min + (dm_max-dm_min)*.5*( tanh(x) + 1. )
        dm = 1. + (float(i)/N)
        m_bs.append(m_bs[-1]+dm)
    norm = M_o/m_bs[-1]
    m_bs = [m_b*norm for m_b in m_bs]

    # Ghosts
    dm_0 = m_bs[1]-m_bs[0]
    m_bs.insert(0, m_bs[0]-dm_0)
    m_bs.insert(0, m_bs[0]-dm_0)
    dm_1 = m_bs[-1]-m_bs[-2]
    m_bs.append(m_bs[-1]+dm_1)
    m_bs.append(m_bs[-1]+dm_1)

    return m_bs

# def get_m_bs(basic_params, problem_params):

#     # The mass coordinate grid
#     # Change however you want, but m_bs[2] = 0 is required (i.e. the grid starts
#     # at m_b = 0, excluding the ghost cells)

#     N = basic_params[1]
#     M_o = basic_params[2]

#     dm_ratio = problem_params[8]
#     N_c = problem_params[9]
#     DN_c = problem_params[10]

#     dm_max = dm_ratio
#     dm_min = 1e0

#     dms = []
#     m_bs = [0.]
#     for i in xrange(N):
#         x = (N_c-i)/DN_c
#         dm = dm_min + (dm_max-dm_min)*.5*( tanh(x) + 1. )
#         m_bs.append(m_bs[-1]+dm)
#     norm = M_o/m_bs[-1]
#     m_bs = [m_b*norm for m_b in m_bs]

#     # Ghosts
#     dm_0 = m_bs[1]-m_bs[0]
#     m_bs.insert(0, m_bs[0]-dm_0)
#     m_bs.insert(0, m_bs[0]-dm_0)
#     dm_1 = m_bs[-1]-m_bs[-2]
#     m_bs.append(m_bs[-1]+dm_1)
#     m_bs.append(m_bs[-1]+dm_1)

#     return m_bs

def get_prims(m, basic_params, problem_params):

    # The primitives, as functions of the mass coordinate

    kappa_T = problem_params[0]
    M_o = problem_params[1]
    R_i = problem_params[2]
    tau_bar_sh = problem_params[3]
    v_u_prime = problem_params[4]
    m_1 = problem_params[5]
    psi = problem_params[6]
    w = problem_params[7]
    t_i = problem_params[8]

    # Mass coordinate
    # m_sh = R_i*R_i*tau_bar_sh/v_u_prime/kappa_T
    # m_sh = .4*R_i*R_i*tau_bar_sh/v_u_prime/kappa_T
    m_sh = .8*R_i*R_i*tau_bar_sh/v_u_prime/kappa_T

    # Determining upstream primitives
    m_over_m_1 = (M_o-2*m_sh)/m_1
    betagamma_u = m_over_m_1**(-psi)
    v_u = betagamma_u/sqrt(1. + betagamma_u*betagamma_u)
    rho_u = m_1/v_u/v_u/v_u/v_u/R_i/R_i/R_i/psi*m_over_m_1**(1.-psi)
    p_u = .25*w*rho_u

    # Useful "constants"
    v_b = (v_u + v_u_prime)/(1. + v_u*v_u_prime)
    p_gamma_bar_u = p_u/rho_u/v_u_prime/v_u_prime
    A = 4./7.*(1. + p_gamma_bar_u)
    B = (3.-4.*p_gamma_bar_u)/2.

    # Compute the profiles
    if m < 2*m_sh:
    # if m < -2*m_sh:

        # Non-relativistic tau_bar
        tau_bar = -v_u_prime*kappa_T/R_i/R_i*(m-m_sh)

        # "Barred" primitives
        v_bar = A - 2./7.*B*tanh(B*tau_bar)
        p_bar = 3./4.*A + 2./7.*B*tanh(B*tau_bar)

        # Real primitives
        v_prime = -v_u_prime*v_bar
        rho = rho_u/v_bar

        # Non-relativistic p
        # p = rho_u*v_u_prime*v_u_prime*p_bar

        # Relativistic "correction"
        # betagamma_u_prime = v_u_prime/sqrt(1. - v_u_prime*v_u_prime)
        gamma_u_prime = 1./sqrt(1. - v_u_prime*v_u_prime)
        p = rho_u*v_u_prime*v_u_prime*gamma_u_prime**4*p_bar

        # Lab frame speed
        v = (v_prime + v_b)/(1. + v_prime*v_b)

    else:

        # Homologous profile
        dm = 1e-5*M_o
        if m > M_o-dm:
            m = M_o-dm
        m_over_m_1 = (M_o-m)/m_1
        betagamma = m_over_m_1**(-psi)
        v = betagamma/sqrt(1. + betagamma*betagamma)
        rho = m_1/v/v/v/v/R_i/R_i/R_i/psi*m_over_m_1**(1.-psi)
        p = .25*w*rho

    # Stabilizing magnetic pressure
    p_b = 0.
    b = sqrt(2.*p_b)

    return v, rho, p, b
