# **Project README file**

After running the simulations of "project/ns_breakout", we realized that we wanted to tweak some values to get the correct time delay of the gamma-rays. This project applies the correction.

## _Simulation details_

A description of the physical scenario, and what we hope to learn from the simulation.

## _To do_

1. a
2. b
3. c

### _Known issues_

* A
* B
* C
