# An example parameter file for the radshock code

# This text is not actually commented out: the code simply
# looks for the variable names given below, then moves one
# word ahead (past the "=") and picks the next string for
# the variable. This means that I can NOT type the variable
# names anywhere else in this file, rather use "variable"
# with quotes in the comments

# Input and output state files (will use only the MHD part of the code if "rad_input_state_file = None")
mhd_input_state_file = state_files/s1_f.msf
rad_input_state_file = state_files/s1_f.rsf
mhd_output_state_file = state_files/s2.msf
rad_output_state_file = state_files/s2.rsf

# Snapshot file names (will use the same name but different extension for all snapshot files)
mhd_output_snapshot_file = snapshot_files/s1_f.mhd

# Debug mode (will print extra information)
debug = 0

# Times for saving snapshots
n_ts = 600
t_i = 6e11
t_f = 1.58e12
log_spaced_time = 1 # [0:linear, 1:log] (if log, "t_i" must not be 0)

# Number of save states (larger files, but can be loaded again); should divide "n_ts" evenly
n_ss = 1

# If non-zero, code saves a snapshot also every n'th iteration
save_every_nth_mhd_iter = 0
save_every_nth_rad_iter = 0

# Time steps
xi_mhd = 1e0 # Multiplies time step by this factor
xi_rad = 1e0

# Gravity
m_grav = 0

# Boundaries [0:vacuum, 1:wall, 2:comoving_wall, 3:periodic, 4:piston (needs parameters!), -1:experimental]
# Regarding piston syntax: give four numbers after a |, e.g. "boundary_L = 4 | 1e10 1e12 0.3 2.0"
# This will define two times, and two piston speeds: t1 = 1e10, t2 = 1e12, betagamma1 = 0.3, betagamma2 = 2.0
# For t < t1: betagamma = betagamma1,
# for t > t2: betagamma = betagamma2,
# for t1 < t < t2: linear interpolation between betagamma1 and betagamma2
# For constant betagamma = 1, use e.g. "boundary_L = 4 | 0 0 1 1"
boundary_L = 4 | 0 0 3.98 3.98
boundary_R = 0

# The parameters below are only needed when radiation is used (i.e. when "rad_state_file != None")

# Rng
seed = 0

# MC photon number modifications (for smaller save state files)
load_photon_multiplier = 1 # Multiplies loaded photons by this number
save_photon_divider = 1 # Divides the number of saved photons by this number

# Average radiation quantities over this optical depth
tau_smoothing = 0.1

# Use gamma-gamma pair production?
use_pairs = 1

# Radiation source terms for hydro coupling [0:off (-> all photons are "test particles"),
#                                            1:direct collection of energy and momentum from all rad. procs,
#                                            2:direct collection of energy and momentum from scatterings only,
#                                            3:compute from radiation moments]
rad_source_terms = 3
