# **Project README file**

Three stages:

s0: initial stage, where piston keeps pushing
s1: stage of constant piston speed, initial radiation leaking out
s2: compression, collisionless shocks are launched

## _Simulation details_

This project simulates the three stages s0 -> s1, s1 -> s2, s2 -> s3 (which is the end state) in three separate runs.
Each run starts where the previous one ended.

Two versions of the simulation are used. All files which includes "_dm" in their names are made
from "sim_init_dm.py", which is identical to "sim_init.py" except that it uses twice the number of
spatial bins (4000 vs 2000) and uses variable mass bins, with the smallest being almost ten times
smaller than "sim_init.py".

Now, added an additional version: "_dm_hd", which has a resolution of 24000 bins, and a much smaller bin size at mass coodinate m ~ 10^26 g.

The plan is to:

1. Run s0 -> s1 [DONE]
2. "Flatten" and remove f_heat (1e2 -> 1e0) s1 to s1_f (used -p 652 700 for s1.msf, -p 540 623 for s1_dm.msf, -p 400 500 for s1_dm_hd.msf) [DONE]
3. Run s1_f -> s2
4. Run s2 -> s3
