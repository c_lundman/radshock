#!/usr/bin/env python

# The INNER PARTS of these functions should be modified by the user, but not their names, input parameters or return statements
# This file is used as a command line argument for the "generate_msf.py" script
# The number of cells N (excluding ghost cells) is given as an argument to "generate_msf.py"
# For now, assumes that the primitives (v, rho, p, b) are given as functions of the mass coordinate, NOT of radius
# If using radiation, the pressure in the 'get_prims' function is the radiation pressure! The gas pressure will be lower by a factor of Z_gamma/2

# Simulation parameters
L = 1e50
Gamma = 1e2
w = 1e-1
tau_i = 3e1
tau_f = 1e-1

# Constants
c = 3e10
kappa_T = 0.4

# The RAD part

def get_rad_params():

    # RAD parameters (simply set rsf_filename = None, and then ignore this function if radiation is not used)

    rsf_filename = 'state_files/decouple_spectrum.rsf' # If 'rsf_filename = None', no .rsf file will be generated
    N_MC = 1e7 # The number of generated Monte Carlo photon packets
    Z_gamma = 1e5 # The photon-to-proton ratio
    # f_heat = 1e0
    f_heat = max(1, 1e-2*Z_gamma) # "Fake heat", which can be used to make the simulation more stable,
                 # but produces incorrect temperatures behind subshocks

    eps_min = 1e-5 # Lower (lab frame) energy in the output spectrum
    eps_max = 1e+2 # Upper (lab frame) energy in the output spectrum
    n_bs_spec = 101 # Number of spatial bin boundaries for spectrum collection
    n_eps_bs = 101 # Number of spectrum bin boundaries
    n_el_spec_bins = 3000 # Number of electron spectrum energy bins
    iteration = 0 # The current radiation iteration
    n_eps_splits_bins = 20 # Number of bins in the "split energy" vector
    photons_per_split = 1 # When photon passes a "split energy", how many photons should it
                          # split into?

    params = [rsf_filename, N_MC, Z_gamma, f_heat, eps_min, eps_max, n_bs_spec, n_eps_bs, n_el_spec_bins, iteration, n_eps_splits_bins, photons_per_split]

    return params

# The MHD part

def get_mhd_params():

    # MHD parameters

    msf_filename = 'state_files/decouple_spectrum.msf'
    N = 32 # The number of spatial bins (excluding ghost cells)
    integrate_forward = True # Assumes that R_i is the leftmost radial coordinate, and integrates to the right to find the grid radii

    # Simulation specific things
    from math import pi, sqrt
    R_star = L*kappa_T/4./pi/c/c/c/Gamma/Gamma/Gamma
    R_i = R_star/tau_i # The inner radial boundary
    v = sqrt(1. - 1./Gamma/Gamma)
    rho = L/c/c/c/4./pi/R_i/R_i/Gamma/Gamma/(1.+w)
    p = .25*rho*w
    M_o = 1e-1*rho*R_i*R_i*R_i/Gamma # Total simulation mass (in spherical geometry: mass per steradian)

    alpha = 2 # The simulation geometry
    gamma_ad = 4./3. # The adiabatic index
    t = R_i # The initial simulation time
    iteration = 0 # The current MHD iteration

    R_f = R_star/tau_f
    print("tau_i = %8.2e, tau_f = %8.2e" % (tau_i, tau_f))
    print("=>")
    print("t_i = %8.2e" % (R_i))
    print("t_f = %8.2e" % (R_f))
    basic_params = [msf_filename, N, M_o, R_i, alpha, gamma_ad, t, iteration]

    # Problem specific parameters (what you put here will be passed on
    # to 'get_m_bs' and 'get_prims')

    problem_params = [v, rho, p]

    return basic_params, problem_params

def get_m_bs(basic_params, problem_params):

    # The mass coordinate grid
    # Change however you want, but m_bs[2] = 0 is required (i.e. the grid starts
    # at m_b = 0, excluding the ghost cells)

    N = basic_params[1]
    M_o = basic_params[2]
    dm = M_o/float(N)
    m_bs = [i*dm for i in xrange(N+5)]
    m_norm = m_bs[2]
    m_bs = [m_b-m_norm for m_b in m_bs]

    return m_bs

def get_prims(m, basic_params, problem_params):

    # The primitives, as functions of the mass coordinate
    # If using radiation, the pressure here will be the radiation pressure,
    # the gas pressure will be lower by a factor Z_gamma/2

    v, rho, p = problem_params
    b = 0

    return v, rho, p, b
