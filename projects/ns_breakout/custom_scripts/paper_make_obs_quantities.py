#/usr/bin/env python

import subprocess

# Notes:
# - This script must be run from its own folder
# - Below I assume that t_offset should be modified by 1 second...

def run_call(call):
    print '$ '+call
    return_code = subprocess.call(call, shell=True)
    if return_code:
        print 'return_code =', return_code

def main():
    only_paper_plots = 1

    # # Make spc files from rsf files
    # run_call("python ../../../scripts/make_spc_from_rsf.py ../state_files/r7.rsf ../snapshot_files/r7_beam_lc.spc --beam --r-bins 1000")
    # run_call("python ../../../scripts/make_spc_from_rsf.py ../state_files/r7.rsf ../snapshot_files/r7_beam_spec.spc --beam --r-bins 60")
    # raise SystemExit

    # IMPORTANT: Assuming t_offset = 1 here!
    # string_t_offset = "--t-offset 1 "
    string_t_offset = "--t-offset 0 "

    # Making all plots
    # string_lc = "python ../../../scripts/plot_observed_spectra.py --light-curve --toggle-xlog --x-lims 0 6 --no-tight-layout " + string_t_offset
    string_lc = "python ../../../scripts/plot_observed_spectra.py --light-curve --toggle-xlog --x-lims 0 5 --no-tight-layout " + string_t_offset
    # run_call(string_lc + "../snapshot_files/r7_beam_lc.spc --toggle-ylog --y-lims 0 2e47 -o ../output_plots/obs_lc.pdf")
    # run_call(string_lc + "../snapshot_files/r7_beam_lc.spc --y-lims 3e44 3e47 -o ../output_plots/obs_lc.pdf")
    run_call(string_lc + "../snapshot_files/r7_beam_lc.spc --toggle-ylog --y-lims 0 2e47 -o ../output_plots/obs_lc.pdf")
    # run_call(string_lc + "../../ns_breakout_experimental/snapshot_files/r3_beam_lc.spc --y-lims 3e44 3e47 -o ../output_plots/obs_lc_log.pdf")
    # run_call(string_lc + "../snapshot_files/r3_beam_lc.spc --toggle-ylog --y-lims 0 1.5e47 -o ../output_plots/obs_lc_cut50.pdf -x keV --lc-multi-cut 50 --z-rev")
    # run_call(string_lc + "../../ns_breakout_experimental/snapshot_files/r3_beam_lc.spc --toggle-ylog --y-lims 0 2e47 -o ../output_plots/obs_lc_cut50.pdf -x keV --lc-multi-cut 50 --z-rev")
    # if not only_paper_plots:
    #     run_call(string_lc + "../../ns_breakout_experimental/snapshot_files/r3_beam_spec.spc --toggle-ylog --y-lims 0 2e47 -o ../output_plots/obs_lc_spec.pdf")
    #     run_call(string_lc + "../../ns_breakout_experimental/snapshot_files/r3_beam_spec.spc --y-lims 3e44 3e47 -o ../output_plots/obs_lc_spec_log.pdf")

    # string_spec = "python ../../../scripts/plot_observed_spectra.py -x kev --x-lims 1 3e4 --y-lims 1e44 1e47 --no-tight-layout " + string_t_offset
    string_spec = "python ../../../scripts/plot_observed_spectra.py -x kev --x-lims 1 6e3 --y-lims 3e44 1e47 --no-tight-layout " + string_t_offset
    # run_call(string_spec + "../snapshot_files/r7_beam_spec.spc --t-offset 1 -t 1.5 2 2.5 3 3.5 4 --legend-loc 1 --z-rev --cmap plasma -o ../output_plots/obs_spec.pdf")
    # run_call(string_spec + "../snapshot_files/r7_beam_spec.spc -t 1.493 2 2.5 3 3.5 4 --legend-loc 1 --z-rev --cmap plasma -o ../output_plots/obs_spec.pdf")
    run_call(string_spec + "../snapshot_files/r7_beam_spec.spc -t 2.493 3 3.5 4 4.5 5 --legend-loc 1 --z-rev --cmap plasma -o ../output_plots/obs_spec.pdf")

if __name__ == '__main__':
    main()
