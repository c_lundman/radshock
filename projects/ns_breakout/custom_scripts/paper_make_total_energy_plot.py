#/usr/bin/env python

import subprocess
from paper_make_obs_quantities import run_call

def main():
    # "Initial" (quasi-steady state, RMS widening)
    # string = "python ../../../scripts/plot_E_vs_t.py ../snapshot_files/r2.spc --y-lims 3e43 1e49 -o ../output_plots/total_energies.pdf"
    # string = "python ../../../scripts/plot_E_vs_t.py ../../ns_breakout_experimental/snapshot_files/r2.spc --toggle-xlog --y-lims 3e43 1e49 --legend-loc 2 -o ../output_plots/total_energies.pdf"
    # string = "python ../../../scripts/plot_E_vs_t.py ../snapshot_files/r1_to_r7.spc --toggle-xlog --x-lims 1.5e12 2.44e12 --y-lims 3e44 1e49 --legend-loc 2 -o ../output_plots/total_energies.pdf"
    string = "python ../../../scripts/plot_E_vs_t.py ../snapshot_files/r1_to_r7.spc --toggle-xlog --x-lims 1.5e12 2.44e12 --y-lims 3e44 4e47 --legend-loc 3 -o ../output_plots/total_energies.pdf"
    run_call(string)

if __name__ == '__main__':
    main()
