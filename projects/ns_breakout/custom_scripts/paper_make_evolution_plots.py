#/usr/bin/env python

import subprocess
from paper_make_obs_quantities import run_call

def main():

    # # Plasma evolution
    # initial_string = "python ../../../scripts/plot_snapshots.py ../snapshot_files/r1_to_r7.mhd u rho u_gamma p_g --toggle-xlog --keep-nth 2 -x m_rev --x-lims 1e25 6e26 --colored-final-line --legend-loc 2 --legend-font-size 20"
    # final_string = " --multiple-panels --multiple-cmaps plasma plasma plasma plasma --multiple-y-lims 1.8e+0 5e+0 1e-12 1e-6 3e-14 3e-8 3e-18 3e-12 -o ../output_plots/plasma_evolution.pdf -n 0 56 112 180 280 "
    # # final_string = " --multiple-panels --multiple-cmaps plasma plasma plasma plasma --multiple-y-lims 1.8e+0 1.1e+1 1e-12 1e-6 3e-14 3e-8 3e-18 3e-12 -n 0 56 112 180 280 "
    # legend_string = " --legend-times --legend-labels-in-plot 2.53e26 1.3e26 7.22e25 4.5e25 3.5e25 "
    # # legend_string = " --legend-times --legend-labels-in-plot 2.535e26 1.3e26 7.17e25 4.4e25 3.2e25 "
    # # legend_string = " --legend-times "

    initial_string = "python ../../../scripts/plot_snapshots.py ../snapshot_files/r1_to_r7.mhd u_gamma u rho --toggle-xlog --keep-nth 2 -x m_rev --x-lims 1e25 6e26 --colored-final-line --legend-loc 2 --legend-font-size 24"
    final_string = " --multiple-panels --multiple-cmaps plasma plasma plasma --multiple-y-lims 8e-15 8e-9 1.8e+0 5e+0 3e-13 3e-7 -o ../output_plots/plasma_evolution.pdf -n 0 56 112 180 280 "
    legend_string = " --legend-times --legend-labels-in-plot 3.29e26 3.95e26 3.95e26 3.95e26 3.95e26 --legend-labels-in-plot-no-rotation "
    run_call(initial_string+final_string+legend_string)

    #

    # # Collisionless shock
    # theta_cmap_string = " --cmap plasma_r"
    # # ss_string = "python ../../../scripts/plot_snapshots.py ../snapshot_files/r1_to_r7.mhd -x m_rev --x-lims 4.5e25 6e25 -n -1 --colored-final-line --no-tight-layout "
    # # ss_string = "python ../../../scripts/plot_snapshots.py ../snapshot_files/r1_to_r7.mhd --keep-nth 2 -x m_rev --x-lims 5e25 6e25 -n -280 --colored-final-line --no-tight-layout "
    # ss_string = "python ../../../scripts/plot_snapshots.py ../snapshot_files/r1_to_r7.mhd --keep-nth 2 --x-lims 5e25 6e25 -x m_rev -n 278 "
    # theta_ss = "--y-lims 1e-3 1e+1 theta_p theta_e theta_c -o ../output_plots/collisionless_temperatures.pdf" + theta_cmap_string
    # run_call(ss_string + theta_ss)

# def main():
#     # Make all plots or only those for the paper?
#     only_paper_plots = 1

#     # Colormaps
#     # betagamma_cmap_string = " --cmap viridis_r"
#     # rho_cmap_string = " --cmap magma_r"
#     betagamma_cmap_string = " --cmap plasma_r"
#     rho_cmap_string = " --cmap plasma_r"
#     p_cmap_string = " --cmap plasma_r"
#     theta_cmap_string = " --cmap plasma_r"

#     # "Initial" (quasi-steady state, RMS widening)
#     initial_string = "python ../../../scripts/plot_snapshots.py ../snapshot_files/r1.mhd -x m_rev --toggle-xlog --x-lims 1e25 1e27 --colored-final-line --legend-short --keep-nth 2 -n 0 100 200 300 --no-tight-layout "
#     betagamma_initial = "betagamma --toggle-ylog --y-lims 1.5 6 --legend-times --legend-loc 1 -o ../output_plots/initial_betagamma.pdf" + betagamma_cmap_string
#     rho_initial = "rho --y-lims 3e-12 3e-7 --legend-times -o ../output_plots/initial_rho.pdf" + rho_cmap_string
#     # p_initial = "p_gamma p --y-lims 3e-17 3e-9 --legend-loc 2 -o ../output_plots/initial_p.pdf" + p_cmap_string
#     p_initial = "p_gamma p --y-lims 3e4 3e12 --cgs --legend-loc 2 -o ../output_plots/initial_p.pdf" + p_cmap_string

#     run_call(initial_string + betagamma_initial)
#     run_call(initial_string + rho_initial)
#     run_call(initial_string + p_initial)
#     if not only_paper_plots:
#         rrr_rho_initial = "rrr_rho --y-lims 1e25 1e29 --legend-times -o ../output_plots/initial_rrr_rho.pdf" + rho_cmap_string
#         # rrr_p_initial = "rrr_p_gamma rrr_p --y-lims 1e19 1e27 --legend-loc 4 -o ../output_plots/initial_rrr_p.pdf" + p_cmap_string
#         rrr_p_initial = "rrr_p_gamma rrr_p --y-lims 1e40 1e48 --cgs --legend-loc 4 -o ../output_plots/initial_rrr_p.pdf" + p_cmap_string
#         thetas_initial = "theta_p theta_e --y-lims 1e-5 1e-1 -o ../output_plots/initial_thetas.pdf" + theta_cmap_string
#         run_call(initial_string + rrr_rho_initial)
#         run_call(initial_string + rrr_p_initial)
#         run_call(initial_string + thetas_initial)

#     # "Final" (compression, subshocks, streaming)
#     final_string = "python ../../../scripts/plot_snapshots.py ../snapshot_files/r2.mhd -x m_rev --toggle-xlog --x-lims 1e25 1e27 -n 0 100 -1 --colored-final-line --no-tight-layout "
#     betagamma_final = "betagamma --toggle-ylog --y-lims 2.5 5 --legend-times -o ../output_plots/final_betagamma.pdf" + betagamma_cmap_string
#     rho_final = "rho --y-lims 3e-13 3e-7 --legend-times -o ../output_plots/final_rho.pdf" + rho_cmap_string
#     # p_final = "p_gamma p --y-lims 1e-17 3e-11 --legend-loc 4 -o ../output_plots/final_p.pdf" + p_cmap_string
#     p_final = "p_gamma p --y-lims 1e4 3e10 --cgs --legend-loc 4 -o ../output_plots/final_p.pdf" + p_cmap_string
#     run_call(final_string + betagamma_final)
#     run_call(final_string + rho_final)
#     run_call(final_string + p_final)
#     if not only_paper_plots:
#         rrr_rho_final = "rrr_rho --y-lims 3e24 3e30 --legend-times -o ../output_plots/final_rrr_rho.pdf" + rho_cmap_string 
#         # rrr_p_final = "rrr_p_gamma rrr_p --y-lims 3e19 1e26 --legend-loc 4 -o ../output_plots/final_rrr_p.pdf" + p_cmap_string
#         rrr_p_final = "rrr_p_gamma rrr_p --y-lims 3e40 1e47 --cgs --legend-loc 4 -o ../output_plots/final_rrr_p.pdf" + p_cmap_string
#         run_call(final_string + rrr_rho_final)
#         run_call(final_string + rrr_p_final)

#     # Subshock
#     ss_string = "python ../../../scripts/plot_snapshots.py ../snapshot_files/r2.mhd -x m_rev --x-lims 4.5e25 6e25 -n -1 --colored-final-line --no-tight-layout "
#     theta_ss = "--y-lims 1e-3 1e+1 theta_p theta_e theta_c -o ../output_plots/subshock_thetas.pdf" + theta_cmap_string
#     rho_ss = "rho --y-lims 3e-12 3e-7 --legend-times -o ../output_plots/subshock_rho.pdf" + rho_cmap_string
#     run_call(ss_string + theta_ss)
#     run_call(ss_string + rho_ss)
#     if not only_paper_plots:
#         betagamma_ss = "betagamma --toggle-ylog --y-lims 3.75 4.75 --legend-times -o ../output_plots/subshock_betagamma.pdf" + betagamma_cmap_string
#         # p_ss = "p_gamma p --y-lims 1e-17 3e-11 --legend-loc 4 -o ../output_plots/subshock_p.pdf" + p_cmap_string
#         p_ss = "p_gamma p --y-lims 1e4 3e10 --cgs --legend-loc 4 -o ../output_plots/subshock_p.pdf" + p_cmap_string
#         run_call(ss_string + betagamma_ss)
#         run_call(ss_string + p_ss)

if __name__ == '__main__':
    main()
